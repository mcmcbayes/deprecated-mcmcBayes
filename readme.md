=======
# mcmcBayes

**Warning: mcmcBayes is in the pre-release stage and is still being refined**

* See the in-progress documentation for mcmcBayes [here](http://reubenjohnston.com/uploads/mcmcBayes_documentation__v0_d.pdf)
* A command cheat sheet is [here](doc/command_cheat_sheet.md)
* Some miscellaneous content is also on the [Wiki](https://gitlab.com/reubenajohnston/mcmcBayes/wikis/home)

## Install notes
* Newer versions of MATLAB (e.g., R2022A) do not include [dom4j](https://github.com/dom4j/dom4j) in the `MATLABJARS=/opt/MATLAB/R2022a/java/jarext` folder
* dom4j.jar that is built properly for the installed version of MATLAB is needed for this distribution
* Run a test sequence using `$ retseq=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation('regressionEvaluation', 'regressionSeqEval-runSingleSim', 'linearFlatSimulated')`

### Build dom4j
* Identify the Java version MATLAB is using
    ```
    >> version -java
    ans =
    'Java 1.8.0_202-b08 with Oracle Corporation Java HotSpot(TM) 64-Bit Server VM mixed mode'
    ```
* Install the appropriate JDK for that version of Java (make sure your JAVA_HOME environment variable is setup properly to use this version of Java)
* Download and install gradle (if not already present) from [here](https://gradle.org/install) (make sure gradle is on your path)
* Download the [dom4j](https://github.com/dom4j/dom4j) source code from github
* Build dom4j using `$ ./gradle build` (binary will be located in `build/libs`)
* Build `code/java/XmlHacks.java` using `$ export MATLABJARS=/opt/MATLAB/R2022a/java/jarext && /usr/java/jdk1.8.0_202/bin/javac -classpath javap -classpath *:$MATLABJARS/xercesImpl.jar:/opt/users/johnsra2/sandbox/dom4j/build/libs/dom4j.jar XmlHacks.java`
* Package XmlHacks.class using `$ jar -cvf XmlHacks.jar XmlHacks.class`
* Ensure MATLAB java path includes above file
    * e.g., `>> javaaddpath('/opt/users/username/sandbox/deprecated-mcmcbayes/code/java')`

