**Note: if you view this file outside of the GitLab wiki, copy the command contents that are located between the two backticks, `.**

### Run unit tests
* Model evaluations
    * `>> results=mcmcBayes.NHPPEvaluation.runUnitTests()`
    * `>> results=mcmcBayes.growthCurveEvaluation.runUnitTests()`
    * `>> results=mcmcBayes.regressionEvaluation.runUnitTests()`
    * `>> results=mcmcBayes.normalEvaluation.runUnitTests()`
* VDMCaseStudy1 (alternately use true to fork multiple simulations in parallel)
    * `>> results=mcmcBayes.VDMCaseStudy1.runCaseStudy('ExpertJudgment1Empirical1', false)`
    * `>> results=mcmcBayes.VDMCaseStudy1.runCaseStudy('ExpertJudgment2Empirical2', false)`
    * `>> results=mcmcBayes.VDMCaseStudy1.runCaseStudy('ExpertJudgment3Empirical3', false)`
    * `>> results=mcmcBayes.VDMCaseStudy1.runCaseStudy('ExpertJudgment4Empirical4', false)`
    * `>> results=mcmcBayes.VDMCaseStudy1.runCaseStudy('ExpertJudgment5Empirical5', false)`
* VDMCaseStudy2
    * `>> results=mcmcBayes.VDMCaseStudy2.runCaseStudy('scalingLinear',false)`
    * `>> results.runScalingDemo()`;
    * `>> results=mcmcBayes.VDMCaseStudy2.runCaseStudy('scalingExponential',false)`
    * `>> results.runScalingDemo()`;

### Individual model execution
* E.g., retseq=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation(seqType,seqUuid,simUuid)
* `>> retseq=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation('regressionEvaluation', 'regressionSeqEval-runSingleSim', 'linearFlatSimulated')`
* `>> retseq=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation('regressionEvaluation', 'regressionSeqEval-runSingleSim', 'polynomialDegree2FlatSimulated')`
* `>> retseq=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation('growthCurveEvaluation', 'growthCurveSeqEval-runSingleSim', 'VerhulstFlatSimulated')`
* `>> retseq=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation('growthCurveEvaluation', 'growthCurveSeqEval-runSingleSim', 'GompertzFlatSimulated')`
* `>> retseq=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation('NHPPEvaluation', 'NHPPSeqEval-runSingleSim', 'BrooksMotleyPresetSimulated')`
* `>> retseq=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation('NHPPEvaluation', 'NHPPSeqEval-runSingleSim', 'GoelOkumotoPresetSimulated')`
* `>> retseq=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation('NHPPEvaluation', 'NHPPSeqEval-runSingleSim', 'GoelPresetSimulated')`
* `>> retseq=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation('NHPPEvaluation', 'NHPPSeqEval-runSingleSim', 'MusaOkumotoPresetSimulated')`
* `>> retseq=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation('NHPPEvaluation', 'NHPPSeqEval-runSingleSim', 'YamadaOhbaOsakiPresetSimulated')`
* `>> retseq=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation('NHPPEvaluation', 'NHPPSeqEval-runSingleSim', 'KuoGhoshPresetSimulated')`

### Override defaultSimulated
* E.g., retseq=mcmcBayes.sequence.overrideDefaultsAndRunSingleSimulation(seqType,seqUuid,simUuid,cfgRunPowerPosteriorMcmcSequence,cfgDebug,samplerType)
* `>> retseq=mcmcBayes.sequence.overrideDefaultsAndRunSingleSimulation('regressionEvaluation','regressionSeqEval-runSingleSim','polynomialDegree2PresetSimulated',false,true,mcmcBayes.t_samplerInterfaces.Stan)`

### Termination and cleanup
* `>> mcmcBayes.clearAll` % clears any artifacts from previous sequences (including killfile)
* `>> mcmcBayes.setTerminateSessions(true)` % shuts down any MATLABEngines running mcmcBayes sequences
* `>> mcmcBayes.setTerminateSessions(false)` % allows newly spawned MATLABEngines to run mcmcBayes sequences
* `>> mcmcBayes.killDefunctPython()` %temporary workaround to clear out defunct python process created by force quitting a sequence

### Formatted result generation
* `>> [archivedSequence]=mcmcBayes.sequence.loadSequence(mcmcBayes.t_sequence.VDMCaseStudy1,'ExpertJudgment2Empirical2')`
* `>> mcmcBayes.sequence.prettyPrintResults(archivedSequence,true);`
* `>> mcmcBayes.sequence.generateFigures(archivedSequence,false,true,true,true);`

### Result archiving (use terminal for Linux and macOS, use msys2 shell for Windows)
* `$ tar -cvf archive_VDMCaseStudy1ExpertJudgment1Empirical1.tar data/sampler*/ExpertJudgment1Empirical1* data/stateArchives/ExpertJudgment1Empirical1* logs/ExpertJudgment1Empirical1*`

### todo, updateme Model inspection (update)
* `>> mcmcBayes.inspectLogFiltered('modeltypeid1*datasetid1*.txt')`
* `>> mcmcBayes.inspectLogFiltered('*modelandverifylog.txt')`
* `>> mcmcBayes.inspectLog(t_mcmcBayesModelNhpp.BrooksMotley)`
* `>> mcmcBayes.inspectLog(mcmcBayesModelRegression.PolynomialOrder2)`
* `>> mcmcBayesSequenceVDMCaseStudy.displayModelPredictionComparison(t_mcmcBayesData.EJCCMPerformanceWeighting,t_mcmcBayesData.PostulatedConFR,1,t_mcmcBayesFigures.MODEL_PREDICTIONS_COMBO)`
* `>> mcmcBayesSequenceNormalModelEvaluation.displayModelPredictionComparison(t_mcmcBayesData.Simulated,t_mcmcBayesData.Simulated,1,t_mcmcBayesFigures.MODEL_PREDICTIONS_COMBO)`
* `>> mcmcBayes.displayFigureFiltered('modeltypeid*priordatatypeid*datatypeid*datasetid*varid*tempid*samplertypeid*ComboDiagnosticsPlot.fig')` 
* `>> mcmcBayes.displayFigureFiltered('*ComboDiagnosticsPlot.fig')`

### todo, updateme Estimate prior distribution (update)
* `>> [priormodelVariables, modelVariables]=mcmcBayes.sequence.estimatePriorDistribution(t_mcmcBayesEvaluateModels.NonDecreasingGroupedIntervalCountsOverTime, t_mcmcBayesModelNhpp.MusaOkumoto, t_mcmcBayesData.EJIndividualCleansed, t_mcmcBayesData.EJIndividualCleansed, [1], -1)`
* `>> [priormodelVariables, modelVariables]=mcmcBayes.sequence.estimatePriorDistribution(t_mcmcBayesEvaluateModels.NonDecreasingGroupedIntervalCountsOverTime, t_mcmcBayesModelNhpp.MusaOkumoto, t_mcmcBayesData.PostulatedConFR, t_mcmcBayesData.Simulated, [1], 15)`
* `>> [priormodelVariables, modelVariables]=mcmcBayes.sequence.estimatePriorDistribution(t_mcmcBayesEvaluateModels.Normal, t_mcmcBayesModelNormal.Derived, t_mcmcBayesData.Empirical1, t_mcmcBayesData.Empirical2, [1], -1)`

### todo, updateme Generate simulated data
* `>> mcmcBayes.sequence.generateTestData(mcmcBayes.t_sequence.normalEvaluation, 'normalSeqEval', mcmcBayes.t_normal.Gaussian, mcmcBayes.t_data.posteriorPredictedData, 'defaultGaussianSimulated', 50, 0.1)` % varargin=mu, r
* `>> mcmcBayes.sequence.generateTestData(mcmcBayes.t_sequence.regressionEvaluation, 'regressionSeqEval', mcmcBayes.t_regression.linear, mcmcBayes.t_data.posteriorPredictedData, 'defaultSimulated', 30, 1.5, 0.5)` % varargin=beta0, beta1, r
* `>> mcmcBayes.sequence.generateTestData(mcmcBayes.t_sequence.regressionEvaluation, 'regressionSeqEval', mcmcBayes.t_regression.polynomialDegree2, mcmcBayes.t_data.posteriorPredictedData, 'defaultSimulated', 10, 2.7, -0.05, 0.5)` % varargin=beta0, beta1, beta2, r
* `>> mcmcBayes.sequence.generateTestData(mcmcBayes.t_sequence.growthCurveEvaluation, 'growthCurveSeqEval', mcmcBayes.t_growthCurve.Verhulst, mcmcBayes.t_data.posteriorPredictedData, 'defaultSimulated', 11, 148, .45, 0.1)` %varargin=beta0, beta1, beta2, r
* `>> mcmcBayes.sequence.generateTestData(mcmcBayes.t_sequence.growthCurveEvaluation, 'growthCurveSeqEval', mcmcBayes.t_growthCurve.Gompertz, mcmcBayes.t_data.posteriorPredictedData, 'defaultSimulated', 11, 148, .35, 0.1)` %varargin=beta0, beta1, beta2, r
* `>> mcmcBayes.sequence.generateTestData(mcmcBayes.t_sequence.NHPPEvaluation, 'NHPPSeqEval', mcmcBayes.t_NHPP.BrooksMotley, mcmcBayes.t_data.posteriorPredictedData, 'defaultSimulated', 148*0.02)` %varargin=zeta
* `>> mcmcBayes.sequence.generateTestData(mcmcBayes.t_sequence.NHPPEvaluation, 'NHPPSeqEval', mcmcBayes.t_NHPP.GoelOkumoto, mcmcBayes.t_data.posteriorPredictedData, 'defaultSimulated', 148, 0.07)` %varargin=u, zeta
* `>> mcmcBayes.sequence.generateTestData(mcmcBayes.t_sequence.NHPPEvaluation, 'NHPPSeqEval', mcmcBayes.t_NHPP.Goel, mcmcBayes.t_data.posteriorPredictedData, 'defaultSimulated', 148, 0.9, 0.3)` %varargin=u, zeta, kappa
* `>> mcmcBayes.sequence.generateTestData(mcmcBayes.t_sequence.NHPPEvaluation, 'NHPPSeqEval', mcmcBayes.t_NHPP.MusaOkumoto, mcmcBayes.t_data.posteriorPredictedData, 'defaultSimulated', 148*.1, 0.018)` %varargin=zeta, kappa
* `>> mcmcBayes.sequence.generateTestData(mcmcBayes.t_sequence.NHPPEvaluation, 'NHPPSeqEval', mcmcBayes.t_NHPP.YamadaOhbaOsaki, mcmcBayes.t_data.posteriorPredictedData, 'defaultSimulated', 148, 0.2)` %varargin=u, zeta
* `>> mcmcBayes.sequence.generateTestData(mcmcBayes.t_sequence.NHPPEvaluation, 'NHPPSeqEval', mcmcBayes.t_NHPP.KuoGhosh, mcmcBayes.t_data.posteriorPredictedData, 'defaultSimulated', int32([0; 8; 31; 52; 93; 136]), 0.1)` %varargin=Lambdap, c

### General MATLAB
* `>> clc` %clear the console
* `>> clear` %clear the workspace (variables)
* `>> close all` %close all the figures
* `>> fclose('all')` %close all open files
* `>> format long` %set the console output format to long fixed-decimal
* `>> dbstop if error` %set an error breakpoint that will pause execution on the occurrence of any run-time errors
* `>> set(0,'DefaultFigureWindowStyle','docked')` %sets the WindowStyle property for default figures as 'docked' (use 'default' to revert back to normal)
* `>> java.util.UUID.randomUUID()` %generates a UUID

### Process information (for macOS, use `$ brew install pstree` to install; for Windows, use `$ pacman -s psmisc` to install)
* `$ pstree -p`
* `$ pstree -p parentPIDnum`

### Packaging
* `>> packageToolbox('C:\sandbox\mcmcBayes\code\matlab\mcmcBayes.prj','C:\sandbox\mcmcBayes\code\matlab\mcmcBayes.mltbx')`
* `$ git archive -o mcmcbayes-latest.zip HEAD`
