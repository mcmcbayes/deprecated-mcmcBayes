%coded=int8('handel')-MOL;
%msgStr=int8('See the MCMCBayes GitLab project for details.  REub is off to drink a tasty IPA now, good night!')-MOL;
%command=int8('player = audioplayer(y, Fs); play(player);')-MOL;

                                            
MOL=42;                                     %Using command encoding for less text
datStr=[ 62 55 68 58 59 66 ];               %Don�t panic, we know the secret decoder key
msgStr=[ 41  59  59 -10  74  62  59 -10 ...
         35  25  35  25  24  55  79  59 ...
         73 -10  29  63  74  34  55  56 ...
        -10  70  72  69  64  59  57  74 ...
        -10  60  69  72 -10  58  59  74 ...
         55  63  66  73   4 -10 -10  40 ...
         27  75  56 -10  63  73 -10  69 ...
         60  60 -10  74  69 -10  58  72 ...
         63  68  65 -10  55 -10  74  55 ...
         73  74  79 -10  31  38  23 -10 ...
         68  69  77   2 -10  61  69  69 ...
         58 -10  68  63  61  62  74  -9 ];
cmdStr=[ 70  66  55  79  59  72  10  19 ...
        -10  55  75  58  63  69  70  66 ...
         55  79  59  72  -2  79   2 -10 ...
         28  73  -1  17 -10  70  66  55 ...
         79  -2  70  66  55  79  59  72 ...
         -1  17 ];
dataVar=char(datStr+MOL);                   %Decode it (Obviously we use the answer to life, the Universe, well, everything)
eval(sprintf('load ''%s''',dataVar))        %Load the data
eval(char(cmdStr+MOL));                     %Run the demo
h=waitbar(0,char(msgStr+MOL));              %Statusbar for progress and a descriptive message
for i=1:8; pause(1); waitbar(i/8); end; close(h); %Final loop