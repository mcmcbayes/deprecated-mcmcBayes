# Ubuntu notes
## Adding new partition
* `$ sudo -s fdisk -l`
    *  You should see a `/dev/sd*` listed without a partition table
* `$ sudo apt-get install gparted`
    * Use gparted to create a partition table for `/dev/sd*` (https://help.ubuntu.com/community/HowtoPartition/CreatingPartitions)
* Mounting the device
    * `$ sudo mkdir /sandbox`
    * `$ sudo chmod 777 /mnt/ext`
    * Get the UUID of the newly created partition (e.g., `$ blkid /dev/sdb1`) 
    * modify `/etc/fstab` to add the following line
<code>
UUID=<UseValueFromAboveStep> /sandbox        ext4    rw,user,exec,auto 0       2
</code>
    * mount /sandbox (by adding the above line in `/etc/fstab` you do not have to be root to mount/umount)
    * run `$ mount -a` to reload the updated `/etc/fstab`

# Miscellaneous setup specific to Ubuntu VM instances
* For Ubuntu images having inverted mouse scrolling.  The Ubuntu-tweak [tool](http://ubuntu-tweak.com/) allows for changing the scroll setting to "Natural" (ah, now you can be at peace, at least for 14.04 Ubuntu)
    * `$ sudo add-apt-repository ppa:tualatrix/ppa`
    * `$ sudo apt-get update`
    * `$ sudo apt-get install ubuntu-tweak`
    * Run the `Applications Menu->Settings->Ubuntu Tweak`
    * Select the `Tweaks` tab, select the `Miscellaneous` option, and turn on `Natural scrolling`
* Browser interfaces to the VM sometimes don't provide key combinations for sequences including `ctrl-c`, `ctrl-alt-t`, etc.  A handy Ubuntu workaround for this issue is to use a remote desktop connection to the Ubuntu VM.  Notes for installing `XRDP` in Ubuntu 14.04 are below and [here](http://c-nergy.be/blog/?p=5305). You will also need to use the `XFCE` desktop environment (instead of `gnome`).
* VNC and XFCE might cause problems with tab completions in terminal windows.  
	* To fix this, Open the `XFCE Application Menu->Settings->Window Manager`
	  * Click on the Keyboard Tab
	  * Clear the Switch window for same application setting
* VNC sessions may be viewed using: `$ ps -ef |grep vnc`, the session ID number follows `Xvnc :ID`
* kill the unused sessions using: `$ vncserver -kill :1`, or if that doesn't work determine the PID and use `$ kill -9 PID`
* Install VNC
    * Install XRDP Package from Ubuntu Repository `$ sudo apt-get install xrdp`
    * Installing the XFCE4 Desktop environment
        * `$ sudo apt-get update`
        * `$ sudo apt-get install xfce4`
        * XFCE has missing icons for file manager, install these icons `$ sudo apt-get install gnome-icon-theme-full tango-icon-theme`
        * XFCE's default text color is black within terminal (change the terminal color settings, set terminal scrolling to unlimited while your editing)
        * install geany for text editor (gedit has display issues XFCE) `$ sudo apt-get install geany`
    * Configure xrdp to use XFCE desktop environment `$ echo xfce4-session >~/.xsession`
    * Ensure the following sections are present in `/etc/xrdp/xrdp.ini`
<code>
[xrdp3]
name=vnc-any (existing connection)
lib=libvnc.so
ip=127.0.0.1
port=ask5910
username=na
password=ask
</code>
<code>
[xrdp4]
name=sesman-any (new connection)
lib=libvnc.so
ip=127.0.0.1
port=-1
username=ask
password=ask
</code>
    * Restart the XRDP service by issuing the following command `$ sudo service xrdp restart`
* Disable the screen saver in your VMs!  This is a processor and network hog (locally and remotely)
* Tab key not working in XFCE?
    * Open the XFCE Application Menu > Settings > Window Manager
    * Click on the Keyboard Tab
    * Clear the Switch window for same application setting
