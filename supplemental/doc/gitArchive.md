create a *.zip archive from the latest commit: `$ git archive -o mcmcBayes-latest.zip HEAD`

create a *.zip archive from a release: `$ git archive --format=zip v1_0_a > mcmcBayes-v1_0_a.zip`