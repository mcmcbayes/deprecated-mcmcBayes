List the local tags: `$ git tag`

Create a local tag: `$ git tag -a v1_0_a -m "notes."`

Push the local tags to server: `$ git push --tags`

Delete local tags: `$ git tag -d v1_0_a`

Delete remote tags (after you have deleted them locally): `$ git push origin :refs/tags/v1_0_a`