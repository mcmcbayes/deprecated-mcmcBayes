From development station with latest:
* Create the bundle from development branch
```
$ git bundle create mcmcBayes.bundle refs/heads/development
Counting objects: 11629, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3317/3317), done.
Writing objects: 100% (11629/11629), 3.07 MiB | 1.18 MiB/s, done.
Total 11629 (delta 7804), reused 11328 (delta 7572)
```
* Copy the bundle to the remote
```
$ scp mcmcBayes.bundle DestinationHostname:/sandbox/mcmcBayes
```

From remote station that needs update:
* Ensure your local repository does not have any modifications on the files that will be updated
* Verify the bundle
```
$ git ls-remote mcmcBayes.bundle
d983efd60b06af49098a1a45bc2f24a9e3450a15        refs/heads/development
```
* Fetch from the bundle with a mapping
```
$ git fetch mcmcBayes.bundle refs/heads/development:refs/remotes/development
Receiving objects: 100% (11629/11629), 3.07 MiB | 0 bytes/s, done.
Resolving deltas: 100% (7804/7804), done.
From mcmcBayes.bundle
   bc08591..d983efd  development -> development
```
* Merge into our existing development branch
```
$ git merge refs/remotes/development
Updating 20cb1a5..d983efd
Fast-forward
 code/matlab/+mcmcBayes/@VDMCaseStudy1/VDMCaseStudy1.m                                  |  2 +-
 code/matlab/+mcmcBayes/@sequence/sequence.m                                            | 83 ++++++++++++++++++++++++++++++++++++++++++++---------------------------------------
 code/sampler/projectTemplates/Stan/Verhulst/Verhulstb.stan                             |  3 +--
 data/input/VDMCaseStudy1GompertzExpertJudgment1aEmpirical1a_simulationLUT.xml          |  2 +-
 data/input/VDMCaseStudy1GompertzExpertJudgment2aEmpirical2a_simulationLUT.xml          |  2 +-
 data/input/VDMCaseStudy1GompertzExpertJudgment3aEmpirical3a_simulationLUT.xml          |  2 +-
 data/input/VDMCaseStudy1GompertzExpertJudgment4aEmpirical4a_simulationLUT.xml          |  2 +-
 data/input/VDMCaseStudy1GompertzExpertJudgment5aEmpirical5a_simulationLUT.xml          |  2 +-
 data/input/VDMCaseStudy1LinearExpertJudgment1aEmpirical1a_simulationLUT.xml            |  2 +-
 data/input/VDMCaseStudy1LinearExpertJudgment2aEmpirical2a_simulationLUT.xml            |  2 +-
 data/input/VDMCaseStudy1LinearExpertJudgment3aEmpirical3a_simulationLUT.xml            |  2 +-
 data/input/VDMCaseStudy1LinearExpertJudgment4aEmpirical4a_simulationLUT.xml            |  2 +-
 data/input/VDMCaseStudy1LinearExpertJudgment5aEmpirical5a_simulationLUT.xml            |  2 +-
 data/input/VDMCaseStudy1PolynomialDegree2ExpertJudgment1aEmpirical1a_simulationLUT.xml |  2 +-
 data/input/VDMCaseStudy1PolynomialDegree2ExpertJudgment2aEmpirical2a_simulationLUT.xml |  2 +-
 data/input/VDMCaseStudy1PolynomialDegree2ExpertJudgment3aEmpirical3a_simulationLUT.xml |  2 +-
 data/input/VDMCaseStudy1PolynomialDegree2ExpertJudgment4aEmpirical4a_simulationLUT.xml |  2 +-
 data/input/VDMCaseStudy1PolynomialDegree2ExpertJudgment5aEmpirical5a_simulationLUT.xml |  2 +-
 data/input/VDMCaseStudy1VerhulstExpertJudgment1aEmpirical1a_simulationLUT.xml          |  2 +-
 data/input/VDMCaseStudy1VerhulstExpertJudgment2aEmpirical2a_simulationLUT.xml          |  2 +-
 data/input/VDMCaseStudy1VerhulstExpertJudgment3aEmpirical3a_simulationLUT.xml          |  2 +-
 data/input/VDMCaseStudy1VerhulstExpertJudgment4aEmpirical4a_simulationLUT.xml          |  2 +-
 data/input/VDMCaseStudy1VerhulstExpertJudgment5aEmpirical5a_simulationLUT.xml          |  2 +-
 doc/gitBundles.md                                                                      | 28 ++++++++++++++++++++++++++++
 24 files changed, 94 insertions(+), 62 deletions(-)
 create mode 100644 doc/gitBundles.md
```
Notes were located [here](http://programmaticallyspeaking.com/offline-repository-sync-using-git-bundle.html)