### cgroup setup for Linux
* Install cgroup-bin
`$ sudo apt-get install cgroup-bin`
* Create the cgroups (need to do this after each restart)
  * `$ sudo cgcreate -t $USER:$USER -a $USER:$USER -g cpu:/samplercpulimited`
  * `$ sudo cgcreate -t $USER:$USER -a $USER:$USER -g cpu:/matlabcpulimited`
  * `$ sudo cgcreate -t $USER:$USER -a $USER:$USER -g cpuset:/samplercpulimited`
  * `$ sudo cgcreate -t $USER:$USER -a $USER:$USER -g cpuset:/matlabcpulimited`
* Initialize the cgroup settings#this
  * this will split equally between all the processes
      * `$ sudo cgset -r cpu.shares=500 samplercpulimited`
      * `$ sudo cgset -r cpu.shares=500 matlabcpulimited`
  * this will enable all the cpu cores (customize per how many your hardware supports)
      * `$ sudo cgset -r cpuset.cpus=0-3 samplercpulimited`
      * `$ sudo cgset -r cpuset.cpus=0-3 matlabcpulimited`
* mcmcBayes takes care of spawning the processes using `cgexec -g cpu:/samplercpulimited sampler` or `cgexec -g cpu:/matlabcpulimited matlab`
* Check status using
  * `$ cat /sys/fs/cgroup/cpu/samplercpulimited/cpu.shares`
  * `$ cat /sys/fs/cgroup/cpu/matlabcpulimited/cpu.shares`
  * `$ cat /sys/fs/cgroup/cpuset/samplercpulimited/cpuset.cpus`
  * `$ cat /sys/fs/cgroup/cpuset/matlabcpulimited/cpuset.cpus`
* to determine the cgroup a process belongs to
  * `$ cat /proc/PID/cgroup`
