<?xml version="1.0" encoding="UTF-8" standalone="no"?><!-- prologue -->
<!-- Declare the external document type declaration file to validate against (SYSTEM keyword specifies external) -->

<!-- uuid generated in MATLAB using `>> java.util.UUID.randomUUID()` -->
<simulation uuid="VDMCaseStudy1VerhulstExpertJudgment3aEmpirical3a" type="Verhulst" subType="b" samplerType="mcmcBayes.t_samplerInterfaces.Stan" description="Settings for evaluation of Verhulst growth curve model using VDMCaseStudy1ExpertJudgment3a for the prior dataset and VDMCaseStudy1Empirical3a for the posterior." author="reuben@reubenjohnston.com" reference="Johnston, 2017">
	<variables numVars="4" numChains="1" defaultSolver="mcmcBayes.t_variablePointEstimateSolver.SolverMeanLS">
		<variable name="beta0" description="growth rate parameter that supports determining the point of inflection t=β_0/β_2 , N=β_1/2" type="mcmcBayes.t_variable.stochastic" dimensions="mcmcBayes.t_numericDimensions.scalar" id="1">
			<tfPre>mcmcBayes.t_variableTransformPre.none</tfPre>
			<tfPost>mcmcBayes.t_variableTransformPost.none</tfPost>
			<precision>.1</precision>
			<priorDistribution type="mcmcBayes.t_distribution.TZNormal" numParams="2">
				<!-- pr(beta0)~normal(a,b), where a=mean, b=precision -->
				<hyperParameter name="a" description="normal mean" type="double" dimensions="mcmcBayes.t_numericDimensions.scalar" id="1">
					<range>mcmcBayes.t_numericRange.gt0ANDltInf</range>
					<resolution>mcmcBayes.t_numericResolution.continuous</resolution>
					<values>1.0E-15</values><!--This is auto-solved-->
				</hyperParameter>
				<hyperParameter name="b" description="normal precision" type="double" dimensions="mcmcBayes.t_numericDimensions.scalar" id="2">
					<range>mcmcBayes.t_numericRange.gt0ANDltInf</range>
					<resolution>mcmcBayes.t_numericResolution.continuous</resolution>
					<values>0.1</values>
				</hyperParameter>					
			</priorDistribution>
			<solver solverOverride="mcmcBayes.t_variablePointEstimateSolver.SolverMeanLS">
				<min>1.0E-10</min>
				<max>100</max>
				<!-- Initial values for solver.  The solver outputs are used as inputs to mle/ls solvers.  The mle/ls solver outputs are used to estimate prior hyperparameter values for empirical or simulated datasets and additionally to determine initial values for the variable (which are passed in to the MCMC sampler). -->
				<priorInitialSolverStartValue>.1</priorInitialSolverStartValue>
				<posteriorInitialSolverStartValue>.1</posteriorInitialSolverStartValue>
			</solver>	
		</variable>
		<variable name="beta1" description="carrying capacity (maximum population size)" type="mcmcBayes.t_variable.stochastic" dimensions="mcmcBayes.t_numericDimensions.scalar" id="2">
			<tfPre>mcmcBayes.t_variableTransformPre.none</tfPre>
			<tfPost>mcmcBayes.t_variableTransformPost.none</tfPost>
			<precision>.1</precision>
			<priorDistribution type="mcmcBayes.t_distribution.TZNormal" numParams="2">
				<!-- pr(beta1)~normal(c,d), where c=mean, d=precision -->
				<hyperParameter name="c" description="normal mean" type="double" dimensions="mcmcBayes.t_numericDimensions.scalar" id="1">
					<range>mcmcBayes.t_numericRange.gt0ANDltInf</range>
					<resolution>mcmcBayes.t_numericResolution.continuous</resolution>
					<values>1.0E-15</values><!--This is auto-solved-->
				</hyperParameter>
				<hyperParameter name="d" description="normal precision" type="double" dimensions="mcmcBayes.t_numericDimensions.scalar" id="2">
					<range>mcmcBayes.t_numericRange.gt0ANDltInf</range>
					<resolution>mcmcBayes.t_numericResolution.continuous</resolution>
					<values>.01</values>
				</hyperParameter>					
			</priorDistribution>
			<solver solverOverride="mcmcBayes.t_variablePointEstimateSolver.SolverMeanLS">
				<min>1.0E-10</min>
				<max>100</max>
				<!-- Initial values for solver.  The solver outputs are used as inputs to mle/ls solvers.  The mle/ls solver outputs are used to estimate prior hyperparameter values for empirical or simulated datasets and additionally to determine initial values for the variable (which are passed in to the MCMC sampler). -->
				<priorInitialSolverStartValue>20</priorInitialSolverStartValue>
				<posteriorInitialSolverStartValue>20</posteriorInitialSolverStartValue>
			</solver>	
		</variable>
		<variable name="beta2" description="population growth rate that supports determining the point of inflection" type="mcmcBayes.t_variable.stochastic" dimensions="mcmcBayes.t_numericDimensions.scalar" id="3">
			<tfPre>mcmcBayes.t_variableTransformPre.none</tfPre>
			<tfPost>mcmcBayes.t_variableTransformPost.none</tfPost>
			<precision>.01</precision>
			<priorDistribution type="mcmcBayes.t_distribution.TZNormal" numParams="2">
				<!-- pr(beta2)~normal(g,h), where g=mean, h=precision -->
				<hyperParameter name="g" description="normal mean" type="double" dimensions="mcmcBayes.t_numericDimensions.scalar" id="1">
					<range>mcmcBayes.t_numericRange.gt0ANDltInf</range>
					<resolution>mcmcBayes.t_numericResolution.continuous</resolution>
					<values>1.0E-15</values><!--This is not auto-solved-->
				</hyperParameter>
				<hyperParameter name="h" description="normal precision" type="double" dimensions="mcmcBayes.t_numericDimensions.scalar" id="2">
					<range>mcmcBayes.t_numericRange.gt0ANDltInf</range>
					<resolution>mcmcBayes.t_numericResolution.continuous</resolution>
					<values>10</values><!--This is not auto-solved-->
				</hyperParameter>					
			</priorDistribution>
			<solver solverOverride="mcmcBayes.t_variablePointEstimateSolver.SolverMeanLS">
				<min>1.0E-10</min>
				<max>10</max>
				<!-- Initial values for solver.  The solver outputs are used as inputs to mle/ls solvers.  The mle/ls solver outputs are used to estimate prior hyperparameter values for empirical or simulated datasets and additionally to determine initial values for the variable (which are passed in to the MCMC sampler). -->
				<priorInitialSolverStartValue>.1</priorInitialSolverStartValue>
				<posteriorInitialSolverStartValue>.1</posteriorInitialSolverStartValue>
			</solver>	
		</variable>		
		<variable name="r" description="precision for zero mean error term" type="mcmcBayes.t_variable.stochastic" dimensions="mcmcBayes.t_numericDimensions.scalar" id="4">
			<tfPre>mcmcBayes.t_variableTransformPre.none</tfPre>
			<tfPost>mcmcBayes.t_variableTransformPost.none</tfPost>
			<precision>1.0E-3</precision>
			<priorDistribution type="mcmcBayes.t_distribution.Gamma" numParams="2">
				<!-- pr(r)~gamma(p,q), where p=shape, q=rate -->
				<hyperParameter name="p" description="Gamma shape" type="double" dimensions="mcmcBayes.t_numericDimensions.scalar" id="1">
					<range>mcmcBayes.t_numericRange.gt0ANDltInf</range>
					<resolution>mcmcBayes.t_numericResolution.continuous</resolution>
					<values>1</values><!--shape should always be 1-->
				</hyperParameter>
				<hyperParameter name="q" description="Gamma rate" type="double" dimensions="mcmcBayes.t_numericDimensions.scalar" id="2">
					<range>mcmcBayes.t_numericRange.gt0ANDltInf</range>
					<resolution>mcmcBayes.t_numericResolution.continuous</resolution>
					<values>5</values>
				</hyperParameter>
			</priorDistribution>
			<solver solverOverride="mcmcBayes.t_variablePointEstimateSolver.SolverMeanLS">
				<min>1.00E-08</min>
				<max>1.00E+04</max>
				<!-- Initial values for solver.  The solver outputs are used as inputs to mle/ls solvers.  The mle/ls solver outputs are used to estimate prior hyperparameter values for empirical or simulated datasets and additionally to determine initial values for the variable (which are passed in to the MCMC sampler). -->
				<priorInitialSolverStartValue>.1</priorInitialSolverStartValue>
				<posteriorInitialSolverStartValue>.1</posteriorInitialSolverStartValue>
			</solver>	
		</variable>			
	</variables>
	<mcmcSettings>
		<numAdaptSteps>1000</numAdaptSteps>
		<numBurnIn>500000</numBurnIn>
		<numSamples>500000</numSamples>
		<numLagSteps>1</numLagSteps>
		<initialSolverType>mcmcBayes.t_variableInits.Solver</initialSolverType>
	</mcmcSettings>	
	<data>
		<priorDataUuid>VDMCaseStudy1ExpertJudgment3a</priorDataUuid>
		<priorPredictedDataUuid>defaultPriorPredicted</priorPredictedDataUuid>
		<posteriorDataUuid>VDMCaseStudy1Empirical3a</posteriorDataUuid>
		<posteriorPredictedDataUuid>defaultPosteriorPredicted</posteriorPredictedDataUuid>
	</data>
</simulation>	