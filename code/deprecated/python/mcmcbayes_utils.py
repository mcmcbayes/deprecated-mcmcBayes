#!/usr/bin/env python3
#Tested with vanilla Python 3.6
#
#Author: reuben@reubenjohnston.com
# 
#Description: Utility module with OS agnostic functions for mcmcBayes.  Among other 
#  things, it is used to spawn MCMC sampler processes, spawn worker processes 
#  for mcmcBayesEvaluateModels sequences, and monitor these worker processes.
#
#Requirements: 
#  get_os.py, os_comms.py, os_pipe.py, os_semaphore.py, engine_comms_messaging.py, mcmcbayes_utils_command_parser.py, matlab.engine
#  See the MATLAB instructions for installing matlab.engine.
#
#Usage:
#  See mcmcbayes_utils_command_parser.py
#
#Unit tests:
#  To run from python shell
#  $ export PYDEVD_USE_CYTHON=NO && export PYDEVD_USE_FRAME_EVAL=NO && python  
#  >>> import mcmcbayes_utils
#  >>> mcmcbayes_utils.unitTests()
#  To run in PyDev debugger, make sure you set debugging environment variables PYDEVD_USE_CYTHON=NO and PYDEVD_USE_FRAME_EVAL=NO.  
#    Create a test program with the two commands listed above and run it in a debug session.
#  One must run a sequence in MATLAB to create needed files; otherwise runSampler, spawnSamplerProcess, and updateSamplerStatus 
#    unit tests will fail.  E.g., >> retseq=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation('regressionEvaluation', 
#    'regressionSeqEval-runSingleSim', 'polynomialDegree2FlatSimulated')
#
#History:
#  2019-02-11 - Initial version validated by Reub on Ubuntu 18.04 and Windows 6.1.7601

from enum import IntEnum
from os_processes.OSProcesses import OSProcesses
        
def checkKillFile(killFile):
    import os,sys
    
    retVal=False
    try:
        retVal=os.path.exists(killFile)
    except Exception:
        print("Exception, ", sys.exc_info()[0], ", in checkKillFile()")
    return(retVal)

def killPID(PID):
    import sys,psutil,os
    
    #may need an OS mutex
    try:
        message=('\t\tKilling PID=%s') % (str(PID))
        print(message)
        p=psutil.Process(PID)
        p.kill()
    except psutil.NoSuchProcess:
        message=('psutil.NoSuchProcess exception, %s , in PID()') % (sys.exc_info()[0])
        print(message)        
    except Exception:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)
                
def killPIDs(parentPID):
    import sys,psutil,os
    
    try:
        message=("\t\tKilling parentPID=%d and all subprocesses") % (parentPID)
        print(message)
        p=psutil.Process(parentPID)
        for subproc in p.children(recursive=True): #for some reason windows psutil requires using p.get_children()
            killPID(subproc.pid)
        killPID(parentPID)
    except Exception:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)
        
def killNamedProc(processName):
    import sys,psutil,os
    
    try:
        message=('\t\tKilling processes with name=%s') % (processName)
        print(message)
        for proc in psutil.process_iter(): #for some reason windows psutil requires using p.get_children()
            process = psutil.Process(proc.pid)# fetch the process info using PID
            pname = process.name()# query the process name
            if processName == pname:
                killPID(proc.pid)
    except Exception:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)
        
def runBashCommand(debug, msgFile, shellStartScript, startShellArgs, shellCmd, shellArgs):
    import os,sys,time
    import os_processes
    
    #execute runSampler as a process            
    try:
        pid=spawnBashCommand(debug, msgFile, shellStartScript, startShellArgs, shellCmd, shellArgs)

        if debug:
            message=('\t\tParent waiting for child pid=%d to finish') % (pid)
            print(message)

        finished=False
        while not finished:
            try:
                os_processes.OSProcesses.poll(pid, 1)
                finished=True
            except TimeoutError:
                continue    
 
    except Exception:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)                      

def spawnBashCommand(debug, msgFile, shellStartScript, startShellArgs, shellCmd, shellArgs):
    import psutil
    from os_processes import OSProcesses
    
    startShellArgs.append('-c')
    startShellArgs.append(shellCmd)
    
    for shellCmdArg in shellArgs:
        if not shellCmdArg=='':
            startShellArgs.append(shellCmdArg)
    
    if debug:
        message=("\t\tspawnBashCommand is spawning worker process for runBashCommand(%s)") % (shellCmd)
        print(message)
      
    pid=OSProcesses.forkIndirect(shellStartScript, startShellArgs, shellCmd)

    psutilproc = psutil.Process(pid)# fetch the process info using PID
    procname = psutilproc.name()# query the process name
    proccmdline = psutilproc.cmdline()
            
    msgFilefd=open(msgFile,'w')#always create a new one

    message="##PID##\n%d\n" % (pid)
    msgFilefd.write(message)
    message="##PROCESSNAME##\n%s\n" % (procname)
    msgFilefd.write(message)
    message="##PROCESSCMDLINE##\n%s\n" % (' '.join(proccmdline))
    msgFilefd.write(message)
    message="##STATE##\n%s\n" % ('mcmcBayes.t_runState.Running')
    msgFilefd.write(message)
    msgFilefd.close()
    
    return(pid)

def updateBashCommandStatus(debug, UUIDMsgFile, PID):
    #This function will check whether the process identified by samplerPID exists.  If so, the state remains
    #  "Running" and this function returns;  otherwise, it will open the file designated by UUIDmsgfilestr to 
    #  update the ##SAMPLERSTATE## and ##SAMPLEREXITCODE## lines properly.  In doing the latter, it reads in
    #  the entire file into a local buffer that is updated appropriately and then saved back into the file.
    #  Uses a string, samplerName, when we're polling Stan, to check whether a process with that name exists.
    #  Otherwise, we're either polling JAGS or OpenBugs and we simply look for samplerPID existence.
    import sys,psutil,os
    import os_processes
    
    try:
        #Are we still running?    
        try:
            os_processes.OSProcesses.poll(PID, 1)
        except TimeoutError:
            state="mcmcBayes.t_runState.Running"

#TODO: add a simple error check that analyzes logfile for keywords ('deleting', 'error', 'stuck', 'inconsistent', others?)                      
#TODO: need to pass in logfile path
                
        #Update the UUIDMsgFile if not running    
        if not 'state' in locals():
            UUIDmsgfd=open(UUIDMsgFile,'r+')
            state="mcmcBayes.t_runState.Finished"
            #this only updates file when the sampler process is not detected
            bufferlines=UUIDmsgfd.read().splitlines() #read the file into buffer and split into lines
            idx_state=-1
            if bufferlines:
                for index, line in enumerate(bufferlines): #loop across the buffer
                    line=line.strip()
                    bufferlines[index]=line
                    if line=='##STATE##': 
                        # when ##SAMPLERSTATE## exists in buffer, replace it in the buffer (we can only have 
                        #   one SAMPLERSTATE variable in the message file)
                        idx_state=index+1 #save the index
                    elif line=='##EXITCODE##':
                        tempstr=bufferlines[index+1] #read next value 
                        exitcode=int(tempstr) #convert XXX to an integer
                        if exitcode==0: #set samplerstate to Finished or Error
                            state="mcmcBayes.t_runState.Finished"
                        else:
                            state="mcmcBayes.t_runState.Error"
            if idx_state==-1: #append ##SAMPLERSTATE##, 'samplerState=mcmcBayes.t_runState.Idle'
                bufferlines.append('##STATE##')
                bufferlines.append(state)
            else: #insert state at idx_state
                bufferlines[idx_state]=state
            UUIDmsgfd.seek(0,0) #reposition to beginning of file
            for line in bufferlines: #save the buffer
                UUIDmsgfd.write(line+'\n')
            UUIDmsgfd.close()
        print("\t\tstate=", state)
    except Exception:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)
                   
def runMATLABEngine(startupDir, killFile, maxEngines, matlabfunc, matlabRunView, enginesIndex, notOSComms, debug):
    #This function runs as a worker process and is responsible for starting and monitoring an independent MATLAB engine 
    #  running matlabfunc.  This function opens an OS semaphore that is used to confirm MATLAB resources are available 
    #  (between all the other worker process).  Prior to starting the MATLAB engine, this function waits to acquire the 
    #  semaphore.  This function opens an OS named pipe to send status messages to the parent process monitoring the state
    #  of all the workers.  The workers send messages when they start processing and when they complete.  Additionally, this 
    #  function monitors for the killFile existence and will terminate the MATLAB engine and exit on detecting its existence.
    #  There are essentially four state areas for this function:
    #  1. Setup and open the OS semaphore and named pipe
    #  2. Wait to acquire the semaphore, then start the MATLAB engine and begin execution of matlabfunc, send started message
    #     to the parent process monitoring the workers
    #  3. Wait for matlabfunc execution to finish and shutdown the MATLAB engine, send finished message to the parent process
    #  4. Close the OS semaphore and named pipe and exit    
    
    import os,time,sys,psutil
    import matlab.engine
    import get_os,os_comms,os_pipe,os_semaphore
    from engine_comms_messaging import EngineCommsMessage
    from engine_comms_messaging import EngineStatus
    from os_semaphore import OSSemaphore
   
    try:
        OStype=get_os.osType()#check the OS type and instantiate the appropriate OSPipe
                
        if notOSComms==False:
            speaker=os_comms.OSComms(enginesIndex,os_pipe.Role.SPEAKER)     
            speaker.setup()#blocking
            semRole=os_semaphore.Role.USER
        else:
            semRole=os_semaphore.Role.CREATOR
            
        enginesSemaphore=OSSemaphore.static__init__(semRole)             
        enginesSemaphore.setup('enginesSemaphore',1,1)
       
        if not notOSComms:
            while not speaker.ready.is_set():
                time.sleep(1)
      
        #get the PID
        PID=os.getpid()
        
        semTimeout=2
        
        engine = None
        enginePID = None
        backgroundjob = None
                
        okToStartEngine=False        
        engineComms=EngineCommsMessage.new(PID,enginesIndex,EngineStatus.WAITING)
        if not notOSComms:
            sermsg=engineComms.encode() 
            speaker.speak(sermsg)    
        print("\t\tWorker=", str(PID)," for runMATLABEngine(enginesIndex=",str(enginesIndex),", WAITING)")
        
        while not engineComms.engineStatus==EngineStatus.COMPLETED and not engineComms.engineStatus==EngineStatus.KILLED and \
              not engineComms.engineStatus==EngineStatus.ERROR:
            time.sleep(1)
            if checkKillFile(killFile):
                if engineComms.engineStatus==EngineStatus.RUNNING:
                    engine.exit() #shut down the engine
                    engineComms.engineStatus= EngineStatus.KILLED
                print("\t\tWorker=", str(PID)," for runMATLABEngine(enginesIndex=",str(enginesIndex),", KILLED)")
                if not notOSComms:
                    sermsg=engineComms.encode() 
                    speaker.speak(sermsg)    
                break 
            if okToStartEngine:
                if engineComms.engineStatus==EngineStatus.RUNNING:
                    #check if the engine is finished, and if so update engineFinished
                    if not psutil.pid_exists(enginePID):#check if the MATLAB engine PID went away  
                        message="\t\tMATLAB command %s exited unexpectedly" % matlabfunc
                        print(message)
                        enginesSemaphore.release()   
                        engineComms.engineStatus=EngineStatus.ERROR
                        print("\t\tWorker=", str(PID)," for runMATLABEngine(enginesIndex=",str(enginesIndex),", ERROR)", )
                        if not notOSComms:        
                            sermsg=engineComms.encode() 
                            speaker.speak(sermsg)                                              
                    if backgroundjob.done():
                        #we don't have any results as nargout=0
                        engine.eval('close all hidden;',nargout=0)#runs 'close all hidden' in the MATLAB engine
                        time.sleep(5) #short delay before shutting down the engine
                        engine.exit() #shut down the engine
                        enginesSemaphore.release()
                        engineComms.engineStatus=EngineStatus.COMPLETED
                        print("\t\tWorker=", str(PID)," for runMATLABEngine(enginesIndex=",str(enginesIndex),", COMPLETED)", )                       
                        if not notOSComms:        
                            sermsg=engineComms.encode() 
                            speaker.speak(sermsg)                          
                else:#start it
                    if matlabRunView=='mcmcBayes.t_runView.Foreground':
                        engine=matlab.engine.start_matlab('-desktop') 
                    else:#'mcmcBayes.t_runView.Background'
                        engine=matlab.engine.start_matlab()#spawns a background job with matlab to perform matlabfun
                        #start the desktop later with engine.desktop(nargout=0)
                    engineComms.engineStatus=EngineStatus.CREATED
                    print("\t\tWorker=", str(PID)," for runMATLABEngine(enginesIndex=",str(enginesIndex),", CREATED)", )                       
                    if not notOSComms:        
                        sermsg=engineComms.encode() 
                        speaker.speak(sermsg)                          
                    commandStr='cd '+startupDir+';'
                    if OStype==get_os.OsType.LINUX or OStype==get_os.OsType.MAC:
                        commandStr.replace('\\','/')
                    engine.eval(commandStr,nargout=0)
                    commandStr='run startup.m;'
                    engine.eval(commandStr,nargout=0)
                    enginePID=int(engine.eval("feature('GetPid');",nargout=1))
                    message="\t\tMATLAB engine running: %s" % matlabfunc
                    print(message)                    
                    backgroundjob=engine.eval(matlabfunc,nargout=0,background=True) 
                    #runs the MATLAB command specified by matlabfuncs[index] in the MATLAB engine
                    engineComms.engineStatus=EngineStatus.RUNNING
                    print("\t\tWorker=", str(PID)," for runMATLABEngine(enginesIndex=",str(enginesIndex),", RUNNING)", )
                    if not notOSComms:        
                        sermsg=engineComms.encode() 
                        speaker.speak(sermsg)  
            else:#wait for start condition                                                   
                numengines = __checkMATLABInstances()#get the current count of running matlab engines
                if numengines<maxEngines:#try to obtain the semaphore                    
                    result=enginesSemaphore.acquire(semTimeout)
                    if result:#result should be True
                        okToStartEngine=True
                    
        engineComms.engineStatus=EngineStatus.SHUTDOWN
        print("\t\tWorker=", str(PID)," for runMATLABEngine(enginesIndex=",str(enginesIndex),", SHUTDOWN)", )
        if not notOSComms:        
            sermsg=engineComms.encode() 
            speaker.speak(sermsg)  
            speaker.teardown()
        enginesSemaphore.teardown()
         
    except Exception:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)
        time.sleep(3)#add a short delay to allow viewing

def spawnMATLABEngines(engineStartDir, killFile, maxNumEngines, engineCommands, engineLogs, engineRunView, debug):    
    #This function spawns individual matlab worker processes for running each entry in engineCommands[] (in a parallel MATLAB 
    #  engine) and monitors their status.  It creates an OS semaphore to synchronize the number of presently running MATLAB 
    #  engines between all the worker processes.  This semaphore's max value is initialized to maxNumEngines.  The worker 
    #  processes will block until they can acquire the semaphore.  This function creates OS named pipes to receive status 
    #  messages from each of the worker processes.  The workers send messages when they start processing and when they 
    #  complete.  This function monitors the state for each of the workers by receiving and processing these incoming 
    #  messages.  There are essentially four state areas for this function:
    #  1. Construct the OS resources and all the worker processes
    #  2. Wait for all the workers to start the MATLAB engines and begin execution of their individual entry from engineCommands[]
    #  3. Wait for all the workers to finish executing their individual entry from engineCommands[]
    #  4. Shutdown all the workers and destruct the OS resources created    
    #  Additionally, this function monitors for the killFile existence and will terminate all children processes and itself on
    #  detecting its existence.
    
    import os,subprocess,sys,time
    import get_os,os_comms,os_semaphore,os_pipe   
    from os_semaphore import OSSemaphore
    from os_processes import OSProcesses
    from engine_comms_messaging import EngineCommsMessage
    from engine_comms_messaging import EngineStatus

    try:
        numEngines=len(engineCommands)
        OStype=get_os.osType()#check the OS type and instantiate the appropriate OSPipe
        listeners=[False]*numEngines#pipe objects for communicating with each worker process
        Processes=[False]*numEngines#worker process subprocess objects
        LogFiles=[False]*numEngines#logfile handles passed to each subprocess.popen (for its stdout and stderr pipes) 
        engineComms=[None]*numEngines#message handling and tracking engine status

        notOSComms=False

        message="\t\tspawnMATLABEngine_MONITOR Started, PID=%d." % (os.getpid())
        print(message)    
        
        enginesSemaphore=OSSemaphore.static__init__(os_semaphore.Role.CREATOR)      
        enginesSemaphore.setup('enginesSemaphore',maxNumEngines,maxNumEngines)        
        print("\t\tConstructed the mcmcBayesSemaphore for tracking MATLAB instances")
    
        #spawn all the worker processes
        for engineIndex in range(0,numEngines): #iterate through all the workers (range converts max to max-1)  
            if checkKillFile(killFile):
                print("\t\tkillfile detected!")
                break
            
            listeners[engineIndex]=os_comms.OSComms(engineIndex,os_pipe.Role.LISTENER)
            
            command=[engineStartDir, killFile, maxNumEngines, engineCommands[engineIndex], engineRunView, engineIndex, notOSComms, debug]

            LogFiles[engineIndex]=open(engineLogs[engineIndex], 'w')               
            message=("\t\tspawning worker process for runMATLABEngine(%s)") % (command)
            print(message)
            #spawn the worker process
            listeners[engineIndex].setup()
            
            #Processes[engineIndex]=subprocess.Popen(command, stdout=LogFiles[engineIndex], stderr=LogFiles[engineIndex])
            #runMATLABEngine(startupDir, killFile, maxEngines, matlabfunc, matlabRunView, enginesIndex, notOSComms, debug)
#todo: need to pass LogFiles[engineIndex] to child; add an logFile argument to runMATLABEngine
            Processes[engineIndex]=OSProcesses.fork(runMATLABEngine,command)
            
            engineComms[engineIndex]=EngineCommsMessage.new(Processes[engineIndex],engineIndex,EngineStatus.NULL)

            message=("\t\trunMATLABEngine_WORKER%d started and connected to the message pipe.") %(engineIndex)
            print(message)
      
        message=("\t\tspawnMATLABEngine_MONITOR constructed all the OS objects and started the worker processes, PID=%d.") % (os.getpid())
        print(message)    
    
        if maxNumEngines<=2:
            message="\t\twarning, maxNumEngines=%d is very low, this script might not complete if you have other instances of MATLAB running" \
                % (maxNumEngines)
            print (message)
        elif maxNumEngines<=0:
            raise ValueError("error, maxNumEngines=%d will not complete") % (maxNumEngines)
    
        allFinished=False
        while allFinished==False:
            time.sleep(1)
            if checkKillFile(killFile):
                print("\t\tkillfile detected!")
                break
            tempFinished=[False]*numEngines
            for engineIndex in range(0,numEngines): #iterate through all the workers (range converts max to max-1)  
                if engineComms[engineIndex].engineStatus==EngineStatus.ERROR:
                    tempFinished[engineIndex]=True 
                    message="\t\trunMATLABEngine_WORKER%d error" %(engineIndex)
                    print(message)
                elif engineComms[engineIndex].engineStatus==EngineStatus.KILLED:
                    tempFinished[engineIndex]=True 
                    message="\t\trunMATLABEngine_WORKER%d killed" %(engineIndex)
                    print(message)
                else:
                    sermsg=listeners[engineIndex].listen()#any new messages
                    if not sermsg==None:
                        msg=EngineCommsMessage.decode(sermsg)
                        if not isinstance(msg,EngineCommsMessage):
                            raise
                        if not msg.engineStatus==engineComms[engineIndex].engineStatus:
                            engineComms[engineIndex].engineStatus=msg.engineStatus
                            message="\t\trunMATLABEngine_WORKER%d changed to %s" %(engineIndex,engineComms[engineIndex].engineStatus)
                    else:
                        if engineComms[engineIndex].engineStatus==EngineStatus.COMPLETED or \
                            engineComms[engineIndex].engineStatus==EngineStatus.SHUTDOWN:
                            tempFinished[engineIndex]=True
                        else:
                            try: 
                                OSProcesses.poll(Processes[engineIndex],1)
                                if engineComms[engineIndex].engineStatus==EngineStatus.WAITING:#worker process not running
                                    engineComms[engineIndex].engineStatus=EngineStatus.ERROR
                                    tempFinished[engineIndex]=True
                                    message="\t\trunMATLABEngine_WORKER%d premature exit" %(engineIndex)
                                    print(message)                                
                            except TimeoutError:#worker still running
                                pass
            if not False in tempFinished:
                allFinished=True
    
        message="\t\tspawnMATLABEngine_MONITOR All the worker processes have completed running their entry in engineCommands[]" 
        print(message)    
    
        if checkKillFile(killFile):
            time.sleep(10)#add a short delay to allow workers time to shutdown
        else:
            time.sleep(1)#add a short delay for workers to close pipe handles
            
        for index in range(0,numEngines):
            #we are either done or killFile was detected, cleanup and kill any running subprocesses (range converts max to max-1)  
            LogFiles[index].close()
            #Processes[index].terminate()
            listeners[index].teardown()
            if debug:
                try:
                    f=open(engineLogs[index],'r')
                    print(f.read())
                    f.close()
                except:
                    pass
        
        del Processes
        enginesSemaphore.teardown()
    
        message="\t\tspawnMATLABEngine_MONITOR Finished, PID=%d" % (os.getpid())
        print(message)
        time.sleep(3)#add a short delay to allow viewing
    except Exception:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)
        time.sleep(3)#add a short delay to allow viewing

def __checkMATLABInstances():
    import sys,psutil
    import get_os
    
    #Determines and returns the number of processes named 'matlab'    
    OStype=get_os.osType()#check the OS type and instantiate the appropriate OSPipe
    if OStype==get_os.OsType.WINDOWS:
        PROCNAME="*/bin/*/MATLAB*" 
        #specify more than just the executable (as some extra processes typically exist with 'matlab' in the name)
    elif OStype==get_os.OsType.LINUX:
        PROCNAME="*/bin/*/MATLAB*" 
        #specify more than just the executable (as some extra processes typically exist with 'matlab' in the name)
    elif OStype==get_os.OsType.MAC:
        PROCNAME="*/Applications/*/MATLAB*" 
        #specify more than just the executable (as some extra processes typically exist with 'matlab' in the name)
    count=0
    try:
        for proc in psutil.process_iter():
            if PROCNAME in proc.name().lower() :
                count=count+1
    except Exception:
        print("Exception, %s , in __checkMATLABInstances()", sys.exc_info()[0])

    return(count)
 
def __psnameExists(name):
    import sys,psutil
    
    try:
        for proc in psutil.process_iter(): 
            process = psutil.Process(proc.pid)# fetch the process info using PID
            pname = process.name()# query the process name
            if pname == name:
                print(name," in ",pname)
                return True
        return False
    except Exception:
        print("Exception, ", sys.exc_info()[0] , ", in __psnameExists()")                
        return False

def unitTests():
    from os_semaphore import OSSemaphore
    from os_pipe import OSPipe
    import os_processes
    import os_comms,engine_comms_messaging,mcmcbayes_utils
    from mcmcbayes_utils_command_parser import CommandType
    
    #mcmcbayes_utils.unitTest(-1)#this will test printing help
    OSSemaphore.unitTest()
    OSPipe.unitTest()
    engine_comms_messaging.EngineCommsMessage.unitTest()
    os_comms.OSComms.unitTest()
    os_processes.OSProcesses.unitTest()
    mcmcbayes_utils.unitTest(CommandType.RUNBASHCMD)
    mcmcbayes_utils.unitTest(CommandType.SPAWNBASHCMD) #Also tests CommandType.UPDATEBASHCMDSTATUS
    #mcmcbayes_utils.unitTest(CommandType.RUNMATLABENGINE)
    #mcmcbayes_utils.unitTest(CommandType.SPAWNMATLABENGINES)
    #TODO: mcmcbayes_utils.unitTest(CommandType.CHECKKILLFILE)
    #mcmcbayes_utils.unitTest(CommandType.KILLPID)
    #mcmcbayes_utils.unitTest(CommandType.KILLPIDS)
    #TODO: mcmcbayes_utils.unitTest(CommandType.KILLNAMEDPROC)#unitTest() not appropriate on Windows

def unitTest(testId):
    import os,sys,time
    import mcmcbayes_utils_command_parser, get_os, process_related_testing_support
    from mcmcbayes_utils_command_parser import CommandType
    
    OStype=get_os.osType()#check the OS type and instantiate the appropriate OSPipe    
    if OStype==get_os.OsType.WINDOWS:
        drive='C:'
        mcmcBayesPath='/sandbox/mcmcBayes'
        shellStartScript=drive+mcmcBayesPath.replace('/','\\')+'\\code\\scripts\\startBashWinOs.cmd'
        msys2Path='C:\\opt\\msys64'
        terminalPath=msys2Path+'\\msys2.exe'
        shellPath=msys2Path+'\\usr\\bin\\bash.exe'                
        pythonPath=drive+'/opt/Python36/python.exe'
        killFile=drive+mcmcBayesPath+'/temp/aLittleBitOfPoison' 
        engineStartDir=drive+mcmcBayesPath+'/code/matlab'
        pythonPath='C:\\opt\\Python36\\python.exe' 
    elif OStype==get_os.OsType.LINUX or OStype==get_os.OsType.MAC:
        mcmcBayesPath='/sandbox/mcmcBayes'
        if OStype==get_os.OsType.MAC:
            shellStartScript=mcmcBayesPath+'/code/scripts/startBashMacOs'
            terminalPath='/Applications/Alacritty.app/Contents/MacOS/alacritty'
            shellPath='/bin/bash'
            pythonPath='/Library/Frameworks/Python.framework/Versions/3.6/bin/python3' 
        else:
            shellStartScript=mcmcBayesPath+'/code/scripts/startBashUbuntuOs'
            terminalPath='gnome-terminal'
            shellPath='/bin/bash'
            pythonPath='python3'
        killFile=mcmcBayesPath+'/temp/aLittleBitOfPoison' 
        engineStartDir=mcmcBayesPath+'/code/matlab'
    pythonScript='mcmcbayes_utils_command_parser.py'
   
    if testId==CommandType.RUNBASHCMD:
        #test runBashCommand(shellPath, shellCmd, shellArgs, shellCmdLog, killFile, UUIDMsgFile, debug)

        print('Running unitTest() for '+testId.toStr())        
        command='RUNBASHCMD'
        if OStype==get_os.OsType.WINDOWS:
            #shellCmd needs to be a bash path string
            shellCmd=('/'+drive.replace(':','')+mcmcBayesPath+'/code/scripts/demo')
            #shellLog needs to be a bash path string
            shellLog=('/'+drive.replace(':','')+mcmcBayesPath+'/logs/demo-log.txt')
            #msgFile needs to be a Python path string
            msgFile=(drive+mcmcBayesPath+'/temp/demo-status.txt')
        elif OStype==get_os.OsType.LINUX or OStype==get_os.OsType.MAC:
            shellCmd=(mcmcBayesPath+'/code/scripts/demo')
            #shellLog needs to be a bash path string
            shellLog=(mcmcBayesPath+'/logs/demo-log.txt')
            #msgFile needs to be a Python path string
            msgFile=(mcmcBayesPath+'/temp/demo-status.txt')

        if os.path.exists(msgFile):
            os.remove(msgFile)
         
        if OStype==get_os.OsType.WINDOWS:
            shellArgs=['\'Howdy ho!\'', '15']
            shellTitle='\"Run demo\"'
        else:
            shellArgs=['\'Howdy ho!\'', '15']
            shellTitle='\'Run demo\''
        mcmcbayes_utils_command_parser.altParse([command,
                                                 '--msgFile', msgFile,
                                                 '--debug',
                                                 '--killFile', killFile, 
                                                 '--terminal', terminalPath,
                                                 '--shellStartScript', shellStartScript,
                                                 '--shell', shellPath,
                                                 '--shellTitle', shellTitle,
                                                 '--shellLog', shellLog,
                                                 '--shellCmd', shellCmd,                                                     
                                                 '--shellArgs', shellArgs[0], shellArgs[1]])         
        print('unitTest() for '+testId.toStr()+' finished')
    elif testId==CommandType.SPAWNBASHCMD:
        #test spawnSamplerProcess(pythonPath, pythonScript, killFile, samplerShortcut, samplerScript, samplerLogFile, samplerType, UUIDmsgfilestr, 
        #  samplerRunView, debug)

        print('Running unitTest() for '+testId.toStr())
        command='SPAWNBASHCMD'
        if OStype==get_os.OsType.WINDOWS:
            #shellCmd needs to be a bash path string
            shellCmd=('/'+drive.replace(':','')+mcmcBayesPath+'/code/scripts/demo')
            #shellLog needs to be a bash path string
            shellLog=('/'+drive.replace(':','')+mcmcBayesPath+'/logs/demo-log.txt')
            #msgFile needs to be a Python path string
            msgFile=(drive+mcmcBayesPath+'/temp/demo-status.txt')
        elif OStype==get_os.OsType.LINUX or OStype==get_os.OsType.MAC:
            shellCmd=(mcmcBayesPath+'/code/scripts/demo')
            #shellLog needs to be a bash path string
            shellLog=(mcmcBayesPath+'/logs/demo-log.txt')
            #msgFile needs to be a Python path string
            msgFile=(mcmcBayesPath+'/temp/demo-status.txt')        

        if os.path.exists(msgFile):
            os.remove(msgFile)
         
        if OStype==get_os.OsType.WINDOWS:
            shellArgs=['\'Howdy ho!\'', '15']
            shellTitle='\"Run demo\"'
        else:
            shellArgs=['\'Howdy ho!\'', '15']
            shellTitle='\'Run demo\''
        mcmcbayes_utils_command_parser.altParse([command,
                                                 '--msgFile', msgFile,
                                                 '--debug',
                                                 '--killFile', killFile,   
                                                 '--terminal', terminalPath, 
                                                 '--shellStartScript', shellStartScript,
                                                 '--shell', shellPath,
                                                 '--shellTitle', shellTitle,
                                                 '--shellLog', shellLog,
                                                 '--shellCmd', shellCmd,                                                     
                                                 '--shellArgs', shellArgs[0], shellArgs[1]])                
        
        while not os.path.exists(msgFile):
            time.sleep(0.1)
        
        #read the pid from UUIDMsgFile
        pid=-1
        while pid==-1:
            time.sleep(0.1)
            msgFilefd=open(msgFile,'r+')
            bufferlines=msgFilefd.read().splitlines() #read the file into buffer and split into lines
            msgFilefd.close()
            idx_pid=-1
            for index, line in enumerate(bufferlines): #loop across the buffer
                line=line.strip()
                bufferlines[index]=line
                if line=='##PID##': 
                    idx_pid=index+1 #save the index
            if idx_pid>=0:
                pid=int(bufferlines[idx_pid])
        
        message=('\t\tPID=%d') %(pid)
        print(message)
        
        #use UPDATEBASHCMDSTATUS to query status and wait for completion
        finished=False
        while not finished:
            time.sleep(.5)
            mcmcbayes_utils_command_parser.altParse(['UPDATEBASHCMDSTATUS',
                                             '--msgFile', msgFile,
                                             '--debug',
                                             '--processPID', str(pid)])
            msgFilefd=open(msgFile,'r+')
            bufferlines=msgFilefd.read().splitlines() #read the file into buffer and split into lines
            msgFilefd.close()
            for index, line in enumerate(bufferlines): #loop across the buffer
                line=line.strip()
                bufferlines[index]=line
                if line=='##STATE##': 
                    idx_state=index+1 #save the index
            state=bufferlines[idx_state]
            if state=='mcmcBayes.t_runState.Finished' or state=='mcmcBayes.t_runState.Error':
                finished=True       
        
        print('unitTest() for '+testId.toStr()+' finished')             
    elif testId==CommandType.UPDATEBASHCMDSTATUS:
        #updateSamplerStatus(UUIDmsgfilestr, samplerPIDorName, useAltRunCheck)
  
        print('unitTest() for '+testId.toStr()+' is run indirectly via SPAWNBASHCMD')  
    elif testId==CommandType.CHECKKILLFILE:
#TODO: finish        
        #create the file
        #call checkkillfile and confirm result
        #remove the file
        #call checkkillfile and confirm result
        pass
    elif testId==CommandType.KILLPID:
        #test killPID(killPID)    
        
        print('Running unitTest() for '+testId.toStr())        
        command='KILLPID'
        args=list()
        pid=OSProcesses.fork(process_related_testing_support.sleepyProcess,args)
        time.sleep(5)#short delay so we can verify in process monitor
        mcmcbayes_utils_command_parser.altParse([command,
                                             '--debug',
                                             '--PID', str(pid)])
        print('unitTest() for '+testId.toStr()+' finished')           
    elif testId==CommandType.KILLPIDS:
        #test killPIDS(killParentPID)    
        
        print('Running unitTest() for '+testId.toStr())        
        command='KILLPIDS'
        args=list()
        parentPID=OSProcesses.fork(process_related_testing_support.friskyProcess,args)
        time.sleep(5)#short delay so we can verify in process monitor
        mcmcbayes_utils_command_parser.altParse([command,
                                                 '--debug',
                                                 '--parentPID', str(parentPID)])     
        print('unitTest() for '+testId.toStr()+' finished')
    elif testId==CommandType.KILLNAMEDPROC:
        #test killNamedProc(killProcName)    
#TODO: not implemented for Windows       
        print('Running unitTest() for '+testId.toStr())        
        command='KILLNAMEDPROC'
        processName='dopplegangerProcess'
        process_related_testing_support.altMain(['--cmdId', '3', 
                                                 '--processName', processName, 
                                                 '--spawn'])
        time.sleep(5)#short delay so we can verify in process monitor
        mcmcbayes_utils_command_parser.altParse([command,
                                                 '--debug',
                                                 '--processName', processName])     
        print('unitTest() for '+testId.toStr()+' finished') 
    elif testId==CommandType.RUNMATLABENGINE:
        #test runMATLABEngine(startupDir, killFile, maxEngines, matlabfunc, matlabRunView, enginesIndex, notOSComms)

        print('Running unitTest() for '+testId.toStr())
        command='RUNMATLABENGINE'
        maxNumEngines=str(3)
        engineCommands=list()
        engineCommands.append('why;disp(\'\tEngine#1 is pausing(1)\');pause(1);')
        engineRunView='mcmcBayes.t_runView.Background'#mcmcBayes.t_runView.Foreground
        engineIndex=str(0)
        
        mcmcbayes_utils_command_parser.altParse([command, 
                                                '--debug', 
                                                '--notOSComms', 
                                                '--engineStartDir', engineStartDir, 
                                                '--killFile', killFile, 
                                                '--maxNumEngines', maxNumEngines, 
                                                '--engineCommands', engineCommands[0], 
                                                '--engineRunView', engineRunView, 
                                                '--engineIndex', engineIndex])
        print('unitTest() for '+testId.toStr()+' finished')          
    elif testId==CommandType.SPAWNMATLABENGINES:
        #test spawnMATLABEngines(pythonPath, pythonScript, startupDir, killFile, maxEngines, matlabfuncs, logfilenames, matlabRunView, debug)

        print('Running unitTest() for '+testId.toStr())        
        command='SPAWNMATLABENGINES'
        maxNumEngines=str(3)
        engineCommands=list()
        engineLogs=list()
        engineCommands.append('why;disp(\'\tEngine#1 is pausing(1)\');pause(1);')
        engineCommands.append('why;disp(\'\tEngine#2 is pausing(1)\');pause(1);')  #eval is run on this string in mcmcBayesUtilsCommandParser
        engineRunView='mcmcBayes.t_runView.Background'#mcmcBayes.t_runView.Foreground
        if OStype==get_os.OsType.WINDOWS:
            engineLogs.append(drive+mcmcBayesPath.replace('\\','/')+'/temp/log1.txt')
            engineLogs.append(drive+mcmcBayesPath.replace('\\','/')+'/temp/log2.txt')
        elif OStype==get_os.OsType.LINUX or OStype==get_os.OsType.MAC:
            engineLogs.append(mcmcBayesPath+'/temp/log1.txt')
            engineLogs.append(mcmcBayesPath+'/temp/log2.txt')
         
        mcmcbayes_utils_command_parser.altParse([command,
                                                 '--debug',
                                                 '--engineStartDir', engineStartDir,
                                                 '--killFile', killFile,
                                                 '--maxNumEngines', maxNumEngines,
                                                 '--engineCommands', engineCommands[0], engineCommands[1],
                                                 '--engineLogs', engineLogs[0], engineLogs[1],
                                                 '--engineRunView', engineRunView])  
        print('unitTest() for '+testId.toStr()+' finished')     
    else:#e.g., pass testId=-1
        #test help

        print('Running unitTest() for help')        
        print('\tTesting $ python mcmcbayes_utils_command_parser.py --help')        
        mcmcbayes_utils_command_parser.altParse(['--help'])
        print('\tTesting $ python mcmcbayes_utils_command_parser.py --help')        
        mcmcbayes_utils_command_parser.altParse(['-h'])
        print('unitTest() for help finished')     