import multiprocessing, os, psutil, sys, subprocess, time
from os_processes import OSProcesses

class WindowsOS(OSProcesses): 
    @staticmethod
    def fork(function, args):#forked child does not return; parent returns pid
        try:
            try:
                multiprocessing.set_start_method('spawn')#only supposed to call this once
            except RuntimeError:
                pass
            p=multiprocessing.Process(target=function,args=args)
            p.start()
            pid=p.pid
            
            message=('Created new child pid=%d, parent returning')  % (pid)
            print(message)
            return(pid)
    
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)    
    
     
    @staticmethod
    def forkIndirect(command, args, proclocatestr):#forked child does not return; parent returns pid
        try:
            tcommand=list()
            tcommand.append(command)
            for arg in args:#append each shellArg in shellArgs list
                if not arg=='':
                    tcommand.append(arg)      
            popen=subprocess.Popen(' '.join(tcommand))#Windows CreateProcess spawns a new process indirectly

            message=('popen pid=%d')  % (popen.pid)
            print(message)
            
            popen.wait()#wait for the initial process to complete creation of its child
            
            pid=OSProcesses.getpid(proclocatestr)

            if not pid==None:
                message=('Created new child pid=%d, parent returning')  % (pid)
            else:
                message=('Either child finished too quickly or was not created')
                print(message)
                raise
            print(message)
            
            return(pid)
    
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            
    @staticmethod
    def poll(pid,timeout):#raises TimeoutError after sleeping (if exists, running) or returns (does not exist, finished)
        #use os.waitpid
        try:
            time.sleep(timeout)
    
            proc=psutil.Process(pid)
    
            if proc.is_running():
                raise(TimeoutError)#still running
            #else, finished, just return
        except TimeoutError:
            raise
        except psutil.NoSuchProcess:
            return #finished, just return
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)  