import os, psutil, subprocess, sys, time
from os_processes import OSProcesses

class PosixOS(OSProcesses):  
    @staticmethod
    def fork(function, args):#forked child does not return; parent returns pid
        try:
            pid=os.fork()
            strargs=''
            for arg in args:
                if not strargs=='':
                    strargs=strargs+', '+str(arg)
                else:
                    strargs=str(arg)
            
            command=('function(%s)') % (strargs)
            if pid==0:#child
                exec(command)
                os._exit(0)
            #parent
            message=('Created new child pid=%d, parent returning')  % (pid)
            print(message)
            return(pid)
    
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)  
            
    @staticmethod
    def forkIndirect(command, args, proclocatestr):#forked child does not return; parent returns pid
        try:
            tcommand=list()
            tcommand.append(command)
            for arg in args:#append each shellArg in shellArgs list
                if not arg=='':
                    tcommand.append(arg)      
            popen=subprocess.Popen(tcommand)

            message=('popen pid=%d')  % (popen.pid)
            print(message)
            
            #popen.wait()#wait for the initial process to complete creation of its child
            
            #pid=OSProcesses.getpid(proclocatestr)
            pid=popen.pid

            message=('Created new child pid=%d, parent returning')  % (pid)
            print(message)
            return(pid)
    
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            
    @staticmethod
    def poll(pid,timeout):#raises TimeoutError after sleeping (if exists, running) or returns (does not exist, finished)
        #use os.waitpid
        try:
            time.sleep(timeout)
    
            proc=psutil.Process(pid)
    
            if proc.is_running():
                if proc.status()=='zombie':
                    return
                else:
                    raise(TimeoutError)#still running
            #else, finished, just return
        except TimeoutError:
            raise
        except psutil.NoSuchProcess:
            return #finished, just return
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)  