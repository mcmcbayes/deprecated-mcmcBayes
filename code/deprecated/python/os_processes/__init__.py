#Created a package for Namespace name conflicts (e.g., Role and Status)
from os_processes.OSProcesses import OSProcesses
#the following have imports that are missing on non-native OS so import within a try/except statement
try:
    from os_processes.PosixOS import PosixOS
except ModuleNotFoundError:
    pass
try:
    from os_processes.WindowsOS import WindowsOS
except ModuleNotFoundError:
    pass