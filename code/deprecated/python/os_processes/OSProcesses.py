import os,psutil,pymsgbox,sys,time
import get_os,os_processes

class OSProcesses(object):
    ##################################################################################################      
    #ABSTRACT METHODS
    ##################################################################################################      
    @staticmethod
    #Traditional fork concept where the current process (parent) creates a new process (child), runs 
    #Python function with args only in the child, and has the parent return the pid of the child.  On 
    #PosixOS, uses os.fork() and on WindowsOS uses multiprocessing.Process (with spawn method).
    def fork(function, args):#forked child does not return; parent returns pid
        try:
            OStype=get_os.osType()
            if OStype==get_os.OsType.WINDOWS:   
                pid=os_processes.WindowsOS.fork(function,args)   
            else:
                pid=os_processes.PosixOS.fork(function,args)
             
            message=('child pid=%d') % (pid)
            print(message)         
            return(pid)
        except SystemExit:#gracefully close out the child
            sys.exit()        
    
    @staticmethod
    #Alternate fork concept where the current process (parent) creates a new process (child), runs 
    #an OS command with args only in the child, and has the parent return the pid of the child.  
    #Uses subprocess.Popen for all instances.
    def forkIndirect(command, args, proclocatestr):#forked child does not return; parent returns pid
        OStype=get_os.osType()
        if OStype==get_os.OsType.WINDOWS:   
            pid=os_processes.WindowsOS.forkIndirect(command,args,proclocatestr)   
        else:
            pid=os_processes.PosixOS.forkIndirect(command,args,proclocatestr)
        message=('child pid=%d') % (pid)
        print(message)         
        return(pid)
    
    @staticmethod
    def poll(pid,timeout):#raises TimeoutError after sleeping (if exists, running) or returns (does not exist, finished)
        try:
            OStype=get_os.osType()
            if OStype==get_os.OsType.WINDOWS:   
                pid=os_processes.WindowsOS.poll(pid,timeout)  
            else:
                pid=os_processes.PosixOS.poll(pid,timeout)
        except:
            raise              
    
    @staticmethod
    def getpid(procname):
        #find the child looking for proclocatestr in cmdline and get its pid (necessary for locating specific spawned child of tcommand)
        try:
            procs = psutil.process_iter()
            #procs = sorted(procs, key=str.lower proc: proc.name)#using lambda function sorts by date
            pid=None
            for proc in procs:
                try:
                    #print(proc)
                    cmdline=proc.cmdline()
                    for arg in cmdline:
                        if procname in arg:
                            pid=proc.pid
                            print(proc.cmdline())
                            message=('found process, %s pid=%d') % (proc,pid)
                            print(message)
                except psutil.AccessDenied:
                    continue
                except SystemError:#Windows 10 raises this on some processes
                    continue
            return(pid)        
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)            
        
    ##################################################################################################      
    #METHODS
    ##################################################################################################      
    @staticmethod
    def unitTest():     
        cZZZ=15   
        cTIMEOUT=1
        cSLEEP=0.5
        try:
            print('Running unitTest() for OSProcesses')      
             
            OStype=get_os.osType()
            if OStype==get_os.OsType.WINDOWS:#use subprocess.Popen
                terminalpath=r'C:\opt\msys64\msys2.exe'
                shellpath=r'C:\opt\msys64\usr\bin\bash.exe'
                windowtitle='\"Spawn demo\"'
                procname='/C/sandbox/mcmcBayes/code/scripts/demo'
                logfile='/C/sandbox/mcmcBayes/logs/demo-log.txt'
                #C:\sandbox\mcmcBayes\code\scripts\startBash.cmd --title "Spawn demo" --msys2 C:\opt\msys64 --mingw64 --log /C/sandbox/mcmcBayes/logs/demo-log.txt --bash C:\opt\msys64\usr\bin\bash.exe --cmd /C/sandbox/mcmcBayes/code/scripts/demo \"Howdy ho^^^!\" 15
                command=r'C:\sandbox\mcmcBayes\code\scripts\startBashWinOs.cmd'
                args=['-r', terminalpath,
                      '-s', shellpath,
                      '-t', windowtitle, 
                      '-l', logfile,                   
                      '-c', procname, 
                      '\'Howdy ho!\'', str(cZZZ)]#args
            else:
                shellpath='/bin/bash'
                windowtitle='\'Spawn demo\''
                procname='/sandbox/mcmcBayes/code/scripts/demo'
                logfile='/sandbox/mcmcBayes/logs/demo-log.txt'
                #//sandbox/mcmcBayes/code/scripts/startBash --title 'Spawn demo' --log /sandbox/mcmcBayes/logs/demo-log.txt --cmd /sandbox/mcmcBayes/code/scripts/demo \'Howdy ho\!\' 15
                if OStype==get_os.OsType.MAC:
                    terminalpath='/Applications/Alacritty.app/Contents/MacOS/alacritty'
                    command=r'/sandbox/mcmcBayes/code/scripts/startBashMacOs'
                else:
                    terminalpath='gnome-terminal'
                    command=r'/sandbox/mcmcBayes/code/scripts/startBashUbuntuOs'
                args=['-r', terminalpath,
                      '-s', shellpath,
                      '-t', windowtitle,
                      '-l', logfile, 
                      '-c', procname, 
                      '\'Howdy ho!\'', str(cZZZ)]#args
            pid=OSProcesses.forkIndirect(command,args,procname)
                  
            running=True
            while running:
                try:
                    OSProcesses.poll(pid,cTIMEOUT)
                    running=False
                except TimeoutError:
                    time.sleep(cSLEEP)
                    print('\tParent waiting...')
                    continue
                  
            message=('Child finished')
            print(message)

            pid=OSProcesses.fork(OSProcesses.demo,['\'Howdy ho!\'',15])
            running=True
            while running:
                try:
                    OSProcesses.poll(pid,cTIMEOUT)
                    running=False
                except TimeoutError:
                    time.sleep(cSLEEP)
                    print('\tParent waiting...')
                    continue
                 
            message=('Child finished')
            print(message)
            
            #maybe os.waitpid?
            
            message=('Parent exiting')
            print(message)
            

            print('unitTest() for OSProcesses finished')
        
        except SystemExit:
            sys.exit()
        
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise           

    @staticmethod
    def demo(message, sleepTime):
        try:
            #pymsgbox.alert(text=message, title='Demo', timeout=sleepTime*1000)
            #tkinter is not thread safe and pymsgbox will crash on MacOS when called from child
            message=('Child is running')
            print(message)
            message=('\tChild sleeping...')
            print(message)
            time.sleep(sleepTime)
            message=('\tChild woke up...')
            print(message)
        
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise