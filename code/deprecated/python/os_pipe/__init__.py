#Created a package for Namespace name conflicts (e.g., Role and Status)
from os_pipe.OSPipe import OSPipe
from os_pipe.OSPipe import Role
from os_pipe.OSPipe import Status
#the following have imports that are missing on non-native OS so import within a try/except statement
try:
    from os_pipe.PosixOS import PosixOS
except ModuleNotFoundError:
    pass
try:
    from os_pipe.WindowsOS import WindowsOS
except ModuleNotFoundError:
    pass