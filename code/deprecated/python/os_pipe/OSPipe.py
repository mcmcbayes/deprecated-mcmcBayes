#!/usr/bin/env python3
#Tested with vanilla Python 3.6
#
#Author: reuben@reubenjohnston.com
# 
#Description: Utility class for named pipes that are used to communicate between 
#  a parent process and its children.  Presently supports both Windows (using pywin32 
#  extensions) and Linux (using the python posix_ipc package).  There should be one
#  LISTENER process and one or more SPEAKER processes.
#
#Requirements: 
#  get_os.py, os_semaphore.py
#  For Windows, install pywin32 extensions.  For Linux, install posix_ipc.
#  Windows install using the command prompt executing with Administrator privileges)
#    C:\> pip3 install pywin32
#  Linux install from bash:
#    $ sudo pip3 install posix_ipc
#
#Usage:  Call the constructor for the appropriate OS you are using and specify the 
#  LISTENER or SPEAKER role and the index for the pipe.  Then, initialize by calling
#  setup() (Note, this is a blocking function).  Once the connections are established,
#  the speaker may write bytes to the fifo by calling speak() and the listener
#  should monitor the fifo using peek() to check for incoming messages and reading
#  bytes from the fifo by calling listen().  Call the teardown() operation to 
#  gracefully close and remove any OS artifacts. 
#
#Unit tests:
#  To run from python shell
#  >>> import os_pipe
#  >>> unitTest()
#  To run in PyDev debugger, make sure you set debugging environment variables 
#    PYDEVD_USE_CYTHON=NO and PYDEVD_USE_FRAME_EVAL=NO.  Create a test program with 
#    the two commands listed above and run it in a debug session.
#
#History:
#  2019-02-11 - Initial version validated by Reub on Ubuntu 18.04 and Windows 6.1.7601

import abc,ctypes,io,json,os,queue,subprocess,sys,threading,time
from enum import IntEnum
from typing import Type
from threading import Event
from io import FileIO

import get_os
from get_os import OsType  
import os_semaphore,os_pipe
from os_semaphore import OSSemaphore

class Role(IntEnum):
    #Enumerated type for the OSPipe roles
    LISTENER=1
    SPEAKER=2
    
    @staticmethod
    def fromStr(strvalue: str) -> "Role":
        #Description: Creates a dictionary of values (Role) and keys (strings that represent their corresponding name).
        #  Uses strvalue as a key into the dict to determine and return its associated Role.
        #Returns: Role associated with value
        
        roles = {}
        roles['LISTENER'] = Role.LISTENER
        roles['SPEAKER']  = Role.SPEAKER
        assert strvalue in roles, "%s not a recognized Role" % strvalue
        return roles[strvalue]
    
    def toStr(self) -> str:
        #Description: Converts the Role to a str
        #Returns: str associated with Role

        if self==Role.LISTENER:
            retVal='LISTENER'
        elif self==Role.SPEAKER:
            retVal='SPEAKER'
        else:
            raise
            
        return(retVal)

class Status(IntEnum):
    UNKNOWN=1
    CREATED=2
    OPENED=3
    CLOSED=4
    DESTROYED=5
    
    @staticmethod   
    def fromStr(strvalue: str) -> "Status":
        #Description: Creates a dictionary of values (Status) and keys (strings that represent their corresponding name).
        #  Uses strvalue as a key into the dict to determine and return its associated Status.
        #Returns: Status associated with value
        
        statuses = {}
        statuses['UNKNOWN'] = Status.UNKNOWN
        statuses['CREATED']  = Status.CREATED
        statuses['OPENED']  = Status.OPENED
        statuses['CLOSED']  = Status.CLOSED
        statuses['DESTROYED']  = Status.DESTROYED
        assert strvalue in statuses, "%s not a recognized Status" % strvalue
        return statuses[strvalue]
    
    def toStr(self) -> str:
        #Description: Converts the Status to a str
        #Returns: str associated with Status

        if self==Status.UNKNOWN:
            retVal='UNKNOWN'
        elif self==Status.CREATED:
            retVal='CREATED'
        elif self==Status.OPENED:
            retVal='OPENED'
        elif self==Status.CLOSED:
            retVal='CLOSED'
        elif self==Status.DESTROYED:
            retVal='DESTROYED'
        else:
            raise
            
        return(retVal)   

class OSPipe(object):
    #Parent class
    cSEMAPHOREMAXVAL=1
    cSEMAPHOREINITIALVAL=cSEMAPHOREMAXVAL
    cSEMAPHORETIMEOUT=5
    cSEMAPHOREBASENAME='accessSemaphoreOSPipe'    
    
    cTESTSLEEPINTERVAL=0.1#s
    
    testReady=Event()#Event that indicates the worker thread should shut itself down
    testTerminate=Event()#Event that indicates the worker thread has started and self-initialized   
    testMsgQueue=queue.Queue() 
    testOSSemaphore=None
       
    ##################################################################################################      
    #ABSTRACT PROPERTIES
    ##################################################################################################      
    @abc.abstractproperty
    def handle(self):
        pass  
    
    @abc.abstractproperty
    def name(self):
        pass
    
    ##################################################################################################      
    #PROPERTIES
    ##################################################################################################      
    @property
    def role(self) -> Type[Role]:
        #role-Role SPEAKER or LISTENER
        return(self.__role)   
    @role.setter
    def role(self, value: Type[Role]):
        if isinstance(value,Role):
            self.__role=value
        else:
            raise TypeError("role must be Role type")
        
    @property
    def index(self) -> int:
        #index-int index of the pipe
        return(self.__index)  
    @index.setter
    def index(self, value: int):
        if isinstance(value,int):
            self.__index=value
        else:
            raise TypeError("index must be int type")      
        
    @property
    def status(self) -> Type[Status]:
        #status-Status for the pipe
        return(self.__status)
    @status.setter
    def status(self, value: Type[Status]):#ignore the undefined variable error in Eclipse
        if isinstance(value,Status):
            self.__status=value
        else:
            raise TypeError("status must be Status type")
           
    @property
    def semaphoreName(self) -> str:
        #semaphoreName-name for the OSSemaphore
        return (self.cSEMAPHOREBASENAME+str(self.index))

    @property
    def osSemaphore(self) -> Type[OSSemaphore]:
        #osSemaphore-OSSemaphore for accessing the OSPipe
        return self.__osSemaphore   
    @osSemaphore.setter
    def osSemaphore(self, value: Type[OSSemaphore]):        
        if isinstance(value,os_semaphore.OSSemaphore):
            self.__osSemaphore=value
        else:
            raise TypeError('osSemaphore must be os_semaphore.OSSemaphore type')
 
    ##################################################################################################      
    #ABSTRACT METHODS
    ##################################################################################################      
    @abc.abstractmethod
    def check(self):
        pass
    
    @abc.abstractmethod
    def checkHandles(self):
        pass

    @abc.abstractmethod
    def peek(self):
        pass
    
    @abc.abstractmethod
    def speak(self):
        pass

    @abc.abstractmethod
    def listen(self):
        pass

    @abc.abstractmethod 
    def _construct(self):
        pass
    
    @abc.abstractmethod
    def _destruct(self):
        pass
    
    @abc.abstractmethod
    def _open(self):
        pass
    
    @abc.abstractmethod 
    def _close(self):
        pass
    
    ##################################################################################################      
    #METHODS
    ##################################################################################################      
    @staticmethod
    def static__init__(role: Type[Role], idx: int) -> "OSPipe":#resolve it later via using string with name
        ostype=get_os.osType()
        if ostype==OsType.LINUX or ostype==OsType.MAC:
            retval=os_pipe.PosixOS(role,idx)
        elif ostype==OsType.WINDOWS:
            retval=os_pipe.WindowsOS(role,idx)
        return retval
            
    @staticmethod
    def unitTest():        
        try:
            print('Running unitTest() for OSPipe')      
            print('\tTest constructor for Role.LISTENER')      
            print('\tTest constructor for Role.SPEAKER')      
            pipeId=1
            pipeListener=OSPipe.static__init__(Role.LISTENER,pipeId)
            pipeSpeaker=OSPipe.static__init__(Role.SPEAKER,pipeId)

            listenerWorker=threading.Thread(target=pipeListener.run, name='listenerWorker', args=[])#create the worker thread
            speakerWorker=threading.Thread(target=pipeSpeaker.run, name='speakerWorker', args=[])#create the worker thread

            listenerWorker.start()
            speakerWorker.start()
            while not (pipeListener.testReady.is_set()):#ensure listener ready first (need to start them both before this because they block on each other)
                time.sleep(1)            
            while not (pipeSpeaker.testReady.is_set()):
                time.sleep(1)                      
                        
            message1='hello reub 123.'
            sermsg1=json.dumps(message1).encode()
            print('\tSpeaker Write to queue: '+message1)
            pipeSpeaker.testMsgQueue.put(sermsg1,block=False,timeout=None)                

            received=False
            while not received:
                if not pipeListener.testMsgQueue.empty():
                    serializedmsg=pipeListener.testMsgQueue.get(block=False,timeout=None)                
                    message2=json.loads(serializedmsg.decode())         
                    if isinstance(message2,str):
                        print('\tListener Read from queue: '+message2)                        
                        received=True
                time.sleep(1)

            if not (message1==message2):
                print('\tError, received message does not equal sent')
            else:
                print('\tReceived message equals sent')

            pipeSpeaker.testTerminate.set()
            pipeListener.testTerminate.set()

            while not (pipeSpeaker.testTerminate.is_set() and pipeListener.testTerminate.is_set()):
                time.sleep(1)            

            listenerWorker.join()
            speakerWorker.join()
            
            print('\tTest teardown() for Role.SPEAKER')      
            pipeSpeaker.teardown()
            print('\tTest teardown() for Role.LISTENER')      
            pipeListener.teardown()

            print('unitTest() for OSPipe finished')
        
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise        

    def run(self):
        #worker thread for unitTest()
        try:
            self.testMsgQueue=queue.Queue(maxsize=0)

            print('\t\t'+self.role.toStr()+' worker is starting')
            self.setup()#blocking                 
            time.sleep(self.cTESTSLEEPINTERVAL)
            print('\t\t'+self.role.toStr()+' worker is running')                                
            
            self.testReady.set()    

            while not self.testTerminate.is_set():
                time.sleep(self.cTESTSLEEPINTERVAL)
                if self.role==Role.LISTENER:
                    msglen=self.peek()
                    if msglen>0:
                        serializedmsg=self.listen(msglen)
                        if not serializedmsg==None:
                            print('\t\t'+self.role.toStr()+' worker writing to queue')                                                  
                            self.testMsgQueue.put(serializedmsg,block=False,timeout=None)
                            print('\t\t'+self.role.toStr()+' worker wrote to queue')                                                  
#TODO: what if there are multiple messages in pipe?
                else:#Role.SPEAKER                
                    if not self.testMsgQueue.empty():#poll queue for available message
                        print('\t\t'+self.role.toStr()+' worker reading from queue')                                                  
                        serializedmsg=self.testMsgQueue.get(block=False,timeout=None)
                        print('\t\t'+self.role.toStr()+' worker read from queue')                                                  
                        self.speak(serializedmsg)#if present, read next from queue and write to pipe
                        
            self.testMsgQueue.queue.clear()#clear out the msgQueue
            
            print('\t\t'+self.role.toStr()+' worker is exiting')
                      
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise         

    def __init__(self, role, idx) -> "OSPipe":
        #Description: initializes newly instantiated OSPipe(role,idx) objects by setting self.Role=setrole, self.index=idx and 
        #  self.status=Status.UNKNOWN
        #Returns: OSPipe object        
        self.role=role
        self.index=idx
        self.status=Status.UNKNOWN
        
        self.testReady.clear()
        self.testTerminate.clear()        
        
        try:
            if role==Role.LISTENER:
                self.osSemaphore=OSSemaphore.static__init__(os_semaphore.Role.CREATOR)
            else:
                self.osSemaphore=OSSemaphore.static__init__(os_semaphore.Role.USER)
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise              

    def setup(self):
        #Description: constructs or opens the named pipe; blocks
        #Returns: None
        try:
            if self.role==Role.LISTENER:
                print('\t\tListener calling semaphore setup()')
                self.osSemaphore.setup(self.semaphoreName,self.cSEMAPHOREINITIALVAL,self.cSEMAPHOREMAXVAL)
                print('\t\tListener calling pipe construct()')
                self._construct()
                while self.check()==False:#wait for speaker to connect
                    time.sleep(1)                
                print('\t\tListener calling pipe open()')
                self._open()#blocks
            else:
                print('\t\tSpeaker calling semaphore setup()')
                self.osSemaphore.setup(self.semaphoreName)
                
                print('\t\tSpeaker calling pipe open()')
                while not self._open():#wait for listener to construct
                    time.sleep(1)                 
                print('\t\tSpeaker waiting for listener to connect()')
                while self.check()==False:#wait for listener to connect
                    time.sleep(1)
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise
    
    def teardown(self):
        #Description: destructs or closes the named pipe
        #Returns: None
        try:
            if self.role==Role.LISTENER:
                print('\t\tListener closing pipe')
                self._close()
#TODO: should the listener block waiting for all speakers to disconnect?
                print('\t\tListener destroying pipe')
                self._destruct()  
            else:
                print('\t\tSpeaker closing pipe')
                self._close()
                print('\t\tSpeaker destroying pipe')
                self._destruct()
            self.osSemaphore.teardown()
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise
                                    
##################################################################################################      
    
