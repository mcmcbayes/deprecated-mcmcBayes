#!/usr/bin/env python3
#Tested with vanilla Python 3.6
#
#Author: reuben@reubenjohnston.com
# 
#Description: Defines messages passed between the mcmcBayes MATLAB engine monitor and the 
#  individual MATLAB engine workers.  Message content is the pickled and serialized form of the 
#  EngineCommsMessage class instance and encoding and decoding of its content is performed 
#  using jsonpickle.  Elements in the message include:    
#  * maxEngines-the maximum number of MATLAB engines that can run simultaneously
#  * pid-is the id for the MATLAB engine's spawned process
#  * engineIndex-is the index for the MATLAB engine (zero to maxEngines-1)
#  * engineStatus-is the status of the MATLAB engine (integer form of enumerated tengineStatus type)
#
#Requirements: 
#  For Windows, install jsonpickle extensions.  For Linux, install jsonpickle.
#  Windows install using the command prompt executing with Administrator privileges)
#    C:\> pip3 install jsonpickle
#  Linux install from bash:
#    $ sudo pip3 install jsonpickle
#
#Usage:
#  See the unit tests below or spawnMATLABEngines() and runMATLABEngine() in mcmcbayes_utils.py
# 
#Unit tests:
#  To run from python shell
#  >>> import engine_comms_messaging
#  >>> engine_comms_messaging.unitTest()
#  To run in PyDev debugger, make sure you set debugging environment variables PYDEVD_USE_CYTHON=NO and 
#    PYDEVD_USE_FRAME_EVAL=NO.  Create a test program with the two commands listed above and run it in a debug session.
#
#History:
#  2019-02-11 - Initial version validated by Reub on Ubuntu 18.04 and Windows 6.1.7601

from enum import IntEnum
   
class EngineCommsMessage(object):       
    ##################################################################################################      
    #PROPERTIES
    ##################################################################################################      
    #class properties (with getter/setters)
    @property
    def pid(self):
        return self.__pid
    @pid.setter
    def pid(self, value):
        if isinstance(value, int):
            self.__pid=value
        else:
            raise TypeError("pid must be int type")
    
    @property
    def engineIndex(self):
        return self.__engineIndex
    @engineIndex.setter
    def engineIndex(self, value):
        if isinstance(value, int):
            self.__engineIndex=value
        else:
            raise TypeError("engineIndex must be int type")

    @property
    def engineStatus(self):
        return self.__engineStatus   
    @engineStatus.setter
    def engineStatus(self, value):
        if isinstance(value, EngineStatus):
            self.__engineStatus=value
        else:
            raise TypeError("engineStatus must be EngineStatus type")
        
    ##################################################################################################      
    #METHODS
    ##################################################################################################      
    @staticmethod
    def unitTest():
        pid=1
        engineIndex=0
        engineStatus=EngineStatus.NULL
        
        print('Running unitTest() for EngineCommsMessage')
        tMsg1=EngineCommsMessage.new(pid,engineIndex,engineStatus)
        print('\tTest encode()')
        temp=tMsg1.encode()
        print('\tTest decode()')
        tMsg2=EngineCommsMessage.decode(temp)
        if not (tMsg1==tMsg2):
            print('\tError, decoded result does not equal encoded input')
        else:
            print('\tDecoded result equals encoded input')
        print('unitTest() for EngineCommsMessage finished')
    
    @staticmethod
    def new(pid, engineIndex, engineStatus):
        retObj=EngineCommsMessage()
        retObj.pid=pid
        retObj.engineIndex=engineIndex
        retObj.engineStatus=engineStatus
        
        return(retObj)
        
    def __eq__(self,other):
        #overload the == operator
        if isinstance(other,type(None)):
            return False
        else:
            return (self.pid==other.pid) and \
                   (self.engineIndex==other.engineIndex) and \
                   (self.engineStatus==other.engineStatus)
               
    def toStr(self):
        retVal=('EngineCommsMessage::pid='+str(self.pid)+', engineIndex='+str(self.engineIndex)+
                ', engineStatus='+self.engineStatus.toStr())
        return(retVal)
    
    @staticmethod 
    def decode(serializedmsg):
        #decodes the JSON formatted packedmsg into an engineCommsMessage object
        import jsonpickle,sys,os,base64
        
        try:
            packedmsg=base64.decodestring(serializedmsg)
            retEngineCommsMessage = jsonpickle.decode(packedmsg)
            #print(serializedmsg,' decoded into: ',retEngineCommsMessage.toStr())#debug only
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise
        return(retEngineCommsMessage)
                   
    def encode(self):
        import jsonpickle,sys,os,base64
        
        packedmsg=None
        try:
            serializedmsg=jsonpickle.encode(self)
            packedmsg=base64.encodestring(serializedmsg.encode())
            #print(self.toStr(),' encoded into: ',packedmsg)#debug only
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise
        return(packedmsg)    
    
class EngineStatus(IntEnum):
    #Enumerated type for the mcmcBayesComms connectionStatus
    NULL=0 #not created
    WAITING=1#waiting for semaphore to create MATLAB engine
    CREATED=2 #MATLAB engine is created
    RUNNING=3 #MATLAB engine is running command
    COMPLETED=4 #MATLAB engine completed command
    SHUTDOWN=5 #MATLAB engine was shutdown after completing
    KILLED=6 #poison file detected and MATLAB engine was killed
    ERROR=7 #error occurred and MATLAB engine was shutdown

    @staticmethod   
    def fromStr(value):
        #Creates a dictionary of values (CommandType) and keys (strings that represent their corresponding name).
        #Uses strvalue as a key into the dict to determine and return its associated CommandType.
        statuses = {}
        statuses['NULL'] = EngineStatus.NULL
        statuses['WAITING']  = EngineStatus.WAITING
        statuses['CREATED']  = EngineStatus.CREATED
        statuses['RUNNING']  = EngineStatus.RUNNING
        statuses['COMPLETED']  = EngineStatus.COMPLETED
        statuses['SHUTDOWN']  = EngineStatus.SHUTDOWN
        statuses['KILLED']  = EngineStatus.KILLED
        statuses['ERROR']  = EngineStatus.ERROR
        assert value in statuses, "%s not a recognized EngineStatus" % value
        return statuses[value]
    
    def toStr(self):
        if self.value==EngineStatus.NULL:
            retVal='NULL'
        elif self.value==EngineStatus.WAITING:
            retVal='WAITING'
        elif self.value==EngineStatus.CREATED:
            retVal='CREATED'
        elif self.value==EngineStatus.RUNNING:
            retVal='RUNNING'
        elif self.value==EngineStatus.COMPLETED:
            retVal='COMPLETED'      
        elif self.value==EngineStatus.SHUTDOWN:
            retVal='SHUTDOWN' 
        elif self.value==EngineStatus.KILLED:
            retVal='KILLED' 
        elif self.value==EngineStatus.ERROR:
            retVal='ERROR' 
        else:
            raise ValueError
        
        return(retVal)
