#!/usr/bin/env python3
#Tested with vanilla Python 3.6
#
#Author: reuben@reubenjohnston.com
# 
#Description: Interface module to call mcmcBayesUtils module's functions from OS command line
#
#Requirements: 
#  None
#
#Usage:
#  $ python mcmcBayes_utils_command_parser.py --help
#
#Unit tests:
#  Tested indirectly using the unit tests in mcmcbayes_utils.py
#
#History:
#  2019-02-11 - Initial version validated by Reub on Ubuntu 18.04 and Windows 6.1.7601

import multiprocessing
from enum import IntEnum

class CommandType(IntEnum):
    #Enumerated type for all the mcmcBayes commands
    #This needs to be located before main()
    RUNBASHCMD=1
    SPAWNBASHCMD=2
    UPDATEBASHCMDSTATUS=3
    RUNMATLABENGINE=21
    SPAWNMATLABENGINES=22
    UPDATEMATLABENGINESSTATUS=23
    SETKILLFILE=31
    CLEARKILLFILE=32
    CHECKKILLFILE=33
    KILLPID=41
    KILLPIDS=42
    KILLNAMEDPROC=43

    @staticmethod
    def fromStr(strvalue):
        #Creates a dictionary of values (CommandType) and keys (strings that represent their corresponding name).
        #Uses strvalue as a key into the dict to determine and return its associated CommandType.
        cmds = {}
        cmds['RUNBASHCMD'] = CommandType.RUNBASHCMD
        cmds['SPAWNBASHCMD'] = CommandType.SPAWNBASHCMD
        cmds['UPDATEBASHCMDSTATUS'] = CommandType.UPDATEBASHCMDSTATUS
        cmds['RUNMATLABENGINE'] = CommandType.RUNMATLABENGINE
        cmds['SPAWNMATLABENGINES'] = CommandType.SPAWNMATLABENGINES
        cmds['UPDATEMATLABENGINESSTATUS'] = CommandType.UPDATEMATLABENGINESSTATUS
        cmds['SETKILLFILE'] = CommandType.SETKILLFILE
        cmds['CLEARKILLFILE'] = CommandType.CLEARKILLFILE
        cmds['CHECKKILLFILE'] = CommandType.CHECKKILLFILE
        cmds['KILLPID']  = CommandType.KILLPID
        cmds['KILLPIDS'] = CommandType.KILLPIDS
        cmds['KILLNAMEDPROC'] = CommandType.KILLNAMEDPROC
        assert strvalue in cmds, "%s not a recognized mcmcbayes_utils_command_parser.CommandType" % strvalue
        return cmds[strvalue]
    
    def toStr(self):
        #Description: Converts the CommandType to a str
        #Returns: str associated with CommandType

        if self==CommandType.RUNBASHCMD:
            retVal='RUNBASHCMD'
        elif self==CommandType.SPAWNBASHCMD:
            retVal='SPAWNBASHCMD'
        elif self==CommandType.UPDATEBASHCMDSTATUS:
            retVal='UPDATEBASHCMDSTATUS'
        elif self==CommandType.RUNMATLABENGINE:
            retVal='RUNMATLABENGINE'
        elif self==CommandType.SPAWNMATLABENGINES:
            retVal='SPAWNMATLABENGINES'
        elif self==CommandType.UPDATEMATLABENGINESSTATUS:
            retVal='UPDATEMATLABENGINESSTATUS'
        elif self==CommandType.SETKILLFILE:
            retVal='SETKILLFILE'
        elif self==CommandType.CLEARKILLFILE:
            retVal='CLEARKILLFILE'
        elif self==CommandType.CHECKKILLFILE:
            retVal='CHECKKILLFILE'
        elif self==CommandType.KILLPID:
            retVal='KILLPID'
        elif self==CommandType.KILLPIDS:
            retVal='KILLPIDS'
        elif self==CommandType.KILLNAMEDPROC:
            retVal='KILLNAMEDPROC'
        else:
            raise
            
        return(retVal)     

def altParse(argsDict):
    import os,sys
    
    sys.argv.clear()
    
    sys.argv.append('mcmcbayes_utils_command_parser.py')
    for idx,arg in enumerate(argsDict):
        sys.argv.insert(idx+1,arg)
    #message=('Executing: mcmcbayes_utils_command_parser.main with argv=%s') % (sys.argv)
    #print(message)
    parse()
    
def parse():             
    import argparse,sys,os,time
    import mcmcbayes_utils,get_os
    from os_pipe import Role
   
    try:
        OStype=get_os.osType()#check the OS type and instantiate the appropriate OSPipe    

        #Implement an ArgumentParser for these commands:
        descStr="""Interface module to call mcmcBayesUtils module's functions from OS command line"""
                
        parser=argparse.ArgumentParser(description=descStr,formatter_class=argparse.RawTextHelpFormatter,add_help=False)
        #Notes:
        #  nargs='?' one optional argument will be consumed from the command line (if present)
        #  nargs='+' gathers arguments up to the next -- separator into a list (also requires at least one argument)        
        #  Don't have a list (e.g., engineCommands) or optional argument (e.g., debug) right before positional arguments (i.e., 
        #    commandtype)
        parser.add_argument('commandtype', nargs='?', type=str, help='identifies the command to execute, must be type '+
                            'CommandType (RUNBASHCMD, SPAWNBASHCMD, UPDATEBASHCMDSTATUS, RUNMATLABENGINE, SPAWNMATLABENGINES, '+
                            'UPDATEMATLABENGINESSTATUS, SETKILLFILE, CLEARKILLFILE, CHECKKILLFILE, KILLPID, KILLPIDS, KILLNAMEDPROC, )')     

        parser.add_argument('--msgFile', nargs='?', type=str, help='provide the full file-system path location for '+
                            'UUIDMsgFile (i.e., messaging interface between MATLAB/python).')
        parser.add_argument('--help', '-h', action='store_true', help='show this help message and exit')
        parser.add_argument('--debug', action='store_true', help='specify this for extra debugging messages to the console')
        parser.add_argument('--killFile', nargs='?', type=str, help='provide the full file-system path location for the killFile')
        parser.add_argument('--terminal', nargs='?', type=str, help='provide the full file-system path location for terminal')
        parser.add_argument('--shellStartScript', nargs='?', type=str, help='provide the full file-system path location for script to start bash')
        parser.add_argument('--shell', nargs='?', type=str, help='provide the full file-system path location for bash shell')
        parser.add_argument('--shellTitle', nargs='?', type=str, help='provide the command window name for bash commands')
        parser.add_argument('--shellLog', nargs='?', type=str, help='provide the full file-system path location for the '+
                            'log file to create') 
        parser.add_argument('--shellRunView', nargs='?', type=str, help='specifies to run either in the foreground or background', 
                            default='mcmcBayes.t_runView.Foreground')
        parser.add_argument('--shellCmd', nargs='?', type=str, help='provide the command to run in bash')
        parser.add_argument('--shellArgs', nargs='+', type=str, help='list of shell arguments') #list
        parser.add_argument('--engineNoOSComms', action='store_true', help='specify this when testing RUNMATLABENGINE() directly '+
                            '(i.e., not indirectly via SPAWNMATLABENGINES)')   
        parser.add_argument('--engineStartDir', nargs='?', type=str, help='provide the startup directory for the MATLAB engines to use')
        parser.add_argument('--engineMaxNum', nargs='?', type=int, help='provide the maximum number of MATLAB engines that can run '+
                            'simultaneously', default=1)
        parser.add_argument('--engineRunView', nargs='?', type=str, help='specifies to run the MATLAB engine command window either '+
                            'foreground or background', default='mcmcBayes.t_runView.Foreground')
        parser.add_argument('--engineIndex', nargs='?', type=int, help='provide the index for the MATLAB engine in runMATLABEngine()')     
        parser.add_argument('--engineCommands', nargs='+', type=str, help='list of matlab command line functions to execute in '+
                            'SPAWNMATLABENGINES') #list
        parser.add_argument('--engineLogs', nargs='+', type=str, help='list of full-path log filenames for capturing the output '+
                            'from each MATLAB engine created by SPAWNMATLABENGINES')               
        parser.add_argument('--processPID', nargs='?', type=int, help='provide the operating system process identifier (PID)')
        parser.add_argument('--processParentPID', nargs='?', type=int, help='provide the operating system process identifier '+
                            '(PID) of the parent process')
        parser.add_argument('--processName', nargs='?', type=str, help='provide the operating system process name')    
        
        try:
            print('ArgumentParser.parse_args() argv='+','.join(sys.argv))
            args=parser.parse_args()
        except SystemExit:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            message=('ArgumentParser.parse_args() error, argv=%s') %' '.join(sys.argv)
            print(message)
            message=('%s,%s,%s') % (exc_type, filename, exc_tb.tb_lineno)
            print(message)
            time.sleep(10)
            UsageAndExit(parser)
        
        if args.help:
            UsageAndExit(parser)
        
        if not args.commandtype==None:
            commandtypestr=args.commandtype
            commandtype=CommandType.fromStr(commandtypestr)            
        else:
            UsageAndExit(parser)
        
        debug=args.debug
        if debug:
            import pydevd
            print('Translating args to variables')
        if not args.msgFile==None:
            msgFile=args.msgFile            
        if not args.killFile==None:
            killFile=args.killFile
        if not args.terminal==None:
            terminal=args.terminal 
        if not args.shellStartScript==None:
            shellStartScript=args.shellStartScript        
        if not args.shell==None:
            shell=args.shell            
        if not args.shellTitle==None:
            shellTitle=args.shellTitle            
        if not args.shellLog==None:
            shellLog=args.shellLog
        shellRunView=args.shellRunView
        if not args.shellCmd==None:
            shellCmd=args.shellCmd   
        if not args.shellArgs==None: #list
            shellArgs=args.shellArgs                                   
        engineNoOSComms=args.engineNoOSComms
        if not args.engineStartDir==None:
            engineStartDir=args.engineStartDir
        engineMaxNum=args.engineMaxNum
        engineRunView=args.engineRunView
        if not args.engineIndex==None:
            engineIndex=args.engineIndex
        if not args.engineCommands==None: #list
            engineCommands=args.engineCommands
        if not args.engineLogs==None: #list
            engineLogs=args.engineLogs
        if not args.processPID==None:
            processPID=args.processPID
        if not args.processParentPID==None:
            processParentPID=args.processParentPID
        if not args.processName==None:
            processName=args.processName    
        
        if debug:
            message=('Calling specified commandtype=%s') % (commandtypestr)
            print(message)
        if commandtype==CommandType.RUNBASHCMD or commandtype==CommandType.SPAWNBASHCMD:
            if not 'terminal' in locals() and \
               not 'shellStartScript' in locals() and \
               not 'shell' in locals() and \
               not 'shellCmd' in locals() and \
               not 'shellCmdRunView' in locals() and \
               not 'killFile' in locals() and \
               not 'msgFile' in locals() and \
               not 'debug' in locals():
                print('Incorrect arguments for RUNBASHCMD or SPAWNBASHCMD')
                UsageAndExit(parser)
            if not 'shellArgs' in locals():
                shellArgs=list()
            startShellArgs=list()
            startShellArgs.append('-r')
            startShellArgs.append(terminal)
            startShellArgs.append('-s')
            startShellArgs.append(shell)
            if 'shellTitle' in locals():
                startShellArgs.append('-t')
                startShellArgs.append(shellTitle)
            if 'shellLog' in locals():
                startShellArgs.append('-l')
                startShellArgs.append(shellLog) 
            if shellRunView=='mcmcBayes.t_runView.Background':
                startShellArgs.append('-b')
            if commandtype==CommandType.RUNBASHCMD:
                mcmcbayes_utils.runBashCommand(debug, msgFile, shellStartScript, startShellArgs, shellCmd, shellArgs)
            else:
                mcmcbayes_utils.spawnBashCommand(debug, msgFile, shellStartScript, startShellArgs, shellCmd, shellArgs)
        elif commandtype==CommandType.UPDATEBASHCMDSTATUS:
            if not 'msgFile' in locals() and \
               not 'debug' in locals() and \
               not 'processPID' in locals():
                print('Incorrect arguments for UPDATEBASHCMDSTATUS')
                UsageAndExit(parser)
            mcmcbayes_utils.updateBashCommandStatus(debug, msgFile, processPID)            
        elif commandtype==CommandType.RUNMATLABENGINE:
            #this should only be called as a worker function from SPAWNMATLABENGINES
            if not 'engineStartDir' in locals() and \
               not 'killFile' in locals() and \
               not 'maxNumEngines' in locals() and \
               not 'engineCommand' in locals() and \
               not 'engineRunView' in locals() and \
               not 'engineIndex' in locals() and \
               not 'notOSComms' in locals() and \
               not 'debug' in locals():
                print('Incorrect arguments for RUNMATLABENGINE')
                UsageAndExit(parser)                
            engineCommand=engineCommands[0]#convert the list element into a string (executes only one function in MATLAB)
            mcmcbayes_utils.runMATLABEngine(engineStartDir, killFile, maxNumEngines, engineCommand, engineRunView, engineIndex, 
                    notOSComms, debug)
        elif commandtype==CommandType.SPAWNMATLABENGINES:
            if not 'engineStartDir' in locals() and \
               not 'killFile' in locals() and \
               not 'maxNumEngines' in locals() and \
               not 'engineCommands' in locals() and \
               not 'engineLogs' in locals() and \
               not 'engineRunView' in locals() and \
               not 'debug' in locals():
                print('Incorrect arguments for SPAWNMATLABENGINES')
                UsageAndExit(parser)                                 
            mcmcbayes_utils.spawnMATLABEngines(engineStartDir, killFile, maxNumEngines, engineCommands, engineLogs, engineRunView, debug)            
        elif commandtype==CommandType.UPDATEMATLABENGINESSTATUS:
            UsageAndExit(parser)
        elif commandtype==CommandType.SETKILLFILE:
            UsageAndExit(parser)            
        elif commandtype==CommandType.CLEARKILLFILE:
            UsageAndExit(parser)           
        elif commandtype==CommandType.CHECKKILLFILE:
            if not 'killFile' in locals():
                print('Incorrect arguments for CHECKKILLFILE')
                UsageAndExit(parser)
            mcmcbayes_utils.checkKillFile(killFile)
        elif commandtype==CommandType.KILLPID:
            if not 'PID' in locals():
                print('Incorrect arguments for KILLPID')
                UsageAndExit(parser)
            mcmcbayes_utils.killPID(PID)
        elif commandtype==CommandType.KILLPIDS:
            if not 'parentPID' in locals():
                print('Incorrect arguments for KILLPARENTPID')
                UsageAndExit(parser)
            mcmcbayes_utils.killPIDs(parentPID)
        elif commandtype==CommandType.KILLNAMEDPROC:
            if not 'processName' in locals():
                print('Incorrect arguments for killProcName')
                UsageAndExit(parser)
            mcmcbayes_utils.killNamedProc(processName)                


        else:
            UsageAndExit(parser)
    except SystemExit:
        pass#UsageAndExit calls sys.exit(), so ignore this exception in the try/except block to quit
    except:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)

def UsageAndExit(parser):
    import sys
    
    parser.print_help()
    
    sys.exit()

if __name__=='__main__':
    parse()