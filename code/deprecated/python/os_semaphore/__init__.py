#Created a package for Namespace name conflicts (e.g., Role and Status)
from os_semaphore.OSSemaphore import OSSemaphore
from os_semaphore.OSSemaphore import Role
from os_semaphore.OSSemaphore import Status
#the following have imports that are missing on non-native OS so import within a try/except statement
try:
    from os_semaphore.PosixOS import PosixOS
except ModuleNotFoundError:
    pass
try:
    from os_semaphore.WindowsOS import WindowsOS
except ModuleNotFoundError:
    pass

