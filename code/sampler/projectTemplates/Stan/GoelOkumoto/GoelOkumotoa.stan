//Author: reuben@reubenjohnston.com
//Description: NHPP with the Goel-Okumoto cumulative intensity function (gamma priors for u and zeta)
//References: Goel-Okumoto, 1979
// $ $cmdStanHome/bin/stanc.exe --name=modeltypeid4 --o=$mcmcBayesHome/code/sampler/projectTemplates/Stan/modeltypeid4/modeltypeid4.hpp $mcmcBayesHome/code/sampler/projectTemplates/Stan/modeltypeid4/modeltypeid4.stan
// $ make $mcmcBayesHome/code/sampler/projectTemplates/Stan/modeltypeid4/modeltypeid4.exe
// $ $mcmcBayesHome/code/sampler/projectTemplates/Stan/modeltypeid4/modeltypeid4.exe sample data file=modeltypeid4.data.R init=modeltypeid4.init.R output file=modeltypeid4.output.csv
// $ $cmdStanHome/bin/stansummary.exe modeltypeid4.output.csv
functions { 
	real nhpp_cumulativeintensity(real u, real zeta, real t) {
		return u-u*exp(-zeta*t);	//Model assumes a goel-okumoto nhpp cumulative intensity function, or u*(1-exp(-zeta*t))
	}

	// Define log likelihood function for temperature parametere phi (Note: phi=0 generates the prior's loglikelihood, phi=1 generates the same for the posterior, and the corresponding temperature values in between are generated by the temperature step index, the discretization or k number of steps, and the choice for the temperature parameter c) 
	real custnhpp_log(int[] N, real[] theta, real phi) { 
		real retval=0;
		int Taux=size(N)-1;		
		for (i in 1:Taux) {
			retval=retval-log_rising_factorial(1,N[i+1]-N[i])+(N[i+1]-N[i])*log(theta[i+1]-theta[i])-(theta[i+1]-theta[i]);
		}
		return phi*retval;
	}
}
data {
	real<lower=0, upper=1> phi;
	int<lower=0> Rho;
	int<lower=0> Tau;
	vector[Tau] t[1];
	int N[Rho,Tau];
	real<lower=0> a;
	real<lower=0> b;
	real<lower=0> c;
	real<lower=0> d;
}
parameters {
	real<lower=0> u;
	real<lower=0> zeta;
}
model {
	real r;
 	real theta[Tau];
	u ~ gamma(a, b);
	zeta ~ gamma(c, d);	
	theta[1]=0;
	for (i in 2:Tau) {
		r=nhpp_cumulativeintensity(u,zeta,t[1,i])-nhpp_cumulativeintensity(u,zeta,t[1,i-1]);
		theta[i]=r+theta[i-1];
	}
	if (phi>0) {//we don't need the loglikelihood for the prior step
		for (i in 1:Rho) {
			N[i,:]~custnhpp(theta, phi);
		}
	}
}
