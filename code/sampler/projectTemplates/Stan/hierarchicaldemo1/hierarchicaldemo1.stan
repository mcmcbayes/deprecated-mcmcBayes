//Author: 
//Description: example 2-failure mode hierarchical system demo
//References:
//Comments:

//To manually inspect results using Rstan
//> library("rstan")
//> setwd("pathToSamples.txt")
//> fit <- read_stan_csv("samples.txt", col_major = TRUE)
//> print(fit)

data {
	real<lower=0, upper=1> phi;
//FM1(log-normal)
//Constants
	int<lower=0> fm1Rho;//number of fm1 datasets
	int<lower=0> fm1Tau;//number of fm1 datapoints
	int<lower=0> fm1Upsilon;//number of components influenced by fm1
//Data
	int fm1Comp[fm1Tau,fm1Rho];//independent data variable
	real fm1zlogt[fm1Tau,fm1Rho];//dependent data variable 
	real fm1logt_mean;
	real fm1logt_std;
//Hyperparameters
	real fm1zmpa[fm1Upsilon,1];
	real fm1zmpb[fm1Upsilon,fm1Upsilon];
	real<lower=0> fm1za;
	real<lower=0> fm1zb;

//FM2(log-normal)
//Constants
	int<lower=0> fm2Rho;//number of fm2 datasets
	int<lower=0> fm2Tau;//number of fm2 datapoints
	int<lower=0> fm2Upsilon;//number of components influenced by fm2
//Data
	int fm2Comp[fm2Tau,fm2Rho];//independent data variable
	real fm2zlogt[fm2Tau,fm2Rho];//dependent data variable 
	real fm2logt_mean;
	real fm2logt_std;
//Hyperparameters
	real fm2zmpa[fm2Upsilon,1];
	real fm2zmpb[fm2Upsilon,fm2Upsilon];
	real<lower=0> fm2za;
	real<lower=0> fm2zb;

//System
//Constants
	int<lower=0> sysRho;//number of system-level datasets
	int<lower=0> sysTau;//number of system-level datapoints
	int<lower=0> sysUpsilon;//number of system components
	int numFm;
	int sysFmMap[sysUpsilon,numFm];
//Data
	real syslogt[sysTau,sysRho];
//Hyperparameters
	real<lower=0> sysa;
	real<lower=0> sysb;
}

transformed data {
	print("fm1Rho=", fm1Rho);
	print("fm1Tau=", fm1Tau);
	print("fm1Upsilon=", fm1Upsilon);

	print("fm1Comp=", fm1Comp);
	print("fm1zlogt=", fm1zlogt);
	print("fm1logt_mean=", fm1logt_mean);
	print("fm1logt_std=", fm1logt_std);
	
	print("fm2Rho=", fm2Rho);
	print("fm2Tau=", fm2Tau);
	print("fm2Upsilon=", fm2Upsilon);
	
	print("fm2Comp=", fm2Comp);
	print("fm2zlogt=", fm2zlogt);
	print("fm2logt_mean=", fm2logt_mean);
	print("fm2logt_std=", fm2logt_std);

  print("syslogt=", syslogt);

	print("fm1zmpa=", fm1zmpa, "fm1zmpb=", fm1zmpb);
	print("fm1za=", fm1za, "fm1zb=", fm1zb);
	print("fm2zmpa=", fm2zmpa, "fm2zmpb=", fm2zmpb);
	print("fm2za=", fm2za, "fm2zb=", fm2zb);
	print("sysa=", sysa, "sysb=", sysb);
	
	print("sysFmMap=",sysFmMap);
}

parameters {
//FM1(log-normal)
	real fm1zm[fm1Upsilon,1];
	real fm1zSigma;

//FM2(log-normal)
	real fm2zm[fm2Upsilon,1];
	real fm2zSigma;

//System
	real<lower=0> sysSigma;
}

transformed parameters {
	real fm1predlogTf[fm1Upsilon,1];
	real fm2predlogTf[fm2Upsilon,1];
	real sysCompMinTf[sysUpsilon,1];
	real syslogmu;
	real sysmu;
	
//FM1(log-normal)
	real fm1m[fm1Upsilon,1];
	real fm1Sigma;
//FM2(log-normal)
	real fm2m[fm2Upsilon,1];
	real fm2Sigma;

  for (i in 1:fm1Upsilon) {
    fm1m[i,1]=fm1zm[i,1]*fm1logt_std+fm1logt_mean;
  }
  fm1Sigma=fm1zSigma*fm1logt_std;

//PREDICTIONS,START
	for (i in 1:fm1Upsilon) {
		fm1predlogTf[i,1] = fm1m[i,1] + fm1Sigma;
	}	
//PREDICTIONS,STOP

  for (i in 1:fm2Upsilon) {
    fm2m[i,1]=fm2zm[i,1]*fm2logt_std+fm2logt_mean;
  }
  fm2Sigma=fm2zSigma*fm2logt_std;

//PREDICTIONS,START
	for (i in 1:fm2Upsilon) {
		fm2predlogTf[i,1] = fm2m[i,1] + fm2Sigma;
	}	
//PREDICTIONS,STOP

//PRIORS,SYSTEM (simple series component system)
  //Get the minimum failure time for each component across all the failure modes
  for (i in 1:sysUpsilon) {
    //for the ith component, get its minimum failure time across all the failure modes
	  sysCompMinTf[i,1]=positive_infinity();
    for (j in 1:numFm) {
  		if (sysFmMap[i,j]>0) {
  			if (j==1) {
  				if (fm1predlogTf[sysFmMap[i,j],1]<sysCompMinTf[i,1])
  					sysCompMinTf[i,1]=fm1predlogTf[sysFmMap[i,j],1];
  			}
  			else if (j==2) {
  				if (fm2predlogTf[sysFmMap[i,j],1]<sysCompMinTf[i,1])
  					sysCompMinTf[i,1]=fm2predlogTf[sysFmMap[i,j],1];
  			}
  		}
    }
  }

  //print("sysCompMinTf=", sysCompMinTf[,]);
  //todo: if there is no data for the lower-level architecture, need to create a preset prior for sysCompMinTf using a lognormal distribution
  syslogmu = min(sysCompMinTf[:,1]);
//PRIORS,STOP
  sysmu = 10^syslogmu;
}

model {
//FM1(log-normal)
	real fm1zlogmu[fm1Tau,1];

//FM2(log-normal)
	real fm2zlogmu[fm2Tau,1];

//System

//PRIORS,FM1(log-normal)
	for (i in 1:fm1Upsilon) {
		fm1zm[i,1] ~ normal(fm1zmpa[i,1], fm1zmpb[i,i]);
	}
	fm1zSigma ~ uniform(fm1za, fm1zb);
//PRIORS,STOP

//LIKELIHOOD,START
	if (phi!=0) {
		for (i in 1:fm1Tau) {
			fm1zlogt[i,1] ~ normal(fm1zm[fm1Comp[i,1],1], fm1zSigma);//Stan uses normal(mu,sigma)
		}
	}
//LIKELIHOOD,STOP

//PRIORS,FM2(log-normal)
	for (i in 1:fm2Upsilon) {
		fm2zm[i,1] ~ normal(fm2zmpa[i,1], fm2zmpb[i,i]);
	}
	fm2zSigma ~ uniform(fm2za, fm2zb);
//PRIORS,STOP

//LIKELIHOOD,START
	if (phi!=0) {
		for (i in 1:fm2Tau) {				
			fm2zlogt[i,1] ~ normal(fm2zm[fm2Comp[i,1],1], fm2zSigma);//Stan uses normal(mu,sigma)
		}
	}
//LIKELIHOOD,STOP

//PRIORS,SYSTEM (simple series component system)
  sysSigma ~ uniform(sysa, sysb);
  //print("sysa=", sysa,", sysb=", sysb, ", sysSigma=", sysSigma);
//PRIORS,STOP
  
  //Likelihood System (also incorporates dependent structure of the system)
  for (i in 1:sysTau) {
    //todo: if there is no system level data, syslogt = syslogmu
	  //print("syslogmu=", syslogmu, ", sysSigma=", sysSigma);
	  syslogt[i,1] ~ normal(syslogmu, sysSigma);//Stan uses normal(mu,sigma)
  }
}
