//Author: reuben@reubenjohnston.com
//Description: Linear regression (flat priors for betas and r)
//References: Murphy, 2007 (section 6)
//Comments: Do not confuse the precision of the normal prior distributions (b and d) with the precision of the observation noise, r.
//  Remember, the precision is the inverse of the variance (i.e., larger precisions are associated with smaller variance and vice versa).
// $ $cmdStanHome/bin/stanc.exe --name=modeltypeid1 --o=$mcmcBayesHome/code/sampler/projectTemplates/Stan/modeltypeid1/modeltypeid1.hpp $mcmcBayesHome/code/sampler/projectTemplates/Stan/modeltypeid1/modeltypeid1.stan
// $ make $mcmcBayesHome/code/sampler/projectTemplates/Stan/modeltypeid1/modeltypeid1.exe
// $ $mcmcBayesHome/code/sampler/projectTemplates/Stan/modeltypeid1/modeltypeid1.exe sample data file=modeltypeid1.data.R init=modeltypeid1.init.R output file=modeltypeid1.output.csv
// $ $cmdStanHome/bin/stansummary.exe modeltypeid1.output.csv
functions { 
	// Define log likelihood function for temperature parameter phi (Note: phi=0 generates the prior's loglikelihood, phi=1 generates the same for the posterior, and the corresponding temperature values in between are generated by the temperature step index, the discretization or k number of steps, and the choice for the temperature parameter c) 
	real custnormal_log(real y, real mu, real r, real phi) { 
		return phi*(-log(2*pi())/2+log(r)/2-(r/2)*(y-mu)^2); 
	} 
} 
data {
	real<lower=0, upper=1> phi;
	int<lower=0> Tau;
	real x[Tau,1];
	real y[Tau,1];
}
parameters {
	real beta0;
	real beta1;
	real<lower=0> r;
}
model {
 	real mu[Tau,1];
	//default flat priors for beta0, beta1, r (using their legal value ranges)
	if (phi!=0) {
		for (i in 1:Tau) {
			mu[i,1]=beta0+beta1*x[i,1];
			y[i,1]~custnormal(mu[i,1], r, phi);
		}
	}
}
