#Author: reuben@reubenjohnston.com
#Description: 2nd degree polynomial regression (hierarchical alternative for a Normal-Gamma prior for unknown mu and r)
#References: Murphy 2007 (section 6)
#Comments: Do not confuse the precision of the normal prior distributions (b and d) with the precision of the observation noise, r.
#  Remember, the precision is the inverse of the variance (i.e., larger precisions are associated with smaller variance and vice versa).

#unlike OpenBUGS, JAGS forbids stochastic nodes to be declared twice
data {
##LIKELIHOODDATA,START##
	for (i in 1:Tau) {
		#zero the dummy variables for the custom sampling distribution
		dummylogy[i,1] <- 0
	}
##LIKELIHOODDATA,STOP##
}

model{
	#terms for simplifying the joint loglikelihood below (these are not dependent on independent variables and are located outside the area preprocessed)
	temp1 <- sqrt(r)/sqrt(2*3.141592653589793)
    C <- 1.0e+5 #this just has to be large enough to ensure all dummylog[i]'s >0
	for (i in 1:Tau) {
		#terms for simplifying the joint loglikelihood below
		mu[i,1] <- beta0+beta1*x[i,1]+beta2*pow(x[i,1],2)
	}
##LIKELIHOOD,START##
	#joint likelihood function
	for (i in 1:Tau) {
		#terms for simplifying the joint loglikelihood below
		temp2[i,1] <- pow(y[i,1]-mu[i,1],2)
		#custom sampling distribution for the likelihood (because of phi), using the zeros trick (FN1)
		dummylogy[i,1] ~ dpois(logy[i,1])
		logy[i,1] <- -loglikelihoody[i,1] + C
		loglikelihoody[i,1] <- phi * (log(temp1)-(r/2)*temp2[i,1]) #Model assumes a normal density for likelihood of the data
	}
##LIKELIHOOD,STOP##
	# prior distributions for the regression parameters
##VARIABLE,1##
	beta0 ~ dnorm(a,b*r)
##VARIABLE,2##
	beta1 ~ dnorm(c,d*r)
##VARIABLE,3##
	beta2 ~ dnorm(g,h*r)
	# prior distribution for the precision
##VARIABLE,4##
	r ~ dgamma(p,q)
}

#FN1: "A Poisson(phi) observation of zero has likelihood exp(-phi), so if our observed data is a set of 0's, and 
#phi[i] is set to - log(L[i]), we will obtain the correct likelihood contribution. (Note that phi[i] should always 
#be > 0 as it is a Poisson mean, and so we may need to add a suitable constant to ensure that it is positive.)" 
#Source: "Tricks: Advanced Use of the BUGS Language", http://users.aims.ac.za/~mackay/BUGS/Manuals/Tricks.html
