#Author: reuben@reubenjohnston.com
#Description: NHPP with the Yamada "s-shaped" cumulative intensity function (gamma priors for u and zeta)

#unlike OpenBUGS, JAGS forbids stochastic nodes to be declared twice
data {
##LIKELIHOODDATA,START##
	for (i in 2:Tau) {
		for (j in 1:R) {
			#zero the dummy variables for the custom sampling distribution
			dummylogN[i-1,j] <- 0
		}
	}
##LIKELIHOODDATA,STOP##
}

model {
	#terms for simplifying the joint loglikelihood below (these are not dependent on independent variables and are located outside the area preprocessed)
    C <- 1.0e+5 #this just has to be large enough to ensure all dummylog[i]'s >0
	for (i in 2:Tau) {
		for (j in 1:R) {
			#terms for simplifying the joint loglikelihood below
			#A[i-1,j] <- -u*((1+zeta*t[i,j])*exp(-zeta*t[i,j]))+u*(1+zeta*t[i-1,j])*exp(-zeta*t[i-1,j])
			#What follows is an algebraic manipulation of the above equation (it eliminates rounding error when zeta is very small)
			A[i-1,j] <- -u*exp(-zeta*t[i,j])+u*exp(-zeta*t[i-1,j])-u*zeta*t[i,j]*exp(-zeta*t[i,j])+u*zeta*t[i-1,j]*exp(-zeta*t[i-1,j]);
		}
	}
##LIKELIHOOD,START##
	#joint likelihood function
	for (i in 2:Tau) {
		for (j in 1:R) {
			#terms for simplifying the joint loglikelihood below
			B[i-1,j] <- N[i,j]-N[i-1,j]
			#custom sampling distribution for the NHPP's likelihood using the zeros trick (FN1)
			dummylogN[i-1,j] ~ dpois(logN[i-1,j])
			logN[i-1,j] <- -loglikelihoodN[i-1,j] + C
			loglikelihoodN[i-1,j] <- phi*(-1*logfact(B[i-1,j])+B[i-1,j]*log(A[i-1,j])-A[i-1,j]) 
		}
	}
##LIKELIHOOD,STOP##
	#prior distribution for u
##VARIABLE,1##
	u ~ dgamma(a,b)
	#prior distribution for zeta
##VARIABLE,2##
	zeta ~ dgamma(c,d)
}

#FN1: "A Poisson(phi) observation of zero has likelihood exp(-phi), so if our observed data is a set of 0's, and 
#phi[i] is set to - log(L[i]), we will obtain the correct likelihood contribution. (Note that phi[i] should always 
#be > 0 as it is a Poisson mean, and so we may need to add a suitable constant to ensure that it is positive.)" 
#Source: "Tricks: Advanced Use of the BUGS Language", http://users.aims.ac.za/~mackay/BUGS/Manuals/Tricks.html
