%plotFigs('C:\sandbox\mcmcBayes\figures\', '3.1', 't (weeks)', '$E[N(t|\mathbf{x}_0, \gamma)]$')
%plotFigs('C:\sandbox\mcmcBayes\figures\', '4.1', 't (weeks)', '$E[N(t|\mathbf{x}_0, \gamma)]$')
%plotFigs('C:\sandbox\mcmcBayes\figures\', '4.2', 't (weeks)', '$E[\mathbf{\Delta}|M_{1},t,\mathbf{D}_{\pi(\mathbf{\Theta}}]$')
%plotFigs('C:\sandbox\mcmcBayes\figures\', '4.3', 't (weeks)', '$E[\mathbf{\Delta}|M_{1},t,\mathbf{D}]$')
%plotFigs('C:\sandbox\mcmcBayes\figures\', '4.4', 't (weeks)', '$E[\mathbf{\Delta}|t,\mathbf{D}]$')
%plotFigs('C:\sandbox\mcmcBayes\figures\', '5.1', 'release number', '$N(t=50 weeks)$')
%plotFigs('C:\sandbox\mcmcBayes\figures\', 'A.1', 't (weeks)', '$E[N(t|\mathbf{x}_0, \gamma)]$')
%plotFigs('C:\sandbox\mcmcBayes\figures\', 'A.2', 't (weeks)', '$E[N(t|\mathbf{x}_0, \gamma)]$')
%plotFigs('C:\sandbox\mcmcBayes\figures\', 'A.3', 't (weeks)', '$E[N(t|\mathbf{x}_0, \gamma)]$')
%plotFigs('C:\sandbox\mcmcBayes\figures\', 'A.4', 't (weeks)', '$E[N(t|\mathbf{x}_0, \gamma)]$')
%plotFigs('C:\sandbox\mcmcBayes\figures\', 'A.5', 't (weeks)', '$E[\mathbf{\Delta}|t,\mathbf{D}]$')
%plotFigs('C:\sandbox\mcmcBayes\figures\', 'A.6', 't (weeks)', '$E[\mathbf{\Delta}|t,\mathbf{D}]$')
%plotFigs('C:\sandbox\mcmcBayes\figures\', 'A.7', 't (weeks)', '$E[\mathbf{\Delta}|t,\mathbf{D}]$')
%plotFigs('C:\sandbox\mcmcBayes\figures\', 'A.8', 't (weeks)', '$E[\mathbf{\Delta}|t,\mathbf{D}]$')
%plotFigs('C:\sandbox\mcmcBayes\figures\', '4.1', 't (weeks)', '$E[\mathbf{\Delta}|t,\mathbf{x}_i,\mathbf{D}_0,\mathbf{D}^1/\mathbf{D}^2]$')
function plotFigs(folder, figname, xlab, ylab)
    % Select the figure you want to add axis labels to and then run this script to save
    myfig = get(groot,'CurrentFigure');
    xlabel(xlab,'Interpreter','latex','FontSize',14);
    ylabel(ylab,'Interpreter','latex','FontSize',14);
    myfig.PaperUnits = 'inches';
    myfig.PaperPosition = [0 0 5 5];
    filename=sprintf('%sFigure %s.tif', folder, figname);
    saveas(myfig,filename,'tiffn')
    filename=sprintf('%sFigure %s.fig', folder, figname);
    saveas(myfig,filename,'fig')
end