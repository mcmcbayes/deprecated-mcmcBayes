%Utility function that checks if a file exists and whether it changes size after 1 second
function [condition]=checkFileStatic(filename,timeout)
    %does the file exist, if not exit with condition=-1
    file=java.io.File(filename);
    if ~file.exists()%simpler and more reliable existence check for a file (vs MATLAB's exist)
        condition=false;%file not present
        return
    end
    
    s = dir(filename);%refresh
    startsize=s.bytes;
    pause(timeout);%delay X seconds before reading file size again
    s = dir(filename);%refresh
    size=s.bytes;
    if (size==startsize)
        condition=true;%size didn't change
    else
        condition=false;%size changed
    end
end