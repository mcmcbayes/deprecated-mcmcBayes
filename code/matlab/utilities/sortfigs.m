function sortfigs(figs)
%SORTFIGS Sort figure windows in ascending order.
%
%   SORTFIGS brings up the figures in ascending order so the figure with the
%   lowest figure number becomes the first window.
%   SORTFIGS(FIGS) can be used to specify which figures that should be sorted.

%   Original Author:      Peter J. Acklam
%   Editing Author:       Reuben A. Johnston (updated for MATLAB R2015b)

% Get the handles to the figures to process.
if ~nargin                              % If no input arguments...
   figs = findobj('Type', 'figure');    % ...find all figures.
   indices = [figs.Number]';
   numbers = sort(indices);
   for index=numel(indices):-1:1        % RAJ: use reverse order here
       idx=find(indices==numbers(index));
       tempfigs(index,1)=figs(idx,1);
   end
   figs  = tempfigs;
end

if isempty(figs)
   disp('No open figures or no figures specified.');
   return
end

nfigs = length(figs);                   
for i = 1:nfigs                         % RAJ: use incrementing order here
    set(0,'currentfigure',figs(i));     % Bring up i'th figure.
end