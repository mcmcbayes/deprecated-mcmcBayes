%filename-file to query
%magicstrstart-string to locate for the start of buffer to query
%magicstrstop-string to locate for the stop of buffer to query
function [fieldbuffer]=fquerybuffer(filename, magicstrstart, magicstrstop)
fileID = fopen(filename,'r');
assert(fileID>=0, 'Problem opening the file');

eof=0;
while (eof==0) %read file into a cell array line by line
    tstring=fgets(fileID);%read line and keep newlines
    if exist('tstring','var')==1
        tcell=cellstr(tstring);
        if exist('filebuffer','var')==0
            filebuffer=tcell;
        else
            filebuffer=[filebuffer tcell];
        end
    end
    eof=feof(fileID);
end
fclose(fileID);
%find the index of magicstrstart
found=false;
len=numel(magicstrstart);
index=1;
while (found==false)
    stringout=filebuffer{index};%read the next line
    if numel(stringout)>len
        stringout=stringout(1:len);%compare len(magicstr) characters
    end
    if (strcmp(stringout,magicstrstart)==1)
        found=true;
        bufferstart=index+1;
        break;
    end
    index=index+1;
    assert(index<=numel(filebuffer), 'Problem locating magicstrstart');
end
%find the index of magicstrstop
found=false;
len=numel(magicstrstop);
index=1;
while (found==false)
    stringout=filebuffer{index};%read the next line
    if numel(stringout)>len
        stringout=stringout(1:len);%compare len(magicstr) characters
    end
    if (strcmp(stringout,magicstrstop)==1)
        found=true;
        bufferstop=index-1;
        break;
    end
    index=index+1;
    assert(index<=numel(filebuffer), 'Problem locating magicstrstop');
end
%read the next line into fieldbuffer
fieldbuffer=filebuffer(bufferstart:bufferstop);
