function setVisibility(state)
fighandles=get(groot,'Children');
if strcmp(state,'On')==1
    for handle=fighandles
        set(handle,'Visible','On');
    end
else
    for handle=fighandles
        set(handle,'Visible','Off');
    end
end