classdef conversionUtilities 
    methods (Static)         
        function retlogical = string_toboolean(setstring)
        % string_toboolean Returns true if strcmp(SETSTRING,'true')==1, returns false if strcmp(SETSTRING,'false')==1
        %   RETLOGICAL=string_toboolean(SETSTRING)
        %   Input:
        %     SETSTRING String to type cast to a logical
        %   Output:
        %     RETLOGICAL SETSTRING type cast to a logical
            if ~ischar(setstring)
                error('string_toboolean cannot convert from non char to logical');
            else
                if strcmp(setstring,'true')==1
                    retlogical=true;
                elseif strcmp(setstring,'false')==1
                    retlogical=false;
                else
                    error('string_toboolean cannot convert from improper string input');
                end
            end
        end
        
        function [retstr] = boolean_tostring(value)
            if isa(value,'logical')
                if value
                    retstr='true';                
                else
                    retstr='false';   
                end
            else
                error('boolean_tostring() Conversion failed as value was not a boolean');
            end
        end

        function retmatrix = string_tomatrix(setstring)
        % string_tomatrix converts the input string into a matrix
        %   RETMATRIX=string_tomatrix(SETSTRING)
        %   Input:
        %     SETSTRING String to type cast to a matrix
        %   Output:
        %     RETMATRIX SETSTRING type cast to a matrix
            if ~ischar(setstring)
                error('string_tomatrix cannot convert from non char to vector of double');
            else
                setstring=strrep(setstring,'[','');%remove the leading '['
                setstring=strrep(setstring,']','');%remove the trailing '['
                C = strsplit(setstring);%split on whitespace
                row=1;
                col=1;
                for element=1:numel(C)
                    retmatrix(row,col)=str2num(strrep(C{element},';',''));
                    results=strfind(C{element},';');
                    if ~(isempty(results))
                        col=1;
                        row=row+1;
                    else
                        col=col+1;
                    end
                end
            end
        end
        
        function [retstr] = matrix_tostring(values)
        % MATRIX_TOSTRING Converts a matrix of numbers to string format (e.g., [1 2 3 4]='[1 2 3 4]'
        %   [RETSTR]=MATRIX_TOSTRING(VALUES)
        %   Input:
        %     VALUES Matrix of numerical values to type cast to a string
        %   Output:
        %     RETSTR VALUES matrix type cast to a string
            [rows,cols]=size(values);
            for row=1:rows
                if row==1
                    if rows>1
                        retstr='[';%opening '['               
                    else
                        retstr='';%no brackets
                    end
                end
                for col=1:cols                   
                    if col==cols
                        if ~(row==rows)
                            retstr=sprintf('%s%f; ',retstr,values(row,col));%add a semicolon
                        else
                            retstr=sprintf('%s%f',retstr,values(row,col));%no semicolon or comma
                        end
                    else
                        retstr=sprintf('%s%f,',retstr,values(row,col));%add a comma
                    end
                end
                if rows>1 && row==rows
                    retstr=sprintf('%s]',retstr);%closing ']'                
                end
            end
        end         

        function [retstr] = scalar_tostring(value)
        % VECTOR_TOSTRING Converts a scalar to string format either double or int
        %   [RETSTR]=SCALAR_TOSTRING(VALUE)
        %   Input:
        %     VALUE Numerical value to type cast to a string
        %   Output:
        %     RETSTR VALUE type cast to a string
            if isa(value,'double')
                retstr=sprintf('%.6g',value);%minimum 6 significant digits
            elseif isa(value,'int8') || isa(value,'int16') || isa(value,'int32') || isa(value,'int64') || ...
                    isa(value,'uint8') || isa(value,'uint16') || isa(value,'uint32') || isa(value,'uint64')
                retstr=sprintf('%d',value);
            else
                error('scalar_tostring() unsupported scalar type');
            end
        end             
        
        function [retstr] = cellarray_tostring(values)
        % CELLARRAY_TOSTRING Converts a cell array of numbers to string format. Cells may have {number}, {[vector]}, or {[vector], [vector]} (for stochastic processes).
        %   Input:
        %     VALUES Cell array to type cast to a string
        %   Output:
        %     RETSTR VALUES Cell array type cast to a string
            retstr='{';
            for value=values %each cell array index
                retstr=sprintf('%s{',retstr);
                index=1;
                maxindex=size(value{:},2);
                for value_exp=value{:}
                    cell_exp=value_exp{:};
                    if size(cell_exp,2)>1 %cell is a vector
                        retstr=sprintf('%s%s',retstr,conversionUtilities.vector_tostring(cell_exp));
                    else %cell is a scalar
                        retstr=sprintf('%s%s',retstr,conversionUtilities.scalar_tostring(cell_exp));
                    end
                    if ~(index==maxindex)
                        index=index+1;
                        retstr=sprintf('%s,',retstr);
                    end
                end
                retstr=sprintf('%s}',retstr);
             end
             retstr=sprintf('%s}',retstr);
       end
    end      
 end