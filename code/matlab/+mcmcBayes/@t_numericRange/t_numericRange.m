classdef t_numericRange < int32
    enumeration
        null (0)        %not checked
        gteq0 (1)       %x>=0
        gteq0ANDltInf (2) %0<=x<=Inf
        gt0 (3)         %x>0
        gt0ANDlt1 (4)   %0<x<1
        gt0ANDltInf (5) %0<x<Inf
        eq0OReq1 (6)    %x=0 or x=1
        gtNegInfANDltInf (7) %-Inf<x<Inf
    end
    
    methods (Static)        
        function intvalue=toint(enumobj)
            switch enumobj          
                case {mcmcBayes.t_numericRange.null, mcmcBayes.t_numericRange.gteq0, mcmcBayes.t_numericRange.gteq0ANDltInf, mcmcBayes.t_numericRange.gt0, ...
                        mcmcBayes.t_numericRange.gt0ANDlt1, mcmcBayes.t_numericRange.gt0ANDltInf, mcmcBayes.t_numericRange.eq0OReq1, ...
                        mcmcBayes.t_numericRange.gtNegInfANDltInf}
                    intvalue=int32(enumobj);
                otherwise
                    error('unsupported mcmcBayes.t_numericRange type');
            end                
        end        
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_numericRange.null
                    strvalue='mcmcBayes.t_numericRange.null';
                case mcmcBayes.t_numericRange.gteq0
                    strvalue='mcmcBayes.t_numericRange.gteq0';    
                case mcmcBayes.t_numericRange.gteq0ANDltInf
                    strvalue='mcmcBayes.t_numericRange.gteq0ANDltInf';    
                case mcmcBayes.t_numericRange.gt0
                    strvalue='mcmcBayes.t_numericRange.gt0';                    
                case mcmcBayes.t_numericRange.gt0ANDlt1
                    strvalue='mcmcBayes.t_numericRange.gt0ANDlt1';
                case mcmcBayes.t_numericRange.gt0ANDltInf
                    strvalue='mcmcBayes.t_numericRange.gt0ANDltInf';
                case mcmcBayes.t_numericRange.eq0OReq1
                    strvalue='mcmcBayes.t_numericRange.eq0OReq1';
                case mcmcBayes.t_numericRange.gtNegInfANDltInf
                    strvalue='mcmcBayes.t_numericRange.gtNegInfANDltInf';
                otherwise
                    error('unsupported mcmcBayes.t_numericRange type');
            end
        end

        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_numericRange.null)
                    enumobj=mcmcBayes.t_numericRange.null;
                case int32(mcmcBayes.t_numericRange.gteq0)
                    enumobj=mcmcBayes.t_numericRange.gteq0;
                case int32(mcmcBayes.t_numericRange.gteq0ANDltInf)
                    enumobj=mcmcBayes.t_numericRange.gteq0ANDltInf;                    
                case int32(mcmcBayes.t_numericRange.gt0)
                    enumobj=mcmcBayes.t_numericRange.gt0;
                case int32(mcmcBayes.t_numericRange.gt0ANDlt1)
                    enumobj=mcmcBayes.t_numericRange.gt0ANDlt1;
                case int32(mcmcBayes.t_numericRange.gt0ANDltInf)
                    enumobj=mcmcBayes.t_numericRange.gt0ANDltInf;
                case int32(mcmcBayes.t_numericRange.eq0OReq1)
                    enumobj=mcmcBayes.t_numericRange.eq0OReq1;
                case int32(mcmcBayes.t_numericRange.gtNegInfANDltInf)
                    enumobj=mcmcBayes.t_numericRange.gtNegInfANDltInf;
                otherwise
                    error('unsupported mcmcBayes.t_numericRange type');
            end
        end
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_numericRange.null')==1
                enumobj=mcmcBayes.t_numericRange.null;
            elseif strcmp(strval,'mcmcBayes.t_numericRange.gteq0')==1
                enumobj=mcmcBayes.t_numericRange.gteq0;
            elseif strcmp(strval,'mcmcBayes.t_numericRange.gteq0ANDltInf')==1
                enumobj=mcmcBayes.t_numericRange.gteq0ANDltInf;
            elseif strcmp(strval,'mcmcBayes.t_numericRange.gt0')==1
                enumobj=mcmcBayes.t_numericRange.gt0;
            elseif strcmp(strval,'mcmcBayes.t_numericRange.gt0ANDlt1')==1
                enumobj=mcmcBayes.t_numericRange.gt0ANDlt1;
            elseif strcmp(strval,'mcmcBayes.t_numericRange.gt0ANDltInf')==1
                enumobj=mcmcBayes.t_numericRange.gt0ANDltInf;
            elseif strcmp(strval,'mcmcBayes.t_numericRange.eq0OReq1')==1
                enumobj=mcmcBayes.t_numericRange.eq0OReq1;
            elseif strcmp(strval,'mcmcBayes.t_numericRange.gtNegInfANDltInf')==1
                enumobj=mcmcBayes.t_numericRange.gtNegInfANDltInf;
            else
                error('unsupported mcmcBayes.t_numericRange type');
            end
        end          
    end    
end