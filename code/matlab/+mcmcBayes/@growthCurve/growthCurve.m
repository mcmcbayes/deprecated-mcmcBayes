classdef growthCurve < mcmcBayes.model    
%growthCurve is a base class for growth curve models.  It is a sub class of mcmcBayes.model and mcmcBayes.simulation.
% The currently implemented models, listed hierarchically, are:
%  * mcmcBayes.simulation.model.growthCurve.Verhulst
%  * mcmcBayes.simulation.model.growthCurve.Gompertz
    properties (SetAccess=public, GetAccess=public)
        predictionsNSamples=mcmcBayes.growthCurve.getDefaultNSamples();%Default number of samples to run in calls to simPredictions()
    end

    methods (Abstract, Static)

    end

    methods (Abstract)

    end
    
    methods (Static)
        function test()
            display('Hello!');
        end
             
        function [predictionsNSamples]=getDefaultNSamples()
            predictionsNSamples=50000;
        end        
        
        function [names]=getDefaultDataVariableNames(dataCategory)
            switch dataCategory
                case mcmcBayes.t_dataCategory.independent
                    names={'t'};
                case mcmcBayes.t_dataCategory.dependent
                    names={'N'};
                otherwise
                    error('dataCategory must be mcmcbayes.t_dataCategory.');
            end
        end
        
        function [loglikelihood]=computeLogLikelihood(type, vars, data)
        % [loglikelihood]=computeLogLikelihood(type, vars, data)
        %   Computes the loglikelihood of the data given the model and parameter values.  This is used in the marginal likelihood computation.
        %   Input: 
        %     type - Specifies the model to use for computing the likelihood
        %     vars - Model variables that is an array of mcmcBayes.variable objects 
        %     data - Independent and dependent data variables for the model
        %   Output:
        %     loglikelihood - Computed loglikelihood (vector)
            if (~strcmp(type,'Verhulst') && ~strcmp(type,'Gompertz'))
                error('Incorrect model type.');
            end
                       
            datavarid=1;%one independent variable for growth curve, 't' and one dependent variable for growth curve, 'N(t)'
            t=data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent, datavarid);
            Tau=size(t,1);%Number of data (we are assuming scalar independent variables)
            Norig=cast(data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.dependent, datavarid),'double');

            % Ideally numSamples would be obj.modelVariables(index).MCMC_nsamples_data.  If the MCMC sampler has issues 
            %   (e.g., improper values for the inits) it may stop early.  The user is separately warned of 
            %   improperly sized sample arrays when they are stored by mcmcBayesVariable.set.MCMC_samples(). Set
            %   tsize to the variable with the fewest number of samples.
            numSamples=size(vars(1).MCMC_samples,1);
            for varid=1:size(vars,1)
                if size(vars(varid).MCMC_samples,1)<numSamples
                    numSamples=size(vars(varid).MCMC_samples,1);         
                end
            end                
            loglikelihood=zeros(numSamples,1);

            beta0=cast(vars(1).MCMC_samples,'double');
            beta1=cast(vars(2).MCMC_samples,'double');           
            beta2=cast(vars(3).MCMC_samples,'double');           
            r=cast(vars(4).MCMC_samples,'double');
            if strcmp(type, 'Verhulst')
            	mufunc = @(t,i,beta0,beta1,beta2) beta1./(1+exp(beta0-beta2*t(i)));
            elseif strcmp(type, 'Gompertz')
            	mufunc = @(t,i,beta0,beta1,beta2) beta1.*exp(-exp(beta0-beta2*t(i)));    
            else
                error('unsupported model for mcmcBayes.regression::computeLogLikelihood()');
            end
            for i=1:Tau %row for variable samples, col for each independent variable value
                mu(:,i)=mufunc(t(:,1),i,beta0(1:numSamples),beta1(1:numSamples),beta2(1:numSamples));
            end

            tloglikelihood=zeros(numSamples,Tau);
            for i=1:numSamples
                tloglikelihood(i,:)=1/2*log(r(i))-1/2*log(2*pi)-r(i)/2.*((Norig((1:end),1)'-mu(i,(1:end))).^2);
            end
            for i=1:numSamples
                loglikelihood(i,1)=sum(tloglikelihood(i,:));
            end
            
            if (~isempty(find(isinf(loglikelihood)==1)))
                error('Error condition in computeLogLikelihood(), loglikelihood contains -Inf or Inf');
            elseif ~isempty(find(isnan(loglikelihood)==1))
                error('Error condition in computeLogLikelihood(), loglikelihood contains Nan');
            end                   
        end

        function [dependentVariables]=generateTestData(type, predictionsNSamples, data, varargin)
            dependentVariables=generateTestData@mcmcBayes.model(type, predictionsNSamples, data, varargin{:});
        end
        
        function [depVarsValuesHat, retNhat]=simPredictions(type, predictionsNSamples, predictionsToCapture, hingeThreshold, data, varargin)          
        % [dependentVariablesHat]=simPredictions(modeltype, predictionsNSamples, dependentVariablesHatTypes, hingeThreshold, independentVariables, dependentVariableName, description, arglist)
        %   Function to simulate predictions using one of the regression models (it is static so we can simulate data without instantiating a class)
        %   Input:
        %     modeltype - Specifies the model to use for generating the predictions
        %     predictionsNSamples - Number of samples to run when generating the predictions
        %     dependentVariablesHatTypes - Vector of t_mcmcBayesDependentVariablesHat that specifies which thetahat to generate predictions for
        %     hingeThreshold - Specifies the hinge threshold for determining the thetaHat edge cases for generating dependentVariablesHat.{hingelo/hingehi}
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     dependentVariableName - Name to use for the predictions data
        %     description - Description for the predictions data
        %     arglist - Cell array with the values to use for the model variables when prediction with the model (i.e., these are the thetaHat)
        %   Output:
        %     dependentVariablesHat - Struct with elements for each of the t_mcmcBayesDependentVariablesHat types specified in dependentVariablesHatTypes 
        %     (e.g., the mean and the {hingelo/hingehi} values from the histogram of the predicted values over predictionsNSamples samples)
            if (~strcmp(type,'Verhulst') && ~strcmp(type,'Gompertz'))
                error('Incorrect model type.');
            end
            datavarid=1;
            t=cast(data.indVarsValues{datavarid,1},'double');
           
            depdatavarid=1;
            depVarName=data.depVarsName{depdatavarid};
            depVarDesc=data.depVarsDesc{depdatavarid};
            depVarTransformPre=data.depVarsTfPre(depdatavarid);
            depVarTransformPost=data.depVarsTfPost(depdatavarid);            
            
            numdatapoints=numel(t);
            beta0=varargin{1};
            beta1=varargin{2};
            beta2=varargin{3};
            r_f0=varargin{4};
            if strcmp(type, 'Verhulst')
                for datapoint=1:numdatapoints
                    %convert r to MATLAB's normal(mu,sigma), or sigma=1/sqrt(r)
                    m(datapoint)=beta1/(1+exp(beta0-beta2*t(datapoint)));
                    pd_f0=makedist('Normal',m(datapoint),1/sqrt(r_f0));
                    pd_f0=truncate(pd_f0,0,Inf);
                    Nhat_f0(datapoint,:)=random(pd_f0,1,predictionsNSamples);
                end
            elseif strcmp(type, 'Gompertz')
                for datapoint=1:numdatapoints
                    %convert r to MATLAB's normal(mu,sigma), or sigma=1/sqrt(r)
                    m(datapoint)=beta1*exp(-exp(beta0-beta2*t(datapoint)));
                    pd_f0=makedist('Normal',double(m(datapoint)),1/sqrt(r_f0));
                    pd_f0=truncate(pd_f0,0,Inf);
                    Nhat_f0(datapoint,:)=random(pd_f0,1,predictionsNSamples);
                end
            else
                error('unsupported model for mcmcBayes.growthCurve::simPredictions()');
            end
            Nhat=Nhat_f0;
            
%             if (type==mcmcBayes.t_growthCurve.VerhulstScaled) || (type==mcmcBayes.t_growthCurve.GompertzScaled)
%                 %compute the scaling term
%                 sim_fhandle_scaling=varargin{5};
%                 x=varargin{6};
%                 c=varargin{7};
%                 r_s=varargin{8};
%                 if isequal(x,zeros(size(x,1),1))
%                     for datapoint=1:numdatapoints
%                         Nhat(datapoint,:)=Nhat_f0(datapoint,:);%sim_fhandle_scaling(x,c) should be 1
%                     end
%                 else
%                     pd_s=makedist('Normal',sim_fhandle_scaling(x,c),1/sqrt(r_s));
%                     shat=random(pd_s,1,predictionsNSamples);
%                     for datapoint=1:numdatapoints
%                         Nhat(datapoint,:)=max(0,Nhat_f0(datapoint,:).*shat);%zero should be minimum
%                     end
%                 end
%             end
            
            binprec=(ceil(max(max(Nhat)))-floor(min(min(Nhat))))/1000;
            bins=floor(min(min(Nhat))):binprec:ceil(max(max(Nhat)));
            depVarsValuesHat=mcmcBayes.model.capturePredictionThresholds(predictionsNSamples, predictionsToCapture, hingeThreshold, bins, ...
                mcmcBayes.t_numericDimensions.vector, Nhat, data.depVarsType{depdatavarid});
            retNhat=Nhat;
        end
    end
    
    methods 
        function obj=growthCurve(settype, setsubType, setname, setdescription, setauthor, setreference, setprefix, setcomputemsfe)
        % obj=growthCurve(settype, setsubType, setname, setdescription, setauthor, setreference, setprefix, setcomputemsfe)
        %   The constructor for growthCurve.
        % Input:
        %
        % Output:
        %   obj Constructed growthCurve object
            if nargin == 0
                superargs={};
            else
                superargs{1}=settype;
                superargs{2}=setsubType;
                superargs{3}=setname;
                superargs{4}=setdescription;
                superargs{5}=setauthor;
                superargs{6}=setreference;
                superargs{7}=setprefix;   
                superargs{8}=setcomputemsfe;
            end

            obj=obj@mcmcBayes.model(superargs{:});
        end
               
        function displayPredictions(obj, tempid)
        % displayPredictions(obj)
        %   Displays the values (in the console) for the already generated model predictions in obj.dependentVariablesHat.
        %   Input:
        %     tempid - Temperature id to compute the display the predictions for            

            stdout=1;
            %print the original data
            datavarid=1;                
            if tempid==0
                t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorData, mcmcBayes.t_dataCategory.independent, datavarid);
                if obj.priorData.dataSource~=mcmcBayes.t_dataSource.Preset %don't print preset, it is empty
                    fprintf(stdout,'t=%s\n',conversionUtilities.matrix_tostring(t));
                    Norig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorData, mcmcBayes.t_dataCategory.dependent, datavarid);
                    fprintf(stdout,'Norig=%s\n',conversionUtilities.matrix_tostring(Norig));
                end
            else
                t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.independent, datavarid);
                fprintf(stdout,'t=%s\n',conversionUtilities.matrix_tostring(t));
                Norig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.dependent, datavarid);
                fprintf(stdout,'Yorig=%s\n',conversionUtilities.matrix_tostring(Norig));
            end
            
            %print the predictions
            for Yhattype=obj.predictionsToCapture%use the vector obj.predictionsToCapture to determine which structure elements from priorPredictedData to show
                datavarid=1;
                if tempid==0
                    t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.independent, datavarid, 'mean');%just use mean (all the same)
                else
                    t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.independent, datavarid, 'mean');%just use mean (all the same)
                end
                switch Yhattype
                    case mcmcBayes.t_dataResult.hingelo
                        if tempid==0
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');%just use mean (all the same)
                        else
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');%just use mean (all the same)
                        end
                        name='Nhat.hingelo';
                    case mcmcBayes.t_dataResult.mean
                        if tempid==0
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');%just use mean (all the same)
                        else
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');%just use mean (all the same)
                        end
                        name='Nhat.mean';
                    case mcmcBayes.t_dataResult.hingehi
                        if tempid==0
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');%just use mean (all the same)
                        else
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');%just use mean (all the same)
                        end
                        name='Nhat.hingehi';
                    otherwise
                        error('Cannot identify the appropriate structure element in obj.predictionsToCapture');
                end
                if Yhattype==mcmcBayes.t_dataResult.mean
                    fprintf(stdout,'t=%s\n',conversionUtilities.matrix_tostring(t));
                    fprintf(stdout,'%s=%s\n',name,conversionUtilities.matrix_tostring(Nhat));
                end
            end
        end
        
        function plotPredictions(obj, chainID, tempID)
        % plotPredictions(obj, tempid)
        %   Plots the model simulation results, using point estimates for Thetahat, over the original data.
        %   Input:
        %     tempid - Temperature id to compute the display the predictions for

            sessiondetails=sprintf('%s-%s-chainid%d-tempid%d',obj.filenamePrefix, obj.uuid, chainID, tempID);            
                  
            xmin=0;
            xmax=0;
            ymin=0;
            ymax=0;
            
            fig();
            hold on;           
            %plot the original data
            datavarid=1;
            if tempID==0
                t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorData, mcmcBayes.t_dataCategory.independent, datavarid);
                if obj.priorData.dataSource~=mcmcBayes.t_dataSource.Preset
                    Norig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorData, mcmcBayes.t_dataCategory.dependent, datavarid);
                end
            else
                t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.independent, datavarid);
                Norig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.dependent, datavarid); 
            end
            if (tempID==0)
                if exist('Norig','var')
                    ymin=min([ymin min(Norig)]);%could be < 0
                    ymax=max([ymax max(Norig)]);%shouldn't be < 0
                    plot(t,Norig,'g*');
                end
            else
                ymin=min([ymin min(Norig)]);%could be < 0
                ymax=max([ymax max(Norig)]);%shouldn't be < 0
                plot(t,Norig,'r*');
            end
            %do the scaling after computing it (otherwise, if it is in a loop, it will compound on the scaling)
            ymax=1.1*ymax;
            if (ymin==ymax)%avoid the case when they're both 0
                ymax=ymin+1;
            end
            
            %plot the predicted data 
            datavarid=1;                
            if (tempID==0)
                t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.independent, datavarid, 'mean');%just use mean (all the same)
            else
                t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.independent, datavarid, 'mean');%just use mean (all the same)
            end  
            xmin=min([xmin min(t)]);%could be < 0
            xmax=max([xmax max(t)]);%shouldn't be < 0            
            for predictionToCapture=obj.predictionsToCapture%use the vector obj.dependentVariablesHatTypes to determine which rows from dependentVariablesHat to show            
                if (tempID==0)
                    switch predictionToCapture
                        case mcmcBayes.t_dataResult.hingelo
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');
                            plot(t, Nhat, 'g--');%lo bound dashed plot line
                        case mcmcBayes.t_dataResult.mean
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');
                            plot(t, Nhat, 'g-');%mean solid plot line
                        case mcmcBayes.t_dataResult.hingehi
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');
                            plot(t, Nhat, 'g-.');%hi bound dashed plot line
                        otherwise
                            %won't get here
                    end
                else
                    switch predictionToCapture
                        case mcmcBayes.t_dataResult.hingelo
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');
                            plot(t, Nhat, 'r--');%lo bound dashed plot line
                        case mcmcBayes.t_dataResult.mean
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');
                            plot(t, Nhat, 'r-');%mean solid plot line
                        case mcmcBayes.t_dataResult.hingehi
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');
                            plot(t, Nhat, 'r-.');%hi bound dashed plot line
                        otherwise
                            error('Cannot identify the appropriate structure in obj.posteriorPredictedData');
                    end
                end
                ymin=min([ymin min(Nhat)]);%could be < 0
                ymax=max([ymax max(Nhat)]);%shouldn't be < 0
            end                 
            
            if size(unique(t),1)==size(t,1) && size(unique(t),1)<10 %No duplicates
                set(gca,'XTickMode','manual');
                set(gca,'xTick',[]);
                set(gca,'xTick',t);
            end
            %do the scaling after computing it (otherwise, if it is in a loop, it will compound on the scaling)
            if (ymin<0)
                ymin=1.1*ymin;
            else
                ymin=0.9*ymin;
            end
            ymax=1.1*ymax;
            if (ymin==ymax)%avoid the case when they're both 0
                ymax=ymin+1;
            end
            set(gca,'xLim',[xmin xmax]);
            set(gca,'yLim',[ymin ymax]);
            tempstr=sprintf('Model prediction results for %s, %s', obj.name, sessiondetails);
            title(tempstr);
            filename=sprintf('%s%s-PredictionsSet.fig',obj.osInterface.figuresDir,sessiondetails);%need _PredictionsSet for filename parsing
            savefig(gcf,filename);
            
            hold off;        
        end                        
    end

    methods (Access=protected)    
        function [status]=checkDataCompatibility(obj, datatype)
        % [status]=checkDataCompatibility(obj, datatype)
        %   Checks the compatibility of priorDependentVariables with the specified mcmcBayesModelRegression type
            switch datatype
                case mcmcBayes.t_data.priordata
                	status=obj.priorData.checkDataForInfNanValues();
                case mcmcBayes.t_data.priorbasedpredictions
                	status=obj.priorPredictedData.checkDataForInfNanValues();
                case mcmcBayes.t_data.posteriordata
                	status=obj.posteriorData.checkDataForInfNanValues();
                case mcmcBayes.t_data.posteriorbasedpredictions
                	status=obj.posteriorPredictedData.checkDataForInfNanValues();
            end
        end
    end
    
    methods %getter/setter functions       
          
    end
end
