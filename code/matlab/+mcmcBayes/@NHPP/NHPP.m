classdef NHPP < mcmcBayes.model   
%NHPP is a base class for Non-homogeneous Poisson process models.  It is a sub class of mcmcBayes.model and mcmcBayes.simulation.    
% The currently implemented models, listed hierarchically, are:
%  * mcmcBayes.simulation.model.NHPP.BrooksMotley
%  * mcmcBayes.simulation.model.NHPP.GoelOkumoto
%  * mcmcBayes.simulation.model.NHPP.Goel
%  * mcmcBayes.simulation.model.NHPP.MusaOkumoto
%  * mcmcBayes.simulation.model.NHPP.YamadaOhbaOsaki
%  * mcmcBayes.simulation.model.NHPP.KuoGhosh
    properties (Access=public)
        predictionsNSamples=mcmcBayes.NHPP.getDefaultNSamples();%Default number of samples to run in calls to simPredictions()
        predictionsIntervalNmax=mcmcBayes.NHPP.getDefaultPredictionsIntervalNmax();%Specifies the max interval for the set [1:MAX] (steps of 1) to compute NHPP PMFs for in simPredictions()
        predictionsfunctionHandleCif%Specifies the NHPPs cumulative intensity function (CIF) to use in simPredictions()
    end

    methods (Abstract)

    end

    methods (Abstract, Static, Access=protected)        
        [fhandle]=get_fhandle_CIF();        
    end
    
    methods (Static)
        function test()
            display('Hello!');
        end
        
        function [predictionsNSamples]=getDefaultNSamples()
            predictionsNSamples=50000;
        end
               
        function [predictionsIntervalNmax]=getDefaultPredictionsIntervalNmax()
            predictionsIntervalNmax=150;
        end        
        
        function [names]=getDefaultDataVariableNames(dataCategory)
            switch dataCategory
                case mcmcBayes.t_dataCategory.independent
                    names={'t'};
                case mcmcBayes.t_dataCategory.dependent
                    names={'N'};
                otherwise
                    error('dataCategory must be mcmcbayes.t_dataCategory.');
            end
        end
        
        function [loglikelihood]=computeLogLikelihood(type, vars, data)            
        % [loglikelihood]=computeLogLikelihood(type, vars, data)
        %   Computes the loglikelihood of the data given the model and parameter values.  This is used in the marginal likelihood computation.
        %   Input: 
        %     type - Specifies the model to use for computing the likelihood
        %     vars - Model variables that is an array of mcmcBayes.variable objects 
        %     data - Independent and dependent data variables for the model
        %   Output:
        %     loglikelihood - Computed loglikelihood (vector)
            if ~strcmp(type, 'BrooksMotley') && ~strcmp(type, 'GoelOkumoto') && ~strcmp(type, 'Goel') && ...
                    ~strcmp(type, 'MusaOkumoto') && ~strcmp(type, 'YamadaOhbaOsaki') && ~strcmp(type, 'KuoGhosh')
                error('Incorrect modeltype.');
            end
            
            datavarid=1;%one independent variable for nhpp, 't' and one dependent variable for nhpp, 'N(t)'
            t=data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent, datavarid);
            R=size(t,1);%Number of intervals
            Tau=size(t,2);%Number of data (we are assuming vector independent variables)
            Norig=cast(data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.dependent, datavarid),'double');
            % Ideally numSamples would be obj.modelVariables(index).MCMC_nsamples_data.  If the MCMC sampler has issues 
            %   (e.g., improper values for the inits) it may stop early.  The user is separately warned of 
            %   improperly sized sample arrays when they are stored by mcmcBayesVariable.set.MCMC_samples(). Set
            %   tsize to the variable with the fewest number of samples.
            if strcmp(type, 'KuoGhosh')
                numSamples=size(vars(1).MCMC_samples,2);
            else
                numSamples=size(vars(1).MCMC_samples,1);
            end
            loglikelihood=zeros(numSamples,1);

            for data=1:Tau
                A=zeros(numSamples,R-1);
                B=zeros(numSamples,R-1);
                if strcmp(type, 'BrooksMotley')          
                    if (size(vars,1)~=1)
                        error('incorrect size for variables');
                    end
                    zeta=cast(vars(1).MCMC_samples,'double');
                    Amacro = @(t,i,zeta) zeta.*t(i)-zeta.*t(i-1);
                    for i=2:R
                        A(:,i-1)=Amacro(t,i,zeta);
                    end             
                elseif strcmp(type, 'GoelOkumoto')
                    if (size(vars,1)~=2)
                        error('incorrect size for variables');
                    end
                    u=cast(vars(1).MCMC_samples,'double');
                    zeta=cast(vars(2).MCMC_samples,'double');
                    Amacro = @(t,i,u,zeta) -u.*exp(-zeta.*t(i))+u.*exp(-zeta.*t(i-1));
                    for i=2:R
                        A(:,i-1)=Amacro(t,i,u,zeta);
                    end                                      
                elseif strcmp(type, 'Goel')
                    if (size(vars,1)~=3)
                        error('incorrect size for variables');
                    end
                    u=cast(vars(1).MCMC_samples,'double');
                    zeta=cast(vars(2).MCMC_samples,'double');
                    kappa=cast(vars(3).MCMC_samples,'double');
                    Amacro = @(t,i,u,zeta,kappa) -u.*exp(-zeta.*t(i).^kappa)+u.*exp(-zeta.*t(i-1).^kappa);
                    for i=2:R
                        A(:,i-1)=Amacro(t,i,u,zeta,kappa);
                    end                   
                elseif strcmp(type, 'MusaOkumoto')
                    if (size(vars,1)~=2)
                        error('incorrect size for variables');
                    end
                    Amacro = @(t,i,zeta,kappa) (1./kappa).*log1p(zeta.*kappa.*t(i))-(1./kappa).*log1p(zeta.*kappa.*t(i-1));
                    zeta=cast(vars(1).MCMC_samples,'double');
                    kappa=cast(vars(2).MCMC_samples,'double');
                    for i=2:R
                        A(:,i-1)=Amacro(t,i,zeta,kappa);
                    end                 
                elseif strcmp(type, 'YamadaOhbaOsaki')
                    if (size(vars,1)~=2)
                        error('incorrect size for variables');
                    end
                    Amacro = @(t,i,u,zeta) (-u.*zeta.*t(i).*exp(-zeta.*t(i))+u.*zeta.*t(i-1).*exp(-zeta.*t(i-1)))+(-u.*exp(-zeta.*t(i))+u.*exp(-zeta.*t(i-1)));
                    u=cast(vars(1).MCMC_samples,'double');
                    zeta=cast(vars(2).MCMC_samples,'double');
                    for i=2:R
                        A(:,i-1)=Amacro(t,i,u,zeta);
                    end                 
                elseif strcmp(type, 'KuoGhosh')
                    if (size(vars,1)~=2)
                        error('incorrect size for variables');
                    end
                    theta=cast(vars(1).MCMC_samples,'double');
                    Amacro = @(j,theta) theta(j,:)-theta(j-1,:);
                    for i=2:R
                        for j=2:i
                            A(:,i-1)=Amacro(j,theta)';
                        end
                    end
                else
                    error('unsupported mcmcBayes.t_NHPP for computeLogLikelihood()');
                end                     

                Bmacro = @(n,i) (n(i)-n(i-1));        
                for i=2:R
                    B(1,i-1)=Bmacro(Norig(:,data),i);%Compute B for Norig(:,data)
                end
                for i=2:numSamples %Copy the value for B to every entry (don't have to copy first)
                    B(i,:)=B(1,:);
                end                
                rawloglikelihood=zeros(numSamples,R-1);
                for i=1:R-1
                    rawloglikelihood(:,i)=log(A(:,i)).*B(:,i)-log(factorial(B(:,i)))-A(:,i);
                end
            end
            for j=1:numSamples
                loglikelihood(j,1)=sum(rawloglikelihood(j,:));
            end                
            if (~isempty(find(isinf(loglikelihood)==1)))
                warning('Error condition in computeLogLikelihood(), likelihood contains -Inf or Inf.  Perhaps your interval values are too large (N(t)-N(t-1)>170) and the factorial(N(t)-N(t-1)) is blowing up or there was an zero-interval in the sample (i.e., no counts)');
            elseif ~isempty(find(isnan(loglikelihood)==1))
                warning('Error condition in computeLogLikelihood(), likelihood contains Nan');
            end
         end
        
        %override the mcmcBayes.model.generateTestData
        function [dependentVariables]=generateTestData(type, predictionsNSamples, data, varargin)
        % [dependentVariables]=generateTestData(modeltype, predictionsNSamples, independentVariables, dependentVariableName, meanVariableValues)
        %   Generates simulated data to use with model evaluation.
        %   Input:
        %     modeltype - Specifies the model to use for generating the test data
        %     predictionsNSamples - Number of samples to run when generating the test data
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayes.numeric objects
        %     dependentVariableName - Name to use for the generated test data
        %     thetaHat - Variable point estimates to use in the model for generating the simulated data (mcmcBayes.numeric)
        %   Output:
        %     dependentVariables - Array of mcmcBayes.numeric containing the simulated data            independentVariablesVector=cast(mcmcBayes.numeric.toVector(independentVariables),'double');
            datavarid=1;
            depVarName=data.depVarsName{datavarid};
            if strcmp(type, 'KuoGhosh') %simNHPP for t_NHPP.KuoGhosh expects arglist=[{t},{Thetahat(t)}]
                t=cast(data.indVarsValues{datavarid,1},'double');
%todo: support arbitrary lengths
                paramsstr=sprintf('%s=[%d; %d; %d; %d; %d; %d], %s=[%d; %d; %d; %d; %d; %d]','t',t,depVarName,varargin{1}(:));
            else
                for varid=1:numel(varargin{1})
                    if varid==1
                        paramsstr=sprintf('%f', varargin{varid});
                    else
                        paramsstr=sprintf('%s, %f', paramsstr, varargin{varid});
                    end
                end
            end
            dispstr=sprintf('Generating simdata using %s model', type, paramsstr);
            disp(dispstr);
            if strcmp(type, 'BrooksMotley')
                    predictionsfunctionHandleCif=mcmcBayes.BrooksMotley.get_fhandle_CIF();
            elseif strcmp(type, 'GoelOkumoto')
                predictionsfunctionHandleCif=mcmcBayes.GoelOkumoto.get_fhandle_CIF();
            elseif strcmp(type, 'Goel')
                predictionsfunctionHandleCif=mcmcBayes.Goel.get_fhandle_CIF();
            elseif strcmp(type, 'MusaOkumoto')
                predictionsfunctionHandleCif=mcmcBayes.MusaOkumoto.get_fhandle_CIF();
            elseif strcmp(type, 'YamadaOhbaOsaki')
                predictionsfunctionHandleCif=mcmcBayes.YamadaOhbaOsaki.get_fhandle_CIF();                    
            elseif strcmp(type, 'KuoGhosh')
                predictionsfunctionHandleCif=mcmcBayes.KuoGhosh.get_fhandle_CIF();
                tvarargin=cell(size(varargin));
                tvarargin(1)={t};
                tvarargin(2)=varargin(1);
                varargin=tvarargin;
            else
                error('unsupported mcmcBayes.t_NHPP for generateTestData()');
            end
            predictionsIntervalNmax=150;            
            [tempdependentVariables, Nhat]=mcmcBayes.NHPP.simPredictions(type, predictionsNSamples, predictionsIntervalNmax, predictionsfunctionHandleCif, [mcmcBayes.t_dataResult.mean], ...
                0.05, data, varargin{:});
            dependentVariables = tempdependentVariables.mean;           
        end
        
        function [depVarsValuesHat, retNhat]=simPredictions(type, predictionsNSamples, predictionsIntervalNmax, predictionsfunctionHandleCif, predictionsToCapture, hingeThreshold, ...
                data, varargin)
        % [dependentVariablesHat]=simPredictions(modeltype, predictionsNSamples, dependentVariablesHatTypes, hingeThreshold, independentVariables, dependentVariableName, description, arglist)
        %   Function to simulate predictions using one of the nhpp models (it is static so we can simulate data without instantiating a class).  Simulates NHPP 
        %   counts from specified time intervals using the inverse transform method.  Each interval is simulated predictionsNSamples times and the expected interval 
        %   counts are returned in dependentVariablesHat.
        %   Input:
        %     modeltype - Specifies the model to use for generating the predictions
        %     predictionsNSamples - Number of samples to run when generating the predictions
        %     dependentVariablesHatTypes - Vector of t_mcmcBayesDependentVariablesHat that specifies which thetahat to generate predictions for
        %     hingeThreshold - Specifies the hinge threshold for determining the thetaHat edge cases for generating dependentVariablesHat.{hingelo/hingehi}
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayes.numeric objects
        %     dependentVariableName - Name to use for the predictions data
        %     description - Description for the predictions data
        %     arglist - Cell array with the values to use for the model variables when prediction with the model (i.e., these are the thetaHat)
        %   Output:
        %     dependentVariablesHat - Struct with elements for each of the t_mcmcBayesDependentVariablesHat types specified in dependentVariablesHatTypes 
        %     (e.g., the mean and the {hingelo/hingehi} values from the histogram of the predicted values over predictionsNSamples samples)
            
            datavarid=1;
            t=cast(data.indVarsValues{datavarid,1},'double');
            
            depdatavarid=1;
            depVarName=data.depVarsName{depdatavarid};
            depVarDesc=data.depVarsDesc{depdatavarid};
            depVarTransformPre=data.depVarsTfPre(depdatavarid);
            depVarTransformPost=data.depVarsTfPost(depdatavarid);
            
            numdatapoints=numel(t);                        
            numintervals=numel(t)-1;                        
            x=0:1:predictionsIntervalNmax;
            f=zeros(numintervals,numel(x));
            F=zeros(numintervals,numel(x));
            for datapoint=2:numdatapoints
                S=t(datapoint-1);
                T=t(datapoint);
                CIF_S=cast(predictionsfunctionHandleCif(S,varargin{:}),'double');
                CIF_T=cast(predictionsfunctionHandleCif(T,varargin{:}),'double');
                for j=1:numel(x)
                    f(datapoint-1,j)=(((CIF_T-CIF_S)^cast(x(j),'double'))/(factorial(cast(x(j),'double'))))*exp(-(CIF_T-CIF_S));%calculate the PMF for each interval
                end                
                F(datapoint-1,:)=cumsum(f(datapoint-1,:));%estimate the CDF using the cumulative sum of the interval PMFS
            end
            % uncomment the below lines to see plots of interval pmfs and cdfs
%             fig;
%             hold on;
%             stem(x,f(1,:),'r','Marker','none')
%             stem(x,f(2,:),'c','Marker','none')
%             stem(x,f(3,:),'b','Marker','none')
%             stem(x,f(4,:),'y','Marker','none')
%             stem(x,f(5,:),'g','Marker','none')
%             title('PMFs');
%             hold off;
%             fig;
%             hold on;
%             stem(x,F(1,:),'r','Marker','none')
%             stem(x,F(2,:),'c','Marker','none')
%             stem(x,F(3,:),'b','Marker','none')
%             stem(x,F(4,:),'y','Marker','none')
%             stem(x,F(5,:),'g','Marker','none')
%             title('CDFs');
%             hold off;
            interval_counts=zeros(numintervals, predictionsNSamples);
            Nhat=zeros(numintervals, predictionsNSamples);
            warned=false;
            
            for col=1:predictionsNSamples
                for interval=1:numintervals%This inner loop implements the inverse transform sampling method
                    j=1;
                    found=false;
                    U=rand;%Generate a [0,1] uniform r.v.
                    while (found==false)
                        if (U>F(interval,j))
                            if (j==numel(F(interval,:)))
                                j=1;
                                U=rand;                                
                                if warned==false %problem if we're here, warn the user
                                    disp('Problem, uniform rand() is greater than the values in the cdf (i.e., cdf maximum is <1).  Therefore, increase predictionsIntervalNmax!');
                                    warned=true;
                                end
                            else
                                j=j+1;
                            end
                            continue;
                        else
                            found=true;
                            break;
                        end
                    end
                    interval_counts(interval,col)=x(j);
                end
                Nhat(:,col)=cumsum(interval_counts(:,col));%combine the interval counts to a cumulative set
                pause(0);%helps ctrl-c response
                drawnow;%Force display to update immediately
            end
            
            %reshape Nhat with N(t=0)=0
            tNhat=zeros(numdatapoints,predictionsNSamples);
            tNhat(2:end,:)=Nhat;
            Nhat=tNhat;

            binprec=1;
            bins=0:binprec:ceil(max(max(Nhat)));
            depVarsValuesHat=mcmcBayes.model.capturePredictionThresholds(predictionsNSamples, predictionsToCapture, hingeThreshold, bins, ...
                mcmcBayes.t_numericDimensions.vector, Nhat, data.depVarsType{depdatavarid});
            retNhat=Nhat;
        end                        
    end
    
    methods
        function obj=NHPP(settype, setsubType, setname, setdescription, setauthor, setreference, setprefix, setcomputemsfe)
        % obj=NHPP(setmodeltype, setpriordatatype, setdatatype, setsamplerinterface, setosInterface)
        %   The constructor for NHPP.
        % Input:
        %   setmodeltype - Specifies the type of the sub-model being instantiated
        %   setpriordatatype - t_mcmcBayesData type that specifies the priorDependentVariables (e.g., CCMPerformanceWeighting, EqualWeighting, Individual, and Simulated)
        %   setdatatype - t_mcmcBayesData type that specifies dependentVariables (e.g., ConFR, LinIFR, ExpIFR, LinDFR, ExpDFR, and Simulated)
        %   setdatasetid - Specifies the dataset id to load for this model evaluation
        %   setsamplerinterface - instanted sampler interface, mcmcBayesSamplerInterfaces class (e.g., mcmcBayesSamplerInterfacesOpenBUGS or mcmcBayesSamplerInterfacesJAGS)
        %   setosInterface - instantiated os interface, mcmcBayesosInterfaces class (e.g., mcmcBayesosInterfaces_UbuntuLinux or mcmcBayesosInterfaces_Windows7)
        % Output:
        %   obj Constructed NHPP object
            if nargin == 0
                superargs={};
            else
                superargs{1}=settype;
                superargs{2}=setsubType;
                superargs{3}=setname;
                superargs{4}=setdescription;
                superargs{5}=setauthor;
                superargs{6}=setreference;
                superargs{7}=setprefix;
                superargs{8}=setcomputemsfe;
            end

            obj=obj@mcmcBayes.model(superargs{:});
        end
        
        %override the mcmcBayes.model.generateTestData (due to KG model)
        function [retPredictedData, retNhat]=generatePredictions(obj, chainid, tempid)
        % [retmcmcBayesDependentHat]=generatePredictions(obj, tempid)
        %   Generates model predictions using the model.
        %   Input:
        %     tempid - Temperature id to compute the generate the predictions for
        %   Output:
        %     retmcmcBayesDependentHat Struct with elements for each of the t_mcmcBayesDependentVariablesHat types specified in obj.dependentVariablesHatTypes 
        %     (e.g., the mean and the {hingelo/hingehi} values from the histogram of the predicted values over predictionsNSamples samples)

            %setup the independentVars
            if (tempid==0)
                t=cast(cell2mat(obj.priorPredictedData.mean.indVarsValues(:,1)),'double');%just run simulations for first dataset
            else
                t=cast(cell2mat(obj.posteriorPredictedData.mean.indVarsValues(:,1)),'double');%just run simulations for first dataset
            end
            
            %setup the arglist
            if strcmp(obj.type, 'KuoGhosh') %simNHPP for t_NHPP.KuoGhosh expects arglist=[{t},{Thetahat(t)}]
                if tempid==0
                    switch obj.priorPredictedData.mean.dataSource
                        case mcmcBayes.t_dataSource.PredictedFromSolverPointEstimates
                            arglist=[{t}, {obj.priorVariables(1,chainid,1).distribution.parameters(1).values}];
                        otherwise
                            error('unsupported obj.posteriorPredictionsSource');
                    end
                else
                    switch obj.posteriorPredictedData.mean.dataSource
                        case mcmcBayes.t_dataSource.PredictedFromMCMCMeanPointEstimates
                            arglist=[{t}, {obj.posteriorVariables(1,chainid,tempid).MCMC_mean}];
                        otherwise
                            error('unsupported obj.posteriorPredictionsSource');
                    end
                end
            else %simNHPP for the parametric models expects arglist=[{Thetahat(t)}]
                if tempid==0
                    switch obj.priorPredictedData.mean.dataSource
                        case mcmcBayes.t_dataSource.PredictedFromSolverPointEstimates
                            for varid=1:size(obj.priorVariables,1)
                                if varid==1
                                    arglist=[{obj.priorVariables(varid,chainid,1).distribution.estimateMeanFromHyperparameters()}];
                                else
                                    arglist=[arglist {obj.priorVariables(varid,chainid,1).distribution.estimateMeanFromHyperparameters()}];
                                end
                            end
                        otherwise
                            error('unsupported obj.priorPredictedData.mean.dataSource');
                    end
                else
                    switch obj.posteriorPredictedData.mean.dataSource
                        case mcmcBayes.t_dataSource.PredictedFromMCMCMeanPointEstimates
                            for varid=1:size(obj.posteriorVariables,1)
                                if varid==1
                                    arglist=[{obj.posteriorVariables(varid,chainid,tempid).MCMC_mean}];
                                else
                                    arglist=[arglist {obj.posteriorVariables(varid,chainid,tempid).MCMC_mean}];
                                end
                            end
                        otherwise
                             error('unsupported obj.posteriorPredictedData.mean.dataSource');
                    end
                end
            end
            
            if tempid==0
                [retPredictedData, retNhat]=mcmcBayes.NHPP.simPredictions(obj.type, obj.predictionsNSamples, obj.predictionsIntervalNmax, obj.predictionsfunctionHandleCif, ...
                    obj.predictionsToCapture, obj.predictionsHingeThreshold, obj.priorPredictedData.mean, arglist{:});
            else
                [retPredictedData, retNhat]=mcmcBayes.NHPP.simPredictions(obj.type, obj.predictionsNSamples, obj.predictionsIntervalNmax, obj.predictionsfunctionHandleCif, ...
                    obj.predictionsToCapture, obj.predictionsHingeThreshold, obj.posteriorPredictedData.mean, arglist{:});                
            end
        end
                
        function displayPredictions(obj, tempid)
        % displayPredictions(obj)
        %   Displays the values (in the console) for the already generated model predictions in obj.dependentVariablesHat.
        %   Input:
        %     tempid - Temperature id to compute the display the predictions for            

            stdout=1;
            %print the original data
            datavarid=1;
            if tempid==0
                t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorData, mcmcBayes.t_dataCategory.independent, datavarid);                
                if obj.priorData.dataSource~=mcmcBayes.t_dataSource.Preset %don't print preset, it is empty
                    fprintf(stdout,'t=%s\n',conversionUtilities.matrix_tostring(t));
                    Norig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorData, mcmcBayes.t_dataCategory.dependent, datavarid);
                    fprintf(stdout,'Norig=%s\n',conversionUtilities.matrix_tostring(Norig));
                end
            else
                t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.independent, datavarid);             
                fprintf(stdout,'t=%s\n',conversionUtilities.matrix_tostring(t));
                Norig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.dependent, datavarid);
                fprintf(stdout,'Norig=%s\n',conversionUtilities.matrix_tostring(Norig));
            end
            
            %print the predictions
            for Nhattype=obj.predictionsToCapture%use the vector obj.predictionsToCapture to determine which structure elements from priorPredictedData to show
                datavarid=1;
                if tempid==0
                    t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.independent, datavarid, 'mean');%just use mean (all the same)
                else
                    t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.independent, datavarid, 'mean');%just use mean (all the same)
                end
                switch Nhattype
                    case mcmcBayes.t_dataResult.hingelo
                        if tempid==0
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');
                        else
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');
                        end
                        name='Nhat.hingelo';
                    case mcmcBayes.t_dataResult.mean
                        if tempid==0
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');
                        else
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');
                        end
                        name='Nhat.mean';
                    case mcmcBayes.t_dataResult.hingehi
                        if tempid==0
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');
                        else
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');
                        end
                        name='Nhat.hingehi';
                    otherwise
                        error('Cannot identify the appropriate structure element in obj.predictionsToCapture');
                end
                if Nhattype==mcmcBayes.t_dataResult.mean
                    fprintf(stdout,'t=%s\n',conversionUtilities.matrix_tostring(t));
                    fprintf(stdout,'%s=%s\n',name,conversionUtilities.matrix_tostring(Nhat));
                end
            end
        end
        
        function plotPredictions(obj, chainId, tempId)
        % plotPredictions(obj, tempid)
        %   Plots the model simulation results, using point estimates for Thetahat, over the original data.
        %   Input:
        %     tempid - Temperature id to compute the display the predictions for

            sessiondetails=sprintf('%s-%s-chainid%d-tempid%d',obj.filenamePrefix, obj.uuid, chainId, tempId);            
                  
            xmin=0;
            xmax=0;
            ymin=0;
            ymax=0;           
            
            fig();
            hold on;           
            %plot the original data
            datavarid=1;
            if tempId==0
                t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorData, mcmcBayes.t_dataCategory.independent, datavarid);
                if obj.priorData.dataSource~=mcmcBayes.t_dataSource.Preset
                    Norig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorData, mcmcBayes.t_dataCategory.dependent, datavarid);
                end
            else
                t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.independent, datavarid);
                Norig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.dependent, datavarid);  
            end
            if (tempId==0)
                if exist('Norig','var')
                    ymin=min([ymin min(Norig)]);%could be < 0
                    ymax=max([ymax max(Norig)]);%shouldn't be < 0
                    plot(t,Norig,'g*');
                end
            else
                ymin=min([ymin min(Norig)]);%could be < 0
                ymax=max([ymax max(Norig)]);%shouldn't be < 0
                plot(t,Norig,'r*');
            end
            %do the scaling after computing it (otherwise, if it is in a loop, it will compound on the scaling)
            ymax=1.1*ymax;
            if (ymin==ymax)%avoid the case when they're both 0
                ymax=ymin+1;
            end
            
            %plot the predicted data
            datavarid=1;
            if (tempId==0)
                t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.independent, datavarid, 'mean');%just use mean (all the same)
            else
                t=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.independent, datavarid, 'mean');%just use mean (all the same)
            end
            xmin=min([xmin min(t)]);%could be < 0
            xmax=max([xmax max(t)]);%shouldn't be < 0            
            for predictionToCapture=obj.predictionsToCapture%use the vector obj.dependentVariablesHatTypes to determine which rows from dependentVariablesHat to show            
                if (tempId==0)
                    switch predictionToCapture
                        case mcmcBayes.t_dataResult.hingelo
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');
                            plot(t, Nhat, 'g--');%lo bound dashed plot line
                        case mcmcBayes.t_dataResult.mean
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');
                            plot(t, Nhat, 'g-');%mean solid plot line
                        case mcmcBayes.t_dataResult.hingehi
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');
                            plot(t, Nhat, 'g-.');%hi bound dashed plot line
                        otherwise
                            %won't get here
                    end
                    ymin=min([ymin min(Nhat)]);%could be < 0
                    ymax=max([ymax max(Nhat)]);%shouldn't be < 0
                else
                    switch predictionToCapture
                        case mcmcBayes.t_dataResult.hingelo
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');
                            plot(t, Nhat, 'r--');%lo bound dashed plot line
                        case mcmcBayes.t_dataResult.mean
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');
                            plot(t, Nhat, 'r-');%mean solid plot line
                        case mcmcBayes.t_dataResult.hingehi
                            Nhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');
                            plot(t, Nhat, 'r-.');%hi bound dashed plot line
                        otherwise
                            error('Cannot identify the appropriate structure in obj.posteriorPredictedData');
                    end
                end
            end                 
            
            set(gca,'XTickMode','manual');
            set(gca,'xTick',[]);
            set(gca,'xTick',t);
            %do the scaling after computing it (otherwise, if it is in a loop, it will compound on the scaling)
            if (ymin<0)
                ymin=1.1*ymin;
            else
                ymin=0.9*ymin;
            end
            ymax=1.1*ymax;
            if (ymin==ymax)%avoid the case when they're both 0
                ymax=ymin+1;
            end
            set(gca,'xLim',[xmin xmax]);
            set(gca,'yLim',[ymin ymax]);
            tempstr=sprintf('Model prediction results for %s, %s', obj.name, sessiondetails);
            title(tempstr);
            filename=sprintf('%s%s-PredictionsSet.fig',obj.osInterface.figuresDir,sessiondetails);%need _PredictionsSet for filename parsing
            savefig(gcf,filename);
            
            hold off;
        end                
    end

    methods (Access=protected)
        function [status]=checkDataCompatibility(obj, datatype)
        % [status]=checkPriorDepVarDataCompatibility(obj, priorDependentVariables)
        %   Checks the compatibility of priorDependentVariables with the specified NHPP type

            switch datatype
                case mcmcBayes.t_data.priordata
                	status=obj.priorData.checkDataForInfNanValues();
                    switch obj.modeltype
                        case {t_NHPP.BrooksMotley, t_NHPP.GoelOkumoto, t_NHPP.Goel, ...
                                t_NHPP.MusaOkumoto, t_NHPP.YamadaOhbaOsaki}
                            %no special checks
                        case t_NHPP.KuoGhosh
                            %all intervals must be >0
                            status = ~status && obj.priorData.checkDepVarForZeroIntervals(); 
                        otherwise
                            error('Unsupported t_mcmcBayes type')
                    end                         
                case mcmcBayes.t_data.priorbasedpredictions
                	status=obj.priorPredictedData.checkDataForInfNanValues();
                case mcmcBayes.t_data.posteriordata
                	status=obj.posteriorData.checkDataForInfNanValues();
                    switch obj.modeltype
                        case {t_NHPP.BrooksMotley, t_NHPP.GoelOkumoto, t_NHPP.Goel, ...
                                t_NHPP.MusaOkumoto, t_NHPP.YamadaOhbaOsaki}
                            %no special checks
                        case t_NHPP.KuoGhosh
                            %all intervals must be >0
                            status = ~status && obj.priorData.checkDepVarForZeroIntervals(); 
                        otherwise
                            error('Unsupported t_mcmcBayes type')
                    end                         
                case mcmcBayes.t_data.posteriorbasedpredictions
                	status=obj.posteriorPredictedData.checkDataForInfNanValues();
            end                       
        end
    end
    
    methods %getter/setter functions
                        
    end
end