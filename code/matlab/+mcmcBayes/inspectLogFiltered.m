function inspectLogFiltered(filterspec)
% inspectLogFiltered(filterspec)
%   Prompts the user with a log file selection dialog that is filtered per filterspec.
%   Input:
%     filterspec - Specifies the filter for log files
    logdir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.logsDir);
    filterspec=sprintf('%s%s',logdir,filterspec);
    [filename, pathname]=uigetfile(filterspec,'Open simulation log file','MultiSelect','on');
    if isa(filename,'cell')
        for index=1:size(filename,2)
            filestr=mcmcBayes.osInterfaces.convPathStr(strcat(pathname,filename{index}));
            edit(filestr);
        end
    else
        if ~filename==0
            filestr=mcmcBayes.osInterfaces.convPathStr(strcat(pathname,filename));
            edit(filestr);
        else
            disp('No file selected.');                    
        end
    end
end