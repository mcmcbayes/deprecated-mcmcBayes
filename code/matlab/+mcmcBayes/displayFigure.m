function displayFigure()
% displayFigure(modeltype)
%   Prompts the user with a figure file selection dialog.
%   Input:
    filterspec=sprintf('*.fig');
    mcmcBayes.displayFigureFiltered(filterspec);
end