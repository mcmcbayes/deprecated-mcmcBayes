classdef KuoGhosh < mcmcBayes.NHPP
    methods(Access = public, Static)
        function test()
            display('Hello!');
        end
        
        function retData=transformDataPre(vars, data)
            retData=data;%no special transforms for this model (everything handled by mcmcBayes.data.constructData())
        end
                
        function [solverInits]=determineSolverInits(vars, data)
            %needs to return a cell array due to some models having different sized variables
            if (data.dataSource==mcmcBayes.t_dataSource.Preset)
                %Initial values are Lambdap(t) for the prior (in the case of preset)
                solverInits=[{vars(1).distribution.parameters(1).values} {vars(2).distribution.parameters(1).values}];
            else
                %Initial values are the average of the data
                depVarsValues=data.depVarsValues;
                %Solve for zeta given N(t=end)=u*zeta*t
                thetahat=cast(depVarsValues{1,1},'double');
                solverInits=[{mean(thetahat,2)} {vars(2).distribution.parameters(1).values}];%row vector mean
            end
        end        
        
        function [thetahat]=SolverMeanCustom2(runPowerPosteriorMCMCSequence, subtype, vars, data) 
            %needs to return a cell array due to some models having different sized variables
            thetahat=mcmcBayes.KuoGhosh.determineSolverInits(vars, data);%identical to determineSolverInits
        end
    end

    methods(Access = protected, Static)                              
        function [fhandle]=get_fhandle_CIF()
        % [fhandle]=get_fhandle_CIF()
        %   Provides the cumulative intensity function for the mcmcBayesModelNhpp.generatePredictions function
        %   Output:
        %     fhandle - returns the handle to the equation for the CIF
            fhandle=@(t,t_,ExpectedNt_) cast(ExpectedNt_(find(t_==t)),'double');
        end        
    end

    methods(Access = private, Static)
        function [retvalue]=minimizingFunction(x,t,rIndices,rdata,c)            
            %Construct the PMF            
            a=rdata*x(1);%we are solving for a/global_data=x(1)
            b=1/c;%convert rate to scale
            [pmf]=mcmcBayes.KuoGhosh.estimatePMF(a, b, t, rIndices);

            E_f=0;%expected value for a discrete rv is sum(f_x(i)*x(i))
            for Sn_index=1:numel(rIndices)%Nindices is 0:n
                Sn=cast(rIndices(Sn_index),'double');
                E_f=E_f+Sn*pmf(Sn_index);%increment the expected value sum
            end           
            
            %compute and return the difference between E_f and the data
            retvalue=E_f-rdata;
        end
               
        function [pmf]=estimatePMF(a, b, t, rIndicesVector)
            %returns a col vector with the estimated pmf
            %a is the shape parameter for the gamma distributed arrivals
            %b is the scale parameter for the gamma distributed arrivals
            %t is the time to compute the pmf up to
            %rIndicesVector is an array of 0:step 1:N, having the values we're going to estimate the pmf for
            F=zeros(numel(rIndicesVector),1);
            pmf=zeros(numel(rIndicesVector),1);
            for Sn_index=1:numel(rIndicesVector)%Nindices is 0:n
                Sn=cast(rIndicesVector(Sn_index),'double');
                if Sn==0%Sn=0 is incompatible with the Gamma process!
                    pmf(Sn_index)=0;
                elseif Sn==1
                    %compute Pr(r>0) and Pr(r<1)
                    a2=a*(Sn)/b;
                    pd2=makedist('Gamma',a2,b);
                    F(Sn_index)=cdf(pd2,t);%{S1<=t}={r(t)>=1}
                    pmf(Sn_index)=1-F(Sn_index);%{r(t)=0}=1-{r(t)>=1}
                    if pmf(Sn_index)<1.0E-99%bugs limitation
                        pmf(Sn_index)=0;
                    end
                else%Sn>1
                    %for MATLAB's form of gamma dist, we want the mean for the interval to be E[pd(t)]=a*b, so if
                    %E[pd(t=10)]=(Lambdap(rindex+1)-Lambdap(rindex)), then a=(Lambdap(rindex+1)-Lambdap(rindex))/b
                    a1=a*(Sn-1)/b;
                    pd1=makedist('Gamma',a1,b);
                    a2=a*(Sn)/b;
                    pd2=makedist('Gamma',a2,b);
                    F(Sn_index)=cdf(pd1,t);%{Sn<=t}={r(t)>=n}
                    F(Sn_index+1)=cdf(pd2,t);%{Sn+1<=t}={r(t)>=n+1}
                    pmf(Sn_index)=F(Sn_index)-F(Sn_index+1);%{r(t)=n}={r(t)>=n}-{r(t)>=n+1}
                    if pmf(Sn_index)<1.0E-99%bugs limitation
                        pmf(Sn_index)=0;
                    end                    
                end
            end   
            pmf=pmf';
        end
    end
    
    methods 
        function obj=KuoGhosh(setsimulationStruct, setprefix, setcomputemsfe)
        % KuoGhosh(setsimulationStruct, setprefix, setcomputemsfe)
        %   Constructor for the KuoGhosh class
        %   Input:
        %     setsimulationStruct - 
        %     setprefix -    
        %   Output:
        %     obj - instantiated KuoGhosh object
            if nargin == 0
                superargs={};
            else
                switch setsimulationStruct.subType
                    case 'a'
                        %disp('Kuo-Ghosh NHPP model is using modelsubtype a (Theta(t)~GAMMA(Lambda(t)*c,c))');
                    otherwise
                        error('Unsupported subtype');
                end
                superargs{1}='KuoGhosh';
                superargs{2}=setsimulationStruct.subType;
                superargs{3}='Kuo-Ghosh non-parametric NHPP';
                superargs{4}='Gamma process failure rate';
                superargs{5}='Kuo, Ghosh 2001, 1997';
                superargs{6}='reuben@reubenjohnston.com';
                superargs{7}=setprefix;
                superargs{8}=setcomputemsfe;
            end
            
            obj=obj@mcmcBayes.NHPP(superargs{:});
            
            if nargin>0 %i am not sure why, but it is necessary to set these when loading
                obj.predictionsNSamples=mcmcBayes.NHPP.getDefaultNSamples();
                obj.predictionsIntervalNmax=mcmcBayes.NHPP.getDefaultPredictionsIntervalNmax();
                obj.predictionsfunctionHandleCif=mcmcBayes.KuoGhosh.get_fhandle_CIF; 
            end
        end
        
        %Augment the hyperparameters set by using the hyperparameters lambdap, and c to create, as well as include, rIndices and 
        %  r_ (pmf corresponding to each index in rIndices) in the hyperparameters.
        %Convert Theta(t) to r(t) name and initials form; for the latter, convert Theta(t) into corresponding r(t) intervals and use 
        %  r_(i)=find(rIndices==r(i)).
        %rIndices is the vector of realistically achievable values for r while r_ is the corresponding pmf for each of these values.  
        %Estimation of r_ is very slow, so we should only perform it once.  Therefore, on the first call, cache the answer and, for any 
        %  subsequent calls, use the cached values
        function [retObj]=transformVarsPre(obj, chainID, tempID)
        %needs to be performed on all vars
        %this function simply adjusts the hyperparameters, for tempID==0,retObj.powerPosteriorN+1
            stdout=1;
            retObj=obj;
            if (tempID==0)
                %retObj.posteriorVariables(1,chainID,tempID)
                varID=1;                
                setParameters=retObj.priorVariables(varID,chainID).distribution.parameters;         
                paramID=1;%Lambdap(t)
                sizer=size(setParameters(paramID).values,1)-1;            
                t=retObj.priorVariables(varID,chainID).indVarsValues{:};%stochastic process has indVarsValues
                Lambdap=setParameters(1).values;
                c=setParameters(2).values;
                maxN=0;
                numdatapoints=size(retObj.priorData.depVarsValues{1,1},2);
                for datapointid=1:numdatapoints
                    tvec=retObj.priorData.depVarsValues{1,1}(:,datapointid);
                    if max(tvec)>maxN
                        maxN=max(tvec);
                    end
                end
                varID=2;
                rIndicesMax=retObj.priorVariables(varID,chainID).distribution.parameters(1).values;%don't use setParameters here       
                rIndices=int32([0:1:round(rIndicesMax)])';
                
                options = statset('MaxIter',800, 'MaxFunEvals',1200);

                for i=1:sizer %Construct the PMF
                    statusstr=sprintf('For modelid8, constructing the prior distribution''s PMF for interval %d.', i);
                    setDesktopStatus(statusstr)   

                    %Estimate the best value to use for a
                    rdata=Lambdap(i+1)-Lambdap(i);        
                    rt=t(i+1)-t(i);
                    varsDefaultInits=[.1];%default value for a
                    varsLowerBounds=[1.0E-15];
                    varsUpperBounds=[100];  %max value for a (was 10, how did I choose this?)                         
                    if (rdata==0)
                        error('Zero intervals for this model are not allowed!');
                    end

                    fhandle=@(x) mcmcBayes.KuoGhosh.minimizingFunction(x,rt,rIndices,rdata,c);

                    [thetahat res] = lsqnonlin(fhandle,varsDefaultInits,varsLowerBounds, varsUpperBounds);

                    %Generate the pmfs for each of the intervals
                    b=1/c;%convert rate to scale
                    [pr_r(i,:)]=mcmcBayes.KuoGhosh.estimatePMF(rdata*thetahat, b, rt, rIndices);

                    fig;
                    bar(rIndices,pr_r(i,:));%comment out when not debugging

                    fprintf(stdout,'sum(pmf(%d,:))=%f',i,sum(pr_r(i,:)));
                    if abs(sum(pr_r(i,:))-1)>eps(1000)
                        warning('If using OpenBUGS, the PMF will not be auto-normalized.  Try increasing rIndicesMax.');
                    end
                    
    %Leave the below in to prevent encountering issue described!
    %if Lambdap is significantly lower than the data values, the approach below will be problematic as it will clip Nindices and pmf too small                
    %                 if numel(find(abs(cumsum(pmf(i,:))-1)<=eps(1000)))>0
    %                     newNindicesmax(i)=min(find(abs(cumsum(pmf(i,:))-1)<=eps(1000)));%find the value which the cumulative sum of the pmf is approximately 1
    % and select the minimum of that or x10 of the posterior interval data to clip
    %                 else
    %                     newNindicesmax(i)=numel(Nindices);
    %                 end
    
                    r(i,1)=cast(retObj.priorVariables(1,chainID).MCMC_initvalue{1}(i+1)-retObj.priorVariables(1,chainID).MCMC_initvalue{1}(i),'int32');
                    r_(i,1)=find(rIndices==r(i,1));
                end
                %leave Lambdap and c hyperparameters even though the sampler isn't necessarily using them (this avoids the need to archive them elsewhere)
                setParameters(3,1)=mcmcBayes.hyperparameter('rIndices', 'rIndices is the vector of realistically achievable values for r', ...
                    mcmcBayes.t_numericRange.gteq0ANDltInf, mcmcBayes.t_numericResolution.discrete, rIndices, 'int32', mcmcBayes.t_numericDimensions.vector);  
                setParameters(4,1)=mcmcBayes.hyperparameter('pr_r', 'pr_r is the corresponding pmf for each of these values', ...
                    mcmcBayes.t_numericRange.gteq0ANDltInf, mcmcBayes.t_numericResolution.continuous, pr_r, 'double', mcmcBayes.t_numericDimensions.vector);
                retObj.priorVariables(1,chainID).distribution.parameters=setParameters;
                
                %convert Theta(t) to r_(t) (name and initials)
                retObj.priorVariables(1,chainID).name='r_(t)';
                retObj.priorVariables(1,chainID).MCMC_initvalue={r_};
            else
%                 if retObj
%                 
%             elseif tempID==retObj.powerPosteriorN+1
                %retObj.posteriorVariables(1,chainID,tempID)
                varID=1;                
                setParameters=retObj.posteriorVariables(varID,chainID,tempID).distribution.parameters;         
                paramID=1;%Lambdap(t)
                sizer=size(setParameters(paramID).values,1)-1;            
                t=retObj.posteriorVariables(varID,chainID,tempID).indVarsValues{:};%stochastic process has indVarsValues
                Lambdap=setParameters(1).values;
                c=setParameters(2).values;
                maxN=0;
                numdatapoints=size(retObj.posteriorData.depVarsValues{1,1},2);
                for datapointid=1:numdatapoints
                    tvec=retObj.posteriorData.depVarsValues{1,1}(:,datapointid);
                    if max(tvec)>maxN
                        maxN=max(tvec);
                    end
                end
                varID=2;
                rIndicesMax=retObj.posteriorVariables(varID,chainID,tempID).distribution.parameters(1).values;%don't use setParameters here       
                rIndices=int32([0:1:round(rIndicesMax)])';
                
                options = statset('MaxIter',800, 'MaxFunEvals',1200);

                for i=1:sizer %Construct the PMF
                    statusstr=sprintf('For modelid8, constructing the prior distribution''s PMF for interval %d.', i);
                    setDesktopStatus(statusstr)   

                    %Estimate the best value to use for a
                    rdata=Lambdap(i+1)-Lambdap(i);        
                    rt=t(i+1)-t(i);
                    varsDefaultInits=[.1];%default value for a
                    varsLowerBounds=[1.0E-15];
                    varsUpperBounds=[100];  %max value for a (was 10, how did I choose this?)                         
                    if (rdata==0)
                        error('Zero intervals for this model are not allowed!');
                    end

                    fhandle=@(x) mcmcBayes.KuoGhosh.minimizingFunction(x,rt,rIndices,rdata,c);

                    [thetahat res] = lsqnonlin(fhandle,varsDefaultInits,varsLowerBounds, varsUpperBounds);

                    %Generate the pmfs for each of the intervals
                    b=1/c;%convert rate to scale
                    [pr_r(i,:)]=mcmcBayes.KuoGhosh.estimatePMF(rdata*thetahat, b, rt, rIndices);

                    fig;
                    bar(rIndices,pr_r(i,:));%comment out when not debugging

                    fprintf(stdout,'sum(pmf(%d,:))=%f',i,sum(pr_r(i,:)));
                    if abs(sum(pr_r(i,:))-1)>eps(1000)
                        warning('If using OpenBUGS, the PMF will not be auto-normalized.  Try increasing rIndicesMax.');
                    end
                    
    %Leave the below in to prevent encountering issue described!
    %if Lambdap is significantly lower than the data values, the approach below will be problematic as it will clip Nindices and pmf too small                
    %                 if numel(find(abs(cumsum(pmf(i,:))-1)<=eps(1000)))>0
    %                     newNindicesmax(i)=min(find(abs(cumsum(pmf(i,:))-1)<=eps(1000)));%find the value which the cumulative sum of the pmf is approximately 1
    % and select the minimum of that or x10 of the posterior interval data to clip
    %                 else
    %                     newNindicesmax(i)=numel(Nindices);
    %                 end
    
                    r(i,1)=cast(retObj.posteriorVariables(1,chainID,tempID).MCMC_initvalue{1}(i+1)-retObj.posteriorVariables(1,chainID,tempID).MCMC_initvalue{1}(i),'int32');
                    r_(i,1)=find(rIndices==r(i,1));
                end
                %leave Lambdap and c hyperparameters even though the sampler isn't necessarily using them (this avoids the need to archive them elsewhere)
                setParameters(3,1)=mcmcBayes.hyperparameter('rIndices', 'rIndices is the vector of realistically achievable values for r', ...
                    mcmcBayes.t_numericRange.gteq0ANDltInf, mcmcBayes.t_numericResolution.discrete, rIndices, 'int32', mcmcBayes.t_numericDimensions.scalar);  
                setParameters(4,1)=mcmcBayes.hyperparameter('pr_r', 'pr_r is the corresponding pmf for each of these values', ...
                    mcmcBayes.t_numericRange.gteq0ANDltInf, mcmcBayes.t_numericResolution.continuous, pr_r, 'double', mcmcBayes.t_numericDimensions.vector);
                retObj.posteriorVariables(1,chainID,tempID).distribution.parameters=setParameters;
                
                %convert Theta(t) to r_(t) (name and initials)
                retObj.posteriorVariables(1,chainID,tempID).name='r_(t)';
                retObj.posteriorVariables(1,chainID,tempID).MCMC_initvalue={r_};
            end
        end
        
        %Convert r_(t) to Theta(t) name and initials form; for the latter, convert r_(t) into corresponding r(t) intervals by using 
        %  r(i)=rIndices(r_(i)) and where Theta(t)=cumsum([r(t(1)) r(t(2)) r(t(N))]), for i=1:N datapoints in t.
        function retObj=transformVarsPost(obj, chainID, tempID)
        %Overriding mcmcBayes.model.transformVarsPost()
        %needs to be performed on all vars, simultaneously
        %this function simply adjusts the hyperparameters, for all tempIDs
            retObj=obj;
            datavarid=1;%only one independent/dependent variable
            if (tempID==0)
                r_=retObj.priorVariables(1,chainID).MCMC_samples;
                rIndices=retObj.priorVariables(1,chainID).distribution.parameters(3).values;
                for i=1:size(r_,1)+1
                    if i==1
                        Theta=zeros(size(r_,1)+1,size(r_,2));%Theta will have 1+number of intervals in r
                    else
                        for j=1:size(r_,2)
                            r(i-1,j)=rIndices(r_(i-1,j));
                        end                        
                        Theta(i,:)=r(i-1,:)+Theta(i-1,:);
                    end
                end
                retObj.priorVariables(1,chainID).name='Theta(t)';
                retObj.priorVariables(1,chainID).MCMC_samples=Theta;
                
                %convert the initial values back
                clear r;
                clear r_;
                clear Theta;
                r_=retObj.priorVariables(1,chainID).MCMC_initvalue{:};
                for i=1:size(r_,1)
                    r(i,1)=rIndices(r_(i,1));
                end                
                Theta=[0; cumsum(r)];
                retObj.priorVariables(1,chainID).MCMC_initvalue={Theta};
            else
                %convert the samples back
                r_=retObj.posteriorVariables(1,chainID,tempID).MCMC_samples;
                rIndices=retObj.posteriorVariables(1,chainID,tempID).distribution.parameters(3).values;
                for i=1:size(r_,1)+1
                    if i==1
                        Theta=zeros(size(r_,1)+1,size(r_,2));%Theta will have 1+number of intervals in r
                    else
                        for j=1:size(r_,2)
                            r(i-1,j)=rIndices(r_(i-1,j));
                        end                        
                        Theta(i,:)=r(i-1,:)+Theta(i-1,:);
                    end
                end
                retObj.posteriorVariables(1,chainID,tempID).name='Theta(t)';
                retObj.posteriorVariables(1,chainID,tempID).MCMC_samples=Theta;
                
                %convert the initial values back
                clear r;
                clear r_;
                clear Theta;
                r_=retObj.posteriorVariables(1,chainID,tempID).MCMC_initvalue{:};
                for i=1:size(r_,1)
                    r(i,1)=rIndices(r_(i,1));
                end                
                Theta=[0; cumsum(r)];
                retObj.posteriorVariables(1,chainID,tempID).MCMC_initvalue={Theta};
            end
        end
    end    
end
        
