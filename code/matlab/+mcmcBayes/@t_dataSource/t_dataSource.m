classdef t_dataSource < int32
    enumeration
        None (1) % no data (e.g., when there are all flat priors there would be no priorData or priorPredictedData)
        Preset (2) % specified data (e.g., independent data that corresponds with dependent data for Simulated/Postulated posteriorData or 
                   %   PredictedFromMCMCMean posteriorPredictedData)
        Empirical (3) % empirical data
        Simulated (4) % simulated data generated using specified values for model parameters (to test model fitment results regenerate the specified values)
        Postulated (5) % postulated dataset generated to mimic empirical data of a particular pattern
        EJCCMPerformanceWeighting (20)  % prior data from expert judgment derived aggregate based on Cooke classical model performance weights
        EJEqualWeighting (21)  % prior data from expert judgment derived aggregate based on equal weights
        EJIndividual (22) % prior data from expert judgment individual datasets
        EJIndividualCleansed (23) % prior data from expert judgment individual datasets who met calibration threshold
        PredictedFromSolverPointEstimates (30) % predicted data simulated using MCMC sample mean values for model parameters
        PredictedFromMCMCMeanPointEstimates (31) % predicted data simulated using MCMC sample mean values for model parameters
    end
    
    methods (Static)        
        function intvalue=toint(enumobj)
            switch enumobj          
                case {mcmcBayes.t_dataSource.None,mcmcBayes.t_dataSource.Preset,mcmcBayes.t_dataSource.Empirical,mcmcBayes.t_dataSource.Simulated,...
                      mcmcBayes.t_dataSource.Postulated, mcmcBayes.t_dataSource.EJCCMPerformanceWeighting,mcmcBayes.t_dataSource.EJEqualWeighting,...
                      mcmcBayes.t_dataSource.EJIndividual,mcmcBayes.t_dataSource.EJIndividualCleansed, ...
                      mcmcBayes.t_dataSource.PredictedFromSolverPointEstimates, mcmcBayes.t_dataSource.PredictedFromMCMCMeanPointEstimates}
                    intvalue=int32(enumobj);                    
                otherwise
                    error('unsupported mcmcBayes.t_dataSource type');
            end                
        end        
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_dataSource.None
                    strvalue='mcmcBayes.t_dataSource.None';
                case mcmcBayes.t_dataSource.Preset
                    strvalue='mcmcBayes.t_dataSource.Preset';
                case mcmcBayes.t_dataSource.Empirical
                    strvalue='mcmcBayes.t_dataSource.Empirical';
                case mcmcBayes.t_dataSource.Simulated
                    strvalue='mcmcBayes.t_dataSource.Simulated';
                case mcmcBayes.t_dataSource.Postulated
                    strvalue='mcmcBayes.t_dataSource.Postulated';
                case mcmcBayes.t_dataSource.EJCCMPerformanceWeighting
                    strvalue='mcmcBayes.t_dataSource.EJCCMPerformanceWeighting';
                case mcmcBayes.t_dataSource.EJEqualWeighting
                    strvalue='mcmcBayes.t_dataSource.EJEqualWeighting';
                case mcmcBayes.t_dataSource.EJIndividual
                    strvalue='mcmcBayes.t_dataSource.EJIndividual';
                case mcmcBayes.t_dataSource.EJIndividualCleansed
                    strvalue='mcmcBayes.t_dataSource.EJIndividualCleansed';
                case mcmcBayes.t_dataSource.PredictedFromSolverPointEstimates
                    strvalue='mcmcBayes.t_dataSource.PredictedFromSolverPointEstimates';
                case mcmcBayes.t_dataSource.PredictedFromMCMCMeanPointEstimates
                    strvalue='mcmcBayes.t_dataSource.PredictedFromMCMCMeanPointEstimates';
                otherwise
                    error('unsupported mcmcBayes.t_dataSource type');
            end
        end
        
        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_dataSource.None)
                    enumobj=mcmcBayes.t_dataSource.None;
                case int32(mcmcBayes.t_dataSource.Preset)
                    enumobj=mcmcBayes.t_dataSource.Preset;
                case int32(mcmcBayes.t_dataSource.Empirical)
                    enumobj=mcmcBayes.t_dataSource.Empirical;
                case int32(mcmcBayes.t_dataSource.Simulated)
                    enumobj=mcmcBayes.t_dataSource.Simulated;
                case int32(mcmcBayes.t_dataSource.Postulated)
                    enumobj=mcmcBayes.t_dataSource.Postulated;                    
                case int32(mcmcBayes.t_dataSource.EJCCMPerformanceWeighting)
                    enumobj=mcmcBayes.t_dataSource.EJCCMPerformanceWeighting;                    
                case int32(mcmcBayes.t_dataSource.EJEqualWeighting)
                    enumobj=mcmcBayes.t_dataSource.EJEqualWeighting;                    
                case int32(mcmcBayes.t_dataSource.EJIndividual)
                    enumobj=mcmcBayes.t_dataSource.EJIndividual;                    
                case int32(mcmcBayes.t_dataSource.EJIndividualCleansed)
                    enumobj=mcmcBayes.t_dataSource.EJIndividualCleansed;                    
                case int32(mcmcBayes.t_dataSource.PredictedFromSolverPointEstimates)
                    enumobj=mcmcBayes.t_dataSource.PredictedFromSolverPointEstimates;                    
                case int32(mcmcBayes.t_dataSource.PredictedFromMCMCMeanPointEstimates)
                    enumobj=mcmcBayes.t_dataSource.PredictedFromMCMCMeanPointEstimates;                    
                otherwise
                    error('unsupported mcmcBayes.t_dataSource');                    
            end
        end
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_dataSource.None')==1
                enumobj=mcmcBayes.t_dataSource.None;
            elseif strcmp(strval,'mcmcBayes.t_dataSource.Preset')==1
                enumobj=mcmcBayes.t_dataSource.Preset;
            elseif strcmp(strval,'mcmcBayes.t_dataSource.Empirical')==1
                enumobj=mcmcBayes.t_dataSource.Empirical;
            elseif strcmp(strval,'mcmcBayes.t_dataSource.Simulated')==1
                enumobj=mcmcBayes.t_dataSource.Simulated;
            elseif strcmp(strval,'mcmcBayes.t_dataSource.Postulated')==1
                enumobj=mcmcBayes.t_dataSource.Postulated;                
            elseif strcmp(strval,'mcmcBayes.t_dataSource.EJCCMPerformanceWeighting')==1
                enumobj=mcmcBayes.t_dataSource.EJCCMPerformanceWeighting;
            elseif strcmp(strval,'mcmcBayes.t_dataSource.EJEqualWeighting')==1
                enumobj=mcmcBayes.t_dataSource.EJEqualWeighting;
            elseif strcmp(strval,'mcmcBayes.t_dataSource.EJIndividual')==1
                enumobj=mcmcBayes.t_dataSource.EJIndividual;
            elseif strcmp(strval,'mcmcBayes.t_dataSource.EJIndividualCleansed')==1
                enumobj=mcmcBayes.t_dataSource.EJIndividualCleansed;               
            elseif strcmp(strval,'mcmcBayes.t_dataSource.PredictedFromSolverPointEstimates')==1
                enumobj=mcmcBayes.t_dataSource.PredictedFromSolverPointEstimates;
            elseif strcmp(strval,'mcmcBayes.t_dataSource.PredictedFromMCMCMeanPointEstimates')==1
                enumobj=mcmcBayes.t_dataSource.PredictedFromMCMCMeanPointEstimates;
            else
                error('unsupported mcmcBayes.t_dataSource');
            end
        end                
    end
end