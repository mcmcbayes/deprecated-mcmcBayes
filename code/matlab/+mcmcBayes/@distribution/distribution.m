%distribution is a class used to store mcmc sampling setup and resulting statistical information values.
%distribution is a value class and is also not an abstract class. 

classdef distribution
    properties
        type%t_model_distribution
        name%name for the distribution
        description%describes the distribution
        category%mcmcBayes.t_distributionCategory
        resolution%mcmcBayes.t_numericResolution   
        range%enum type (e.g., real, natural)
        parameters%model_parameter or model_parameter[] (depends on type)
    end
    
    methods (Static)              
        function [shape]=solve_gamma_shape(mean, rate)
            % SOLVE_GAMMA_SHAPE Approximates Gamma distribution parameters given MEAN and RATE.  One use is for
            %  estimation of prior distribution hyperparameters.  For the Gamma distribution, E[X]~=MEAN=SHAPE/RATE.
            % [SHAPE]=SOLVE_GAMMA_SHAPE(MEAN, RATE)
            % Input:
            %   MEAN Variable's Mean, which approximates E[X]
            %   RATE Gamma distribution RATE parameter
            % Output:
            %   SHAPE Gamma distribution SHAPE parameter
            shape=mean*rate;
        end
        
        function [rate]=solve_gamma_rate(mean, shape)
            % SOLVE_GAMMA_RATE Approximates Gamma distribution parameters given MEAN and SHAPE.  One use is for
            %  estimation of prior distribution hyperparameters.  For the Gamma distribution, E[X]~=MEAN=SHAPE/RATE.
            % [RATE]=SOLVE_GAMMA_RATE(MEAN, SHAPE)
            % Input:
            %   MEAN Variable's Mean, which approximates E[X]
            %   SHAPE Gamma distribution SHAPE parameter
            % Output:
            %   RATE Gamma distribution RATE parameter
            rate=shape/mean;
        end
        
        function [shape1]=solve_beta_shape1(mean, shape2)
            shape1=shape2/(1/mean-1);
        end
        
        function [retstr]=tostring(tdistribution)
            hpid=1;%hyperparameter index for setParameters
            retstr=sprintf('%s(',tdistribution.name);
            for hpid=1:size(tdistribution.parameters,1)
                if numel(tdistribution.parameters(hpid).values)>1
                    retstr=sprintf('%s%s=%s',retstr,tdistribution.parameters(hpid).name,conversionUtilities.matrix_tostring(tdistribution.parameters(hpid).values));
                else
                    retstr=sprintf('%s%s=%s',retstr,tdistribution.parameters(hpid).name,conversionUtilities.scalar_tostring(tdistribution.parameters(hpid).values));
                end
                if hpid==size(tdistribution.parameters,1)
                    retstr=sprintf('%s)',retstr);
                else
                    retstr=sprintf('%s, ',retstr);
                end
                hpid=hpid+1;
            end
        end
    end

    methods
        function obj = distribution(settype,setparameters) %constructor
            if nargin > 0 %necessary condition for creating empty object arrays
                obj.type=settype;
                obj.parameters=setparameters;%depends on type
               
                switch settype                   
                    % discrete Univariate
                    case mcmcBayes.t_distribution.DiscreteConstant
                        obj.name='const';
                        obj.description='x = assumed discrete constant';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.discrete;
                        obj.range=mcmcBayes.t_numericRange.gtNegInfANDltInf;
                    case mcmcBayes.t_distribution.Bernoulli
                        obj.name='dbern';
                        obj.description='r ~ dbern(p)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.discrete;
                        obj.range=mcmcBayes.t_numericRange.eq0OReq1;
                    case mcmcBayes.t_distribution.Binomial
                        obj.name='dbin';
                        obj.description='r ~ dbin(p, n)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.discrete;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.Categorical
                        obj.name='dcat';
                        obj.description='r ~ dcat(p[])';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.discrete;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.Negative_Binomial
                        obj.name='dnegbin';
                        obj.description='x ~ dnegbin(p, r)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.discrete;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.Poisson
                        obj.name='dpois';
                        obj.description='r ~ dpois(lambda)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.discrete;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.Geometric
                        obj.name='dgeom';
                        obj.description='r ~ dgeom(p)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.discrete;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.GeometricAlternate
                        obj.name='dgeom0';
                        obj.description='r ~ dgeom0(p)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.discrete;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.NonCentralHypergeometric
                        obj.name='dhyper';
                        obj.description='x ~ dhyper(n, m, N, psi)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.discrete;
                        obj.range=mcmcBayes.t_numericRange.null;
                        % continuous Univariate
                    case mcmcBayes.t_distribution.ContinuousConstant
                        obj.name='const';
                        obj.description='x = assumed discrete constant';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.gtNegInfANDltInf;
                    case mcmcBayes.t_distribution.Beta
                        obj.name='dbeta';
                        obj.description='p ~ dbeta(a, b)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.gt0ANDlt1;
                    case mcmcBayes.t_distribution.ChiSquared
                        obj.name='dchisqr';
                        obj.description='x ~ dchisqr(k)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.DoubleExponential
                        obj.name='ddexp';
                        obj.description='x ~ ddexp(mu, tau)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.Exponential
                        obj.name='dexp';
                        obj.description='x ~ dexp(lambda)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.Flat
                        obj.name='dflat';
                        obj.description='x ~ dflat()';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.Gamma
                        obj.name='dgamma';
                        obj.description='x ~ dgamma(r, mu)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.gteq0;
                    case mcmcBayes.t_distribution.GeneralizedExtremeValue
                        obj.name='dgev';
                        obj.description='x ~ dgev(mu,sigma,eta)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.GeneralizedF
                        obj.name='df';
                        obj.description='x ~ df(n,m,mu,tau)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.GeneralizedGamma
                        obj.name='dggamma';
                        obj.description='x ~ dggamma(r, mu, beta)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.GeneralizedPareto
                        obj.name='dgpar';
                        obj.description='x ~ dgpar(mu,sigma,eta)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.GenericLogLikelihoodDistribution
                        obj.name='dloglik';
                        obj.description='x ~dloglik(lambda)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.LogNormal
                        obj.name='dlnorm';
                        obj.description='x ~ dlnorm(mu, tau)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.Logistic
                        obj.name='dlogis';
                        obj.description='x ~ dlogis(mu, tau)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.Normal
                        obj.name='dnorm';
                        obj.description='x ~ dnorm(mu, tau)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.gtNegInfANDltInf;
                    case mcmcBayes.t_distribution.TZNormal
                        obj.name='dnorm';
                        obj.description='x ~ dnorm(mu, tau)T(0,)';%syntax is currently identical for present samplers (OpenBUGS, JAGS)
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.gt0ANDltInf;                                                
                    case mcmcBayes.t_distribution.Pareto
                        obj.name='dpar';
                        obj.description='x ~ dpar(alpha, c)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.StudentT
                        obj.name='dt';
                        obj.description='x ~ dt(mu, tau, k)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.Uniform
                        obj.name='dunif';
                        obj.description='x ~ dunif(a, b)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.gtNegInfANDltInf;
                    case mcmcBayes.t_distribution.Weibull
                        obj.name='dweib';
                        obj.description='x ~ dweib(v, lambda)';
                        obj.category=mcmcBayes.t_distributionCategory.Univariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                        % discrete Multivariate
                    case mcmcBayes.t_distribution.Multinomial
                        obj.name='dmulti';
                        obj.description='x[] ~ dmulti(p[], N)';
                        obj.category=mcmcBayes.t_distributionCategory.Multivariate;
                        obj.resolution=mcmcBayes.t_numericResolution.discrete;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.GammaProcess
                        %Not a distribution in JAGS and OpenBUGS but we can approximate it numerically; approximation is not possible in Stan
                        obj.name='GAMMA';
                        obj.description='x[] ~ GAMMA(t, c*Lambdap[t], c)';
                        obj.category=mcmcBayes.t_distributionCategory.Multivariate;
                        obj.resolution=mcmcBayes.t_numericResolution.discrete;
                        obj.range=mcmcBayes.t_numericRange.gteq0;
                        % continuous Multivariate
                    case mcmcBayes.t_distribution.Dirichlet
                        obj.name='ddirich';
                        obj.description='p[] ~ ddirich(alpha[])';
                        obj.category=mcmcBayes.t_distributionCategory.Multivariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.MultivariateNormal
                        obj.name='dmnorm';
                        obj.description='x[] ~ dmnorm(mu[], T[,])';
                        obj.category=mcmcBayes.t_distributionCategory.Multivariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.gtNegInfANDltInf;
                    case mcmcBayes.t_distribution.MultivariateStudentT
                        obj.name='dmt';
                        obj.description='x[] ~ dmt(mu[], T[,], k)';
                        obj.category=mcmcBayes.t_distributionCategory.Multivariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    case mcmcBayes.t_distribution.Wishart
                        obj.name='dwish';
                        obj.description='x[,] ~ dwish(R[,], k)';
                        obj.category=mcmcBayes.t_distributionCategory.Multivariate;
                        obj.resolution=mcmcBayes.t_numericResolution.continuous;
                        obj.range=mcmcBayes.t_numericRange.null;
                    otherwise
                        error('Unsupported type for distribution.');
                end
            end
        end
        
        function retobj = estimateHyperparameters(obj, varMean)
            retobj=obj;
            switch obj.type
                case {mcmcBayes.t_distribution.DiscreteConstant, mcmcBayes.t_distribution.ContinuousConstant}
                    retobj.parameters(1).values = obj.parameters(1).values;                
                case {mcmcBayes.t_distribution.Normal, mcmcBayes.t_distribution.TZNormal}
                    retobj.parameters(1).values = varMean;
                case mcmcBayes.t_distribution.Gamma
                    retobj.parameters(2).values = obj.parameters(1).values/varMean;
                case mcmcBayes.t_distribution.GammaProcess
                    retobj.parameters(1).values = cast(varMean,'int32');
                otherwise
                    error('Unsupported distribution.');
            end
        end
        
        function [mean] = estimateMeanFromHyperparameters(obj)
            switch obj.type
                case {mcmcBayes.t_distribution.Normal, mcmcBayes.t_distribution.LogNormal, mcmcBayes.t_distribution.TZNormal}
                    mean = obj.parameters(1).values;
                case mcmcBayes.t_distribution.Beta
                    mean = obj.parameters(1).values/(obj.parameters(1).values+obj.parameters(2).values);
                case mcmcBayes.t_distribution.Gamma
                    mean = obj.parameters(1).values/obj.parameters(2).values;
                case mcmcBayes.t_distribution.Uniform
                    mean = (obj.parameters(1).values+obj.parameters(2).values)/2;
                case {mcmcBayes.t_distribution.ContinuousConstant, mcmcBayes.t_distribution.DiscreteConstant}
                    mean = obj.parameters(1).values;
                case mcmcBayes.t_distribution.MultivariateNormal
                    mean = obj.parameters(1).values;
                case mcmcBayes.t_distribution.Flat
                    mean = 1;%just need a legal value
                case mcmcBayes.t_distribution.GammaProcess
                    mean = obj.parameters(1).values;%Lambdap
                otherwise
                    error('Unsupported distribution.');
            end
        end
        
        function [samples] = random(obj, rows, cols)           
            switch obj.type
                case mcmcBayes.t_distribution.Gamma
                    shape=obj.parameters(1).values;
                    rate=obj.parameters(2).values;
                    fprintf(1,'Sampling %d,%d vector from Gamma(%f,%f)\n',rows,cols,shape,rate);
                    pd=makedist('Gamma',shape,1/rate);%matlab's form is inverse for rate
                    samples=random(pd,rows,cols);
                case mcmcBayes.t_distribution.Beta
                    shape1=obj.parameters(1).values;
                    shape2=obj.parameters(2).values;
                    fprintf(1,'Sampling %d,%d vector from Beta(%f,%f)\n',rows,cols,shape1,shape2);
                    pd=makedist('Beta',shape1,shape2);%matlab's form is inverse for rate
                    samples=random(pd,rows,cols);                    
                otherwise
                    error('samples for this distribution are not implemented yet.');
            end
        end
        
        function plot(obj,name)
            dataVarID=1;
            dataSetID=1;
            switch obj.type
                case {mcmcBayes.t_distribution.Normal, mcmcBayes.t_distribution.TZNormal}
                    mu=obj.parameters(1).values;%hyperparameter 1 is mean
                    r=obj.parameters(2).values;%hyperparameter 2 is precision
                    minx=mu-3*1/sqrt(r);
                    maxx=mu+3*1/sqrt(r);
                    if obj.type==mcmcBayes.t_distribution.TZNormal
                        minx=0;
                    end
                    x=minx:(maxx-minx)/1000:maxx;
                    Yhat=pdf('Normal',x,mu,1/sqrt(r));
                    titlestr=sprintf('%s~Normal(mu=%f,r=%f)',name,mu,r);
                    fig;
                    plot(x,Yhat);
                    title(titlestr);
                case mcmcBayes.t_distribution.Uniform
                    a=obj.parameters(1).values;%hyperparameter 1 is mean
                    b=obj.parameters(2).values;%hyperparameter 2 is precision
                    minx=a;
                    maxx=b;
                    x=minx:(maxx-minx)/1000:maxx;
                    Yhat=pdf('Uniform',x,a,b);
                    titlestr=sprintf('%s~Uniform(a=%f,b=%f)',name,a,b);
                    fig;
                    plot(x,Yhat);
                    title(titlestr);                       
                case mcmcBayes.t_distribution.MultivariateNormal
                    mu=obj.parameters(1).values;%hyperparameter 1 is mean
                    r=obj.parameters(2).values;%hyperparameter 2 is precision
                    for i=1:size(mu,1)
                        minx(i,1)=mu(i)-3*1/sqrt(r(i,i));
                        maxx(i,1)=mu(i)+3*1/sqrt(r(i,i));
                        x(i,:)=minx(i):(maxx(i)-minx(i))/1000:maxx(i);
                        Yhat(i,:)=pdf('Normal',x(i,:),mu(i),1/sqrt(r(i,i)));
                        titlestr=sprintf('%s(%d)~Normal(mu=%f,r=%f)',name,i,mu,r);
                        fig;
                        plot(x,Yhat(i,:));
                        title(titlestr);
                    end
                case mcmcBayes.t_distribution.Gamma
                    a=obj.parameters(1).values;%hyperparameter 1 is shape
                    b=obj.parameters(2).values;%hyperparameter 2 is rate
                    minx=max(1.0e-10,a/b-3*1/b);%gamma must be >0
                    maxx=a/b+3*1/b;
                    x=minx:(maxx-minx)/1000:maxx;
                    Yhat=pdf('Gamma',x,a,1/b);
                    titlestr=sprintf('%s~Gamma(shape=%f,rate=%f)',name,a,b);
                    fig;
                    plot(x,Yhat);
                    title(titlestr);                    
                case mcmcBayes.t_distribution.GammaProcess
                    warning('Plot Gamma Process prior: todo');
                    return;%return before call to plot
                case {mcmcBayes.t_distribution.ContinuousConstant, mcmcBayes.t_distribution.DiscreteConstant}
                    return;%ignore these
                otherwise
                    error('todo');
            end
        end
    end
    
    methods %getter/setter functions
        function obj = set.type(obj,settype)
            if ~isa(settype,'mcmcBayes.t_distribution')
                error('distribution.type must be type mcmcBayes.t_distribution');
            else
                obj.type = settype;
            end
        end
        
        function value = get.type(obj)
            value=obj.type;
        end
        
        function obj = set.name(obj,setname)
            if ~ischar(setname)
                error('distribution.name must be a char[]');
            else
                obj.name = setname;
            end
        end
        
        function value = get.name(obj)
            value=obj.name;
        end
        
        function obj = set.description(obj,setdescription)
            if ~ischar(setdescription)
                error('distribution.description must be a char[]');
            else
                obj.description = setdescription;
            end
        end
        
        function value = get.description(obj)
            value=obj.description;
        end

        function obj = set.category(obj,setcategory)
            if ~isa(setcategory,'mcmcBayes.t_distributionCategory')
                error('distribution.category must be type mcmcBayes.t_distributionCategory');
            else
                obj.category = setcategory;
            end
        end
        
        function value = get.category(obj)
            value=obj.category;
        end        
        
        function obj = set.resolution(obj,setresolution)
            if ~isa(setresolution,'mcmcBayes.t_numericResolution')
                error('distribution.resolution must be type mcmcBayes.t_numericResolution');
            else
                obj.resolution = setresolution;
            end
        end
        
        function value = get.resolution(obj)
            value=obj.resolution;
        end        
        
        function obj = set.range(obj,setrange)
            if ~isa(setrange,'mcmcBayes.t_numericRange')
                error('distribution.range must be type mcmcBayes.t_numericRange');
            else
                obj.range = setrange;
            end
        end
        
        function value = get.range(obj)
            value=obj.range;
        end           
        
        function obj = set.parameters(obj,setparameters)
            if ~isa(setparameters,'mcmcBayes.hyperparameter') && obj.type~=mcmcBayes.t_distribution.Flat
                error('distribution.parameters must be of type mcmcBayes.hyperparameter or obj.type must be type mcmcBayes.t_distribution.Flat');
            else
                switch obj.type                   
                    % discrete Univariate
                    case mcmcBayes.t_distribution.DiscreteConstant
                        if ~(size(setparameters)==1)
                            warning('Incorrect size for setparameters.');
                        else
%todo: revisit below                            
                            if ~(((setparameters(1).resolution==mcmcBayes.t_numericResolution.discrete) || (setparameters(1).resolution==mcmcBayes.t_numericResolution.continuous)) ...
                                    && ((setparameters(1).range==mcmcBayes.t_numericRange.gtNegInfANDltInf) || (setparameters(1).range==mcmcBayes.t_numericRange.gt0ANDltInf)))
                                 warning('Incorrect resolution or range for setparameters(1).');
                            end
                        end                 
                    case mcmcBayes.t_distribution.Bernoulli
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.Binomial
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.Categorical
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.Negative_Binomial
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.Poisson
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.Geometric
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.GeometricAlternate
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.NonCentralHypergeometric
                        warning('(todo) Not checking setparameters.');                       
                    % continuous Univariate
                    case mcmcBayes.t_distribution.ContinuousConstant
%                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.Beta
                        if ~(size(setparameters,1)==2)
                            warning('Incorrect size for setparameters.');
                        else
                            if ~((setparameters(1).resolution==mcmcBayes.t_numericResolution.continuous) && (setparameters(1).range==mcmcBayes.t_numericRange.gt0))
                                 warning('Incorrect resolution or range for setparameters(1).');
                            end
                            if ~((setparameters(2).resolution==mcmcBayes.t_numericResolution.continuous) && (setparameters(2).range==mcmcBayes.t_numericRange.gt0))
                                 warning('Incorrect resolution or range for setparameters(2).');
                            end                            
                        end                    
                    case mcmcBayes.t_distribution.ChiSquared
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.DoubleExponential
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.Exponential
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.Flat
                        %warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.Gamma
                        if ~(size(setparameters,1)==2)
                            warning('Incorrect size for setparameters.');
                        else
                            if ~((setparameters(1).resolution==mcmcBayes.t_numericResolution.continuous) && (setparameters(1).range==mcmcBayes.t_numericRange.gt0))
                                 warning('Incorrect resolution or range for setparameters(1).');
                            end
                            if ~((setparameters(2).resolution==mcmcBayes.t_numericResolution.continuous) && (setparameters(2).range==mcmcBayes.t_numericRange.gt0))
                                 warning('Incorrect resolution or range for setparameters(2).');
                            end                            
                        end
                    case mcmcBayes.t_distribution.GeneralizedExtremeValue
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.GeneralizedF
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.GeneralizedGamma
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.GeneralizedPareto
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.GenericLogLikelihoodDistribution
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.LogNormal
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.Logistic
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.Normal
                        if ~(size(setparameters,1)==2)
                            warning('Incorrect size for setparameters.');
                        else
                            if ~((setparameters(1).resolution==mcmcBayes.t_numericResolution.continuous) && (setparameters(1).range==mcmcBayes.t_numericRange.gtNegInfANDltInf))
                                 warning('Incorrect resolution or range for setparameters(1).');
                            end
                            if ~((setparameters(2).resolution==mcmcBayes.t_numericResolution.continuous) && (setparameters(2).range==mcmcBayes.t_numericRange.gt0ANDltInf))
                                 warning('Incorrect resolution or range for setparameters(2).');
                            end                            
                        end
                    case mcmcBayes.t_distribution.TZNormal
                        if ~(size(setparameters,1)==2)
                            warning('Incorrect size for setparameters.');
                        else
                            if ~((setparameters(1).resolution==mcmcBayes.t_numericResolution.continuous) && (setparameters(1).range==mcmcBayes.t_numericRange.gt0ANDltInf))
                                 warning('Incorrect resolution or range for setparameters(1).');
                            end
                            if ~((setparameters(2).resolution==mcmcBayes.t_numericResolution.continuous) && (setparameters(2).range==mcmcBayes.t_numericRange.gt0ANDltInf))
                                 warning('Incorrect resolution or range for setparameters(2).');
                            end                            
                        end                            
                    case mcmcBayes.t_distribution.Pareto
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.StudentT
                        warning('(todo) Not checking setparameters.');                       
                    case mcmcBayes.t_distribution.Uniform
                        if ~(size(setparameters,1)==2)
                            warning('Incorrect size for setparameters.');
                        else
                            if ~((setparameters(1).resolution==mcmcBayes.t_numericResolution.continuous) && (setparameters(1).range==mcmcBayes.t_numericRange.gtNegInfANDltInf))
                                 warning('Incorrect resolution or range for setparameters(1).');
                            end
                            if ~((setparameters(2).resolution==mcmcBayes.t_numericResolution.continuous) && (setparameters(2).range==mcmcBayes.t_numericRange.gtNegInfANDltInf))
                                 warning('Incorrect resolution or range for setparameters(2).');
                            end                            
                        end
                    case mcmcBayes.t_distribution.Weibull
                        warning('(todo) Not checking setparameters.');                       
                    % discrete Multivariate
                    case mcmcBayes.t_distribution.Multinomial
                        warning('(todo) Not checking setparameters.');     
                    case mcmcBayes.t_distribution.GammaProcess %(Lambdap[],c)
                        warning('Need to update size check');
                        if ~((size(setparameters,1)==2) || (size(setparameters,1)==4))%size two prior to transformVarsPre, size four afterwards
                            warning('Incorrect size for setparameters.');
                        else
                            if ~((setparameters(1).resolution==mcmcBayes.t_numericResolution.discrete) && (setparameters(1).range==mcmcBayes.t_numericRange.gteq0ANDltInf))
                                warning('Incorrect resolution or range for setparameters(1).');%only allow zero at t=0
                            end
                            if ~((setparameters(2).resolution==mcmcBayes.t_numericResolution.continuous) && (setparameters(2).range==mcmcBayes.t_numericRange.gt0ANDltInf))
                                warning('Incorrect resolution or range for setparameters(2).');
                            end
                        end
                    % continuous Multivariate
                    case mcmcBayes.t_distribution.Dirichlet
                        warning('(todo) Not checking setparameters.');     
                    case mcmcBayes.t_distribution.MultivariateNormal                     
                        if ~(size(setparameters,1)==2)
                            warning('Incorrect size for setparameters.');
                        else
                            if ~((setparameters(1).resolution==mcmcBayes.t_numericResolution.continuous) && (setparameters(1).range==mcmcBayes.t_numericRange.gtNegInfANDltInf))
                                 warning('Incorrect resolution or range for setparameters(1).');
                            end
                            if ~((setparameters(2).resolution==mcmcBayes.t_numericResolution.continuous) && (setparameters(2).range==mcmcBayes.t_numericRange.gteq0ANDltInf))
                                 warning('Incorrect resolution or range for setparameters(2).');
                            end                            
                        end                                                
                    case mcmcBayes.t_distribution.MultivariateStudentT
                        warning('(todo) Not checking setparameters.');     
                    case mcmcBayes.t_distribution.Wishart
                        warning('(todo) Not checking setparameters.');     
                    otherwise
                        error('Unsupported type for model_distribution.');
                end
                obj.parameters=setparameters;
            end
        end
        
        function value = get.parameters(obj)
            value=obj.parameters;
        end               

        %this function is used by mcmcBayesVariable to confirm variable values are valid for the distribution
        function status = checkValue(obj, setvalue)
            status=true;
            if ~iscell(setvalue) && ~isnumeric(setvalue)
                error('distribution::checkValue, setvalue must be a numeric');
            else
                if iscell(setvalue)
                    setvalue=setvalue{:};
                end
                switch obj.resolution
                    case mcmcBayes.t_numericResolution.null
%                         warning('distribution::checkValue, not checking setvalue resolution');
                    case mcmcBayes.t_numericResolution.discrete
                        if ~isinteger(setvalue)%confirm the value is an integer
                            status=false;
                            warning('distribution::checkValue, setvalue is not an integer but the resolution is discrete');
                        end                       
                    case mcmcBayes.t_numericResolution.continuous
                        if ~isa(setvalue,'double')%confirm the value is a double
                            status=false;
                            warning('distribution::checkValue, setvalue is not a double but the resolution is continuous');
                        end
                    otherwise
                        error('distribution::checkValue, invalid type for obj.resolution');
                end
                switch obj.range
                    case mcmcBayes.t_numericRange.null
%                         warning('distribution::checkValue, not checking setvalue range');
                    case mcmcBayes.t_numericRange.gteq0
                        if ~(setvalue>=0)
                            status=false;
                            %warning('distribution value=%f, must be >= 0',setvalue);
                        end
                    case mcmcBayes.t_numericRange.gt0
                        if ~(setvalue>0)
                            status=false;
                            %warning('distribution value=%f, must be > 0',setvalue);
                        end
                    case mcmcBayes.t_numericRange.gt0ANDlt1
                        if ~((setvalue>0) && (setvalue<1))
                            status=false;
                            %warning('distribution value=%f, must be > 0 and < 1',setvalue);
                        end
                    case mcmcBayes.t_numericRange.gt0ANDltInf
                        if ~((setvalue>0) && ~isinf(setvalue))
                            status=false;
                            %warning('distribution value=%f, must be > 0 and < Inf',setvalue);
                        end                        
                    case mcmcBayes.t_numericRange.eq0OReq1
                        if ~((setvalue==0) || (setvalue==1))
                            status=false;
                            %warning('distribution value=%f, must be = 0 or 1',setvalue);
                        end
                    case mcmcBayes.t_numericRange.gtNegInfANDltInf
                        if isinf(setvalue)
                            status=false;
                            %warning('distribution value=%f, setvalue must not be infinite',setvalue);
                        end                        
                    otherwise
                        error('distribution::checkValue, invalid type for obj.range');
                end
            end       
        end      
        
        function status = checkValueArray(obj, setvalueArray)
            status=true;
            if ~isnumeric(setvalueArray)
                error('distribution::checkValue, setvalue must be numeric');
            else
                switch obj.resolution
                    case mcmcBayes.t_numericResolution.Null
%                        warning('distribution::checkValue, not checking setvalueArray resolution');
                    case mcmcBayes.t_numericResolution.discrete
                        for index=1:numel(setvalueArray)
                            if ~isinteger(setvalueArray(index))%confirm the value is an integer
                                status=false;
                            end
                        end
                        if ~status
                            warning('distribution::checkValue, setvalue is not an integer but the resolution is discrete');
                        end                           
                    case mcmcBayes.t_numericResolution.continuous
                        for index=1:numel(setvalueArray)
                            if ~isa(setvalueArray(index),'double')%confirm the value is a double
                                status=false;                                
                            end
                        end
                        if ~status
                            warning('distribution::checkValue, setvalue is not a double but the resolution is continuous');
                        end                        
                    otherwise
                        error('distribution::checkValue, invalid type for obj.resolution');
                end
                switch obj.range
                    case mcmcBayes.t_numericRange.null
%                        warning('distribution::checkValue, not checking setvalue range');
                    case mcmcBayes.t_numericRange.gteq0
                        for index=1:numel(setvalueArray)
                            if ~(setvalueArray(index)>=0)
                                status=false;                               
                            end
                        end
                        if ~status
                            warning('distribution value must be >= 0');
                        end
                    case mcmcBayes.t_numericRange.gt0
                        for index=1:numel(setvalueArray)
                            if ~(setvalueArray(index)>0)
                                status=false;
                            end
                        end
                        if ~status
                            warning('distribution value must be > 0');
                        end
                    case mcmcBayes.t_numericRange.gt0ANDlt1
                        for index=1:numel(setvalueArray)
                            if ~((setvalueArray(index)>0) && (setvalueArray(index)<1))
                                status=false;                                
                            end
                        end
                        if ~status
                            warning('distribution value must be > 0 and < 1');
                        end                          
                    case mcmcBayes.t_numericRange.gt0ANDltInf
                        for index=1:numel(setvalueArray)                        
                            if ~((setvalueArray(index)>0) && ~isinf(setvalueArray(index)))
                                status=false;                              
                            end                              
                        end
                        if ~status
                            warning('distribution value must be > 0 and < Inf');
                        end                          
                    case mcmcBayes.t_numericRange.eq0OReq1
                        for index=1:numel(setvalueArray)                       
                            if ~((setvalueArray(index)==0) || (setvalueArray(index)==1))
                                status=false;
                            end
                        end
                        if ~status
                            warning('distribution::checkValue, setvalue must be = 0 or 1');
                        end                           
                    case mcmcBayes.t_numericRange.gtNegInfANDltInf
                        for index=1:numel(setvalueArray)                       
                            if isinf(setvalueArray(index))
                                status=false;                                
                            end           
                        end
                        if ~status
                            warning('distribution::checkValue, setvalue must not be infinite');
                        end                        
                    otherwise
                        error('distribution::checkValue, invalid type for obj.range');
                end
            end              
        end              
    end
end