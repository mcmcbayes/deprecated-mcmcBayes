classdef model < mcmcBayes.simulation
    % model is a sub class of simulation and contains routines that are generic to the model
    
    methods (Abstract, Static)
        [loglikelihood]=computeLogLikelihood(type, variables, data);
        [retData]=transformDataPre(modelVariables, data);
    end
    
    methods (Abstract)
        displayPredictions(obj, tempid);           
        plotPredictions(obj, tempid);
    end

    methods (Abstract, Access=protected)    
        [status]=checkDataCompatibility(obj, datatype);
    end
    
    methods (Static, Access=protected)
        function [meanVars]=estimateModelVariables(runPowerPosteriorMCMCSequence, pointEstimateSolverType, type, subtype, vars, data)
%todo: needs to be a cell array due to some models having different sized variables
            if pointEstimateSolverType==mcmcBayes.t_variablePointEstimateSolver.SolverMeanAnalytic
                for varid=1:size(vars,1)
                    meanVars(varid,1)={vars(varid,1).distribution.estimateMeanFromHyperparameters()};
                end
            else
                handle=mcmcBayes.simulation.getHandleVariablePointEstimateSolver(type, pointEstimateSolverType);
%todo: needs to be a cell array due to some models having different sized variables
                evalstr=sprintf('[meanVars]=handle(runPowerPosteriorMCMCSequence, subtype, vars, data);');
                eval(evalstr);
            end
        end
    end
    
    methods (Static)
        function test()
            disp('Hello!');
        end
        
        %this is the general form that can be overridden (e.g., for NHPP)
        function [dependentVars]=generateTestData(type, predictionsNSamples, data, varargin)
        % [dependentVariables]=generateTestData(modeltype, predictionsNSamples, independentVariables, dependentVariableName, meanVariableValues)
        %   Generates simulated data to use with model evaluation.
        %   Input:
        %     modeltype - Specifies the model to use for generating the test data
        %     predictionsNSamples - Number of samples to run when generating the test data
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     dependentVariableName - Name to use for the generated test data
        %     thetaHat - Variable point estimates to use in the model for generating the simulated data (mcmcBayesNumeric)
        %   Output:
        %     dependentVariables - Array of mcmcBayesNumeric containing the simulated data
            for varid=1:size(varargin, 2)
                if varid==1
                    paramsstr=sprintf('%f', varargin{varid});
                else
                    paramsstr=sprintf('%s, %f', paramsstr, varargin{varid});
                end
            end
            dispstr=sprintf('Generating simdata using %s(%s)', type, paramsstr);
            disp(dispstr);
 
            handle=mcmcBayes.simulation.getHandleSimPredictions(type);
            evalstr=sprintf('[tempdependentVars]=handle(type, predictionsNSamples, [mcmcBayes.t_dataResult.mean], 0.05, data, varargin{:});');
            eval(evalstr);
            dependentVars = tempdependentVars.mean;   
        end           
            
        function [solverInits]=determineSolverInits(type, vars, data)
        % [solverInits]=determineSolverInits(type, vars, data)
        %   Returns a cell array, one cell per variable, that has the initial values to pass in to the solver
            tData=data;
            %copy the data and revert the data copy back to raw data via getTransformedDataValuesPost
            if size(tData.indVarsValues,1)>0%Some models do not have independent data
                for indvarid=1:size(tData.indVarsValues,1)
                    tData.indVarsValues(indvarid,1)={data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent,indvarid)};
                end
            end
            for depvarid=1:size(tData.depVarsValues,1)
                tData.depVarsValues(depvarid,1)={data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.dependent,depvarid)};
            end          
            handle=mcmcBayes.simulation.getHandleDetermineSolverInits(type);
            evalstr=sprintf('[solverInits]=handle(vars, data);');
            eval(evalstr);            
        end
        
        function [retVars]=determineMCMCInits(runPowerPosteriorMCMCSequence, defaultPointEstimateSolverType, solverOverrides, type, subtype, vars, data)
            stdout=1;

%todo: need to deal with solverOverrides            
            if defaultPointEstimateSolverType==mcmcBayes.t_variablePointEstimateSolver.None && solverOverrides(1)==mcmcBayes.t_variablePointEstimateSolver.None %no solver
                retVars=vars;
                return;
            end

%todo: dataTransformPre will already have occurred here and we must perform initials estimation using raw data to avoid multiple adjustments
%(e.g., adjustment for sampling and for predictions)            
            switch data.dataSource
                case {mcmcBayes.t_dataSource.None, mcmcBayes.t_dataSource.Preset}                   
                    [varPointEstimates]=mcmcBayes.model.estimateModelVariables(runPowerPosteriorMCMCSequence, ...
                        mcmcBayes.t_variablePointEstimateSolver.SolverMeanAnalytic, type, subtype, vars, data);
                    for varid=1:size(vars,1)
                        retVars(varid,1)=vars(varid,1);
                        retVars(varid,1).MCMC_initvalue=varPointEstimates(varid);
                    end
                    fprintf(stdout,'Preset variable''s MCMC sampler initials for the %s model.\n', type);
                otherwise
                    tData=data;
                    %copy the data and revert the data copy back to raw data via getTransformedDataValuesPost
                    if size(tData.indVarsValues,1)>0%Some models do not have independent data
                        for indvarid=1:size(tData.indVarsValues,1)
                            tData.indVarsValues(indvarid,1)={data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent,indvarid)};
                        end
                    end
                    for depvarid=1:size(tData.depVarsValues,1)
                        tData.depVarsValues(depvarid,1)={data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.dependent,depvarid)};
                    end                    
                    
                    [varPointEstimates]=mcmcBayes.model.estimateModelVariables(runPowerPosteriorMCMCSequence, defaultPointEstimateSolverType, type, subtype, ...
                        vars, tData);
                    for varid=1:size(vars,1)%use varPointEstimates as inits for each variable
                        retVars(varid,1)=vars(varid,1);
                        retVars(varid,1).MCMC_initvalue=varPointEstimates(varid);
                        fprintf(stdout,'\tPosterior initial for %s=%s.\n',retVars(varid).name,conversionUtilities.matrix_tostring(retVars(varid).MCMC_initvalue{:}));                        
                    end
                    strSolverType=strrep(mcmcBayes.t_variablePointEstimateSolver.tostring(defaultPointEstimateSolverType),'mcmcBayes.t_variablePointEstimateSolver.','');
                    fprintf(stdout,'Computed variable''s MCMC sampler initials for the %s model using %s.\n', type, strSolverType);                    
            end
        end        
        
        function [retPriorVars]=determinePriorPointEstimates(runPowerPosteriorMCMCSequence, defaultPointEstimateSolverType, solverOverrides, type, ...
                subtype, priorVars, priorData)            
            stdout=1;
            
%todo: need to deal with solverOverrides            
            if defaultPointEstimateSolverType==mcmcBayes.t_variablePointEstimateSolver.None && solverOverrides(1)==mcmcBayes.t_variablePointEstimateSolver.None %no solver
                retPriorVars=priorVars;
                return;
            end

            switch priorData.dataSource
                case {mcmcBayes.t_dataSource.None, mcmcBayes.t_dataSource.Preset}
                    for varid=1:size(priorVars,1)
                        retPriorVars(varid,1)=priorVars(varid,1);
                    end
                    fprintf(stdout,'Preset variable distribution hyperparameters for the %s model.\n', type);
                otherwise
                    %dataTransformPre will already have occurred here and we must perform hyperparameter estimation using raw data to avoid multiple adjustments
                    %(e.g., adjustment for sampling and for predictions)
                    tpriorData=priorData;
                    %copy the data and revert the data copy back to raw data via getTransformedDataValuesPost
                    if size(tpriorData.indVarsValues,1)>0%Some models do not have independent data
                        for indvarid=1:size(tpriorData.indVarsValues,1)
                            tpriorData.indVarsValues(indvarid,1)={priorData.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent,indvarid)};
                        end
                    end
                    for depvarid=1:size(tpriorData.depVarsValues,1)
                        tpriorData.depVarsValues(depvarid,1)={priorData.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.dependent,depvarid)};
                    end
                    
                    %use the now raw data copy for estimation
                    [varPointEstimates]=mcmcBayes.model.estimateModelVariables(runPowerPosteriorMCMCSequence, defaultPointEstimateSolverType, type, ...
                        subtype, priorVars, tpriorData);                    
                    fprintf(stdout,'Distributions are: ');
                    for varid=1:size(priorVars,1)%convert varPointEstimates to hyperparams for each variable
                        if varid>1
                            fprintf(stdout,', ');
                        end
                        retPriorVars(varid,1)=priorVars(varid);
                        retPriorVars(varid,1).distribution=priorVars(varid,1).distribution.estimateHyperparameters(varPointEstimates{varid});
                        fprintf(stdout,'%s~%s', priorVars(varid).name, mcmcBayes.distribution.tostring(retPriorVars(varid,1).distribution));                                            
                    end
                    strSolverType=strrep(mcmcBayes.t_variablePointEstimateSolver.tostring(defaultPointEstimateSolverType),'mcmcBayes.t_variablePointEstimateSolver.','');
                    fprintf(stdout,'.\nComputed variable distribution hyperparameters for the %s model using %s on the prior data.\n', type, strSolverType);                    
            end            
        end                

        function [MSFE]=computeMSFE(data, Yhat)
        % [MSFE]=computeMSFE(dependentVariables, dependentVariablesHat)
        %   Computes the mean square forecasting error of the data given the model dependent variables and predictions.  
        %   Input: 
        %     dependentVariables - Dependent variables for the model that is a cell array of simulationNumeric objects.  
        %     dependentVariablesHat - Predicted dependent variables for the model that is a cell array of simulationNumeric objects.  
        %   Output:
        %     MSFE - Computed mean square forecasting error            
            datSize=size(data.depVarsValues{:});
            defdims=data.depVarsDimensions;
            if defdims==mcmcBayes.t_numericDimensions.scalar
                Tau=datSize(1);%numOrigDataPoints
                R=1;
            elseif defdims==mcmcBayes.t_numericDimensions.vector
                R=datSize(1);%numRows (at some point we may have variables that are different sizes, thus we could need an array for R, one for each variable)
                Tau=datSize(2);%numOrigDataPoints
            else
                error('matrices todo');
            end     
            
            datavarid=1;
            Yorig=cast(data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.dependent, datavarid), 'double');
            
            if defdims==mcmcBayes.t_numericDimensions.scalar%Tau x 1
                N=size(Yhat,1)*size(Yhat,2);
                numDataSets=size(Yorig,1)/size(Yhat,1);
                if (size(Yorig,1)/size(Yhat,1)~=floor(size(Yorig,1)/size(Yhat,1)))
                    warning('Number of predictions is not an even multiple of original data size.');
                end
                sizeDataSet=size(Yhat,1);
                sumofsquares=0;
                for i=1:numDataSets
                    for k=1:sizeDataSet
                        indices(k,1)=sizeDataSet*(i-1)+k;%{1:sizeDataSet};{sizeDataSet+1:sizeDataSet*2};...
                    end
                    sumofsquares=sumofsquares+sum(sum((Yorig(indices)-Yhat).^2));    
                end
                MSFE=(1/N)*sumofsquares;%this is mse(t)                   
            elseif defdims==mcmcBayes.t_numericDimensions.vector % Tau x R
                N=size(Yhat,1)*size(Yhat,2)*size(Yorig,2);
                sumofsquares=0;
                for i=1:size(Yorig,2)
                    sumofsquares=sumofsquares+sum(sum((Yorig(:,i)-Yhat).^2)); 
                end
                MSFE=(1/N)*sumofsquares;%this is mse(t)   
            else
                error('matrices todo');
            end               
        end              
        
        function [depVarsValuesHat]=capturePredictionThresholds(predictionsNSamples, predictionsToCapture, hingeThreshold, bins, dimensions, data, datatype)
            if dimensions==mcmcBayes.t_numericDimensions.scalar
                M=1;
            elseif dimensions==mcmcBayes.t_numericDimensions.vector
                M=size(data,1);
            else
                error('matrix is todo.');             
            end
            
            if predictionsNSamples>1%only compute the histogram when generated predictionsNSamples
                histout=zeros(M,size(bins,2)-1);%need to create a t=zero histogram
                for datapoint=1:M
                    h=figure;%create a figure for the histogram (so it doesn't steal current handle)
                    if M==1
                        thist=histogram(data(:,1), bins);%approximate pdf
                    else
                        thist=histogram(data(datapoint,:), bins);%approximate pdf
                    end
                    histout(datapoint,:)=cumsum((1/cast(predictionsNSamples,'double'))*thist.Values);%approximate cdf
                    close(h);
                end

                for Yhattype=predictionsToCapture()%use the vector obj.dependentVariablesHatTypes to determine which rows from dependentVariablesHat to show
                    for datapoint=1:M
                        switch Yhattype
                            case mcmcBayes.t_dataResult.hingelo %threshold for 5%
                                tdata(datapoint,1)=cast(bins(find(histout(datapoint,:)>hingeThreshold,1,'first')),datatype);
                            case mcmcBayes.t_dataResult.mean %threshold for 50%
                                tdata(datapoint,1)=cast(bins(find(histout(datapoint,:)>.50,1,'first')),datatype);
                            case mcmcBayes.t_dataResult.hingehi %threshold for 95%
                                tdata(datapoint,1)=cast(bins(find(histout(datapoint,:)>(1-hingeThreshold),1,'first')),datatype);
                        end
                    end
                    switch Yhattype
                        case mcmcBayes.t_dataResult.hingelo %threshold for 5%
                            depVarsValuesHat.hingelo={tdata};%need to update all the array values for this struct in one operation
                        case mcmcBayes.t_dataResult.mean %threshold for 50%
                            depVarsValuesHat.mean={tdata};%need to update all the array values for this struct in one operation
                        case mcmcBayes.t_dataResult.hingehi %threshold for 95%
                            depVarsValuesHat.hingehi={tdata};%need to update all the array values for this struct in one operation
                    end
                end
            else%generated 1 sample
                for Yhattype=predictionsToCapture%use the vector obj.dependentVariablesHatTypes to determine which rows from dependentVariablesHat to show
                    switch Yhattype
                        case mcmcBayes.t_dataResult.hingelo %threshold for 5%
                            depVarsValuesHat.hingelo={cast(data,datatype)};         
                        case mcmcBayes.t_dataResult.mean %threshold for 50%
                            depVarsValuesHat.mean={cast(data,datatype)};         
                        case mcmcBayes.t_dataResult.hingehi %threshold for 95%
                            depVarsValuesHat.hingehi={cast(data,datatype)};         
                    end
                end                                      
            end
        end
        
        function openModelPDFs(type, subtype, datasetid, tempid)
        % displayModelPDFs(modeltype, datasetid, tempid)
        %   Displays previously generated approximated probability density functions (i.e., histogram) for the MCMC samples of the model variables.  
        %   Prompts the user with a filtered list of pdf figures (*PDF.fig or *PMF.fig file types).
        %   Input:
        %     modeltype - Specifies the model to filter on
        %     datasetid - Specifies the dataset identifier to filter on
        %     tempid - Specifies the temperature id to filter on
            figuredir=simulationosInterfaces.getPath(t_simulationPaths.FIGURES_DIR);
            if t_simulation.toint(type)==8
                filterspec=sprintf('%smodeltypeid%d%sdatasetid%d*tempid%d*PMF*.fig', figuredir, t_simulation.toint(type), subtype, datasetid, tempid);
            else
                filterspec=sprintf('%smodeltypeid%d%sdatasetid%d*tempid%d*PDF.fig', figuredir, t_simulation.toint(type), subtype, datasetid, tempid);
            end
            [filename, pathname]=uigetfile(filterspec,'Please select simulation *PMF.fig or *PDF.fig','MultiSelect','on');
            if ~isa(filename,'cell')
                if filename==0%user pressed cancel or didn't select any files when filename==0
                    disp('No file selected.');
                    return                
                else
                    colsize=1;%case where there is only one model variable
                end
            else
                colsize=size(filename,2);%case where there are multiple model variables
            end
            [hfigX]=fig();
            ax=hfigX.CurrentAxes;
            for index=1:colsize
                subax(index)=subp(1,colsize,index);
                if colsize==1
                    filestr=simulationosInterfaces.convPathStr(strcat(pathname,filename));
                else
                    filestr=simulationosInterfaces.convPathStr(strcat(pathname,filename{index}));
                end
                set(0,'CurrentFigure',hfigX);
                hfig=openfig(filestr,'invisible');
                axtemp=hfig.CurrentAxes;
                thandles=get(axtemp,'children');%get handle to all the children in the figure
                copyobj(thandles,subax(index));
                close(hfig);       
            end
        end
        
        function openMCMCDiagnostics(type, subtype, datasetid, varid, tempid)
        % displayMCMCDiagnostics(modeltype, datasetid, varid, tempid)
        %   Displays previously generated MCMC diagnostics for the MCMC samples of the model variable specified by varid in one subplot.  
        %   Prompts the user with a filtered list of figures (*ACF.fig, *ErgodicMean.fig, and *RawSamples.fig file types).
        %   Input:
        %     modeltype - Specifies the model to filter on
        %     datasetid - Specifies the dataset identifier to filter on
        %     varid - Specifies the variable id to filter on
        %     tempid - Specifies the temperature id to filter on                                
            figuredir=simulationosInterfaces.getPath(t_simulationPaths.FIGURES_DIR);
            filterspec=sprintf('%smodeltypeid%d%sdatasetid%dvarid%dtempid%d*.fig', figuredir, t_simulation.toint(type), subtype, datasetid, varid, tempid);
            [filename, pathname]=uigetfile(filterspec,'Please select simulation *ACF.fig, *ErgodicMean.fig, and *RawSamples.fig files','MultiSelect','on');
            if size(filename,2)<3
                warning('You did not select three figures (*ACF.fig, *ErgodicMean.fig, and *RawSamples.fig');
            end
            if ~isa(filename,'cell')
                if filename==0%user pressed cancel or didn't select any files when filename==0
                    disp('No file selected.');
                    return                
                else
                    colsize=1;%case where there is only one model variable
                end
            else
                colsize=size(filename,2);%case where there are multiple model variables
            end            
            [hfigX]=fig();
            ax=hfigX.CurrentAxes;
            for index=1:colsize
                switch index
                    case 1%acf
                        figindex=3;
                    case 2%ergodic mean
                        figindex=2;
                    case 3%raw trace
                        figindex=1;
                    otherwise
                        error('too many files, should only be acf, ergodic mean, and raw trace');
                end
                subax3(figindex)=subp(1,3,figindex);
                filestr=simulationosInterfaces.convPathStr(strcat(pathname,filename{index}));
                set(0,'CurrentFigure',hfigX);
                hfig=openfig(filestr,'invisible');
                axtemp=hfig.CurrentAxes;
                thandles=get(axtemp,'children');%get handle to all the children in the figure
                switch figindex %these commands need the copied axes already present
                    case 1
                        title('Raw Samples');
                    case 2
                        title('Ergodic Mean');
                    case 3
                        title('ACF');
                        xlim(subax3(figindex),[0 150]);
                end           
                copyobj(thandles,subax3(figindex));
                close(hfig);                
            end
        end

        function [phi, loglikelihoodMean, loglikelihoodVar, loglikelihoodMCE]=computeLogLikelihoods(osInterface, type, vars, data, chainid, temperatures, printStatus)
        % [phi, loglikelihoodMean, loglikelihoodVar, loglikelihoodMCE]=computeLogLikelihoods(osInterface, type, vars, data, chainid, temperatures, printStatus)
        %   Computes the log-likelihoods of the data given the model, for each of the temperature identifiers and returns an array of its mean, variance, and 
        %   Monte carlo error along with an array containing the corresponding values of phi for each temperature identifier.
            stdout=1;
            powerposteriorN=mcmcBayes.simulation.getDefaultPowerPosteriorN;
            powerposteriorK=mcmcBayes.simulation.getDefaultPowerPosteriorK;            
            phi=zeros(powerposteriorN+1,1);
            loglikelihoodMean=zeros(powerposteriorN+1,1);
            loglikelihoodVar=zeros(powerposteriorN+1,1);
            loglikelihoodMCE=zeros(powerposteriorN+1,1);
            
            for tempid=temperatures
                phi(tempid,1)=mcmcBayes.simulation.getPhi(tempid, powerposteriorN, powerposteriorK);
            end
            
            for tempid=temperatures %Compute the loglikelihoods of each tempid
                if printStatus
                    fprintf(stdout,'Computing likelihood for %s model, chainId=%d, tempId=%d, phi=%e.\n', type, ...
                        chainid, tempid, phi(tempid));
                end
                
                handle=mcmcBayes.simulation.getHandleComputeLogLikelihood(type);
                evalstr=sprintf('loglikelihood=handle(type, vars(:, chainid, tempid), data);');
                eval(evalstr);
                
                loglikelihoodMean(tempid)=mean(loglikelihood);%although this vector contains log domain, this should be ok for how it is used
                loglikelihoodVar(tempid)=var(loglikelihood);%although this vector contains log domain values, this should be ok for how it is used
                loglikelihoodMCE(tempid)=mcmcBayes.variable.computeMCE(loglikelihood);%although this vector contains log domain values, this should be ok for how it is used
                
                if printStatus
                    fprintf(stdout,'\tFor chainId=%d, tempId=%d, loglikelihood_mean=[%f], loglikelihood_var=[%f], loglikelihood_mce=[%f]\n', ...
                        chainid, tempid, loglikelihoodMean(tempid), loglikelihoodVar(tempid), loglikelihoodMCE(tempid));
                end
                
                osInterface.checkForKillFile();%for lengthy computations
            end
        end                
    end
    
    methods
        function obj=model(settype, setsubType, setname, setdescription, setauthor, setreference, setprefix, setcomputemsfe)
        % obj=model(setmodeltype, setpriordatatype, setdatatype, setsamplerinterface, setosInterface, setcomputemsfe)
        %   The constructor for model.
        % Input:
        %   setmodeltype - Specifies the type of the sub-model being instantiated
        %   setpriordatatype - t_simulationData type that specifies the priorDependentVariables (e.g., CCMPerformanceWeighting, EqualWeighting, Individual, and Simulated)
        %   setdatatype - t_simulationData type that specifies dependentVariables (e.g., ConFR, LinIFR, ExpIFR, LinDFR, ExpDFR, and Simulated)
        %   setdatasetid - Specifies the dataset id to load for this model evaluation
        %   setsamplerinterface - instanted sampler interface, simulationSamplerInterfaces class (e.g., simulationSamplerInterfacesOpenBUGS or simulationSamplerInterfacesJAGS)
        %   setosInterface - instantiated os interface, simulationosInterfaces class (e.g., simulationosInterfaces_UbuntuLinux or simulationosInterfaces_Windows7)
        % Output:
        %   obj Constructed model object
            if nargin == 0
                superargs={};
            else
                superargs{1}=settype;
                superargs{2}=setsubType;
                superargs{3}=setname;
                superargs{4}=setdescription;
                superargs{5}=setauthor;
                superargs{6}=setreference;
                superargs{7}=setprefix;
                superargs{8}=setcomputemsfe;
            end
            obj=obj@mcmcBayes.simulation(superargs{:});            
        end

        %this is the general form that can be overridden (e.g., for NHPP)
        function [retPredictedData, retNhat]=generatePredictions(obj, chainid, tempid)
        % [retmcmcBayesDependentHat]=generatePredictions(obj, tempid)
        %   Generates model predictions using the model.  Predictions always need to be generated at natural values, as there would be no std or mean 
        %   to allow conversion back from normalized or meanCentered.
        %   Input:
        %     tempid - Temperature id to compute the generate the predictions for
        %   Output:
        %     retmcmcBayesDependentHat Struct with elements for each of the t_mcmcBayesDependentVariablesHat types specified in obj.dependentVariablesHatTypes 
        %     (e.g., the mean and the {hingelo/hingehi} values from the histogram of the predicted values over predictionsNSamples samples)           
            if tempid==0
                switch obj.priorPredictedData.mean.dataSource
                    case mcmcBayes.t_dataSource.PredictedFromSolverPointEstimates
                        for varid=1:size(obj.priorVariables,1)
                            if varid==1
                                arglist=[{obj.priorVariables(varid,chainid,1).distribution.estimateMeanFromHyperparameters()}];
                            else
                                arglist=[arglist {obj.priorVariables(varid,chainid,1).distribution.estimateMeanFromHyperparameters()}];
                            end
                        end
                    otherwise
                        error('unsupported obj.priorPredictedData.mean.dataSource');
                end
            else
                switch obj.posteriorPredictedData.mean.dataSource
                    case mcmcBayes.t_dataSource.PredictedFromMCMCMeanPointEstimates
                        for varid=1:size(obj.posteriorVariables,1)
                            if varid==1
                                arglist=[{obj.posteriorVariables(varid,chainid,tempid).MCMC_mean}];
                            else
                                arglist=[arglist {obj.posteriorVariables(varid,chainid,tempid).MCMC_mean}];
                            end
                        end
                    otherwise
                        error('unsupported obj.posteriorPredictedData.mean.dataSource');
                end
            end

            handle=mcmcBayes.simulation.getHandleSimPredictions(obj.type);
            if tempid==0
                evalstr=sprintf('[retPredictedData, retNhat]=handle(obj.type, obj.predictionsNSamples, obj.predictionsToCapture, obj.predictionsHingeThreshold,  obj.priorPredictedData.mean, arglist{:});');
            else
                evalstr=sprintf('[retPredictedData, retNhat]=handle(obj.type, obj.predictionsNSamples, obj.predictionsToCapture, obj.predictionsHingeThreshold, obj.posteriorPredictedData.mean, arglist{:});');
            end       
            eval(evalstr);
        end
        
        function [retObj]=generatePredictionsSet(obj, bool_plotpredictions, chainId, tempId)
        % [retobj]=generatePredictionsSet(obj, bool_plotpredictions, tempid)
        %   Simulates the model using model variable point estimates and returns the model predictions vector.
        %   Input:
        %     bool_plotpredictions - If true, will plot the model simulations predictions against the original data
        %     chainId - 
        %     tempid - Temperature id to generate the predictions for            
        %   Output:
        %     retObj - Updated model object with the newly generated priorDependentVariablesHat or dependentVariablesHat and corresponding MSFE.
            stdout=1;
            fprintf(stdout,'Generating predictions using %s, chainid=%d, tempid=%d\n', obj.name, chainId, tempId);
            if (tempId==0)
                [tpriorPredictedDataVarsValues, retNhatPrior]=obj.generatePredictions(chainId, tempId);
                obj.priorPredictedData.hingelo.depVarsValues=tpriorPredictedDataVarsValues.hingelo;%assuming one dependentVar
                obj.priorPredictedData.mean.depVarsValues=tpriorPredictedDataVarsValues.mean;%assuming one dependentVar
                obj.priorPredictedData.hingehi.depVarsValues=tpriorPredictedDataVarsValues.hingehi;%assuming one dependentVar
            else
                [tposteriorPredictedData, retNhatPosterior]=obj.generatePredictions(chainId, tempId);
                obj.posteriorPredictedData.hingelo.depVarsValues=tposteriorPredictedData.hingelo;%assuming one dependentVar
                obj.posteriorPredictedData.mean.depVarsValues=tposteriorPredictedData.mean;%assuming one dependentVar
                obj.posteriorPredictedData.hingehi.depVarsValues=tposteriorPredictedData.hingehi;%assuming one dependentVar
            end
            obj.displayPredictions(tempId);
            if (tempId==0)        
                if ~(obj.priorData.dataSource==mcmcBayes.t_dataSource.None) && ~(obj.priorData.dataSource==mcmcBayes.t_dataSource.Preset) && obj.doMsfe
                    obj.priorMsfe=mcmcBayes.model.computeMSFE(obj.priorData, retNhatPrior);
                    fprintf(stdout,'\tmsfe=%f\n',obj.priorMsfe);
                else
                    obj.priorMsfe=NaN;
                    disp('Not computing msfe (e.g., turned off or priordatasource is t_simulationDataSource.Preset or t_simulationDataSource.None}).');
                end
            else
                if obj.doMsfe
                    obj.posteriorMsfe=mcmcBayes.model.computeMSFE(obj.posteriorData, retNhatPosterior);
                    fprintf(stdout,'\tmsfe=%f\n',obj.posteriorMsfe);
                else
                    obj.posteriorMsfe=NaN;
                    disp('Not computing msfe (e.g., turned off).');
                end
            end
            if bool_plotpredictions
                obj.plotPredictions(chainId, tempId);  
            end
            retObj=obj;
        end
        
        function displayVariablesStats(obj, varID, chainID, tempID)
        % displayVariablesStats(obj, varID, chainID, tempID) 
        %   Display the model variable MCMC statistics.
        %   Input:
        %     tempid - Temperature id to display the stats for   
            if tempID==0
                dispstr=sprintf('MCMC statistics for %s model (%s), uuid=%s, varID=%d, chainid=%d, tempid=%d',obj.name, obj.reference, obj.uuid, varID, chainID, tempID);
                disp(dispstr);
                obj.priorVariables(varID,chainID,1).displayStats();
            else
                dispstr=sprintf('MCMC statistics for %s model (%s), uuid=%s, varID=%d, chainid=%d, tempid=%d',obj.name, obj.reference, obj.uuid, varID, chainID, tempID);
                disp(dispstr);                
                obj.posteriorVariables(varID,chainID,tempID).displayStats();
            end
        end
        
        function plotVariableDiagnostics(obj, varId, chainId, tempId)
        % plotVariableDiagnostics(obj, varID, chainID, tempID)
        %   Outputs several MCMC diagnostic plots, plots for variable distribution pdf, ergodic mean, raw samples, and autocorrelation function
        %   Input:
        %     varid - Specifies the variable id to filter on
        %     tempid - Specifies the temperature id to filter on            
            stdout = 1;
            
            sessiondetails=sprintf('%s-%s-varid%d-chainid%d-tempid%d',obj.filenamePrefix, obj.uuid, varId, chainId, tempId);            
                        
            if tempId==0
                fprintf(stdout, 'Plotting MCMC diagnostics for %s model variable %s\n',obj.name,obj.priorVariables(varId,chainId,1).name);

                varSizes=size(obj.priorVariables(varId,chainId).MCMC_samples);
                if numel(varSizes==2) %scalar or vector
                    if varSizes(2)==1 %scalar
                        Tau=varSizes(1);
                        N=1;
                        M=1;
                    else %vector
                        N=varSizes(1);
                        Tau=varSizes(2);
                        M=1;
                    end
                else
                    N=varSizes(1);
                    M=varSizes(2);
                    Tau=varSizes(3);
                end   
                
                if obj.priorVariables(varId,chainId).type==mcmcBayes.t_variable.constant
                    dispstr=sprintf('\tNot plotting constant variable %s\n',obj.name);
                    disp(dispstr);
                else
                    if M>1
                        error('todo');%matrices not supported yet
                    end
                    
                    for elemid=1:N%create the figure handles
                        tfig_pdf(elemid)=fig();
                        tfig_ergmean(elemid)=fig();
                        tfig_raw(elemid)=fig();
                        tfig_acf(elemid)=fig();
                    end
                    obj.priorVariables(varId,chainId).plotpdf(tfig_pdf,obj.osInterface.figuresDir,sessiondetails);
                    obj.priorVariables(varId,chainId).plotergodicmean(tfig_ergmean,obj.osInterface.figuresDir,sessiondetails);
                    obj.priorVariables(varId,chainId).plothistory(tfig_raw,obj.osInterface.figuresDir,sessiondetails);
                    obj.priorVariables(varId,chainId).plotautocorrelation(tfig_acf,obj.osInterface.figuresDir,sessiondetails);   
                    
                    for elemid=1:N%plot each of the variables on a row
                        data=obj.priorVariables(varId,chainId).MCMC_samples;                       
                        sampledata=data(:,elemid);

                        hfigmulti(elemid)=fig();
                        set(0,'CurrentFigure',hfigmulti(elemid));
                        subax(elemid,1)=subp(1,4,1);
                        axtemp=tfig_pdf(elemid).CurrentAxes;
                        thandles=get(axtemp,'children');%get handle to all the children in the figure
                        copyobj(thandles,subax(elemid,1));
                        close(tfig_pdf(elemid));
                        tempstr=sprintf('pdf vs variable value');
                        title(subax(elemid,1), tempstr,'Interpreter','none');
                        ylabel(subax(elemid,1), 'pdf');
                        xlabel(subax(elemid,1), 'variable value');

                        subax(elemid,2)=subp(1,4,2);
                        axtemp=tfig_ergmean(elemid).CurrentAxes;
                        thandles=get(axtemp,'children');%get handle to all the children in the figure
                        copyobj(thandles,subax(elemid,2));
                        close(tfig_ergmean(elemid));
                        tempstr=sprintf('ergodic mean vs sample number');
                        title(subax(elemid,2), tempstr,'Interpreter','none');
                        ylabel(subax(elemid,2), 'ergodic mean');
                        xlabel(subax(elemid,2), 'sample number');

                        subax(elemid,3)=subp(1,4,3);
                        axtemp=tfig_raw(elemid).CurrentAxes;
                        thandles=get(axtemp,'children');%get handle to all the children in the figure
                        copyobj(thandles,subax(elemid,3));
                        close(tfig_raw(elemid));
                        tempstr=sprintf('raw samples vs sample number');
                        title(subax(elemid,3), tempstr,'Interpreter','none');
                        ylabel(subax(elemid,3), 'raw samples');
                        xlabel(subax(elemid,3), 'sample number');                    

                        subax(elemid,4)=subp(1,4,4);
                        axtemp=tfig_acf(elemid).CurrentAxes;
                        thandles=get(axtemp,'children');%get handle to all the children in the figure
                        copyobj(thandles,subax(elemid,4));
                        close(tfig_acf(elemid));
                        xlim(subax(elemid,4), [0 obj.priorVariables(varId,chainId,1).MCMC_acfmaxlag]);
                        tempstr=sprintf('ACF vs lags');
                        title(subax(elemid,4), tempstr,'Interpreter','none');
                        ylabel(subax(elemid,4), 'ACF');
                        xlabel(subax(elemid,4), 'lags');

                        %close when obj.type==t_simulationVariable.StochasticProcess && elemid==1
                        if obj.priorVariables(varId,chainId).type==mcmcBayes.t_variable.stochasticProcess && elemid==1
                            close(hfigmulti(elemid));%Kuo-Ghosh model special case
                        else
                            if obj.priorVariables(varId,chainId).type==mcmcBayes.t_variable.stochasticProcess
                                tempstr=sprintf('Diagnostics for %s(%d), %s',obj.name,elemid-1,sessiondetails);  %elemid is off by one                          
                                filename=sprintf('%s%s-elemid%d-ComboDiagnosticsPlot.fig', obj.osInterface.figuresDir, ...
                                    sessiondetails, elemid-1);
                            else 
                                tempstr=sprintf('Diagnostics for %s, %s',obj.name,sessiondetails);
                                filename=sprintf('%s%s-ComboDiagnosticsPlot.fig', obj.osInterface.figuresDir, ...
                                    sessiondetails);
                            end
                            suptitle(tempstr);
                            savefig(filename);
                            %close(hfigmulti(elemid));%close it to prevent clutter
                        end
                    end
                end
            else
                fprintf(stdout, 'Plotting MCMC diagnostics for %s model variable %s, chainid=%d, tempid=%d\n',obj.name,...
                    obj.posteriorVariables(varId,chainId,tempId).name, chainId, tempId);
                
                varSizes=size(obj.posteriorVariables(varId,chainId,tempId).MCMC_samples);
                if numel(varSizes==2) %scalar or vector
                    if varSizes(2)==1 %scalar
                        Tau=varSizes(1);
                        N=1;
                        M=1;
                    else %vector
                        N=varSizes(1);
                        Tau=varSizes(2);
                        M=1;
                    end
                else
                    N=varSizes(1);
                    M=varSizes(2);
                    Tau=varSizes(3);
                end                       
                
                if obj.posteriorVariables(varId,chainId,tempId).type==mcmcBayes.t_variable.constant
                    dispstr=sprintf('\tNot plotting constant variable %s\n',obj.name);
                    disp(dispstr);
                else
                    if M>1
                        error('todo');%matrices not supported yet
                    end
                    
                    for elemid=1:N%create the figure handles
                        tfig_pdf(elemid)=fig();
                        tfig_ergmean(elemid)=fig();
                        tfig_raw(elemid)=fig();
                        tfig_acf(elemid)=fig();
                    end
                    obj.posteriorVariables(varId,chainId,tempId).plotpdf(tfig_pdf,obj.osInterface.figuresDir,sessiondetails);
                    obj.posteriorVariables(varId,chainId,tempId).plotergodicmean(tfig_ergmean,obj.osInterface.figuresDir,sessiondetails);
                    obj.posteriorVariables(varId,chainId,tempId).plothistory(tfig_raw,obj.osInterface.figuresDir,sessiondetails);
                    obj.posteriorVariables(varId,chainId,tempId).plotautocorrelation(tfig_acf,obj.osInterface.figuresDir,sessiondetails);        

                    for elemid=1:N%plot each of the variables on a row
                        data=obj.posteriorVariables(varId,chainId,tempId).MCMC_samples;                       
                        sampledata=data(:,elemid);

                        hfigmulti(elemid)=fig();
                        set(0,'CurrentFigure',hfigmulti(elemid));
                        subax(elemid,1)=subp(1,4,1);
                        axtemp=tfig_pdf(elemid).CurrentAxes;
                        thandles=get(axtemp,'children');%get handle to all the children in the figure
                        copyobj(thandles,subax(elemid,1));
                        close(tfig_pdf(elemid));
                        tempstr=sprintf('pdf vs variable value');
                        title(subax(elemid,1), tempstr,'Interpreter','none');
                        ylabel(subax(elemid,1), 'pdf');
                        xlabel(subax(elemid,1), 'variable value');

                        subax(elemid,2)=subp(1,4,2);
                        axtemp=tfig_ergmean(elemid).CurrentAxes;
                        thandles=get(axtemp,'children');%get handle to all the children in the figure
                        copyobj(thandles,subax(elemid,2));
                        close(tfig_ergmean(elemid));
                        tempstr=sprintf('ergodic mean vs sample number');
                        title(subax(elemid,2), tempstr,'Interpreter','none');
                        ylabel(subax(elemid,2), 'ergodic mean');
                        xlabel(subax(elemid,2), 'sample number');

                        subax(elemid,3)=subp(1,4,3);
                        axtemp=tfig_raw(elemid).CurrentAxes;
                        thandles=get(axtemp,'children');%get handle to all the children in the figure
                        copyobj(thandles,subax(elemid,3));
                        close(tfig_raw(elemid));
                        tempstr=sprintf('raw samples vs sample number');
                        title(subax(elemid,3), tempstr,'Interpreter','none');
                        ylabel(subax(elemid,3), 'raw samples');
                        xlabel(subax(elemid,3), 'sample number');                    

                        subax(elemid,4)=subp(1,4,4);
                        axtemp=tfig_acf(elemid).CurrentAxes;
                        thandles=get(axtemp,'children');%get handle to all the children in the figure
                        copyobj(thandles,subax(elemid,4));
                        close(tfig_acf(elemid));
                        if tempId==0
                            xlim(subax(elemid,4), [0 obj.priorVariables(varId,chainId,1).MCMC_acfmaxlag]);
                        else
                            xlim(subax(elemid,4), [0 obj.posteriorVariables(varId,chainId,tempId).MCMC_acfmaxlag]);
                        end
                        tempstr=sprintf('ACF vs lags');
                        title(subax(elemid,4), tempstr,'Interpreter','none');
                        ylabel(subax(elemid,4), 'ACF');
                        xlabel(subax(elemid,4), 'lags');

                        %close when obj.type==t_simulationVariable.StochasticProcess && elemid==1
                        if tempId==0
                            if obj.priorVariables(varId,chainId,1).type==mcmcBayes.t_variable.stochasticProcess && elemid==1
                                close(hfigmulti(elemid));%Kuo-Ghosh model special case
                            else
                                if obj.priorVariables(varId,chainId,1).type==mcmcBayes.t_variable.stochasticProcess
                                    tempstr=sprintf('Diagnostics for %s(%d), %s',obj.name,elemid-1,sessiondetails);  %elemid is off by one                          
                                    filename=sprintf('%s%s-elemid%d-ComboDiagnosticsPlot.fig', obj.osInterface.figuresDir, ...
                                        sessiondetails, elemid-1);
                                else 
                                    tempstr=sprintf('Diagnostics for %s, %s',obj.name,sessiondetails);
                                    filename=sprintf('%s%s-ComboDiagnosticsPlot.fig', obj.osInterface.figuresDir, ...
                                        sessiondetails);
                                end
                                suptitle(tempstr);
                                savefig(filename);
                                %close(hfigmulti(elemid));%close it to prevent clutter
                            end
                        else
                            if obj.posteriorVariables(varId,chainId,tempId).type==mcmcBayes.t_variable.stochasticProcess && elemid==1
                                close(hfigmulti(elemid));%Kuo-Ghosh model special case
                            else
                                if obj.posteriorVariables(varId,chainId,tempId).type==mcmcBayes.t_variable.stochasticProcess
                                    tempstr=sprintf('Diagnostics for %s(%d), %s', obj.name, elemid-1, sessiondetails);  %elemid is off by one                          
                                    filename=sprintf('%s%s-elemid%d-ComboDiagnosticsPlot.fig', obj.osInterface.figuresDir, ...
                                        sessiondetails, elemid-1);
                                else 
                                    tempstr=sprintf('Diagnostics for %s, %s', obj.name, sessiondetails);
                                    filename=sprintf('%s%s-ComboDiagnosticsPlot.fig', obj.osInterface.figuresDir, ...
                                        sessiondetails);
                                end
                                suptitle(tempstr);
                                savefig(filename);
                                %close(hfigmulti(elemid));%close it to prevent clutter
                            end
                        end
                    end
                end
            end
        end
        
        function [retObj]=transformVarsPre(obj, chainID, tempID)
        %no special transforms for this model unless overridden by lower-level children
        %needs to be performed on all vars
            retObj=obj;
        end
        
        function [retObj]=transformVarsPost(obj, chainID, tempID)
        %no special transforms for this model unless overridden by lower-level children
        %needs to be performed on all vars, simultaneously
            retObj=obj;           
            if (tempID==0)
                numVars=size(retObj.priorVariables,1);
                for varID=1:numVars
                    retObj.priorVariables(varID,chainID,1)=retObj.priorVariables(varID,chainID,1);
                end
            else
                numVars=size(retObj.posteriorVariables,1);
                for varID=1:numVars
                    retObj.posteriorVariables(varID,chainID,tempID)=retObj.posteriorVariables(varID,chainID,tempID);
                end
            end
        end
        
        function [retValues]=getTransformedDataValuesPost(obj, datatype, category, datavarid, result)
            %independent variables' transformPost should be mcmcBayes.t_dataTransformPost.{none,fromlog10,fromNormalized,fromlog10Normalized}
            %result must be '', or for predicted data, mean, hingelo, or hingehi
            if category==mcmcBayes.t_dataCategory.independent
                switch datatype
                    case mcmcBayes.t_data.priorData
                        retValues=obj.priorData.getTransformedDataValuesPost(category,datavarid);
                    case mcmcBayes.t_data.priorPredictedData
                        evalstr=sprintf('retValues=obj.priorPredictedData.%s.getTransformedDataValuesPost(category,datavarid);',result);
                        eval(evalstr);
                    case mcmcBayes.t_data.posteriorData
                        retValues=obj.posteriorData.getTransformedDataValuesPost(category,datavarid);
                    case mcmcBayes.t_data.posteriorPredictedData
                        evalstr=sprintf('retValues=obj.posteriorPredictedData.%s.getTransformedDataValuesPost(category,datavarid);',result);
                        eval(evalstr);
                    otherwise
                        error('datatype must be mcmcBayes.t_data.'); 
                end
                    return;
            end
            %if dependent variables' transformPost is not mcmcBayes.t_dataTransformPost_ext.{fromNormalizeTDCCreep_t}, then it must be
            %mcmcBayes.t_dataTransformPost.{none,fromlog10,fromNormalized,fromlog10Normalized}, so call data.transformDataPost          
            switch datatype
                %for each below, we only need to check the data.transformDataPost for one datapoint (they're all the same)
                case mcmcBayes.t_data.priorData
                    retValues=obj.priorData.getTransformedDataValuesPost(category,datavarid);
                    return;
                case mcmcBayes.t_data.priorPredictedData
                    evalstr=sprintf('retValues=obj.priorPredictedData.%s.getTransformedDataValuesPost(category,datavarid);',result);
                    eval(evalstr);
                    return;
                case mcmcBayes.t_data.posteriorData
                    retValues=obj.posteriorData.getTransformedDataValuesPost(category,datavarid);
                    return;
                case mcmcBayes.t_data.posteriorPredictedData
                    evalstr=sprintf('retValues=obj.posteriorPredictedData.%s.getTransformedDataValuesPost(category,datavarid);',result);
                    eval(evalstr);
                    return;
                otherwise
                    error('datatype must be mcmcBayes.t_data.'); 
            end
        end

        function [retObj]=computeMargLogLikelihood(obj, bool_debugEnable)
        % [retobj]=computeMargLogLikelihood(obj, bool_debugEnable, datasetid)
        %   Performs the sampling sequence to estimate the marginal loglikelihood of the data given the model 
        %   using the power posterior method (Friel et al. 2008).
        %
        %   The joint likelihood of the data vector for each of the power posterior temperature iterations 
        %   (i.e., pr(data|phi,thetahat)) must be computed prior to estimating the likelihood's mean and variance for 
        %   each temperature sample set. It is incorrect to compute pr(data|phi,thetahat) separately for each data 
        %   point in the set and combine afterwards.
        %
        %   The marginal loglikelihood method requires computing, for each value of phi, log(pr(data|phi,thetahat)) 
        %   for every sample.  These values are used to derive a mean, and the set of n+1 means (one for each
        %   value of phi) is used with the trapezoidal rule to approximate the log-likelihood integral across
        %   phi=[0,1].  Unfortunately, for discrete models based on Poisson processes (HPP, Nhpp), it
        %   is very likely that in a large sample set from log(pr(data|phi,thetahat)), the likelihood for at least
        %   one of the samples will be 0.  This results in a log-likelihood for that sample being -Infinity and is 
        %   obviously problematic for this application, as it causes the mean of the log-likelihood for that value 
        %   of phi to also be -Infinity.  
        %
        %   Alternately, this method computes the likelihood, or pr(data|phi,thetahat), for evey sample.  In the
        %   natural domain, the samples having zero likelihood do not cause the mean of the likelihoods for 
        %   the respective sample set to be -Infinity.  Furthermore, it is not likely that for every sample in
        %   the set the likelihood will be zero (causing the mean of the likelihoods to be zero).  Consequently, 
        %   conversion back to the log-domain by taking the log of each of the n+1 likelihood means, results in a 
        %   finite value.
        %   Input:
        %     bool_debugEnable - If true, sampler state will be saved more rigorously to support debugging 
        %   Output:
        %     retob - Updated simulation object
            stdout=1;
            fprintf(stdout,'Computing marginal log-likelihood of the data given the %s model\n',obj.name);

            if bool_debugEnable                
                filename=sprintf('%s%s-preMargLogLikelihood.mat', mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.archivesDir), ...
                    obj.filenamePrefix, obj.uuid);                             
                save(filename,'obj');%save the model pre marginal loglikelihood computation
            end          
            
            temperatures=1:obj.powerPosteriorN+1;
            chainid=1;
            
            printStatus=true;
            [phi, loglikelihood_mean, loglikelihood_var, loglikelihood_mce]=mcmcBayes.model.computeLogLikelihoods(obj.osInterface, obj.type, ...
                obj.posteriorVariables, obj.posteriorData, chainid, temperatures, printStatus);            
            
            %powerposterior_marginalLogLikelihood_alt is implementation of the modified powerposterior Friel et al. 2013
            %powerposterior_mce is implementation of Friel et al. 2008
            %use trapezoidal rule to estimate margloglikelihood integral (will have issues with any temperature loglikelihood_mean or loglikelihood_var being -Inf, Inf, or Nan )                       
            for tempid=1:cast(obj.powerPosteriorN,'double')%1-based alternative to 0:n-1
                weights(tempid)=phi(tempid+1)-phi(tempid);
                vals(tempid)=(loglikelihood_mean(tempid+1)+loglikelihood_mean(tempid))/2;
                bias(tempid)=(1/12)*weights(tempid)^2*(loglikelihood_var(tempid+1)-loglikelihood_var(tempid));%temp for margloglikelihood computation
                %fprintf(1,'for %d, weights=%f,vals=%f\n',tempid,weights(tempid),vals(tempid));
                if tempid>1
                    mcevals(tempid)=1/2*weights(tempid)^2*loglikelihood_mce(tempid); 
                else
                    mcevals(tempid)=0;
                end
                if isinf(vals(tempid)) || isnan(vals(tempid)) || isinf(bias(tempid)) || isnan(bias(tempid)) || isinf(mcevals(tempid)) || isnan(mcevals(tempid))
                    warning(strcat('There was a margloglikelihood calculation issue: \n\tloglikelihood_mean(%d)=[%f], \n\tloglikelihood_var(%d)=[%f],',...
                        '\n\tloglikelihood_mean(%d)=[%f], \n\tloglikelihood_var(%d)=[%f], \n\t\n)'),tempid,loglikelihood_mean(tempid),tempid,...
                        loglikelihood_var(tempid),tempid+1,loglikelihood_mean(tempid+1),tempid+1,loglikelihood_var(tempid+1));
                end
            end
            mcevals(tempid+1)=1/2*(phi(obj.powerPosteriorN+1)-phi(obj.powerPosteriorN))^2*loglikelihood_mce(obj.powerPosteriorN+1);%last computation for mce
            mce=sum(mcevals);
            
            obj.posteriorMarginalLogLikelihood=sum(weights.*vals);
            obj.posteriorMarginalLogLikelihoodAlt=sum(weights.*vals)-sum(bias);
            obj.powerPosteriorMce=mce;
            obj.klDistance=loglikelihood_mean(obj.powerPosteriorN+1)-obj.posteriorMarginalLogLikelihoodAlt;
            dispstr=sprintf('\tFor uuid=%s, log(pr(data))=%f, alt. log(pr(data))=%f, log(pr(data|theta(t))=%f, mce(log(pr(data|theta(t)))=%f,KL=%f', obj.uuid, ...
				obj.posteriorMarginalLogLikelihood, obj.posteriorMarginalLogLikelihoodAlt, loglikelihood_mean(obj.powerPosteriorN+1), obj.powerPosteriorMce, ...
                obj.klDistance);
            disp(dispstr);
            setDesktopStatus(dispstr)        
            
            %Always save the post state as it is used to compute the Bayes factor values after all model sequences have completed
            filename=sprintf('%s%s-%s-postMargLogLikelihood.mat', mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.archivesDir), ...
                obj.filenamePrefix, obj.uuid);                             
            save(filename,'obj');%save the model post marginal loglikelihood computation

            retObj=obj;
        end                                              
    end
end