classdef t_dataTransformPost < int32
    enumeration
        none (1)
        fromlog10 (2)%convert back from data scaled using log10(data)
        fromln (3)
        fromNormalized (4)%convert back from data normalized using (data-mean(data))/std(data)
        fromlog10Normalized (5)
        fromMeanCentered (6)
        %put externals in t_dataTransformPost_ext
    end
    methods (Static)        
        function intvalue=toint(enumobj)
            switch enumobj          
                case {mcmcBayes.t_dataTransformPost.none,mcmcBayes.t_dataTransformPost.fromlog10,...
                      mcmcBayes.t_dataTransformPost.fromln, mcmcBayes.t_dataTransformPost.fromNormalized,...
                      mcmcBayes.t_dataTransformPost.fromlog10Normalized, mcmcBayes.t_dataTransformPost.fromMeanCentered}
                    intvalue=int32(enumobj);                    
                otherwise
                    intvalue=mcmcBayes.t_dataTransformPost_ext.toint(enumobj);
            end                
        end        
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_dataTransformPost.none
                    strvalue='mcmcBayes.t_dataTransformPost.none';
                case mcmcBayes.t_dataTransformPost.fromlog10
                    strvalue='mcmcBayes.t_dataTransformPost.fromlog10';
                case mcmcBayes.t_dataTransformPost.fromln
                    strvalue='mcmcBayes.t_dataTransformPost.fromln';
                case mcmcBayes.t_dataTransformPost.fromNormalized
                    strvalue='mcmcBayes.t_dataTransformPost.fromNormalized';
                case mcmcBayes.t_dataTransformPost.fromlog10Normalized
                    strvalue='mcmcBayes.t_dataTransformPost.fromlog10Normalized';
                case mcmcBayes.t_dataTransformPost.fromMeanCentered
                    strvalue='mcmcBayes.t_dataTransformPost.fromMeanCentered';
                otherwise 
                    strvalue=mcmcBayes.t_dataTransformPost_ext.tostring(enumobj);
            end
        end
        
        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_dataTransformPost.none)
                    enumobj=mcmcBayes.t_dataTransformPost.none;
                case int32(mcmcBayes.t_dataTransformPost.fromlog10)
                    enumobj=mcmcBayes.t_dataTransformPost.fromlog10;
                case int32(mcmcBayes.t_dataTransformPost.fromln)
                    enumobj=mcmcBayes.t_dataTransformPost.fromln;
                case int32(mcmcBayes.t_dataTransformPost.fromNormalized)
                    enumobj=mcmcBayes.t_dataTransformPost.fromNormalized;
                case int32(mcmcBayes.t_dataTransformPost.fromlog10Normalized)
                    enumobj=mcmcBayes.t_dataTransformPost.fromlog10Normalized;
                case int32(mcmcBayes.t_dataTransformPost.fromMeanCentered)
                    enumobj=mcmcBayes.t_dataTransformPost.fromMeanCentered;
                otherwise 
                    enumobj=mcmcBayes.t_dataTransformPost_ext.int2enum(intval);
            end
        end
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_dataTransformPost.none')==1
                enumobj=mcmcBayes.t_dataTransformPost.none;
            elseif strcmp(strval,'mcmcBayes.t_dataTransformPost.fromlog10')==1
                enumobj=mcmcBayes.t_dataTransformPost.fromlog10;
            elseif strcmp(strval,'mcmcBayes.t_dataTransformPost.fromln')==1
                enumobj=mcmcBayes.t_dataTransformPost.fromln;
            elseif strcmp(strval,'mcmcBayes.t_dataTransformPost.fromNormalized')==1
                enumobj=mcmcBayes.t_dataTransformPost.fromNormalized;
            elseif strcmp(strval,'mcmcBayes.t_dataTransformPost.fromlog10Normalized')==1
                enumobj=mcmcBayes.t_dataTransformPost.fromlog10Normalized;
            elseif strcmp(strval,'mcmcBayes.t_dataTransformPost.fromMeanCentered')==1
                enumobj=mcmcBayes.t_dataTransformPost.fromMeanCentered;
            else 
                enumobj=mcmcBayes.t_dataTransformPost_ext.string2enum(strval);
            end
        end
    end
end