classdef normal < mcmcBayes.model    
%normal is a base class for the Normal model.  It is a sub class of mcmcBayes.model and mcmcBayes.simulation. 
% The currently implemented models, listed hierarchically, are:
%  * mcmcBayes.simulation.model.normal.Gaussian
    properties (SetAccess=public,GetAccess=public)
        predictionsNSamples=mcmcBayes.normal.getDefaultNSamples();%Default number of samples to run in calls to simPredictions()
    end

    methods (Abstract, Static)

    end
    
    methods (Static)
        function test()
            display('Hello!');
        end
        
        function [predictionsNSamples]=getDefaultNSamples()
            predictionsNSamples=50000;
        end        
                
        function [names]=getDefaultDataVariableNames(dataCategory)
            switch dataCategory
                case mcmcBayes.t_dataCategory.independent
                    error('There are no independent variables for the Normal model!')
                case mcmcBayes.t_dataCategory.dependent
                    names={'y'};
                otherwise
                    error('dataCategory must be mcmcbayes.t_dataCategory.');
            end
        end        
        
        function [loglikelihood]=computeLogLikelihood(type, vars, data)
        % [likelihood]=computeLogLikelihood(modeltype, independentVariables, modelVariables, dependentVariables)
        %   Computes the likelihood of the data given the model and parameter values.  This is used in the marginal likelihood computation.
        %   Input: 
        %     modeltype - Specifies the model to use for computing the likelihood
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     modelVariables - Distribution model variables that is an array of mcmcBayesVariable (indexed by varid, tempid) objects 
        %     dependentVariables - Dependent variables for the model that is a cell array of mcmcBayesNumeric objects.  Can also have multiple
        %     sets of data, where there is a vector row for each dataset.  
        %   Output:
        %     likelihood - Computed likelihood
            if (~strcmp(type,'Gaussian'))
                error('Incorrect modeltype.');
            end       
            datavarid=1;
            yorig=cast(data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.dependent, datavarid), 'double');
            Tau=size(yorig,1);%Number of data (we are assuming scalar independent variables)
            
            % Ideally numSamples would be obj.modelVariables(index).MCMC_nsamples_data.  If the MCMC sampler has issues 
            %   (e.g., improper values for the inits) it may stop early.  The user is separately warned of 
            %   improperly sized sample arrays when they are stored by mcmcBayesVariable.set.MCMC_samples(). Set
            %   tsize to the variable with the fewest number of samples.
            numSamples=size(vars(1).MCMC_samples,1);
            for varid=1:size(vars,1)
                if size(vars(varid).MCMC_samples,1)<numSamples
                    numSamples=size(vars(varid).MCMC_samples,1);         
                end
            end                
            tloglikelihood=zeros(numSamples,Tau);

            %need to already have called transformVarsPost on variables
            mu=cast(vars(1).MCMC_samples,'double');
            r=cast(vars(2).MCMC_samples,'double');    
            tsize=size(mu,1);
            if (size(vars,1)~=2)
                error('incorrect size for variables');
            end

            tloglikelihood=zeros(tsize,Tau);
            for i=1:Tau
                tloglikelihood(:,i)=-1/2*log(2*pi)+1/2*log(r(1:tsize))-1/2*r(1:tsize).*(yorig(i)-mu(1:tsize)).^2;
            end
            
            loglikelihood=sum(tloglikelihood,2);
            
            if (~isempty(find(isinf(loglikelihood)==1)))
                error('Error condition in computeLogLikelihood(), loglikelihood contains -Inf or Inf');
            elseif ~isempty(find(isnan(loglikelihood)==1))
                error('Error condition in computeLogLikelihood(), loglikelihood contains Nan');
            end                             
        end
                
        function [depVarsValuesHat, retYhat]=simPredictions(type, predictionsNSamples, predictionsToCapture, hingeThreshold, data, varargin)
        % [dependentVariablesHat]=simPredictions(modeltype, predictionsNSamples, dependentVariablesHatTypes, hingeThreshold, independentVariables, dependentVariableName, description, arglist)
        %   Function to simulate predictions using one of the regression models (it is static so we can simulate data without instantiating a class)
        %   Input:
        %     modeltype - Specifies the model to use for generating the predictions
        %     predictionsNSamples - Number of samples to run when generating the predictions
        %     dependentVariablesHatTypes - Vector of t_mcmcBayesDependentVariablesHat that specifies which thetahat to generate predictions for
        %     hingeThreshold - Specifies the hinge threshold for determining the thetaHat edge cases for generating dependentVariablesHat.{hingelo/hingehi}
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     dependentVariableName - Name to use for the predictions data
        %     description - Description for the predictions data
        %     arglist - Cell array with the values to use for the model variables when prediction with the model (i.e., these are the thetaHat)
        %   Output:
        %     dependentVariablesHat - Struct with elements for each of the t_mcmcBayesDependentVariablesHat types specified in dependentVariablesHatTypes 
        %     (e.g., the mean and the {hingelo/hingehi} values from the histogram of the predicted values over predictionsNSamples samples)
            if (~strcmp(type,'Gaussian'))
                error('Incorrect modeltype.');
            end
            
            depdatavarid=1;
            depVarName=data.depVarsName{depdatavarid};
            depVarDesc=data.depVarsDesc{depdatavarid};
            depVarTransformPre=data.depVarsTfPre(depdatavarid);
            depVarTransformPost=data.depVarsTfPost(depdatavarid);            
            
            numdatapoints=1;
            mu=varargin{1};
            r=varargin{2};%convert r to MATLAB's normal(mu,sigma), or sigma=1/sqrt(r)
            Yhat(:,1)=random('Normal',mu,1/sqrt(r),1,predictionsNSamples);

            binprec=(ceil(max(max(Yhat)))-floor(min(min(Yhat))))/1000;
            bins=floor(min(min(Yhat))):binprec:ceil(max(max(Yhat)));
            depVarsValuesHat=mcmcBayes.model.capturePredictionThresholds(predictionsNSamples, predictionsToCapture, hingeThreshold, bins, ...
                mcmcBayes.t_numericDimensions.scalar, Yhat, data.depVarsType{depdatavarid});          
            retYhat=Yhat;
        end
    end
    
    methods 
        function obj=normal(settype, setsubtype, setname, setdescription, setauthor, setreference, setprefix)%constructor for normal type           
        % obj=normal(setmodeltype, setpriordatatype, setdatatype, setsamplerinterface, setosinterface)
        %   The constructor for normal.
        % Input:
        %   setmodeltype - Specifies the type of the sub-model being instantiated
        %   setpriordatatype - t_mcmcBayesData type that specifies the priorDependentVariables (e.g., CCMPerformanceWeighting, EqualWeighting, Individual, and Simulated)
        %   setdatatype - t_mcmcBayesData type that specifies dependentVariables (e.g., ConFR, LinIFR, ExpIFR, LinDFR, ExpDFR, and Simulated)
        %   setdatasetid - Specifies the dataset id to load for this model evaluation
        %   setsamplerinterface - instanted sampler interface, mcmcBayesSamplerInterfaces class (e.g., mcmcBayesSamplerInterfacesOpenBUGS or mcmcBayesSamplerInterfacesJAGS)
        %   setosinterface - instantiated os interface, mcmcBayesOSInterfaces class (e.g., mcmcBayesOSInterfaces_UbuntuLinux or mcmcBayesOSInterfaces_Windows7)
        % Output:
        %   obj Constructed normal object
            if nargin == 0
                superargs={};
            else
                superargs{1}=settype;
                superargs{2}=setsubtype;
                superargs{3}=setname;
                superargs{4}=setdescription;
                superargs{5}=setauthor;
                superargs{6}=setreference;
                superargs{7}=setprefix;
            end

            obj=obj@mcmcBayes.model(superargs{:});            
        end
                
        function displayPredictions(obj, tempid)
        % displayPredictions(obj)
        %   Displays the values (in the console) for the already generated model predictions in obj.dependentVariablesHat.
        %   Input:
        %     tempid - Temperature id to compute the display the predictions for            
            stdout=1;
            %print the original data 
            datavarid=1;                
            if tempid==0
                if obj.priorData.dataSource~=mcmcBayes.t_dataSource.Preset %don't print preset, it is empty
                    Yorig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorData, mcmcBayes.t_dataCategory.dependent, datavarid);
                    fprintf(stdout,'Yorig=%s\n',conversionUtilities.matrix_tostring(Yorig));
                end
            else
                Yorig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.dependent, datavarid);
                fprintf(stdout,'Yorig=%s\n',conversionUtilities.matrix_tostring(Yorig));
            end
            
            %print the predictions
            for Yhattype=obj.predictionsToCapture%use the vector obj.predictionsToCapture to determine which structure elements from priorPredictedData to show
                datavarid=1;
                switch Yhattype
                    case mcmcBayes.t_dataResult.hingelo
                        if tempid==0
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');%just use mean (all the same)
                        else
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');%just use mean (all the same)
                        end
                        name='Yhat.hingelo';
                    case mcmcBayes.t_dataResult.mean
                        if tempid==0
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');%just use mean (all the same)
                        else
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');%just use mean (all the same)
                        end
                        name='Yhat.mean';
                    case mcmcBayes.t_dataResult.hingehi
                        if tempid==0
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');%just use mean (all the same)
                        else
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');%just use mean (all the same)
                        end
                        name='Yhat.hingehi';
                    otherwise
                        error('Cannot identify the appropriate structure element in obj.predictionsToCapture');
                end
                %only display the mean for now
                if Yhattype==mcmcBayes.t_dataResult.mean
                    fprintf(stdout,'%s=%s\n',name,conversionUtilities.matrix_tostring(Yhat));
                end
            end
        end
        
        function plotPredictions(obj, chainId, tempId)
        % plotPredictions(obj, tempid)
        %   Plots the model simulation results, using point estimates for Thetahat, over the original data.
        %   Input:
        %     tempid - Temperature id to compute the display the predictions for
            sessiondetails=sprintf('%s-%s-chainid%d-tempid%d',obj.filenamePrefix, obj.uuid, chainId, tempId);            
                                            
            ymin=-1;
            ymax=1;

            fig();
            hold on;           
            %plot the original data  
            datavarid=1;
            if tempId==0
                if obj.priorData.dataSource~=mcmcBayes.t_dataSource.Preset
                    Yorig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorData, mcmcBayes.t_dataCategory.dependent, datavarid);
                end
            else
                Yorig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.dependent, datavarid); 
            end
            if (tempId==0)
                if exist('Yorig','var')
                    plot(Yorig,zeros(numel(Yorig),1),'g*');
                end
            else
                plot(Yorig,zeros(numel(Yorig),1),'r*');
            end
            
            %plot the predicted data
            datavarid=1;                
            for predictionToCapture=obj.predictionsToCapture%use the vector obj.dependentVariablesHatTypes to determine which rows from dependentVariablesHat to show            
                if (tempId==0)
                    switch predictionToCapture
                        case mcmcBayes.t_dataResult.hingelo
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');
                            plot(Yhat, zeros(numel(Yhat),1), 'gd');%lo bound dashed plot line
                        case mcmcBayes.t_dataResult.mean
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');
                            plot(Yhat, zeros(numel(Yhat),1), 'go');%mean solid plot line
                        case mcmcBayes.t_dataResult.hingehi
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');
                            plot(Yhat, zeros(numel(Yhat),1), 'gd');%hi bound dashed plot line
                        otherwise
                            %won't get here
                    end
                else
                    switch predictionToCapture
                        case mcmcBayes.t_dataResult.hingelo
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');
                            plot(Yhat, zeros(numel(Yhat),1), 'rd');%lo bound dashed plot line
                        case mcmcBayes.t_dataResult.mean
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');
                            plot(Yhat, zeros(numel(Yhat),1), 'ro');%mean solid plot line
                        case mcmcBayes.t_dataResult.hingehi
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');
                            plot(Yhat, zeros(numel(Yhat),1), 'rd');%hi bound dashed plot line
                        otherwise
                            error('Cannot identify the appropriate structure in obj.posteriorPredictedData');
                    end
                end
            end

            set(gca,'yLim',[ymin ymax]);
            tempstr=sprintf('Model prediction results for %s, %s', obj.name, sessiondetails);
            title(tempstr);
            filename=sprintf('%s%s-PredictionsSet.fig',obj.osInterface.figuresDir,sessiondetails);%need _PredictionsSet for filename parsing
            savefig(gcf,filename);
            
            hold off;        
        end                
    end

    methods (Access=protected)    
        function [status]=checkDataCompatibility(obj, datatype)
        % [status]=checkIndVarDataCompatibility(obj, independentVariables)
        %   Checks the compatibility of independentVariables with the specified normal type
            status=true;
        end          
    end    
    
    methods %getter/setter functions       

    end
end
