classdef NHPPEvaluation < mcmcBayes.sequence
% NHPPEvaluation is a class for demonstrating mcmcBayes with six NHPP models.  
% * BrooksMotleyPresetSimulated - (subtype 'a') uses a preset prior with simulated data for the posterior data
% * BrooksMotleyPresetEmpirical1 - (subtype 'a') uses a preset prior to replicate Bortkewitsch, 1898, in a study on the number of soldiers in the Prussian cavalry 
%   killed by horse kicks from 1875 to 1894, for 14 different cavalry corps (estimate should be zeta=0.61).
% * GoelOkumotoPresetSimulated - (subtype 'a') uses a preset prior with simulated data for the posterior data
% * GoelOkumotoPresetEmpirical2 - (subtype 'a') uses a preset prior to replicate Goel, Okumoto 1979, in a study of software errors for NTDS Data (estimate should 
%   be u=33.99, zeta=0.00579).
% * GoelPresetSimulated - (subtype 'a') uses a preset prior with simulated data for the posterior data
% * GoelPresetEmpirical5 - (subtype 'a') uses a preset prior to replicate Goel, 1982, RADC-TR-82, data set 1 from Thayer et al 1976 (estimate should be u=2352, 
%   zeta=0.0232, kappa=1.494)
% * MusaOkumotoPresetSimulated (subtype 'a') uses a preset prior with simulated data for the posterior data
% * MusaOkumotoPresetEmpirical3 - (subtype 'a') uses a preset prior on data from Goel, 1985
% * YamadaOhbaOsakiPresetSimulated (subtype 'a') uses a preset prior with simulated data for the posterior data
% * YamadaOhbaOsakiPresetEmpirical4 - (subtype 'a') uses a preset prior to replicate Ohba, 1984, in a study of software errors over time in an on-line data entry software 
%   package (estimate should be u=71.7, zeta=0.104).
% * KuoGhoshPresetSimulated (subtype 'a') uses a preset prior with simulated data for the posterior data

    properties
        %None        
    end
    
    methods (Static)
        function test
           disp('Hello!'); 
        end
            
        % BrooksMotley-a (JAGS,BUGS,Stan) - Gamma priors (i.e., zeta~gamma(a,b))
        % GoelOkumoto-a (JAGS,BUGS,Stan) - Gamma priors (i.e., u~gamma(a,b), zeta~gamma(c,d))
        % Goel-a (JAGS,BUGS,Stan) - Gamma priors (i.e., u~gamma(a,b), zeta~gamma(c,d), kappa~gamma(g,h))
        % MusaOkumoto-a (JAGS,BUGS,Stan) - Gamma priors (i.e., zeta~gamma(a,b), kappa~gamma(c,d))
        % YamadaOhbaOsaki-a (JAGS,BUGS,Stan) - Gamma priors (i.e., u~gamma(a,b), zeta~gamma(c,d))
        % KuoGhosh-a (JAGS,BUGS) - Gamma process prior (i.e., Theta(t)~GAMMA(Lambda(t)*c,c))
        
        function [simuuids]=getUuids()
            simuuids=[{'BrooksMotleyPresetSimulated'} ...   %BrooksMotley-a
                   {'BrooksMotleyPresetEmpirical1'} ...     %BrooksMotley-a
                   {'GoelOkumotoPresetSimulated'} ...       %GoelOkumoto-a
                   {'GoelOkumotoPresetEmpirical2'} ...      %GoelOkumoto-a
                   {'GoelPresetSimulated'} ...              %Goel-a
                   {'GoelPresetEmpirical5'} ...             %Goel-a
                   {'MusaOkumotoPresetSimulated'} ...       %MusaOkumoto-a
                   {'MusaOkumotoPresetEmpirical3'} ...      %MusaOkumoto-a
                   {'YamadaOhbaOsakiPresetSimulated'} ...   %YamadaOhbaOsaki-a
                   {'YamadaOhbaOsakiPresetEmpirical4'} ...  %YamadaOhbaOsaki-a
                   {'KuoGhoshPresetSimulated'}];            %KuoGhosh-a
        end
        
        function [retSims]=runUnitTests()
            norunpp=false;
            runpp=true;
            dbgmcmc=true;
            nodbgmcmc=false;
            seqType='NHPPEvaluation';
            seqUuid='NHPPEvaluation-runUnitTests';
            simuuids=mcmcBayes.NHPPEvaluation.getUuids();
            %simOverrides[simuuid, runpp, dbgmcmc, {[samplerType]}]
            simOverrides=[simuuids(1), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %BrooksMotleyPresetSimulated
                          simuuids(2), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %BrooksMotleyPresetEmpirical1
                          simuuids(3), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %GoelOkumotoPresetSimulated
                          simuuids(4), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %GoelOkumotoPresetEmpirical2
                          simuuids(5), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %GoelPresetSimulated
                          simuuids(6), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %GoelPresetEmpirical5
                          simuuids(7), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %MusaOkumotoPresetSimulated
                          simuuids(8), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %MusaOkumotoPresetEmpirical3
                          simuuids(9), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %YamadaOhbaOsakiPresetSimulated
                          simuuids(10), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ...%YamadaOhbaOsakiPresetEmpirical4
                          simuuids(11), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ...%KuoGhoshPresetSimulated
                          simuuids(1), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %BrooksMotleyPresetSimulated
                          simuuids(2), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %BrooksMotleyPresetEmpirical1
                          simuuids(3), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %GoelOkumotoPresetSimulated
                          simuuids(4), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %GoelOkumotoPresetEmpirical2
                          simuuids(5), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %GoelPresetSimulated
                          simuuids(6), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %GoelPresetEmpirical5
                          simuuids(7), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %MusaOkumotoPresetSimulated
                          simuuids(8), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %MusaOkumotoPresetEmpirical3
                          simuuids(9), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %YamadaOhbaOsakiPresetSimulated
                          simuuids(10), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ...%YamadaOhbaOsakiPresetEmpirical4
                          simuuids(11), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}];   %KuoGhoshPresetSimulated                       
            for i=1:size(simOverrides,1)
                retSims(i)=mcmcBayes.sequence.overrideDefaultsAndRunSingleSimulation(seqType, seqUuid, simOverrides{i,1:4});                      
            end
        end
        
        function getData(type, data, varargin)                                
        % getData(obj, name, data, varargin)
        %   This function generates a simulated dataset using the parameter point estimates from varargin.
            if (data.dataSource~=mcmcBayes.t_dataSource.Simulated) && (data.dataSource~=mcmcBayes.t_dataSource.Postulated)
                error('Requires mcmcBayes.t_dataSource=Simulated or Postulated for creating data.');
            else
                if strcmp(type,'BrooksMotley') || strcmp(type,'Goel') || strcmp(type,'GoelOkumoto') || ...
                        strcmp(type,'YamadaOhbaOsaki') || strcmp(type,'KuoGhosh')
                    predictionsNSamples=5;%If using leastsquares, works better for 5 (as opposed to 1).  If using mle, use predictionsNSamples=5 for the NHPP models, as for these models the lower values do not converge with mle!!!!!!!!!!!
                elseif strcmp(type,'MusaOkumoto')
                    predictionsNSamples=15;%If using customnonlinear fittype, works better for 15.
                else
                    error('Unsupported NHPPEvaluation sequence type=%s', type);
                end
            end
            
            numdatavars=1;%N(t)
            numgenerateddatasets=size(data.depVarsValues,2);
            dependentVars=cell(numdatavars,numgenerateddatasets);
                
            if strcmp(type,'BrooksMotley')
                %for this mcmcBayes, there are many sets of variable values that will perform equivalently; fix u to verify the simulated data set
                zeta=varargin{1};%'zeta','proportionality factor between expected discoveries and security assessment performance','double'                   
                %mean at at t=50 should be zeta*50, solve for zeta|mean(t=50)=148
                %fhandle=@(x) x(1)*50-148; fsolve(fhandle,[148*.01]); yields zeta=148*0.0199999999087
                for tdatasetid=1:numgenerateddatasets
                    dependentVars(numdatavars,tdatasetid)=mcmcBayes.NHPP.generateTestData(type, predictionsNSamples, data, zeta);%mcmcBayes.NHPP has custom generateTestData
                end
            elseif strcmp(type,'GoelOkumoto')
                u=varargin{1};%'u', 'expected discoveries', 'double'
                zeta=varargin{2};%'zeta','proportionality factor between expected discoveries and security assessment performance', 'double'
                %mean at t=50 should be u*(1-exp(-zeta*50)), solve for u,zeta,kappa|mean(t=50)=148
                %fhandle=@(x) x(1)-x(1)*exp(-x(2)*50)-148; fsolve(fhandle,[148 .01]); yields u=148, zeta=0.1819924144639, however, we reduce zeta for a better dataset
                for tdatasetid=1:numgenerateddatasets
                    dependentVars(numdatavars,tdatasetid)=mcmcBayes.NHPP.generateTestData(type, predictionsNSamples, data, u, zeta);%mcmcBayes.NHPP has custom generateTestData
                end
            elseif strcmp(type,'Goel')
                u=varargin{1};%'u','expected discoveries', 'double'
                zeta=varargin{2};%'zeta','proportionality factor between expected discoveries and security assessment performance', 'double'
                kappa=varargin{3};%'kappa','parameter to customize the form of the proportionality factor', 'double'
                %mean at t=50 should be u*(1-exp(-zeta*50^kappa)), solve for u,zeta,kappa|mean(t=50)=148
                %fhandle=@(x) x(1)*(1-exp(-x(2)*50^x(3)))-148; x=fsolve(fhandle,[148 0.2 0.5]); yields u=148, zeta=0.4340969785038, kappa=0.7664577731978
                for tdatasetid=1:numgenerateddatasets
                    dependentVars(numdatavars,tdatasetid)=mcmcBayes.NHPP.generateTestData(type, predictionsNSamples, data, u, zeta, kappa);%mcmcBayes.NHPP has custom generateTestData
                end
            elseif strcmp(type,'MusaOkumoto')
                %for this mcmcBayes, there are many sets of variable values that will perform equivalently; fix u to verify the simulated data set
                zeta=varargin{1};%'zeta','proportionality factor between expected discoveries and initial security assessment performance', 'double'
                kappa=varargin{2};%'kappa','security assessment performance rate of reduction per discovery', 'double'
                %mean at t=20000 should be 1/kappa*log(1+zeta*kappa*20000), solve for zeta,kappa|mean(t=20000)=148
                %fhandle=@(x) (1/x(2))*log1p(x(1)*x(2)*20000)-148; x=fsolve(fhandle,[148*.1 .1]); zeta=14.8, kappa=0.066842958846148
                for tdatasetid=1:numgenerateddatasets
                    dependentVars(numdatavars,tdatasetid)=mcmcBayes.NHPP.generateTestData(type, predictionsNSamples, data, zeta, kappa);%mcmcBayes.NHPP has custom generateTestData
                end
            elseif strcmp(type,'YamadaOhbaOsaki')
                u=varargin{1};%'u', 'expected discoveries', 'double'
                zeta=varargin{2};%'zeta','steady state security assessment performance', 'double'
                %mean at t=50 should be 148(1-(1+zeta*t)*exp(-zeta*t)), solve for u,zeta|mean(t=50)=148
                %fhandle=@(x) x(1)*(1-(1+x(2)*50)*exp(-x(2)*50))-148; x=fsolve(fhandle,[148 .01]); yields u=148 zeta=0.2305581194492
                for tdatasetid=1:numgenerateddatasets
                    dependentVars(numdatavars,tdatasetid)=mcmcBayes.NHPP.generateTestData(type, predictionsNSamples, data, u, zeta);%mcmcBayes.NHPP has custom generateTestData
                end
            elseif strcmp(type,'KuoGhosh')
                Lambdap=varargin{1};%'Lambdap','best guesses set of point estimates over time for expected interval grouped discoveries', 'int32'
                c=varargin{1};%'c','confidence in best guesses set', 'double'
                %mean at each t is the assumed value, Lambdap
                for tdatasetid=1:numgenerateddatasets
                    dependentVars(numdatavars,tdatasetid)=mcmcBayes.NHPP.generateTestData(type, predictionsNSamples, data, Lambdap, c);%mcmcBayes.NHPP has custom generateTestData
                end
            else
                error('NHPPEvaluation sequence type is not BrooksMotley, GoelOkumoto, Goel, MusaOkumoto, YamadaOhbaOsaki, KuoGhosh.');
            end
            
            disp(conversionUtilities.matrix_tostring(data.indVarsValues{1,1}));
            disp(conversionUtilities.matrix_tostring(dependentVars{1,1}));
            
            plot(data.indVarsValues{1,1},dependentVars{1,1});
        end                             
    end
    
    methods
        function [obj]=NHPPEvaluation(cfgMcmcBayes,repositories,sequenceUuid)           
        % obj=NHPPEvaluation(cfgMcmcBayes,repositories,sequenceUuid)
        %   The constructor for NHPPEvaluation.
            if nargin > 0
                superargs{1}='NHPPEvaluation';
                superargs{2}=cfgMcmcBayes;
                superargs{3}=repositories;
                superargs{4}=sequenceUuid;
            elseif nargin == 0
                superargs={};
            end
            obj=obj@mcmcBayes.sequence(superargs{:});                        
        end        
    end
    
    methods (Static, Access=private)     
   
    end    
    
    methods %getter/setter functions

    end
end