classdef t_mcmcBayesFigures < int32
    enumeration
        %model types
        VARIABLE_PDF (1)
        VARIABLE_ERGODICMEAN (2)
        VARIABLE_TRACE (3)
        VARIABLE_ACF (4)
        MODEL_PREDICTIONS (5)
        MODEL_PREDICTIONS_COMBO (6)
    end
    
    methods (Static)        
        function value=getfilenamestring(obj)
            switch obj
                case t_mcmcBayesFigures.VARIABLE_PDF
                    value='PDF';
                case t_mcmcBayesFigures.VARIABLE_ERGODICMEAN
                    value='ErgodicMean';
                case t_mcmcBayesFigures.VARIABLE_TRACE
                    value=RawSamples';
                case t_mcmcBayesFigures.VARIABLE_ACF
                    value='ACF';
                case t_mcmcBayesFigures.MODEL_PREDICTIONS
                    value='PredictionsSet';
                case t_mcmcBayesFigures.MODEL_PREDICTIONS_COMBO
                    value='CombinedPredictionsSet';
                otherwise
                    error('unsupported t_mcmcBayesFigures type');
            end
        end
    end    
end