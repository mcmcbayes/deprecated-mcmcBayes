function [cfgMcmcBayes] = xml2CfgMcmcBayesStruct(xmlfile)
	doc = xmlread(xmlfile);
    fprintf(1,'Parsed %s\n',xmlfile);    
    %javaaddpath(mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.javaDir));
    %call our java hacks method to insert the DOCTYPE element
    entityname='cfgMcmcBayes';
    dtdfilename=sprintf('%s%s.dtd',mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.configDir),entityname);
    xmlStr=XmlHacks.insert(doc, entityname, dtdfilename);%call Java function  (XmlHacks must be on javaclasspath)
    %now parse the hacked xml
    doc = XmlHacks.xmlreadstring(xmlStr);    
    
    doc.getDocumentElement().normalize();
    root=doc.getDocumentElement();
    rootname=root.getNodeName();
    if ~(strcmp(rootname,'cfgMcmcBayes')==1)
        error('Root element should be cfgMcmcBayes');
    end
       
    %parse the cfgSequence
    cfgSequenceNode=doc.getElementsByTagName("cfgSequence").item(0);
    commandStr=sprintf('cfgSequence.mainSeq=%s;',cast(cfgSequenceNode.getElementsByTagName('mainSeq').item(0).getTextContent(),'char'));
    eval(commandStr);
    
    %parse the cfgSimulation
    cfgSimulationNode=doc.getElementsByTagName("cfgSimulation").item(0);
    
    cfgSimulation.debug=str2num(cast(cfgSimulationNode.getElementsByTagName('debug').item(0).getTextContent(),'char'));
    cfgSimulation.rewindEnable=str2num(cast(cfgSimulationNode.getElementsByTagName('rewindEnable').item(0).getTextContent(),'char'));
    cfgSimulation.runView=mcmcBayes.t_runView.string2enum(cast(cfgSimulationNode.getElementsByTagName('simRunView').item(0).getTextContent(),'char'));
    cfgSimulation.plotDiagnostics=str2num(cast(cfgSimulationNode.getElementsByTagName('plotDiagnostics').item(0).getTextContent(),'char'));
    
    cfgSimulation.sampleMcmcResults=str2num(cast(cfgSimulationNode.getElementsByTagName('sampleMcmcResults').item(0).getTextContent(),'char'));
    cfgSimulation.skipPriorComputations=str2num(cast(cfgSimulationNode.getElementsByTagName('skipPriorComputations').item(0).getTextContent(),'char'));
    cfgSimulation.generatePredictionsUsingPrior=str2num(cast(cfgSimulationNode.getElementsByTagName('generatePredictionsUsingPrior').item(0).getTextContent(),'char'));
    cfgSimulation.plotPredictionsUsingPrior=str2num(cast(cfgSimulationNode.getElementsByTagName('plotPredictionsUsingPrior').item(0).getTextContent(),'char'));
    cfgSimulation.skipPosteriorComputations=str2num(cast(cfgSimulationNode.getElementsByTagName('skipPosteriorComputations').item(0).getTextContent(),'char'));
    cfgSimulation.generatePredictionsUsingPosterior=str2num(cast(cfgSimulationNode.getElementsByTagName('generatePredictionsUsingPosterior').item(0).getTextContent(),'char'));
    cfgSimulation.plotPredictionsUsingPosterior=str2num(cast(cfgSimulationNode.getElementsByTagName('plotPredictionsUsingPosterior').item(0).getTextContent(),'char'));
    
    cfgSimulation.runPowerPosteriorMcmcSequence=str2num(cast(cfgSimulationNode.getElementsByTagName('runPowerPosteriorMcmcSequence').item(0).getTextContent(),'char'));
    cfgSimulation.computeMsfe=str2num(cast(cfgSimulationNode.getElementsByTagName('computeMsfe').item(0).getTextContent(),'char')); 
    
    %parse the cfgOs
    cfgOsNode=doc.getElementsByTagName("cfgOs").item(0);
    cfgOs.python=cast(cfgOsNode.getElementsByTagName('python').item(0).getTextContent(),'char');
    cfgOs.javac=cast(cfgOsNode.getElementsByTagName('javac').item(0).getTextContent(),'char');
    
    %parse the cfgSampler
    cfgSamplerNode=doc.getElementsByTagName("cfgSampler").item(0);
    cfgSampler.samplerInterfacesRunView=mcmcBayes.t_runView.string2enum(cast(cfgSamplerNode.getElementsByTagName('samplerRunView').item(0).getTextContent(),'char'));
    cfgSampler.JAGS.shortCut=cast(cfgSamplerNode.getElementsByTagName("JAGS").item(0).getElementsByTagName("shortCut").item(0).getTextContent(),'char');
    cfgSampler.OpenBugs.shortCut=cast(cfgSamplerNode.getElementsByTagName("OpenBugs").item(0).getElementsByTagName("shortCut").item(0).getTextContent(),'char');
    cfgSampler.Stan.cmdStanDir=cast(cfgSamplerNode.getElementsByTagName("Stan").item(0).getElementsByTagName("cmdStanDir").item(0).getTextContent(),'char');
    cfgSampler.Stan.cmdStanShortCut=cast(cfgSamplerNode.getElementsByTagName("Stan").item(0).getElementsByTagName("cmdStanShortCut").item(0).getTextContent(),'char');
    
    cfgMcmcBayes.cfgSequence=cfgSequence;
    cfgMcmcBayes.cfgSimulation=cfgSimulation;
    cfgMcmcBayes.cfgSampler=cfgSampler;
    cfgMcmcBayes.cfgOs=cfgOs;
end