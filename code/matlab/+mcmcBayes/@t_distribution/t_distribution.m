classdef t_distribution < int32
    enumeration
        % Discrete Univariate	
        DiscreteConstant (1)
        Bernoulli (2)
        Binomial (3)
        Categorical (4)
        Negative_Binomial (5)
        Poisson (6)
        Geometric (7)
        GeometricAlternate (8)
        NonCentralHypergeometric (9)
        % Continuous Univariate	
        ContinuousConstant (100)
        Beta (101)
        ChiSquared (102)
        DoubleExponential (103)
        Exponential (104)
        Flat (105)
        Gamma (106)
        GeneralizedExtremeValue (107)
        GeneralizedF (108)
        GeneralizedGamma (109)
        GeneralizedPareto (110)
        GenericLogLikelihoodDistribution (111)
        LogNormal (112)
        Logistic (113)
        Normal (114)
        TZNormal (115)%normal truncated at zero T(0,Inf)
        Pareto (116)
        StudentT (117)
        Uniform (118)
        Weibull (119)
        % Discrete Multivariate	
        Multinomial (200)
        GammaProcess (201)
        % Continuous Multivariate	
        Dirichlet (300)
        MultivariateNormal (301)
        MultivariateStudentT (302)
        Wishart (303)
    end
    
    methods (Static)
        function intvalue=toint(enumobj)
            switch enumobj
                case {mcmcBayes.t_distribution.DiscreteConstant,mcmcBayes.t_distribution.Bernoulli,mcmcBayes.t_distribution.Binomial,...
                        mcmcBayes.t_distribution.Categorical,mcmcBayes.t_distribution.Negative_Binomial,mcmcBayes.t_distribution.Poisson,...
                        mcmcBayes.t_distribution.Geometric,mcmcBayes.t_distribution.GeometricAlternate,mcmcBayes.t_distribution.NonCentralHypergeometric,...
                        mcmcBayes.t_distribution.ContinuousConstant,mcmcBayes.t_distribution.Beta,mcmcBayes.t_distribution.ChiSquared,...
                        mcmcBayes.t_distribution.DoubleExponential,mcmcBayes.t_distribution.Exponential,mcmcBayes.t_distribution.Flat,...
                        mcmcBayes.t_distribution.Gamma,mcmcBayes.t_distribution.GeneralizedExtremeValue,mcmcBayes.t_distribution.GeneralizedF,...
                        mcmcBayes.t_distribution.GeneralizedGamma,mcmcBayes.t_distribution.GeneralizedPareto,...
                        mcmcBayes.t_distribution.GenericLogLikelihoodDistribution,mcmcBayes.t_distribution.LogNormal,mcmcBayes.t_distribution.Logistic,...
                        mcmcBayes.t_distribution.Normal,mcmcBayes.t_distribution.TZNormal,mcmcBayes.t_distribution.Pareto,...
                        mcmcBayes.t_distribution.StudentT,mcmcBayes.t_distribution.Uniform,mcmcBayes.t_distribution.Weibull,...
                        mcmcBayes.t_distribution.Multinomial,mcmcBayes.t_distribution.GammaProcess,mcmcBayes.t_distribution.Dirichlet,...
                        mcmcBayes.t_distribution.MultivariateNormal,mcmcBayes.t_distribution.MultivariateStudentT,mcmcBayes.t_distribution.Wishart}
                    intvalue=int32(enumobj);
                otherwise
                    error('unsupported mcmcBayes.t_distribution type');                    
            end
        end
                
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_distribution.DiscreteConstant
                    strvalue='mcmcBayes.t_distribution.DiscreteConstant';
                case mcmcBayes.t_distribution.Bernoulli
                    strvalue='mcmcBayes.t_distribution.Bernoulli';
                case mcmcBayes.t_distribution.Binomial
                    strvalue='mcmcBayes.t_distribution.Binomial';
                case mcmcBayes.t_distribution.Categorical
                    strvalue='mcmcBayes.t_distribution.Categorical';
                case mcmcBayes.t_distribution.Negative_Binomial
                    strvalue='mcmcBayes.t_distribution.Negative_Binomial';
                case mcmcBayes.t_distribution.Poisson
                    strvalue='mcmcBayes.t_distribution.Poisson';
                case mcmcBayes.t_distribution.Geometric
                    strvalue='mcmcBayes.t_distribution.Geometric';
                case mcmcBayes.t_distribution.GeometricAlternate
                    strvalue='mcmcBayes.t_distribution.GeometricAlternate';
                case mcmcBayes.t_distribution.NonCentralHypergeometric
                    strvalue='mcmcBayes.t_distribution.NonCentralHypergeometric';
                case mcmcBayes.t_distribution.ContinuousConstant
                    strvalue='mcmcBayes.t_distribution.ContinuousConstant';
                case mcmcBayes.t_distribution.Beta
                    strvalue='mcmcBayes.t_distribution.Beta';
                case mcmcBayes.t_distribution.ChiSquared
                    strvalue='mcmcBayes.t_distribution.ChiSquared';
                case mcmcBayes.t_distribution.DoubleExponential
                    strvalue='mcmcBayes.t_distribution.DoubleExponential';
                case mcmcBayes.t_distribution.Exponential
                    strvalue='mcmcBayes.t_distribution.Exponential';
                case mcmcBayes.t_distribution.Flat
                    strvalue='mcmcBayes.t_distribution.Flat';
                case mcmcBayes.t_distribution.Gamma
                    strvalue='mcmcBayes.t_distribution.Gamma';
                case mcmcBayes.t_distribution.GeneralizedExtremeValue
                    strvalue='mcmcBayes.t_distribution.GeneralizedExtremeValue';
                case mcmcBayes.t_distribution.GeneralizedF
                    strvalue='mcmcBayes.t_distribution.GeneralizedF';
                case mcmcBayes.t_distribution.GeneralizedGamma
                    strvalue='mcmcBayes.t_distribution.GeneralizedGamma';
                case mcmcBayes.t_distribution.GeneralizedPareto
                    strvalue='mcmcBayes.t_distribution.GeneralizedPareto';
                case mcmcBayes.t_distribution.GenericLogLikelihoodDistribution
                    strvalue='mcmcBayes.t_distribution.GenericLogLikelihoodDistribution';
                case mcmcBayes.t_distribution.LogNormal
                    strvalue='mcmcBayes.t_distribution.LogNormal';
                case mcmcBayes.t_distribution.Logistic
                    strvalue='mcmcBayes.t_distribution.Logistic';
                case mcmcBayes.t_distribution.Normal
                    strvalue='mcmcBayes.t_distribution.Normal';
                case mcmcBayes.t_distribution.TZNormal
                    strvalue='mcmcBayes.t_distribution.TZNormal';
                case mcmcBayes.t_distribution.Pareto
                    strvalue='mcmcBayes.t_distribution.Pareto';
                case mcmcBayes.t_distribution.StudentT
                    strvalue='mcmcBayes.t_distribution.StudentT';
                case mcmcBayes.t_distribution.Uniform
                    strvalue='mcmcBayes.t_distribution.Uniform';
                case mcmcBayes.t_distribution.Weibull
                    strvalue='mcmcBayes.t_distribution.Weibull';
                case mcmcBayes.t_distribution.Multinomial
                    strvalue='mcmcBayes.t_distribution.Multinomial';
                case mcmcBayes.t_distribution.GammaProcess
                    strvalue='mcmcBayes.t_distribution.GammaProcess';
                case mcmcBayes.t_distribution.Dirichlet
                    strvalue='mcmcBayes.t_distribution.Dirichlet';
                case mcmcBayes.t_distribution.MultivariateNormal
                    strvalue='mcmcBayes.t_distribution.MultivariateNormal';
                case mcmcBayes.t_distribution.MultivariateStudentT
                    strvalue='mcmcBayes.t_distribution.MultivariateStudentT';
                case mcmcBayes.t_distribution.Wishart
                    strvalue='mcmcBayes.t_distribution.Wishart';
                otherwise
                    error('unsupported mcmcBayes.t_distribution type');                    
            end
        end
        
        function value=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_distribution.DiscreteConstant)
                    value=mcmcBayes.t_distribution.DiscreteConstant;
                case int32(mcmcBayes.t_distribution.Bernoulli)
                    value=mcmcBayes.t_distribution.Bernoulli;
                case int32(mcmcBayes.t_distribution.Binomial)
                    value=mcmcBayes.t_distribution.Binomial;
                case int32(mcmcBayes.t_distribution.Categorical)
                    value=mcmcBayes.t_distribution.Categorical;
                case int32(mcmcBayes.t_distribution.Negative_Binomial)
                    value=mcmcBayes.t_distribution.Negative_Binomial;
                case int32(mcmcBayes.t_distribution.Poisson)
                    value=mcmcBayes.t_distribution.Poisson;
                case int32(mcmcBayes.t_distribution.Geometric)
                    value=mcmcBayes.t_distribution.Geometric;
                case int32(mcmcBayes.t_distribution.GeometricAlternate)
                    value=mcmcBayes.t_distribution.GeometricAlternate;
                case int32(mcmcBayes.t_distribution.NonCentralHypergeometric)
                    value=mcmcBayes.t_distribution.NonCentralHypergeometric;
                case int32(mcmcBayes.t_distribution.ContinuousConstant)
                    value=mcmcBayes.t_distribution.ContinuousConstant;
                case int32(mcmcBayes.t_distribution.Beta)
                    value=mcmcBayes.t_distribution.Beta;
                case int32(mcmcBayes.t_distribution.ChiSquared)
                    value=mcmcBayes.t_distribution.ChiSquared;
                case int32(mcmcBayes.t_distribution.DoubleExponential)
                    value=mcmcBayes.t_distribution.DoubleExponential;
                case int32(mcmcBayes.t_distribution.Exponential)
                    value=mcmcBayes.t_distribution.Exponential;
                case int32(mcmcBayes.t_distribution.Flat)
                    value=mcmcBayes.t_distribution.Flat;
                case int32(mcmcBayes.t_distribution.Gamma)
                    value=mcmcBayes.t_distribution.Gamma;
                case int32(mcmcBayes.t_distribution.GeneralizedExtremeValue)
                    value=mcmcBayes.t_distribution.GeneralizedExtremeValue;
                case int32(mcmcBayes.t_distribution.GeneralizedF)
                    value=mcmcBayes.t_distribution.GeneralizedF;
                case int32(mcmcBayes.t_distribution.GeneralizedGamma)
                    value=mcmcBayes.t_distribution.GeneralizedGamma;
                case int32(mcmcBayes.t_distribution.GeneralizedPareto)
                    value=mcmcBayes.t_distribution.GeneralizedPareto;
                case int32(mcmcBayes.t_distribution.GenericLogLikelihoodDistribution)
                    value=mcmcBayes.t_distribution.GenericLogLikelihoodDistribution;
                case int32(mcmcBayes.t_distribution.LogNormal)
                    value=mcmcBayes.t_distribution.LogNormal;
                case int32(mcmcBayes.t_distribution.Logistic)
                    value=mcmcBayes.t_distribution.Logistic;
                case int32(mcmcBayes.t_distribution.Normal)
                    value=mcmcBayes.t_distribution.Normal;
                case int32(mcmcBayes.t_distribution.TZNormal)
                    value=mcmcBayes.t_distribution.TZNormal;
                case int32(mcmcBayes.t_distribution.Pareto)
                    value=mcmcBayes.t_distribution.Pareto;
                case int32(mcmcBayes.t_distribution.StudentT)
                    value=mcmcBayes.t_distribution.StudentT;
                case int32(mcmcBayes.t_distribution.Uniform)
                    value=mcmcBayes.t_distribution.Uniform;
                case int32(mcmcBayes.t_distribution.Weibull)
                    value=mcmcBayes.t_distribution.Weibull;
                case int32(mcmcBayes.t_distribution.Multinomial)
                    value=mcmcBayes.t_distribution.Multinomial;
                case int32(mcmcBayes.t_distribution.GammaProcess)
                    value=mcmcBayes.t_distribution.GammaProcess;
                case int32(mcmcBayes.t_distribution.Dirichlet)
                    value=mcmcBayes.t_distribution.Dirichlet;
                case int32(mcmcBayes.t_distribution.MultivariateNormal)
                    value=mcmcBayes.t_distribution.MultivariateNormal;
                case int32(mcmcBayes.t_distribution.MultivariateStudentT)
                    value=mcmcBayes.t_distribution.MultivariateStudentT;
                case int32(mcmcBayes.t_distribution.Wishart)
                    value=mcmcBayes.t_distribution.Wishart;
                otherwise
                    error('unsupported typeid');                    
            end
        end 
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_distribution.DiscreteConstant')==1
                enumobj=mcmcBayes.t_distribution.DiscreteConstant;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Bernoulli')==1
                enumobj=mcmcBayes.t_distribution.Bernoulli;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Binomial')==1
                enumobj=mcmcBayes.t_distribution.Binomial;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Categorical')==1
                enumobj=mcmcBayes.t_distribution.Categorical;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Negative_Binomial')==1
                enumobj=mcmcBayes.t_distribution.Negative_Binomial;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Poisson')==1
                enumobj=mcmcBayes.t_distribution.Poisson;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Geometric')==1
                enumobj=mcmcBayes.t_distribution.Geometric;
            elseif strcmp(strval,'mcmcBayes.t_distribution.GeometricAlternate')==1
                enumobj=mcmcBayes.t_distribution.GeometricAlternate;
            elseif strcmp(strval,'mcmcBayes.t_distribution.NonCentralHypergeometric')==1
                enumobj=mcmcBayes.t_distribution.NonCentralHypergeometric;
            elseif strcmp(strval,'mcmcBayes.t_distribution.ContinuousConstant')==1
                enumobj=mcmcBayes.t_distribution.ContinuousConstant;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Beta')==1
                enumobj=mcmcBayes.t_distribution.Beta;
            elseif strcmp(strval,'mcmcBayes.t_distribution.ChiSquared')==1
                enumobj=mcmcBayes.t_distribution.ChiSquared;
            elseif strcmp(strval,'mcmcBayes.t_distribution.DoubleExponential')==1
                enumobj=mcmcBayes.t_distribution.DoubleExponential;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Exponential')==1
                enumobj=mcmcBayes.t_distribution.Exponential;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Flat')==1
                enumobj=mcmcBayes.t_distribution.Flat;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Gamma')==1
                enumobj=mcmcBayes.t_distribution.Gamma;
            elseif strcmp(strval,'mcmcBayes.t_distribution.GeneralizedExtremeValue')==1
                enumobj=mcmcBayes.t_distribution.GeneralizedExtremeValue;
            elseif strcmp(strval,'mcmcBayes.t_distribution.GeneralizedF')==1
                enumobj=mcmcBayes.t_distribution.GeneralizedF;
            elseif strcmp(strval,'mcmcBayes.t_distribution.GeneralizedGamma')==1
                enumobj=mcmcBayes.t_distribution.GeneralizedGamma;
            elseif strcmp(strval,'mcmcBayes.t_distribution.GeneralizedPareto')==1
                enumobj=mcmcBayes.t_distribution.GeneralizedPareto;
            elseif strcmp(strval,'mcmcBayes.t_distribution.GenericLogLikelihoodDistribution')==1
                enumobj=mcmcBayes.t_distribution.GenericLogLikelihoodDistribution;
            elseif strcmp(strval,'mcmcBayes.t_distribution.LogNormal')==1
                enumobj=mcmcBayes.t_distribution.LogNormal;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Logistic')==1
                enumobj=mcmcBayes.t_distribution.Logistic;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Normal')==1
                enumobj=mcmcBayes.t_distribution.Normal;
            elseif strcmp(strval,'mcmcBayes.t_distribution.TZNormal')==1
                enumobj=mcmcBayes.t_distribution.TZNormal;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Pareto')==1
                enumobj=mcmcBayes.t_distribution.Pareto;
            elseif strcmp(strval,'mcmcBayes.t_distribution.StudentT')==1
                enumobj=mcmcBayes.t_distribution.StudentT;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Uniform')==1
                enumobj=mcmcBayes.t_distribution.Uniform;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Weibull')==1
                enumobj=mcmcBayes.t_distribution.Weibull;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Multinomial')==1
                enumobj=mcmcBayes.t_distribution.Multinomial;
            elseif strcmp(strval,'mcmcBayes.t_distribution.GammaProcess')==1
                enumobj=mcmcBayes.t_distribution.GammaProcess;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Dirichlet')==1
                enumobj=mcmcBayes.t_distribution.Dirichlet;
            elseif strcmp(strval,'mcmcBayes.t_distribution.MultivariateNormal')==1
                enumobj=mcmcBayes.t_distribution.MultivariateNormal;
            elseif strcmp(strval,'mcmcBayes.t_distribution.MultivariateStudentT')==1
                enumobj=mcmcBayes.t_distribution.MultivariateStudentT;
            elseif strcmp(strval,'mcmcBayes.t_distribution.Wishart')==1
                enumobj=mcmcBayes.t_distribution.Wishart;
            else
                error('unsupported mcmcBayes.t_distribution type');
            end
        end              
    end
end