classdef t_data < int32
    enumeration
        %data types
        priorData (1)
        posteriorData (2) 
        priorPredictedData (3) 
        posteriorPredictedData (4)
    end
    
    methods (Static)        
        function intvalue=toint(enumobj)
            switch enumobj          
                case {mcmcBayes.t_data.priorData,mcmcBayes.t_data.posteriorData,mcmcBayes.t_data.priorPredictedData,...
                        mcmcBayes.t_data.posteriorPredictedData}
                    intvalue=int32(enumobj);
                otherwise
                    error('unsupported mcmcBayes.t_data type');
            end                
        end        
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_data.priorData
                    strvalue='mcmcBayes.t_data.priorData';
                case mcmcBayes.t_data.posteriorData
                    strvalue='mcmcBayes.t_data.posteriorData';
                case mcmcBayes.t_data.priorPredictedData
                    strvalue='mcmcBayes.t_data.priorPredictedData';
                case mcmcBayes.t_data.posteriorPredictedData
                    strvalue='t_data.posteriorPredictedData';                              
                otherwise
                    error('unsupported mcmcBayes.t_data type');
            end
        end
        
        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_data.priorData)
                    enumobj=mcmcBayes.t_data.priorData;
                case int32(mcmcBayes.t_data.posteriorData)
                    enumobj=mcmcBayes.t_data.posteriorData;
                case int32(mcmcBayes.t_data.priorPredictedData)
                    enumobj=mcmcBayes.t_data.priorPredictedData;
                case int32(mcmcBayes.t_data.posteriorPredictedData)
                    enumobj=mcmcBayes.t_data.posteriorPredictedData;                              
                otherwise
                    error('unsupported mcmcBayes.t_data type');
            end
        end        
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_data.priorData')==1
                enumobj=mcmcBayes.t_data.priorData;
            elseif strcmp(strval,'mcmcBayes.t_data.posteriorData')==1
                enumobj=mcmcBayes.t_data.posteriorData;
            elseif strcmp(strval,'mcmcBayes.t_data.priorPredictedData')==1
                enumobj=mcmcBayes.t_data.priorPredictedData;
            elseif strcmp(strval,'mcmcBayes.t_data.posteriorPredictedData')==1
                enumobj=mcmcBayes.t_data.posteriorPredictedData;
            else
                error('unsupported mcmcBayes.t_data type');
            end
        end             
    end    
end