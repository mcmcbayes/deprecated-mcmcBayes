function [cfgMcmcBayesExtension] = xml2CfgMcmcBayesExtensionStruct(xmlfile,dtdfile)
	doc = xmlread(xmlfile);
    fprintf(1,'Parsed %s\n',xmlfile);    
    %call our java hacks method to insert the DOCTYPE element
    entityname='cfgMcmcBayesExtension';
    xmlStr=XmlHacks.insert(doc, entityname, dtdfile);%call Java function  (XmlHacks must be on javaclasspath)
    %now parse the hacked xml
    doc = XmlHacks.xmlreadstring(xmlStr);    
    
    doc.getDocumentElement().normalize();
    root=doc.getDocumentElement();
    rootname=root.getNodeName();
    if ~(strcmp(rootname,'cfgMcmcBayesExtension')==1)
        error('Root element should be cfgMcmcBayesExtension');
    end
       
    %parse the cfgSequence
    cfgSequenceNode=doc.getElementsByTagName("cfgSequence").item(0);
    commandStr=sprintf('cfgSequence.extSeq=%s;',cast(cfgSequenceNode.getElementsByTagName('extSeq').item(0).getTextContent(),'char'));
    eval(commandStr);
    cfgSequence.numExt=str2num(cast(cfgSequenceNode.getElementsByTagName('numExt').item(0).getTextContent(),'char'));
    
    cfgMcmcBayesExtension.cfgSequence=cfgSequence;
end