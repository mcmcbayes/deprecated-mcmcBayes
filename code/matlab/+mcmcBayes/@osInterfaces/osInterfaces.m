classdef osInterfaces
    properties
        type%mcmcBayes.t_osInterfaces.{Windows7,UbuntuLinux}, automatically set by the constructor
        bashCmdPid%PID of currently running bash command
        bashCmdName%command name of currently running bash command
        bashCmdLine%command line for currently running bash command
        
        bashCmdState%sampler state of currently running samplers
        bashCmdExitCode%exit code of the most recently completed sampler
        bashCmdLogFile%log file for the currently running sampler
        bashCmdUuid
        bashCmdMsgFile
        
        pythonPath
        pythonUtilsScript
        
        javacPath %note: we cannot build the XmlHacks withing McmcBayes because it is needed to read in cfgMcmcBayes.xml
        
        archivesDir %contains *.mat files for restoring previous states (rewind & replay)
        cfgDir %contains configuration files
        inputDataDir %contains the input data files
        samplerDataInDir %contains sampler data initialization files
        samplerInitsDir %contains sampler initialization files
        samplerDataOutDir %contains sampler output files
        docDir %contains documentation
        figuresDir %contains generated images
        logsDir %contains log files from sampler and MATLAB sequences
        matlabDir %contains MATLAB programs
        mcmcBayesDir %directory for the mcmcBayes project
        tempDir %contains temporary files created during a sequence (e.g., files used for messaging between MATLAB and python scripts controlling samplers)
        poisonFile %path to the kill file that shuts down any MATLAB engines and sampler subprocesses that are running     
        formattedOutputDir
        
        maxEngines%i.e., mcmcBayes limits MATLAB engines startup to 4 instances of MATLAB in the OS (set it to how many cpus your computer has)
    end      
    
    methods (Static)         
        function [cfgMcmcBayes,repoList]=loadConfig()
            matlabDir=mcmcBayes.osInterfaces.getSubPath(mcmcBayes.t_paths.matlabDir);
            cfgFilename=sprintf('%s%s',mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.configDir), mcmcBayes.osInterfaces.getConfigFileName());
            cfgMcmcBayes=mcmcBayes.xml2CfgMcmcBayesStruct(cfgFilename);
            reposFilename=sprintf('%s%s',mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.configDir), mcmcBayes.osInterfaces.getReposFileName());
            repositories=mcmcBayes.xml2RepositoriesStruct(reposFilename);
            size=0;
            repoList{1,1}=mcmcBayes.osInterfaces.getmcmcBayesPath();%add main repository
            for i=2:repositories.numRepos+1
                repoList{i,1}=repositories.repository(i-1).path;
                cfgfilename=sprintf('%s%s',repositories.repository(i-1).path,mcmcBayes.osInterfaces.getSubPath(mcmcBayes.t_paths.configDir),mcmcBayes.osInterfaces.getConfigExtFileName());
                dtdfilename=sprintf('%s%s',mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.configDir),'cfgMcmcBayesExtension.dtd');
                cfgMcmcBayesExtension(i-1)=mcmcBayes.xml2CfgMcmcBayesExtensionStruct(cfgfilename,dtdfilename);
                for j=1:cfgMcmcBayesExtension(i-1).cfgSequence.numExt
                    if size==0
                        cfgMcmcBayes.cfgSequence.extSeq{1,1}=cfgMcmcBayesExtension(i-1).cfgSequence.extSeq{j};
                        size=1;
                    else
                        cfgMcmcBayes.cfgSequence.extSeq{size+1,1}=cfgMcmcBayesExtension(i-1).cfgSequence.extSeq{j};
                        size=size+1;
                    end
                end
            end            
        end
        
        function createStartupFile() 
            stdout=1;
            fprintf(stdout,'Creating the startup.m file.\n');      

            %chicken and egg problem with XmlHacks.jar inclusion on javaclasspath (loadConfig needs it there already)
            jarfile=sprintf('%sXmlHacks.jar',mcmcBayes.osInterfaces.convPathStr(mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.javaDir)));
            javaaddpath(jarfile);%check using >> javaclasspath                                  
            
            [cfgMcmcBayes,repoList]=mcmcBayes.osInterfaces.loadConfig;                        
            mainPath=mcmcBayes.osInterfaces.getmcmcBayesPath;
            matlabDir=mcmcBayes.osInterfaces.getSubPath(mcmcBayes.t_paths.matlabDir);
            
            filename=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%sstartup.m',mainPath,matlabDir));
            fid=fopen(filename,'w');%open the file
            fprintf(fid,'javaaddpath(''%s'')\n',jarfile);
            fprintf(fid,'addpath %s\n',mcmcBayes.osInterfaces.convPathStr(sprintf('%s%sinstall',repoList{1},matlabDir)));
            eval(sprintf('addpath %s\n',mcmcBayes.osInterfaces.convPathStr(sprintf('%s%sinstall',repoList{1},matlabDir))));
            if ~isempty(repoList)
                %Loop over the repo paths and add each (repoList includes main repo and externals)
                for i=1:size(repoList,1)
                    fprintf(fid,'addpath %s\n',mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s',repoList{i},matlabDir)));
                    eval(sprintf('addpath %s\n',mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s',repoList{i},matlabDir))));
                    fprintf(fid,'addpath %s\n',mcmcBayes.osInterfaces.convPathStr(sprintf('%s%sutilities',repoList{i},matlabDir)));
                    eval(sprintf('addpath %s\n',mcmcBayes.osInterfaces.convPathStr(sprintf('%s%sutilities',repoList{i},matlabDir))));
                end
            end
            fclose(fid);
        end
        
        function setTerminateSessions(enable)
        % setTerminateSessions(enable)
        %   Wrapper function that calls the setKillScripts function.
        %   Input:
        %     enable - boolean that specifies whether or not to enable/disable termination of any MATLAB python engines that are running
        % Alternately, one can use the python shell and kill defunct python processes.
        % $ python
        % >>> import mcmcBayesUtils
        % >>> mcmcBayesUtils.killpsNames('python.exe')
        % You may have to run it a few times, as it will kill itself too!
            mcmcBayes.osInterfaces.setKillScripts(enable);
        end  

        function killNamedProc(killProcName)
%redo and use mcmcbayes_utils function (i.e., don't write a new python script)
            %Kills the named processes specified
            cfgFilename=sprintf('%s%s',mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.configDir), mcmcBayes.osInterfaces.getConfigFileName());
            cfgMcmcBayes=mcmcBayes.xml2CfgMcmcBayesStruct(cfgFilename);
            OSInterface=mcmcBayes.osInterfaces.getOsInterface(cfgMcmcBayes);

            %Dump the following script to a new file, "tempDir"+"kill.py"
            %#!/usr/bin/env python
            %import sys;
            %sys.path.append("mcmcBayesDir"+"\code\python3")
            %import mcmcBayesUtils;
            %binaryname='python'
            %mcmcbayes_utils.killNamedProc(killProcName)
            
            scriptname=sprintf('%skill.py',OSInterface.tempDir);
            fid=fopen(scriptname,'w');
            fprintf(fid,'#!/usr/bin/env python3\n');
            fprintf(fid,'import sys;\n');
            tempstr=sprintf('sys.path.append(''%s\\code\\python3'');\n',OSInterface.mcmcBayesDir);            
            fprintf(fid,'%s',mcmcBayes.osInterfaces.convPathStr(tempstr));
            fprintf(fid,'import mcmcbayes_utils;\n');
            fprintf(fid,'mcmcbayes_utils.killNamedProc(''%s'');',killProcName);
            fclose(fid);
            
            %run the python scriptname
            switch OSInterface.type
                case {mcmcBayes.t_osInterfaces.UbuntuLinux,mcmcBayes.t_osInterfaces.MacOS}
                    commandstr=sprintf('%s "%s"', OSInterface.pythonPath, scriptname);
                    [status,cmdout] = system(commandstr); 
                case {mcmcBayes.t_osInterfaces.Windows7,mcmcBayes.t_osInterfaces.Windows10}
                    commandstr=sprintf('start "killDefunctPython" %s "%s"', OSInterface.pythonPath, scriptname);
                    [status,cmdout] = system(commandstr); 
                otherwise
                    error('unsupported mcmcBayes.t_osInterfaces');
            end                       
            %don't delete the script here
        end
        
        function [numCPUs]=getNumCPUS(pythonPath)
%redo and use mcmcbayes_utils function            
            OSInterfaceType=mcmcBayes.osInterfaces.getOSInterfaceType();
            switch OSInterfaceType
                case {mcmcBayes.t_osInterfaces.UbuntuLinux,mcmcBayes.t_osInterfaces.MacOS}
                    commandstr=sprintf('%s -c ''import multiprocessing;msg=multiprocessing.cpu_count();print(msg);''',pythonPath);
                case {mcmcBayes.t_osInterfaces.Windows7, mcmcBayes.t_osInterfaces.Windows10}
                    commandstr=sprintf('%s -c "import multiprocessing;msg=multiprocessing.cpu_count();print(msg);"',pythonPath);
                otherwise
                    error('unsupported mcmcBayes.t_osInterfaces');
            end        
            [status,cmdout] = system(commandstr);
            numCPUs=sscanf(cmdout,'%d');
        end
        
        %python dependency on psutil `$ sudo apt-get install python-psutil`
        function retOSInterface=getOsInterface(cfgMcmcBayes)
            OSInterfaceType=mcmcBayes.osInterfaces.getOSInterfaceType();
            switch OSInterfaceType
                case mcmcBayes.t_osInterfaces.MacOS
                    retOSInterface=mcmcBayes.MacOS(cfgMcmcBayes);   
                case mcmcBayes.t_osInterfaces.UbuntuLinux
                    retOSInterface=mcmcBayes.UbuntuLinux(cfgMcmcBayes);   
                case mcmcBayes.t_osInterfaces.Windows7
                    retOSInterface=mcmcBayes.Windows7(cfgMcmcBayes);
                case mcmcBayes.t_osInterfaces.Windows10
                    retOSInterface=mcmcBayes.Windows10(cfgMcmcBayes);
                otherwise
                    error('unsupported mcmcBayes.t_osInterfaces');
            end                 
        end       
        
        function retOSInterfaceType=getOSInterfaceType()
%redo and use mcmcbayes_utils function            
            if strcmp(computer, 'GLNXA64')%use MATLAB to detect OS type
                retOSInterfaceType=mcmcBayes.t_osInterfaces.UbuntuLinux;                   
            elseif strcmp(computer, 'PCWIN64')                                
                [~,rawString]=system('ver');
                rawString=replace(rawString,'[].',' ');
                rawString=replace(rawString,'[',' ');
                rawString=replace(rawString,']',' ');
                rawString=replace(rawString,'.',' ');
                
                for i=1:6
                    [token, remain]=strtok(rawString,' ');
                    switch i
                        case 1
                            %token is 'Microsoft'
                        case 2 %Windows
                            %token is 'Windows'
                        case 3
                            %token is 'Version'
                        case 4
                            winMajVer=str2num(token);
                        case 5
                            winMinVer=str2num(token);
                        case 6
                            winBuild=str2num(token);
                    end
                    rawString=remain;
                end
                
                if winMajVer==6 || winMajVer==7
                    retOSInterfaceType=mcmcBayes.t_osInterfaces.Windows7; 
                elseif winMajVer==10
                    retOSInterfaceType=mcmcBayes.t_osInterfaces.Windows10; 
                else
                    error('Need to implement this windows version');
                end
            elseif strcmp(computer, 'MACI64')
                retOSInterfaceType=mcmcBayes.t_osInterfaces.MacOS;
            else
                error('unsupported mcmcBayes.t_osInterfaces');
            end                 
        end          
        
        function retpath=convPathStr(path)
            OSInterfaceType=mcmcBayes.osInterfaces.getOSInterfaceType();
            switch OSInterfaceType
                case {mcmcBayes.t_osInterfaces.UbuntuLinux,mcmcBayes.t_osInterfaces.MacOS}
                    retpath=strrep(path,'\','/');
                case {mcmcBayes.t_osInterfaces.Windows7, mcmcBayes.t_osInterfaces.Windows10}
                    retpath=strrep(path,'/','\');
                otherwise
                    error('Unsupported mcmcBayes.t_osInterfaces');
            end
        end
        
        function retpath=convPathStrUnix(path)
            switch mcmcBayes.osInterfaces.getOSInterfaceType()
                case {mcmcBayes.t_osInterfaces.Windows7, mcmcBayes.t_osInterfaces.Windows10}
                    retpath=strrep(path,':','');
                    retpath=strrep(retpath,'\','/');
                    retpath=sprintf('/%s',retpath);
                otherwise
                    retpath=path;
            end            
        end        
        
        function mcmcBayesDir=getmcmcBayesPath()
            OSInterfaceType=mcmcBayes.osInterfaces.getOSInterfaceType();
            switch OSInterfaceType
                case {mcmcBayes.t_osInterfaces.UbuntuLinux,mcmcBayes.t_osInterfaces.MacOS}
                    [tempDir,namestr]=fileparts(mfilename('fullpath'));
                    tempDir=strcat(tempDir,'/');
                    mcmcBayesDir=strrep(tempDir,'/code/matlab/+mcmcBayes/@osInterfaces/','');
                case {mcmcBayes.t_osInterfaces.Windows7, mcmcBayes.t_osInterfaces.Windows10}
                    [tempDir,namestr]=fileparts(mfilename('fullpath'));
                    tempDir=strcat(tempDir,'\');
                    mcmcBayesDir=strrep(tempDir,'\code\matlab\+mcmcBayes\@osInterfaces\','');
                otherwise
                    error('Unsupported mcmcBayes.t_osInterfaces');
            end
        end
        
        function path=getPath(subPathType)
            if subPathType==mcmcBayes.t_paths.pythonUtilsFile
                path='';
            else
                mcmcBayesDir=mcmcBayes.osInterfaces.getmcmcBayesPath();
                path=strcat(mcmcBayesDir,mcmcBayes.osInterfaces.getSubPath(subPathType));
            end
        end
        
        function subpath=getSubPath(subPathType)
            ostype=mcmcBayes.osInterfaces.getOSInterfaceType();
            switch subPathType 
                case mcmcBayes.t_paths.archivesDir
                    subpath='/data/stateArchives/';                
                case mcmcBayes.t_paths.configDir
                    subpath='/config/';
                case mcmcBayes.t_paths.configSimsDir
                    subpath='/config/simulations/';                    
                case mcmcBayes.t_paths.docDir
                    subpath='/docs/';
                case mcmcBayes.t_paths.figuresDir
                    subpath='/figures/';
                case mcmcBayes.t_paths.inputDataDir
                    subpath='/data/input/';
                case mcmcBayes.t_paths.logsDir
                    subpath='/logs/';
                case mcmcBayes.t_paths.matlabDir
                    subpath='/code/matlab/';
                case mcmcBayes.t_paths.poisonFile
                    subpath='/temp/aLittleBitOfPoison';
                case mcmcBayes.t_paths.samplerDataInDir
                    subpath='/data/samplerDataIn/';
                case mcmcBayes.t_paths.samplerInitsDir
                    subpath='/data/samplerInits/';
                case mcmcBayes.t_paths.samplerProjectsDir
                    subpath='/code/sampler/projectTemplates/';
                case mcmcBayes.t_paths.samplerScriptsDir
                    subpath='/code/sampler/scriptTemplates/';
                case mcmcBayes.t_paths.samplerDataOutDir
                    subpath='/data/samplerDataOut/';
                case mcmcBayes.t_paths.formattedOutputDir
                    subpath='/data/output/';
                case mcmcBayes.t_paths.tempDir
                    subpath='/temp/';
                case mcmcBayes.t_paths.javaDir
                    subpath="/code/java/";
                otherwise
                    error('Unsupported mcmcBayes.t_paths.');
            end
            if ostype==mcmcBayes.t_osInterfaces.Windows7 || ostype==mcmcBayes.t_osInterfaces.Windows10
                subpath=strrep(subpath,'/','\'); %convert to backslashes
            end
        end
        
        function name=getUtilsModuleName()
            name='os_agnostic_utils';    
        end
                                
        function waitFileExists(fileName)
            while ~mcmcBayes.osInterfaces.doesFileExist(fileName)  
                pause(0.1)
                drawnow
            end             
        end
        
        function waitFileExists_timeout(fileName,timeout)
            compileTimerStart=tic;
            
            while ~mcmcBayes.osInterfaces.doesFileExist(fileName)
                elapsedtime=toc(compileTimerStart);
                if elapsedtime>timeout
                    error(strcat('Timeout waiting on file:',fileName));
                end
            end
        end        
        
        function existence=doesFileExist(fileName)
            retOSInterfaceType=mcmcBayes.osInterfaces.getOSInterfaceType();
            switch retOSInterfaceType
                case {mcmcBayes.t_osInterfaces.Windows7, mcmcBayes.t_osInterfaces.Windows10}
                    tmpName=mcmcBayes.osInterfaces.convPathStr(fileName);
                otherwise
                    tmpName=mcmcBayes.osInterfaces.convPathStrUnix(fileName);
            end
            file=java.io.File(tmpName);
            if file.exists()%simpler and more reliable existence check for a file (vs MATLAB's exist)
                existence=true;
            else
                existence=false;
            end
        end
        
        function setKillScripts(setkill)
%todo: use mcmcbayes_utils.py
            poisonFile=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.poisonFile);
            meaningoflife=42;
            if (setkill)
                disp('Creating the killfile');
                fileid=fopen(poisonFile,'w');%Create the kill file
                %poison=uint8('generate the data using this')+42
                poisoncoded=[116  159  157  158   74  150  147  149  143   74  143  160  143  156  163   74  141  153  161  140  153  163 ...
                              74  157  147  152  145  157   74  146  147  157   74  157  139  142   86   74  157  139  142   74  157  153  ...
                             152  145   74  111  160  143  156  163   74  156  153  157  143   74  146  139  157   74  147  158  157   74  ...
                             158  146  153  156  152];
                poisonstr=char(poisoncoded-meaningoflife);
                fprintf(fileid,'%s\n',poisonstr);
                fclose(fileid);
            else
                disp('Removing the killfile');
                if mcmcBayes.osInterfaces.doesFileExist(poisonFile)
                    delete(poisonFile);%Remove the kill file       
                end
            end
        end                    
        
        function clearAll()
            mcmcBayes.osInterfaces.setKillScripts(false);
            mcmcBayes.osInterfaces.clearArchives();
            mcmcBayes.osInterfaces.clearCODA();
            mcmcBayes.osInterfaces.clearData();
            mcmcBayes.osInterfaces.clearFigures();
            mcmcBayes.osInterfaces.clearInits();
            mcmcBayes.osInterfaces.clearLogs();
            mcmcBayes.osInterfaces.clearTemp();
            mcmcBayes.osInterfaces.clearOutput();
        end        
        
        function clearArchives()
            directory=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.archivesDir);
            ARCHIVEfiles = dir(fullfile(directory,'*.mat'));
            for fileindex=1:numel(ARCHIVEfiles)
                filename=sprintf('%s%s',directory,ARCHIVEfiles(fileindex).name);
                delete(filename);
            end
        end
        
        function clearCODA()
            directory=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataOutDir);
            CODAfiles = dir(fullfile(directory,'*.txt'));
            for fileindex=1:numel(CODAfiles)
                filename=sprintf('%s%s',directory,CODAfiles(fileindex).name);
                delete(filename);
            end
            CODAfiles = dir(fullfile(directory,'*.csv'));
            for fileindex=1:numel(CODAfiles)
                filename=sprintf('%s%s',directory,CODAfiles(fileindex).name);
                delete(filename);
            end            
        end
        
        function clearData()
            directory=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataInDir);
            files = dir(fullfile(directory,'*.txt'));
            for fileindex=1:numel(files)
                filename=sprintf('%s%s',directory,files(fileindex).name);
                delete(filename);
            end
            files = dir(fullfile(directory,'*.R'));
            for fileindex=1:numel(files)
                filename=sprintf('%s%s',directory,files(fileindex).name);
                delete(filename);
            end            
            fclose('all');%sometimes MATLAB leaves these open
            files = dir(fullfile(directory,'*.mat'));
            for fileindex=1:numel(files)
                filename=sprintf('%s/%s',directory,files(fileindex).name);
                delete(filename);
            end            
        end

        function clearFigures()
        % CLEARFIGURES Clears the *.fig files created by the various plot routines.   
            directory=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.figuresDir);
            fclose('all');%sometimes MATLAB leaves these open
            files = dir(fullfile(directory,'*.fig'));
            for fileindex=1:numel(files)
                filename=sprintf('%s%s',directory,files(fileindex).name);
                delete(filename);
            end            
        end          
        
        function clearInits()
            directory=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerInitsDir);
            files = dir(fullfile(directory,'*.txt'));
            for fileindex=1:numel(files)
                filename=sprintf('%s%s',directory,files(fileindex).name);
                delete(filename);
            end
            files = dir(fullfile(directory,'*.R'));
            for fileindex=1:numel(files)
                filename=sprintf('%s%s',directory,files(fileindex).name);
                delete(filename);
            end            
        end
        
        function clearLogs()
            directory=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.logsDir);
            files = dir(fullfile(directory,'*.txt'));
            for fileindex=1:numel(files)
                filename=sprintf('%s%s',directory,files(fileindex).name);
                delete(filename);
            end
        end
                
        function clearTemp()
            directory=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.tempDir);
            msgfiles = dir(fullfile(directory,'*.txt'));
            for fileindex=1:numel(msgfiles)
                filename=sprintf('%s%s',directory,msgfiles(fileindex).name);
                delete(filename);
            end
            scriptfiles = dir(fullfile(directory,'*.py'));
            for fileindex=1:numel(scriptfiles)
                filename=sprintf('%s%s',directory,scriptfiles(fileindex).name);
                delete(filename);
            end              
        end
        
        function clearOutput()
            directory=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.formattedOutputDir);
            files = dir(fullfile(directory,'*.csv'));
            for fileindex=1:numel(files)
                filename=sprintf('%s%s',directory,files(fileindex).name);
                delete(filename);
            end
        end        
        
        function [reposFileName]=getReposFileName()
            reposFileName='repositories.xml';
        end
        
        function [cfgFileName]=getConfigFileName()
            cfgFileName='cfgMcmcBayes.xml';
        end
        
        function [cfgFileName]=getConfigExtFileName()
            cfgFileName='cfgMcmcBayesExtension.xml';
        end        
    end

    methods
        function obj = osInterfaces(settype, cfgMcmcBayes)%constructor
            obj.type=settype;
            obj.bashCmdPid=-1;
            obj.bashCmdState=mcmcBayes.t_runState.Idle;
            obj.bashCmdExitCode=0;
            obj.bashCmdLogFile='';

            obj.archivesDir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.archivesDir); %create /archives
            obj.samplerDataOutDir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataOutDir); %create /coda
            obj.cfgDir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.configDir);
            obj.inputDataDir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.inputDataDir); %create /data            
            obj.samplerDataInDir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataInDir); %create /data
            obj.docDir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.docDir);
            obj.figuresDir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.figuresDir); %create /figures
            obj.samplerInitsDir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerInitsDir); %create /inits
            obj.logsDir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.logsDir); %create /logs
            obj.matlabDir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.matlabDir);
            obj.mcmcBayesDir=mcmcBayes.osInterfaces.getmcmcBayesPath();
            obj.tempDir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.tempDir); %create /temp
            obj.poisonFile=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.poisonFile);
            obj.formattedOutputDir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.formattedOutputDir);
            obj.pythonUtilsScript=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.pythonUtilsFile);
            
            obj.pythonPath=cfgMcmcBayes.cfgOs.python;                         
            obj.javacPath=cfgMcmcBayes.cfgOs.javac;                   
            
            [obj.maxEngines]=mcmcBayes.osInterfaces.getNumCPUS(obj.pythonPath);
            if (obj.maxEngines~=4) && (obj.maxEngines~=8)
                warning('Number of CPUs detected may be incorrect!');
            end
        end
        
        function checkForKillFile(obj)
%todo: use mcmcbayes_utils.py
            if mcmcBayes.osInterfaces.doesFileExist(obj.poisonFile)%if the kill file exists, quit this MATLAB instance
                stdout=1;
                disp('################################################');
                fprintf(stdout,'Kill file detected, quitting MATLAB in 5 seconds');
                setDesktopStatus('Kill file detected, quitting MATLAB in 5 seconds')
                if obj.bashCmdState==mcmcBayes.t_runState.Running
                    obj.killPIDS(obj.bashCmdPid);
                end
                disp('################################################');
                pause(5);
                quit force;
            end
        end          
        
        function killPID(obj, pid)
            %all the python paths should use forward slashes '/'
            fprintf(1,'Killing the process with pid=%d\n',pid);    
            
            switch osInterfaces.getOSInterfaceType()
                case t_mcmcBayesosInterfaces.UbuntuLinux
                    commandstr=sprintf('%s "%s" KILLPID --killPID %d', obj.pythonPath, obj.pythonUtilsScript, pid);
                    [status,cmdout] = system(commandstr); 
                case {t_mcmcBayesosInterfaces.Windows7,mcmcBayes.t_osInterfaces.Windows10}
                    commandstr=sprintf('"%s" "%s" KILLPID --killPID %d', obj.pythonPath, obj.pythonUtilsScript, pid);
                    [status,cmdout] = system(commandstr); 
                otherwise
                    error('unsupported t_mcmcBayesosInterfaces');
            end
            
            if ~status==0 %ok
                error('Error running killPID()');
            else
                disp(cmdout);
            end
        end        
        
        function killPIDS(obj, parentPid)
            %all the python paths should use forward slashes '/'
            fprintf(1,'Killing the processes with parentPid=%d\n',parentPid);
            
            switch osInterfaces.getOSInterfaceType()
                case {t_mcmcBayesosInterfaces.UbuntuLinux,t_mcmcBayesosInterfaces.MacOS}
                    commandstr=sprintf('%s "%s" KILLPIDS --killParentPID %d', obj.pythonPath, obj.pythonUtilsScript, parentPid);
                    [status,cmdout] = system(commandstr); 
                case {t_mcmcBayesosInterfaces.Windows7,mcmcBayes.t_osInterfaces.Windows10}
                    commandstr=sprintf('"%s" "%s" KILLPIDS --killParentPID %d', obj.pythonPath, obj.pythonUtilsScript, parentPid);
                    [status,cmdout] = system(commandstr); 
            end            
            
            if ~status==0 %ok
                error('Error running KILLPIDS()');
            else
                disp(cmdout);
            end
        end   
        
        function spawnMATLABEngines(obj, runView, matlabfuncs, logfilenames)            %all the python paths should use forward slashes '/'
            stdout=1;
            fprintf(stdout, 'forkMATLABScripts()\n');

            logbasepath=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.logsDir);
            
            for index=1:size(matlabfuncs,1)
                logUUID=java.util.UUID.randomUUID;
                logfilename_ext = strcat(logbasepath, cast(logUUID.toString(),'char'), '_log', '.txt');
                if ~isempty(matlabfuncs{index})
                    matlabfunc=sprintf('"%s"',matlabfuncs{index});
                    logfilename=sprintf('"%s%s"',logbasepath,logfilenames{index});
                    switch mcmcBayes.osInterfaces.getOSInterfaceType()
                        %these need to run in the background or they will pend on it finishing
                        case mcmcBayes.t_osInterfaces.MacOS
                            syscommand=sprintf('system(''chmod +x "%s"'');',obj.pythonUtilsScript);%make the script executable
                            eval(syscommand);
                            commandstr=sprintf('%s "%s" SPAWNMATLABENGINES --pythonPath "%s" --pythonScript "%s" --startupDir "%s" --killFile "%s" --maxEngines %d --matlabfuncs %s --logfilenames %s --runView "%s" 2>&1 > %s &', ...
                                obj.pythonPath, obj.pythonUtilsScript, obj.pythonPath, obj.pythonUtilsScript, obj.mcmcBayesDir, obj.poisonFile, obj.maxEngines, matlabfunc, logfilename, mcmcBayes.t_runView.tostring(runView), logfilename_ext);
                        case mcmcBayes.t_osInterfaces.UbuntuLinux
                            mcmcBayes.UbuntuLinux.checkCgroups();
                            syscommand=sprintf('system(''chmod +x "%s"'');',obj.pythonUtilsScript);%make the script executable
                            eval(syscommand);
                            commandstr=sprintf('%s "%s" SPAWNMATLABENGINES --pythonPath "%s" --pythonScript "%s" --startupDir "%s" --killFile "%s" --maxEngines %d --matlabfuncs %s --logfilenames %s --runView "%s" 2>&1 > %s &', ...
                                obj.pythonPath, obj.pythonUtilsScript, obj.pythonPath, obj.pythonUtilsScript, obj.mcmcBayesDir, obj.poisonFile, obj.maxEngines, matlabfunc, logfilename, mcmcBayes.t_runView.tostring(runView), logfilename_ext);
                        case {mcmcBayes.t_osInterfaces.Windows7,mcmcBayes.t_osInterfaces.Windows10} 
                            commandstr=sprintf('start "forkMATLABScripts" /BELOWNORMAL /B "%s" "%s" SPAWNMATLABENGINES --pythonPath "%s" --pythonScript "%s" --startupDir "%s" --killFile "%s" --maxEngines %d --matlabfuncs %s --logfilenames %s --runView "%s" 2>&1 >> %s', ...
                                obj.pythonPath, obj.pythonUtilsScript, obj.pythonPath, obj.pythonUtilsScript, obj.mcmcBayesDir, obj.poisonFile, obj.maxEngines, matlabfunc, logfilename, mcmcBayes.t_runView.tostring(runView), logfilename_ext);
                        otherwise
                            error('unsupported t_mcmcBayesosInterfaces');
                    end
                    fprintf(stdout,'Forking ''%s''\n', commandstr);
                    [status,cmdout] = system(commandstr);

                    if ~status==0 %ok
                        error('Error running forkMATLABScripts()');
                    else
                        disp(cmdout);
                    end
                end
            end
        end        
        
        function deleteCmdMsgFile(obj)
            delete(obj.bashCmdMsgFile)
        end
        
        function retobj=runBashCommand(obj, bashCmdShortcut, bashCmdLogFile, runView)
            %all the paths in the constructed python script should use forward slashes '/'
            stdout=1;
            obj.bashCmdUuid=java.util.UUID.randomUUID;                                 
            
            shellCmd=mcmcBayes.osInterfaces.convPathStrUnix(bashCmdShortcut);
            obj.bashCmdMsgFile = strcat(obj.tempDir, cast(obj.bashCmdUuid.toString(),'char'), '-bashCmdStatus', '.txt');
            obj.bashCmdLogFile=mcmcBayes.osInterfaces.convPathStrUnix(bashCmdLogFile);
            shellCmdRunView=strrep(mcmcBayes.t_runView.tostring(runView),'mcmcBayes.t_runView.','');
            argsstr=sprintf('RUNBASHCMD --msgFile %s --shellRunView %s --shellLog %s --shellCmd %s', ...
                obj.bashCmdMsgFile, shellCmdRunView, obj.bashCmdLogFile, shellCmd);

            commandstr=sprintf('%s -m %s %s', obj.pythonPath, obj.getUtilsModuleName(), argsstr);
            %disp(commandstr);
            [status,cmdout] = system(commandstr,'-echo');
            %disp(cmdout)
            
            if status==0 %ok
                mcmcBayes.osInterfaces.waitFileExists(obj.bashCmdMsgFile)    

                obj.bashCmdName=shellCmd;
                %fprintf(stdout,'PROCESSNAME=%s\n',obj.bashCmdName);                            
                                
                ready=false;                
                while ready==false
                    pause(0.1);
                    drawnow;
                    querystr=fquery(obj.bashCmdMsgFile,'##PID##');
                    if ~strcmp(querystr,'NOTFOUND') && ~isempty(querystr)
                        obj.bashCmdPid=sscanf(querystr,'%d');
                        %fprintf(stdout,'PID=%d\n',obj.bashCmdPid);                        
                        querystr=fquery(obj.bashCmdMsgFile,'##STATE##');%STATE is written last
                        if ~strcmp(querystr,'NOTFOUND') && ~isempty(querystr)                           
                            querystr=fquery(obj.bashCmdMsgFile,'##PROCESSCMDLINE##');
                            if ~strcmp(querystr,'NOTFOUND') && ~isempty(querystr)
                                obj.bashCmdLine=querystr;                            
                                %fprintf(stdout,'PROCESSCMDLINE=%s\n',obj.bashCmdLine);
                                ready=true;
                            end
                        end
                    end                    
                end      
            else
                error('Error with runBashCommand()');                
            end
            retobj=obj;
        end

        function retobj=spawnBashCommand(obj, bashCmdShortcut, bashCmdLogFile, runView)            
            stdout=1;
            %all the paths in the constructed python script should use forward slashes '/'
            obj.bashCmdUuid=java.util.UUID.randomUUID;
            
            shellCmd=mcmcBayes.osInterfaces.convPathStrUnix(bashCmdShortcut);
            obj.bashCmdMsgFile = strcat(obj.tempDir, cast(obj.bashCmdUuid.toString(),'char'), '-bashCmdStatus', '.txt');
            obj.bashCmdLogFile=mcmcBayes.osInterfaces.convPathStrUnix(bashCmdLogFile);
            shellCmdRunView=strrep(mcmcBayes.t_runView.tostring(runView),'mcmcBayes.t_runView.','');

            %remove all the prefix path and '/' characters and use the binary or script name
            if max(strfind(shellCmd,'/'))>1
                startchar=max(strfind(shellCmd,'/'));
            else
                startchar=1;
            end
            shellTitle=shellCmd(startchar+1:end);
            
            switch obj.getOSInterfaceType()
                case {mcmcBayes.t_osInterfaces.MacOS,mcmcBayes.t_osInterfaces.UbuntuLinux}
                    argsstr=sprintf('SPAWNBASHCMD --msgFile %s --shellRunView %s --shellLog %s --shellTitle %s --shellCmd %s', ...
                        obj.bashCmdMsgFile, shellCmdRunView, obj.bashCmdLogFile, shellTitle, shellCmd);
                    commandstr=sprintf('unset LD_LIBRARY_PATH && unset PYTHONPATH && %s -m %s %s', obj.pythonPath, obj.getUtilsModuleName(), argsstr);%For Ubuntu, we have an issue with building CmdStan and the LD_LIBRARY_PATH env variable that MATLAB sets; just unset it for a simple fix
                case {mcmcBayes.t_osInterfaces.Windows7, mcmcBayes.t_osInterfaces.Windows10}                   
                    argsstr=sprintf('SPAWNBASHCMD --msgFile %s --shellRunView %s --shellLog %s --shellTitle "%s" --shellCmd %s', ...
                        obj.bashCmdMsgFile, shellCmdRunView, obj.bashCmdLogFile, shellTitle, shellCmd);
                    commandstr=sprintf('%s -m %s %s', obj.pythonPath, obj.getUtilsModuleName(), argsstr);
            end
            %disp(strcat('spawnBashCommand is calling:  ',commandstr));
            [status,cmdout] = system(commandstr,'-echo');
            %disp(cmdout);
            
            if status==0 %ok
                mcmcBayes.osInterfaces.waitFileExists(obj.bashCmdMsgFile)    

                obj.bashCmdName=shellCmd;
                %fprintf(stdout,'PROCESSNAME=%s\n',obj.bashCmdName);                            
                
                ready=false;                
                while ready==false
                    pause(0.1);
                    drawnow;
                    querystr=fquery(obj.bashCmdMsgFile,'##PID##');
                    if ~strcmp(querystr,'NOTFOUND') && ~isempty(querystr)
                        obj.bashCmdPid=sscanf(querystr,'%d');
                        %fprintf(stdout,'PID=%d\n',obj.bashCmdPid);                        
                        querystr=fquery(obj.bashCmdMsgFile,'##STATE##');%STATE is written last
                        if ~strcmp(querystr,'NOTFOUND') && ~isempty(querystr)                           
                            querystr=fquery(obj.bashCmdMsgFile,'##PROCESSCMDLINE##');
                            if ~strcmp(querystr,'NOTFOUND') && ~isempty(querystr)
                                obj.bashCmdLine=querystr;                            
                                %fprintf(stdout,'PROCESSCMDLINE=%s\n',obj.bashCmdLine);
                                ready=true;
                            end
                        end
                    end                    
                end      
            else
                error('Error with runBashCommand()');                
            end            
            
            retobj=obj;
        end        
        
        function [retobj, commandstr]=updateBashCmdStatus(obj, setcommandstr) %e.g., only care about status for MCMC sampling requests           
             if (isempty(setcommandstr)) %not regenerating commandstr because of a potential memory leak with java when this is run thousands of times
                 argsstr=sprintf('QUERYBASHCMDSTATUS --msgFile %s --processPID=%d', obj.bashCmdMsgFile, obj.bashCmdPid);
                 switch obj.type
                     case {mcmcBayes.t_osInterfaces.MacOS,mcmcBayes.t_osInterfaces.UbuntuLinux}
                        commandstr=sprintf('unset LD_LIBRARY_PATH && unset PYTHONPATH && %s -m %s %s', obj.pythonPath, obj.getUtilsModuleName(), argsstr);%For Ubuntu, we have an issue with building CmdStan and the LD_LIBRARY_PATH env variable that MATLAB sets; just unset it for a simple fix
                     case {mcmcBayes.t_osInterfaces.Windows7, mcmcBayes.t_osInterfaces.Windows10}
                        commandstr=sprintf('%s -m %s %s', obj.pythonPath, obj.getUtilsModuleName(), argsstr);
                 end                 
             else%otherwise, we use the one passed in
                 commandstr=setcommandstr;
             end
            %disp(strcat('updateBashCmdStatus is calling:  ',commandstr));
            [status,cmdout] = system(commandstr,'-echo');
            %disp(cmdout);
            
            if status==0 %ok
                mcmcBayes.osInterfaces.waitFileExists(obj.bashCmdMsgFile)

                statusstr=fquery(obj.bashCmdMsgFile,'##STATE##');
                if ~strcmp(statusstr,'NOTFOUND')
                    statusstr=sscanf(statusstr,'%s');
                    obj.bashCmdState=mcmcBayes.t_runState.fromstring(strcat('mcmcBayes.t_runState.',statusstr));
                    if obj.bashCmdState==mcmcBayes.t_runState.Finished || obj.bashCmdState==mcmcBayes.t_runState.Error
                        exitcodestr=fquery(obj.bashCmdMsgFile,'##EXITCODE##');%read the exitcode from the UUIDfilestr
                        if ~strcmp(exitcodestr,'NOTFOUND')
                            obj.bashCmdState=mcmcBayes.t_runState.Finished;
                            obj.bashCmdExitCode=sscanf(exitcodestr,'%d');
                        end                        
                    end
                end
            else
                error('Error running updateSamplerStatus()');
            end
            retobj=obj;
        end        
    end
    
    methods %getter/setter functions
        function obj = set.type(obj,settype)
            if ~isa(settype,'mcmcBayes.t_osInterfaces')
                error('mcmcBayes.osInterfaces.type must be type mcmcBayes.t_osInterfaces');
            else
                switch settype
                    case {mcmcBayes.t_osInterfaces.Windows7,mcmcBayes.t_osInterfaces.Windows10,mcmcBayes.t_osInterfaces.UbuntuLinux,mcmcBayes.t_osInterfaces.MacOS}
                        obj.type = settype;
                    otherwise
                        error('unsupported osInterfaces subclass');
                end
            end
        end
        
        function value = get.type(obj)
            value=obj.type;
        end

        function obj=set.archivesDir(obj,setarchivesDir) 
            if mcmcBayes.osInterfaces.doesFileExist(setarchivesDir) %is a folder
                obj.archivesDir=setarchivesDir;
            else
                mkdir(setarchivesDir);%try to create it if it does not exist
                if mcmcBayes.osInterfaces.doesFileExist(setarchivesDir) %is a folder
                    obj.archivesDir=setarchivesDir;
                else
                    error('error creating archivesDir folder');
                end
            end
        end
        
        function value=get.archivesDir(obj) 
            value=obj.archivesDir;
        end
        
        function obj=set.samplerDataOutDir(obj,setsamplerDataOutDir) 
            if mcmcBayes.osInterfaces.doesFileExist(setsamplerDataOutDir) %is a folder
                obj.samplerDataOutDir=setsamplerDataOutDir;
            else
                mkdir(setsamplerDataOutDir);%try to create it if it does not exist
                if mcmcBayes.osInterfaces.doesFileExist(setsamplerDataOutDir) %is a folder
                    obj.samplerDataOutDir=setsamplerDataOutDir;
                else
                    error('error creating samplerDataOutDir folder');
                end
            end
        end
        
        function value=get.samplerDataOutDir(obj) 
            value=obj.samplerDataOutDir;
        end

        function obj=set.formattedOutputDir(obj,setformattedOutputDir) 
            if mcmcBayes.osInterfaces.doesFileExist(setformattedOutputDir) %is a folder
                obj.formattedOutputDir=setformattedOutputDir;
            else
                mkdir(setformattedOutputDir);%try to create it if it does not exist
                if mcmcBayes.osInterfaces.doesFileExist(setformattedOutputDir) %is a folder
                    obj.formattedOutputDir=setformattedOutputDir;
                else
                    error('error creating formattedOutputDir folder');
                end
            end
        end
        
        function value=get.formattedOutputDir(obj) 
            value=obj.formattedOutputDir;
        end        

        function obj=set.inputDataDir(obj,setinputDataDir) 
            if mcmcBayes.osInterfaces.doesFileExist(setinputDataDir) %is a folder
                obj.inputDataDir=setinputDataDir;
            else
                mkdir(setinputDataDir);%try to create it if it does not exist
                if mcmcBayes.osInterfaces.doesFileExist(setinputDataDir) %is a folder
                    obj.inputDataDir=setinputDataDir;
                else
                    error('error creating inputDataDir folder');
                end
            end
        end
        
        function value=get.inputDataDir(obj) 
            value=obj.inputDataDir;
        end
        
        function obj=set.samplerDataInDir(obj,setsamplerDataInDir) 
            if mcmcBayes.osInterfaces.doesFileExist(setsamplerDataInDir) %is a folder
                obj.samplerDataInDir=setsamplerDataInDir;
            else
                mkdir(setsamplerDataInDir);%try to create it if it does not exist
                if mcmcBayes.osInterfaces.doesFileExist(setsamplerDataInDir) %is a folder
                    obj.samplerDataInDir=setsamplerDataInDir;
                else
                    error('error creating samplerDataInDir folder');
                end
            end
        end
        
        function value=get.samplerDataInDir(obj) 
            value=obj.samplerDataInDir;
        end

        function obj=set.figuresDir(obj,setfiguresDir) 
            if mcmcBayes.osInterfaces.doesFileExist(setfiguresDir) %is a folder
                obj.figuresDir=setfiguresDir;
            else
                mkdir(setfiguresDir);%try to create it if it does not exist
                if mcmcBayes.osInterfaces.doesFileExist(setfiguresDir) %is a folder
                    obj.figuresDir=setfiguresDir;
                else
                    error('error creating figures folder');
                end
            end
        end
        
        function value=get.figuresDir(obj)
            value=obj.figuresDir;
        end     
        
        function obj=set.samplerInitsDir(obj,setsamplerInitsDir)
            if mcmcBayes.osInterfaces.doesFileExist(setsamplerInitsDir) %is a folder
                obj.samplerInitsDir=setsamplerInitsDir;
            else
                mkdir(setsamplerInitsDir);%try to create it if it does not exist
                if mcmcBayes.osInterfaces.doesFileExist(setsamplerInitsDir) %is a folder
                    obj.samplerInitsDir=setsamplerInitsDir;
                else
                    error('error creating samplerInitsDir folder');
                end
            end
        end
        
        function value=get.samplerInitsDir(obj)
            value=obj.samplerInitsDir;
        end
        
        function obj=set.logsDir(obj,setlogsDir) 
            if mcmcBayes.osInterfaces.doesFileExist(setlogsDir) %is a folder
                obj.logsDir=setlogsDir;
            else
                mkdir(setlogsDir);%try to create it if it does not exist
                if mcmcBayes.osInterfaces.doesFileExist(setlogsDir) %is a folder
                    obj.logsDir=setlogsDir;
                else
                    error('error creating logsDir folder');
                end
            end
        end
        
        function value=get.logsDir(obj)
            value=obj.logsDir;
        end             
        
        function obj=set.tempDir(obj,settempDir) 
            if mcmcBayes.osInterfaces.doesFileExist(settempDir) %is a folder
                obj.tempDir=settempDir;
            else
                mkdir(settempDir);%try to create it if it does not exist
                if mcmcBayes.osInterfaces.doesFileExist(settempDir) %is a folder
                    obj.tempDir=settempDir;
                else
                    error('error creating tempDir folder');
                end
            end
        end
        
        function value=get.tempDir(obj) 
            value=obj.tempDir;
        end                             
        
        function obj=set.pythonPath(obj,setpythonPath)
            if mcmcBayes.osInterfaces.doesFileExist(setpythonPath)
                obj.pythonPath=setpythonPath;
            else
                error('Python shortcut is missing!');
            end
        end
        
        function value=get.pythonPath(obj)
            value=obj.pythonPath;
        end
        
        function obj=set.javacPath(obj,setjavacPath)
            if mcmcBayes.osInterfaces.doesFileExist(setjavacPath)
                obj.javacPath=setjavacPath;
            else
                error('Javac shortcut is missing!');
            end
        end
        
        function value=get.javacPath(obj)
            value=obj.javacPath;
        end
    end
end