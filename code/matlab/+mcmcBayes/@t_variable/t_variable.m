classdef t_variable < int32
    enumeration
        constant (1)
        stochastic (2)
        stochasticProcess (3)
    end
    
    methods (Static)        
        function intvalue=toint(enumobj)
            switch enumobj          
                case {mcmcBayes.t_variable.constant, mcmcBayes.t_variable.stochastic, ...
                        mcmcBayes.t_variable.stochasticProcess}
                    intvalue=int32(enumobj);
                otherwise
                    error('unsupported mcmcBayes.t_variable type');
            end                
        end        
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_variable.constant
                    strvalue='mcmcBayes.t_variable.constant';
                case mcmcBayes.t_variable.stochastic
                    strvalue='mcmcBayes.t_variable.stochastic';                    
                case mcmcBayes.t_variable.stochasticProcess
                    strvalue='mcmcBayes.t_variable.stochasticProcess';                    
                otherwise
                    error('unsupported mcmcBayes.t_variable type');
            end
        end

        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_variable.constant)
                    enumobj=mcmcBayes.t_variable.constant;
                case int32(mcmcBayes.t_variable.stochastic)
                    enumobj=mcmcBayes.t_variable.stochastic;
                case int32(mcmcBayes.t_variable.stochasticProcess)
                    enumobj=mcmcBayes.t_variable.stochasticProcess;
                otherwise
                    error('unsupported mcmcBayes.t_variable type');
            end
        end
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_variable.constant')==1
                enumobj=mcmcBayes.t_variable.constant;
            elseif strcmp(strval,'mcmcBayes.t_variable.stochastic')==1
                enumobj=mcmcBayes.t_variable.stochastic;
            elseif strcmp(strval,'mcmcBayes.t_variable.stochasticProcess')==1
                enumobj=mcmcBayes.t_variable.stochasticProcess;
            else
                error('unsupported mcmcBayes.t_variable type');
            end
        end          
    end    
end
