% mcmcBayes is a readily extensible, open-source, object-oriented software framework supporting Markov chain Monte Carlo (MCMC) based Bayesian analyses using MATLAB
% with Stan, OpenBUGS or JAGS. It was initially written to assist in the evaluation of several popular fault counting models applied to modeling software 
% vulnerability discovery counts (Vulnerability Discovery Models, or VDMs). mcmcBayes is similar to the R-based ShinyStan, CODA and BOA packages, as it supports 
% convergence diagnosis and output analysis. Unlike these, mcmcBayes uses MATLAB and is an automation framework for performing complex sequences of diverse mcmc 
% simulations, model choice using Bayes Factors, and Bayesian model averaging.
%
% View doc/documentation.html locally in a web-browser for more information.
classdef sequence < matlab.mixin.Heterogeneous
    % sequence is a base class for running simulations or custom combinations of them
    % matlab.mixin.Heterogeneous is an abstract class that supports forming heterogeneous arrays

    properties
        % These properties are all mostly related to configuration or sequence control. 
        
        % Settings that are not saved
        cfgMcmcBayes
        repositories
                
        % Miscellaneous objects
        osInterface                             %Instantiated mcmcBayes.osInterfaces object (e.g., mcmcBayes.{UbuntuLinux,Windows7,MacOS})

        % Sequence details and objects
        uuid                                    %Sequence unique identifier.
        type
        simulations                             %Array of instantiated mcmcBayes.simulation objects.
    end   
    
    methods (Abstract)
        getData(obj, type, data, varargin);
    end
    
    methods (Static)
        function test()
            disp('Hello!');
        end
        
        function defSequence=getDefaultSequence(seqType, seqUuid)
        % defSequence=getDefaultSequence(seqType, seqUuid)
        %   Creates a default sequence object of seqType
            set(0,'DefaultFigureWindowStyle','docked')

            [tmpConfig,tmpRepos]=mcmcBayes.osInterfaces.loadConfig;
            
            handle=mcmcBayes.sequence.getHandleConstructor(seqType);            
            defSequence=handle(tmpConfig,tmpRepos,seqUuid);
                        
            uuids=defSequence.getUuids(seqUuid);
            
            predictionsToCapture=mcmcBayes.simulation.getDefaultPredictionsToCapture();
            for i=1:numel(uuids)
                init=false;%sequence, simulationUuid, predictionsToCapture, init
                simStruct=mcmcBayes.simulation.getSimulationFromXML(defSequence,uuids{i},predictionsToCapture, init);

                defSequence.simulations(i)=simStruct;
            end
        end        
                        
        function [retSequence]=loadSequence(seqType, seqUuid)
        % [retSequence]=protLoadSequence(defSequence)
        %   Reconstructs a sequence object from its archived simulation objects.
            stdout=1;
            defSequence=mcmcBayes.sequence.getDefaultSequence(seqType, seqUuid);
            retSequence=defSequence;
            for i=1:numel(defSequence.simulations)
                hashStr=num2str(string2hash(sprintf('%s%d',defSequence.type, mcmcBayes.t_samplerInterfaces.toint( ...
                    defSequence.simulations(i).samplerInterface.type))));
                setPrefix=strcat(defSequence.uuid, '-' , hashStr);
                filename=sprintf('%s-%s-postMargLogLikelihood.mat', setPrefix, defSequence.simulations(i).uuid);
                fprintf(stdout,'Loading %s.\n',filename);
                retSequence.simulations(i)=mcmcBayes.simulation.loadArchive(filename);
            end
        end
        
        function generateFigures(archivedSequence, cfgPlotDiagnostics, cfgPlotPredictionsUsingPrior, cfgRunPowerPosteriorMcmcSequence, cfgPlotPredictionsUsingPosterior) 
        % generateFigures(archivedSequence, cfgPlotDiagnostics, cfgPlotPredictionsUsingPrior, cfgRunPowerPosteriorMcmcSequence, cfgPlotPredictionsUsingPosterior) 
        %   Recreates all the requested figures from the archivedSequence
            for i=1:numel(archivedSequence.simulations)
                if ~isempty(archivedSequence.simulations(i))
                    archivedSequence.simulations(i).generateFigures(cfgPlotDiagnostics, cfgPlotPredictionsUsingPrior, cfgRunPowerPosteriorMcmcSequence, cfgPlotPredictionsUsingPosterior);
                end
            end
            %plot bma
            if cfgRunPowerPosteriorMcmcSequence %setup temperature indices to run sampling sequence for
                plotIndividuals=true;
                plotBma=true;
                printStatus=false;
                [bmaNhat, bmaMsfe]=mcmcBayes.sequence.bma(archivedSequence, '0000000', plotIndividuals, plotBma, printStatus);
            end
        end
        
        function archivedSequence=prettyPrintResults(archivedSequence, cfgRunPowerPosteriorMcmcSequence)
        % prettyPrintResults(archivedSequence, cfgRunPowerPosteriorMcmcSequence)
        %   This function calls subfunctions that pretty print all the results from a session
            mcmcBayes.sequence.prettyPrintVarPriorDistHypers(archivedSequence)
            mcmcBayes.sequence.prettyPrintVarInitials(archivedSequence)
            mcmcBayes.sequence.prettyPrintVarAnalysisResultsAndDiagnostics(archivedSequence)
            mcmcBayes.sequence.prettyPrintForecastingResults(archivedSequence, cfgRunPowerPosteriorMcmcSequence)
        end
                
        function prettyPrintVarPriorDistHypers(archivedSequence)
        % prettyPrintVarPriorDistHypers(archivedSequence)
        %   Pretty prints the prior distribution hyperparameters
            stdout=1;
            fprintf(stdout,'############################################\n');
            fprintf(stdout,'Estimated prior distribution hyperparameters\n');
            fprintf(stdout,'SeqUUID=%s\n',archivedSequence.uuid);
            fprintf(stdout,'############################################\n');

            for i=1:numel(archivedSequence.simulations)
                fprintf(stdout, 'SimUUID=%s, Sampler=%s:\n', archivedSequence.simulations(i).uuid, ...
                    mcmcBayes.t_samplerInterfaces.tostring(archivedSequence.simulations(i).samplerInterface.type));
                if ~isempty(archivedSequence.simulations(i))
                    for varId=1:size(archivedSequence.simulations(i).priorVariables,1)
                        fprintf(stdout, '\t%s~%s\n', archivedSequence.simulations(i).priorVariables(varId).name, ...
                            mcmcBayes.distribution.tostring(archivedSequence.simulations(i).priorVariables(varId).distribution));%fwrite the initial value
                    end
                else
                    fprintf(stdout, '\tNA\n');%fwrite the output results
                end
            end
        end
        
        function prettyPrintVarInitials(archivedSequence)
        % prettyPrintVarInitials(archivedSequence)
        %   Pretty prints the model variable initial values
            stdout=1;
            fprintf(stdout,'#######################################\n');
            fprintf(stdout,'Estimated model variable initial values\n');
            fprintf(stdout,'SeqUUID=%s\n',archivedSequence.uuid);
            fprintf(stdout,'#######################################\n');

            for i=1:numel(archivedSequence.simulations)
                fprintf(stdout, 'SimUUID=%s, Sampler=%s:\n', archivedSequence.simulations(i).uuid, ...
                    mcmcBayes.t_samplerInterfaces.tostring(archivedSequence.simulations(i).samplerInterface.type));
                if ~isempty(archivedSequence.simulations(i))
                    for varId=1:size(archivedSequence.simulations(i).posteriorVariables,1)
                        chanId=1;
                        tempId=archivedSequence.simulations(1).getDefaultPowerPosteriorN+1;
                        if size(archivedSequence.simulations(i).posteriorVariables(varId,chanId,tempId).MCMC_initvalue{:},1)>1
                            fprintf(stdout, '\t%s=%s\n', archivedSequence.simulations(i).posteriorVariables(varId,chanId,tempId).name, ...
                                conversionUtilities.matrix_tostring(archivedSequence.simulations(i).posteriorVariables(varId,chanId,tempId).MCMC_initvalue{:}));%fwrite the initial value
                        else
                            fprintf(stdout, '\t%s=%s\n', archivedSequence.simulations(i).posteriorVariables(varId,chanId,tempId).name, ...
                                conversionUtilities.scalar_tostring(archivedSequence.simulations(i).posteriorVariables(varId,chanId,tempId).MCMC_initvalue{:}));%fwrite the initial value
                        end
                    end
                else
                    fprintf(stdout, '\tNA\n');%fwrite the output results
                end
            end
        end

        function prettyPrintVarAnalysisResultsAndDiagnostics(archivedSequence)
        % prettyPrintVarAnalysisResultsAndDiagnostics(archivedSequence)
        %   Pretty prints the model variable fit results and diagnostics
            stdout=1;
            fprintf(stdout,'##############################################\n');
            fprintf(stdout,'Model variable fitment results and diagnostics\n');
            fprintf(stdout,'SeqUUID=%s\n',archivedSequence.uuid);
            fprintf(stdout,'##############################################\n');

            for i=1:numel(archivedSequence.simulations)
                fprintf(stdout, 'SimUUID=%s, Sampler=%s:\n', archivedSequence.simulations(i).uuid, ...
                    mcmcBayes.t_samplerInterfaces.tostring(archivedSequence.simulations(i).samplerInterface.type));
                fprintf(stdout, '\t           Name:\t        Mean\t      Median\t          SD\t                  {MIN MAX}\t            95%% HPD {LO HI}\t         MCE\n');
                if ~isempty(archivedSequence.simulations(i))
                    numVars=size(archivedSequence.simulations(i).posteriorVariables,1);
                    for varId=1:numVars
                        if numel(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_mean)>1 %vector variable
                            fprintf(stdout, '\t%15s:\t%12s\t%12s\t%12s\t{%12s %12s}\t{%12s %12s}\t%12s\n', ...
                                archivedSequence.simulations(i).posteriorVariables(varId).name, ...
                                conversionUtilities.matrix_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_mean), ...
                                conversionUtilities.matrix_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_median), ...
                                conversionUtilities.matrix_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_sd), ...
                                conversionUtilities.matrix_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_min), ...
                                conversionUtilities.matrix_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_max), ...
                                conversionUtilities.matrix_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_hpdlo), ...
                                conversionUtilities.matrix_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_hpdhi), ...
                                conversionUtilities.matrix_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_mce));%fwrite the output results
                        else %scalar variable
                            fprintf(stdout, '\t%15s:\t%12s\t%12s\t%12s\t{%12s %12s}\t{%12s %12s}\t%12s\n', ...
                                archivedSequence.simulations(i).posteriorVariables(varId).name, ...
                                conversionUtilities.scalar_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_mean), ...
                                conversionUtilities.scalar_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_median), ...
                                conversionUtilities.scalar_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_sd), ...
                                conversionUtilities.scalar_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_min), ...
                                conversionUtilities.scalar_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_max), ...
                                conversionUtilities.scalar_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_hpdlo), ...
                                conversionUtilities.scalar_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_hpdhi), ...
                                conversionUtilities.scalar_tostring(archivedSequence.simulations(i).posteriorVariables(varId).MCMC_mce));%fwrite the output results
                        end
                    end
                else
                    fprintf(stdout, '\tNA\n');%fwrite the output results
                end
            end
        end        
        
        function prettyPrintForecastingResults(archivedSequence, cfgRunPowerPosteriorMcmcSequence)
        % prettyPrintForecastingResults(archivedSequence, cfgRunPowerPosteriorMcmcSequence)
        %   Pretty prints the model prediction results and diagnostics 
            stdout=1;
            fprintf(stdout,'#########################\n');
            fprintf(stdout,'Model forecasting results\n');
            fprintf(stdout,'SeqUUID=%s\n',archivedSequence.uuid);
            fprintf(stdout,'#########################\n');
            
            if cfgRunPowerPosteriorMcmcSequence
                figHash=archivedSequence.uuid;
                plotIndividuals=true;
                plotBma=true;
                printStatus=false;
                [retBmaNhat, retBmaMsfe]=mcmcBayes.sequence.bma(archivedSequence, figHash, plotIndividuals, plotBma, printStatus); 
                retBF=mcmcBayes.sequence.computeBayesFactors(archivedSequence, printStatus);                           
            end
            for i=1:numel(archivedSequence.simulations)
                fprintf(stdout, 'SimUUID=%s, Sampler=%s:\n', archivedSequence.simulations(i).uuid, ...
                    mcmcBayes.t_samplerInterfaces.tostring(archivedSequence.simulations(i).samplerInterface.type));
                if ~isempty(archivedSequence.simulations(i))
                    datavarid=1;
                    fprintf(stdout, '\tPrior-based results\n');
                    fprintf(stdout, '\t\tOriginal Prior Data\n');
                    fprintf(stdout, '\t\t%s=%s\n', archivedSequence.simulations(i).priorData.indVarsName{datavarid}, conversionUtilities.matrix_tostring(archivedSequence.simulations(i).priorData.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent, datavarid)));
                    fprintf(stdout, '\t\t%s=%s\n', archivedSequence.simulations(i).priorData.depVarsName{datavarid}, conversionUtilities.matrix_tostring(archivedSequence.simulations(i).priorData.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.dependent, datavarid)));
                    fprintf(stdout, '\t\tPrior Predicted Data\n');
                    fprintf(stdout, '\t\t%s=%s\n', archivedSequence.simulations(i).priorPredictedData.mean.indVarsName{datavarid}, conversionUtilities.matrix_tostring(archivedSequence.simulations(i).priorPredictedData.mean.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent, datavarid)));
                    fprintf(stdout, '\t\t%s=%s\n', archivedSequence.simulations(i).priorPredictedData.mean.depVarsName{datavarid}, conversionUtilities.matrix_tostring(archivedSequence.simulations(i).priorPredictedData.mean.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.dependent, datavarid)));
                    fprintf(stdout, '\t\t       MSFE\n');
                    fprintf(stdout, '\t\t%0.5e\n',archivedSequence.simulations(i).priorMsfe);                    
                    fprintf(stdout, '\tPosterior-based results\n');
                    fprintf(stdout, '\t\tOriginal Posterior Data\n');
                    fprintf(stdout, '\t\t%s=%s\n', archivedSequence.simulations(i).posteriorData.indVarsName{datavarid}, conversionUtilities.matrix_tostring(archivedSequence.simulations(i).posteriorData.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent, datavarid)));
                    fprintf(stdout, '\t\t%s=%s\n', archivedSequence.simulations(i).posteriorData.depVarsName{datavarid}, conversionUtilities.matrix_tostring(archivedSequence.simulations(i).posteriorData.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.dependent, datavarid)));
                    fprintf(stdout, '\t\tPosterior Predicted Data\n');
                    fprintf(stdout, '\t\t%s=%s\n', archivedSequence.simulations(i).posteriorPredictedData.mean.indVarsName{datavarid}, conversionUtilities.matrix_tostring(archivedSequence.simulations(i).posteriorPredictedData.mean.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent, datavarid)));
                    fprintf(stdout, '\t\t%s=%s\n', archivedSequence.simulations(i).posteriorPredictedData.mean.depVarsName{datavarid}, conversionUtilities.matrix_tostring(archivedSequence.simulations(i).posteriorPredictedData.mean.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.dependent, datavarid)));                   
                    if cfgRunPowerPosteriorMcmcSequence
                        chainId=1;
                        temperatures=archivedSequence.simulations(i).getDefaultPowerPosteriorN+1;
                        printStatus=false;
                        [phi, loglikelihood_mean, loglikelihood_var, loglikelihood_mce]=mcmcBayes.model.computeLogLikelihoods(archivedSequence.simulations(i).osInterface, ...
                            archivedSequence.simulations(i).type, archivedSequence.simulations(i).posteriorVariables, archivedSequence.simulations(i).posteriorData, ...
                            chainId, temperatures, printStatus); 
                        fprintf(stdout, '\t\t       MSFE\t          LL\t    Marg. LL\tMarg. LL MCE\t     KL dist\t         BF (Interpretation)\t\n');
                        fprintf(stdout, '\t\t%0.5e\t%12f\t%12f\t%12f\t%12f\t%0.5e (%s)\n',archivedSequence.simulations(i).posteriorMsfe, loglikelihood_mean(temperatures), ...
                            archivedSequence.simulations(i).posteriorMarginalLogLikelihoodAlt, archivedSequence.simulations(i).powerPosteriorMce, ...
                            archivedSequence.simulations(i).klDistance, retBF(i), mcmcBayes.sequence.interpretBF(retBF(i)));                    
                    else % only MSFE (no BF)
                        fprintf(stdout, '\t\t       MSFE\n');
                        fprintf(stdout, '\t\t%0.5e\n',archivedSequence.simulations(i).posteriorMsfe);                    
                    end
                else
                    fprintf(stdout, 'NA\n');
                end
            end
            if cfgRunPowerPosteriorMcmcSequence
                fprintf(stdout, 'SimUUID=BMA\n');
                fprintf(stdout, '\tPredicted Data\n');
                fprintf(stdout, '\t\t%s=%s\n', archivedSequence.simulations(1).posteriorData.indVarsName{datavarid}, conversionUtilities.matrix_tostring(archivedSequence.simulations(i).posteriorData.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent, datavarid)));
                fprintf(stdout, '\t\t%s=%s\n', archivedSequence.simulations(1).posteriorData.depVarsName{datavarid}, conversionUtilities.matrix_tostring(mean(retBmaNhat,2)));
                fprintf(stdout, '\t\t       MSFE\n');
                fprintf(stdout, '\t\t%0.5e\n',retBmaMsfe);                    
            end            
        end       

        function [retBmaNhat, retBmaMsfe]=bma(archivedSequence, figHash, plotIndividuals, plotBma, printStatus)
        % [retBmaNhat, retBmaMsfe]=bma(archivedSequence, figHash, plotIndividuals, plotBma, printStatus)
        %   Generates the observables from the bma and optionally creates plots
            stdout=1;
            for i=1:numel(archivedSequence.simulations)
                if ~isempty(archivedSequence.simulations(i))
                    prdata(i)=exp(archivedSequence.simulations(i).posteriorMarginalLogLikelihoodAlt);
                else
                    prdata(i)=0;%ensures that we do not sample from the nonexistent model
                end
            end    
            normprdata=prdata./sum(prdata);
            
            %Generate predictions from each model and measure their msfe using posteriorData
            %Create the aggregated predictions and measure its msfe using posteriorData
            varid=1;
            retBmaNhat=zeros(size(archivedSequence.simulations(1).posteriorPredictedData.mean.indVarsValues{:},varid),archivedSequence.simulations(1).predictionsNSamples);%just use the first simulation
            chainid=1;
            tempid=archivedSequence.simulations(1).getDefaultPowerPosteriorN+1;%just use the first simulation
            for i=1:numel(archivedSequence.simulations)
                fprintf(stdout, 'Generating posterior-based observables for %s\n', archivedSequence.simulations(i).name);
                [retPredictedData{i}, retNhat{i}]=archivedSequence.simulations(i).generatePredictions(chainid, tempid);
                [msfe(i)]=mcmcBayes.model.computeMSFE(archivedSequence.simulations(i).posteriorData,retNhat{i});
                retBmaNhat=retBmaNhat+normprdata(i).*retNhat{i};
            end
            retBmaMsfe=mcmcBayes.model.computeMSFE(archivedSequence.simulations(1).posteriorData,retBmaNhat);%just use the first simulations's posteriorData
            %plot the predictions for average and individual models
            if plotBma==true
                fig;
                hold on;
                plot(archivedSequence.simulations(1).posteriorData.indVarsValues{:},archivedSequence.simulations(1).posteriorData.depVarsValues{:},'b*-');%orig, just use the first simulation            
                plot(archivedSequence.simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(retBmaNhat,2),'r-*');%average, just use the first simulation for ind vars
                if plotIndividuals==true
                    for i=1:numel(archivedSequence.simulations)
                        %use independent variables from first model (due to Musa-Okumoto being normalized to effort)
                        if ~isempty(archivedSequence.simulations(i))
                            plot(archivedSequence.simulations(1).posteriorPredictedData.mean.indVarsValues{:},retPredictedData{i}.mean{:},'r:')
                            if printStatus
                                fprintf(1,'For %s model, msfe=%f\n',archivedSequence.simulations(i).name, msfe(i));
                            end
                        end
                    end           
                end
                seqName=strrep(mcmcBayes.t_sequence.tostring(archivedSequence.type), 'mcmcBayes.t_sequence.','');
                filename=sprintf('%s%s-%s-%sBMA%s-chainid%d-tempid%d-PredictionsSet.fig', archivedSequence.osInterface.figuresDir, archivedSequence.uuid, ...
                    figHash, seqName, archivedSequence.uuid, chainid, tempid);            
                savefig(filename);
                hold off;                
            end
        end                
        
        function [retBF]=computeBayesFactors(archivedSequence, printStatus)
        % [retBF]=computeBayesFactors(sequenceType, priordatatype, datatype, datasetid)
        %   This function uses the results from a model and verify sequence that generated predictions from a posterior
        %   distribution for several models and computes their corresponding Bayes factors to support model choice amongst the 
        %   alternatives.  Loads the files to perform the analysis from disk.
            stdout=1;
            LLnum=NaN;
            Num=-1;
            for i=1:numel(archivedSequence.simulations)
                if ~isempty(archivedSequence.simulations(i))
                    ll(i)=archivedSequence.simulations(i).posteriorMarginalLogLikelihoodAlt;             
                else
                    ll(i)=NaN;%ensures that we do not sample from the nonexistent model
                end
                if isnan(LLnum) || ll(i)>LLnum
                    LLnum=ll(i);
                    Num=i;
                end
            end            

            seqName=strrep(mcmcBayes.t_sequence.tostring(archivedSequence.type), 'mcmcBayes.t_sequence.','');
            if printStatus
                fprintf(stdout,'For seqType=%s, seqUuid=%s:\n', seqName, archivedSequence.uuid);
                fprintf(stdout,'\tUsing %s for numerator with LL=%d\n',archivedSequence.simulations(Num).name,ll(Num));
                disp('Kass-Raftery 1995 BF interpretation of scores were:');
                disp('  1 to 3      Not worth more than a bare mention');
                disp('  3 to 20     Positive');
                disp('  20 to 150   Strong');
                disp('  >150        Very strong');
            end
                        
            for Den=1:numel(archivedSequence.simulations)
                LLden=ll(Den);
                if ~isnan(LLden) && ~isnan(LLnum)
                    retBF(Den)=exp(LLnum-LLden);
                    if printStatus
                        fprintf(stdout,'\tBF(%s/%s)=%g/%g=%g, %s\n',archivedSequence.simulations(Num).name,archivedSequence.simulations(Den).name,...
                            LLnum,LLden,retBF(Den),mcmcBayes.sequence.interpretBF(retBF(Den)));
                    end
                else
                    retBF(Den)=NaN;
                    if printStatus
                        fprintf(stdout,'\tBF(???)=NaN\n');
                    end
                end
            end
        end        
        
        function [interpretString]=interpretBF(BF)       
        % [interpretString]=interpretBF(BF) 
        %   This function translates the numeric Bayes factor scores into human readable format.  
        %   The interpretation is based on recommendations from Kass, Raftery 1995 (Very strong BF>=150, 
        %   Strong 150>BF>=20, Positive 20>BF>=3, Not worth more than a bare mention 3>BF>1, H1 (i.e., model is compared
        %   against itself, as it is the alternate hypothesis).        
            if BF>=150
                interpretString='Very strong';
            elseif (20<=BF) && (BF<150)
                interpretString='Strong';
            elseif (3<=BF) && (BF<20)
                interpretString='Positive';
            elseif (1<BF) && (BF<3)
                interpretString='Not worth more than a bare mention';
            elseif BF==1
                interpretString='H1';
            else
                interpretString='NA';
            end
        end             
        
        function generateTestData(seqType, seqUuid, simType, datatype, dataUuid, varargin)
        % generateTestData(seqType, seqUuid, simType, datatype, dataUuid, varargin)
        %   Generates test data for a sequence by using either prior or posterior observables.  First constructs a tmpSequence and then calls its 
        %   getData() function.
            tmpSequence=mcmcBayes.t_sequence.GetSequenceObj(seqType, seqUuid);
            
            predictionsToCapture=mcmcBayes.simulation.getDefaultPredictionsToCapture();
            [data]=mcmcBayes.data.getDataFromXML(tmpSequence, dataUuid, datatype, predictionsToCapture);            
             
             switch datatype
                 case mcmcBayes.t_data.priorPredictedData
                     if (data.mean.dataSource==mcmcBayes.t_dataSource.Simulated) || (priorPredictedData.mean.dataSource==mcmcBayes.t_dataSource.Postulated) 
                        tmpSequence.getData(simType, data.mean, varargin{:});
                     else
                         error('Requires mcmcBayes.t_dataSource=Simulated or Postulated for creating data.');
                     end
                 case mcmcBayes.t_data.posteriorPredictedData
                     if (data.mean.dataSource==mcmcBayes.t_dataSource.Simulated) || (data.mean.dataSource==mcmcBayes.t_dataSource.PostulatedConFR) || ...
                       (data.mean.dataSource==mcmcBayes.t_dataSource.PostulatedExpDFR) || (data.mean.dataSource==mcmcBayes.t_dataSource.PostulatedExpIFR) || ...
                       (data.mean.dataSource==mcmcBayes.t_dataSource.PostulatedIthenDFR) || (data.mean.dataSource==mcmcBayes.t_dataSource.PostulatedLinDFR) || ...
                       (data.mean.dataSource==mcmcBayes.t_dataSource.PostulatedLinIFR)
                     	 tmpSequence.getData(simType, data.mean, varargin{:});
                     else
                         error('Unsupported dataSource');
                     end
                 otherwise
                     error('Only supporting generation of postulated or simulated data for mcmcBayes.t_data.{priorPredictedData,posteriorPredictedData}');
             end
        end
        
        function handle=getHandleConstructor(strSeqType)
            classStr=sprintf('mcmcBayes.%s', strSeqType);
            if (exist(classStr)==8)%is it a valid class on the path
                evalStr=sprintf('handle=@%s;',classStr);%construct the expression to get the handle
                eval(evalStr);%get the handle
            else
                error('not a valid class on the path.');
            end
        end
        
%todo: add a logging switch (logging should be disabled if calling sequence is logging)        
        function [retSim]=useDefaultsAndRunSingleSimulation(seqType, seqUuid, simUuid)
        % [retSim]=useDefaultsAndRunSingleSimulation(seqType, seqUuid, simUuid)
        %   Runs the requested simulation using default configuration settings.
        
            [tmpConfig,tmpRepos]=mcmcBayes.osInterfaces.loadConfig;
        
            handle=mcmcBayes.sequence.getHandleConstructor(seqType);
            
            tmpSequence=handle(tmpConfig,tmpRepos,seqUuid);
                
            %start logging
            logUUID=java.util.UUID.randomUUID;
            logsdir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.logsDir);
            tmplog=sprintf('%s%s-simulationLog.txt',logsdir,logUUID);
            logging.startLogging(tmplog);

            predictionsToCapture=mcmcBayes.simulation.getDefaultPredictionsToCapture();
            init=true;
            tmpSimulation=mcmcBayes.simulation.getSimulationFromXML(tmpSequence, simUuid, predictionsToCapture, init);
            
            tmpSequence.simulations(1)=tmpSimulation;
            
            retSim=tmpSequence.simulations(1).run(tmpSequence.cfgMcmcBayes);    

            %stop logging and save
            permLog=sprintf('%s%s-%s-simulationLog.txt',logsdir,retSim.filenamePrefix,retSim.uuid);
            logging.stopAndSaveLog(tmplog,permLog);        
        end
        
%todo: add a logging switch (logging should be disabled if calling sequence is logging)        
        function [retSim]=overrideDefaultsAndRunSingleSimulation(seqType, seqUuid, simUuid, cfgRunPowerPosteriorMcmcSequence, cfgDebug, samplerType)
        % [retSim]=overrideDefaultsAndRunSingleSimulation(seqType, seqUuid, simUuid)
        %   Runs the requested simulation by overriding default configuration settings.  Used for the model unit tests (allows testing multiple 
        %   instantiations of the model for the different MCMC samplers)
            stdout=1;
        
            [tmpConfig,tmpRepos]=mcmcBayes.osInterfaces.loadConfig;

            handle=mcmcBayes.sequence.getHandleConstructor(seqType);
            
            tmpSequence=handle(tmpConfig,tmpRepos,seqUuid);
                
            %start logging
            logUUID=java.util.UUID.randomUUID;
            logsdir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.logsDir);
            tmplog=sprintf('%s%s-simulationLog.txt',logsdir,logUUID);
            logging.startLogging(tmplog);

            predictionsToCapture=mcmcBayes.simulation.getDefaultPredictionsToCapture();
            init=true;
            simStruct=mcmcBayes.simulation.getSimulationFromXML(tmpSequence, simUuid, predictionsToCapture, init);
            
            %override runPowerPosteriorMcmcSequence and debug
            tmpSequence.cfgMcmcBayes.cfgSimulation.runPowerPosteriorMcmcSequence=cfgRunPowerPosteriorMcmcSequence;
            tmpSequence.cfgMcmcBayes.cfgSimulation.debug=cfgDebug;
            
            %override samplerInterface
            switch samplerType
                case mcmcBayes.t_samplerInterfaces.OpenBUGS
                    samplerInterface=mcmcBayes.OpenBUGS(tmpSequence);
                case mcmcBayes.t_samplerInterfaces.JAGS
                    samplerInterface=mcmcBayes.JAGS(tmpSequence);
                case mcmcBayes.t_samplerInterfaces.Stan
                    samplerInterface=mcmcBayes.Stan(tmpSequence);
                otherwise
                    error('Unsupported mcmcBayes.t_samplerInterfaces.');
            end
            simStruct.samplerInterface=samplerInterface;
                        
            simId=1;
            tmpSequence.simulations(simId)=simStruct;
            
            %need to redo the hash (due to above override)           
            hashStr=num2str(string2hash(sprintf('%s%d',tmpSequence.type, mcmcBayes.t_samplerInterfaces.toint(tmpSequence.simulations(simId).samplerInterface.type))));
            setPrefix=strcat(tmpSequence.uuid, '-' , hashStr);
            if (init==true)
                fprintf(stdout,'Hash=%s created using sequenceType=%s + samplerType=%s.\n', hashStr, tmpSequence.type, ...
                    mcmcBayes.t_samplerInterfaces.tostring(tmpSequence.simulations(simId).samplerInterface.type));
            end            
            %update the corresponding filenamePrefix
            tmpSequence.simulations(simId).filenamePrefix=setPrefix;        
            retSim=tmpSequence.simulations(simId).run(tmpSequence.cfgMcmcBayes);
            
            %stop logging and save
            permLog=sprintf('%s%s-%s-simulationLog.txt',logsdir,retSim.filenamePrefix,retSim.uuid);
            logging.stopAndSaveLog(tmplog,permLog);        
        end             
    end
        
    methods
        function [retobj]=sequence(setType,cfgMcmcBayes,repositories,setUuid)
        % [retobj]=sequence(setType, setUuid)
        %   This is the constructor for the sequence class.  Most of the initial values are read from the cfgFileName settings file, which is 
        %   specific to the particular OS on the system this is running on.
            if nargin > 0
                %close all
                format long;
                dbstop if error;
                set(0,'DefaultFigureWindowStyle','docked');%use 'normal' to revert     

                retobj.uuid=setUuid;
               
                retobj.simulations=mcmcBayes.simulation.empty;
                
                retobj.type=setType;
                retobj.cfgMcmcBayes=cfgMcmcBayes;
                retobj.repositories=repositories;
                retobj.osInterface=mcmcBayes.osInterfaces.getOsInterface(retobj.cfgMcmcBayes);                                
            end      
        end       
        
        function [simIndex]=findSimIndex(obj, simType)
        %[simIndex]=findSimIndex(obj, simType)
        %  Locates the index for simType in obj.simulations()
            simIndex=-1;
            for i=1:numel(obj.simulations)
                if strcmp(obj.simulations(i).type,simType)
                    simIndex=i;
                    break;
                end
            end
            if simIndex==-1
                error('Did not find a %s in the sequence', simType);
            end
        end        
    end
   
    methods %getter/setter functions

    end
end