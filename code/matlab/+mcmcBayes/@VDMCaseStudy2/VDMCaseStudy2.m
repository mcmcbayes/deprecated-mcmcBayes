classdef VDMCaseStudy2 < mcmcBayes.sequence
% VDMCaseStudy1 is a class for demonstrating mcmcBayes with the vulnerability discovery phenomenon.  The prior datasets were generated
% from an expert-judgment workshop (see Johnston 2017) and the ten-models may be evaluated using this paired with multiple postulated datasets.  It
% also supports simulating data and running the analysis on this set.
    properties

    end
        
    methods (Static)
        function [uuids]=getUuids(seqUuid)
            if strcmp(seqUuid,'scalingLinear')==1
                uuids=[{'VDMCaseStudy2ScalingLinearFlatExpertJudgment1'}];
            elseif strcmp(seqUuid,'scalingExponential')==1
                uuids=[{'VDMCaseStudy2ScalingExponentialFlatExpertJudgment2'}];
            else
                error('seqUuid not recognized');                
            end
        end        
        
        function [retSequence]=runCaseStudy(seqUuid, forkIt)
        % [retSequence]=runCaseStudy(seqUuid, forkIt)
        %   Runs the baseline case study example specified by seqUuid.
            seqType=mcmcBayes.t_sequence.VDMCaseStudy2;
            retSequence=mcmcBayes.sequence.getDefaultSequence(seqType, seqUuid);
            sims=mcmcBayes.VDMCaseStudy2.getUuids(seqUuid);
            if forkIt==true
                mcmcBayes.simulation.forkThem(mcmcBayes.t_runView.Foreground, seqType, seqUuid, sims);
                for i=1:size(sims,1)
                    retSequence.simulations(i)=mcmcBayes.simulation.empty;  
                end
            else
                for i=1:size(sims,1)
                    retSequence.simulations(i)=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation(seqType, seqUuid, sims{i});                   
                end
            end          
        end    
        
        function getData(name, data, varargin)                                
        % getData(name, data, varargin)
        %   This function generates a simulated dataset using the parameter point estimates from varargin.
            warning('Does nothing, use the evaluation sequences.');
        end        

%todo: update        
        function displayModelPredictionComparison(priordatatype, datatype, datasetid, figuretype)
        % displayModelPredictionComparison(priordatatype, datatype, datasetid, figuretype)
        %   Used to plot the predictions from all the models in one figure (uses subplot)
        %   Input:
        %     priordatatype - t_mcmcBayesData type that specifies priorDependentVariables (e.g., CCMPerformanceWeighting, EqualWeighting, Individual, and Simulated)
        %     datatype - t_mcmcBayesData type that specifies dependentVariables (e.g., ConFR, LinIFR, ExpIFR, LinDFR, ExpDFR, and Simulated)
        %     datasetid - Specifies the dataset identifier to display the predictions for
        %     figuretype - Specifies the t_mcmcBayesFigures to filter the files on (should be MODEL_PREDICTIONS or MODEL_PREDICTIONS_COMBO)
            priordatatypeid=t_mcmcBayesData.toint(priordatatype);
            datatypeid=t_mcmcBayesData.toint(datatype);
            
            %https://www.mathworks.com/matlabcentral/answers/100687-how-do-i-extract-data-from-matlab-figures
            
            figuredir=mcmcBayesOSInterfaces.getPath(t_mcmcBayesPaths.FIGURES_DIR);
            if ((figuretype~=t_mcmcBayesFigures.MODEL_PREDICTIONS) && (figuretype~=t_mcmcBayesFigures.MODEL_PREDICTIONS_COMBO))
                error('%s is an unsupported figuretype for displayModelPredictionComparison.', t_mcmcBayesFigures.getfilenamestring(figuretype))
            end
            filterspec=sprintf('%smodeltypeid*priordatatypeid%ddatatypeid%ddatasetid%d*samplertypeid*_%s.fig', figuredir, priordatatypeid, datatypeid, ...
                datasetid, t_mcmcBayesFigures.getfilenamestring(figuretype));%PredictionsSet
            [filename, pathname]=uigetfile(filterspec,'Open mcmcBayes figure file','MultiSelect','on');
            if size(filename,2)<10
                warning('You did not select ten models');
            end
            if isa(filename,'cell')
                hfig10=figure;
                ax=hfig10.CurrentAxes;
                for file=1:size(filename,2)
                    filestr=mcmcBayesOSInterfaces.convPathStr(strcat(pathname,filename{file}));
                    A=sscanf(filename{file},'modeltypeid%dpriordatatypeid%ddatatypeid%ddatasetid%dtempid%dsamplertypeid%d*');
                    modeltypeid(file)=A(1);
                    set(0,'CurrentFigure',hfig10);
                    if modeltypeid(file)==9 || modeltypeid(file)==10
                        index=modeltypeid(file)+1;
                        subax(index)=subplot(3,4,index);
                    else
                        index=modeltypeid(file);
                        subax(index)=subplot(3,4,index);
                    end
                    hfig=openfig(filestr,'invisible');
                    axtemp=hfig.CurrentAxes;
                    thandles=get(axtemp,'children');%get handle to all the children in the figure
                    copyobj(thandles,subax(index));
                    close(hfig);
                    subax(index).XTick = [0 10 20 30 40 50];
                    xlim(subax(index),[0 50]);
                    xlabel(subax(index),'time');
                    ylabel(subax(index),'E[N(t)]');
                    dataObjs = get(subax(index),'Children');
                    switch modeltypeid(file) %these commands need the copied axes already present
                        case 1
                            title('Linear regression');
                            if (figuretype == t_mcmcBayesFigures.MODEL_PREDICTIONS_COMBO)
                                hlegend=legend('expert data','prior-based low predictions','prior-based mean predictions','prior-based high predictions', ...
                                    'postulated data','posterior-based low predictions','posterior-based mean predictions','posterior-based high predictions');
                            elseif (figuretype ==t_mcmcBayesFigures.MODEL_PREDICTIONS)
                                hlegend=legend('expert data','low predictions','mean predictions','high predictions','');
                            else
                                error('Unsupported t_mcmcBayesFigures type.');
                            end
                               
                            set(hlegend,'EdgeColor',[1 1 1]); 
                        case 2
                            title('Polynomial (degree-2) regression');
                        case 3
                            title('Brooks-Motley HPP');
                        case 4
                            title('Goel-Okumoto NHPP');
                        case 5
                            title('Goel''s generalized Goel-Okumoto NHPP');
                        case 6
                            title('Musa-Okumoto NHPP');
                            subax(index).XTick = [0 4000 8000 12000 16000 20000];
                            xlim(subax(index),[0 20000]);
                            xlabel(subax(index),'assessment time');
                            ylabel(subax(index),'E[N(tau)]');
                        case 7
                            title('YamadaOhbaOsaki (s-shaped) NHPP');
                        case 8
                            title('Kuo-Ghosh NHPP');             
                        case 9
                            title('Verhulst growth curve');
                        case 10
                            title('Gompertz growth curve');                                
                    end           
                end
            else
                if ~filename==0
                    filestr=mcmcBayesOSInterfaces.convPathStr(strcat(pathname,filename));
                    openfig(filestr,'visible');
                else
                    disp('No file selected.');
                    return
                end                       
            end
        end                
               
        function [xiOut]=featureScaling(xi)
        %[xiOut]=featureScaling(xi)
        %   Feature scales xi to the baseline (x0=[1000; 500; 3; 10; 3; 4; .25; .25; .5; .5])
             for row=1:size(xi,1)
                switch row
                    %Normalize XValues
                    %XValue = -1: maximal scaling down from N0(t)
                    %Xvalue = 0: no scaling from N0(t)
                    %XValue = 1: maximal scaling up from N0(t)
                    case 1%x1=Number of functions
                        XBaseline=1000.0;
                        XRange=[0 100000];
                        XValues=[50 250 1000 10000 100000];%ordered for more discoveries to less discoveries (should see negative)
                        xiOut(row,1)=((xi(row,1)-XBaseline)/(XRange(2)-XRange(1)));
                    case 2%x2=Product unit price
                        XBaseline=500.00;
                        XRange=[0 100000];
                        XValues=[50 500 1000 5000 100000];%ordered for more discoveries to less discoveries (should see negative)
                        xiOut(row,1)=((xi(row,1)-XBaseline)/(XRange(2)-XRange(1)));
                    case 3%x3=Analysis tool quality
                        XBaseline=3.00;
                        XRange=[1 5];
                        XValues=[1 2 3 4 5];%ordered for less discoveries to more discoveries
                        xiOut(row,1)=((xi(row,1)-XBaseline)/(XRange(2)-XRange(1)));
                    case 4%x4=Analysis personnel effort
                        XBaseline=10.00;
                        XRange=[1 30];
                        XValues=[3 5 10 20 30];%ordered for less discoveries to more discoveries
                        xiOut(row,1)=((xi(row,1)-XBaseline)/(XRange(2)-XRange(1)));
                    case 5%x5=Analysis average personnel quality
                        XBaseline=3.00;
                        XRange=[1 5];
                        XValues=[1 2 3 4 5];%ordered for less discoveries to more discoveries
                        xiOut(row,1)=((xi(row,1)-XBaseline)/(XRange(2)-XRange(1)));
                    case 6%x6=Level of dynamic access to artifacts
                        XBaseline=4.00;
                        XRange=[1 5];
                        XValues=[1 2 3 4 5];%ordered for less discoveries to more discoveries
                        xiOut(row,1)=((xi(row,1)-XBaseline)/(XRange(2)-XRange(1)));
                    case 7%x7=Percent of software size reused from previous release
                        XBaseline=0.25;
                        XRange=[0 .75];
                        XValues=[0 .1 .25 .5 .75];%ordered for less discoveries to more discoveries
                        xiOut(row,1)=xi(row,1)-XBaseline;
                    case 8%x8=Percent of available design information
                        XBaseline=0.25;
                        XRange=[0 1];
                        XValues=[0 .15 .25 .5 1.0];%ordered for less discoveries to more discoveries
                        xiOut(row,1)=xi(row,1)-XBaseline;          
                    case 9%x9=Percent of obfuscated software functions
                        XBaseline=0.5;
                        XRange=[0 1];
                        XValues=[0 .25 .5 .75 1.0];%ordered for more discoveries to less discoveries (should see negative)
                        xiOut(row,1)=xi(row,1)-XBaseline;                    
                    case 10%x10=Percent of cleansed software functions
                        XBaseline=0.5;
                        XRange=[0 1];
                        XValues=[0 .25 .5 .75 1.0];%ordered for more discoveries to less discoveries (should see negative)
                        xiOut(row,1)=xi(row,1)-XBaseline;                  
                end
             end
        end                        
    end
    
    methods
        function [obj]=VDMCaseStudy2(cfgMcmcBayes,repositories,sequenceUuid)           
        % obj=VDMCaseStudy2(cfgMcmcBayes,repositories,sequenceUuid)
        %   The constructor for VDMCaseStudy2.
        % Output:
        %   obj Constructed VDMCaseStudy2 object
            if nargin > 0
                superargs{1}='VDMCaseStudy2';
                superargs{2}=cfgMcmcBayes;
                superargs{3}=repositories;
                superargs{4}=sequenceUuid;
            elseif nargin == 0
                superargs={};
            end
            obj=obj@mcmcBayes.sequence(superargs{:});                        
        end
        
        function [retSequence]=runScalingDemo(obj)
        % [retSequence]=runScalingDemo(obj)
        %   Runs example 1 (Firefox 3.0), example 2 (Internet Explorer 7.0), and example 3 (Safari 1.0) using their baseline archived sequences. 
            stdout=1;
            %Run example 1 (Firefox 3.0): x_41=1,000 (baseline), x_41=50, x_41=250, x_41=10,000, x_41=100,000
            fprintf(stdout,'Loading example 1\n');
            seqType=mcmcBayes.t_sequence.VDMCaseStudy1;
            simUuid='ExpertJudgment2Empirical2';
            [retSequence(1)]=mcmcBayes.sequence.loadSequence(seqType, simUuid);
            [Nhat, Msfe]=mcmcBayes.sequence.bma(retSequence(1), 'baseline', false, false);

            fprintf(stdout,'Simulating scaled SR and SAPs for example 1\n');
            Zhat(1)={obj.simulateScaledVDM(Nhat,[1000; 500; 3; 10; 3; 4; .25; .25; .5; .5])};
            Zhat(2)={obj.simulateScaledVDM(Nhat,[50; 500; 3; 10; 3; 4; .25; .25; .5; .5])};
            Zhat(3)={obj.simulateScaledVDM(Nhat,[250; 500; 3; 10; 3; 4; .25; .25; .5; .5])};
            Zhat(4)={obj.simulateScaledVDM(Nhat,[10000; 500; 3; 10; 3; 4; .25; .25; .5; .5])};
            Zhat(5)={obj.simulateScaledVDM(Nhat,[100000; 500; 3; 10; 3; 4; .25; .25; .5; .5])};

            %plot them
            fprintf(stdout,'Plotting scaled SR and SAPs for example 1\n');
            fig;
            hold on;
            plot(retSequence(1).simulations(1).posteriorPredictedData.mean.indVarsValues{:},retSequence(1).simulations(1).posteriorData.depVarsValues{:},'b*');%orig, just use the first simulation
            plot(retSequence(1).simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(Zhat{1}),'r');%baseline, x_41=1,000
            plot(retSequence(1).simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(Zhat{2}),'c');%x_41=50
            plot(retSequence(1).simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(Zhat{3}),'g');%x_41=250
            plot(retSequence(1).simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(Zhat{4}),'m');%x_41=10,000
            plot(retSequence(1).simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(Zhat{5}),'color',[139/256,69/256,19/256]);%x_41=100,000 (brown)
            hold off;        
%                 filename=sprintf('%s%s-%s-%s-chainid%d-tempid%d-PredictionsSet.fig', archivedSequence.osInterface.figuresDir, archivedSequence.uuid, figHash, simUuid, ...
%                     chainid, tempid);            
%                 savefig(filename);   
            fprintf(stdout,'Example 1 results for Firefox 3.0:\n');
            fprintf(stdout,'\tx_0 (i.e., x_41=1,000) (baseline, red), E[Delta|t,D_0]=[%s]\n', conversionUtilities.matrix_tostring(mean(Zhat{1})));
            fprintf(stdout,'\tx_i=x_0 with x_41=50 (cyan), E[Delta|t,x_i,D_0,D^1/D^2]=[%s]\n', conversionUtilities.matrix_tostring(mean(Zhat{2})));
            fprintf(stdout,'\tx_i=x_0 with x_41=250 (green), E[Delta|t,x_i,D_0,D^1/D^2]=[%s]\n', conversionUtilities.matrix_tostring(mean(Zhat{3})));
            fprintf(stdout,'\tx_i=x_0 with x_41=10,000 (magenta), E[Delta|t,x_i,D_0,D^1/D^2]=[%s]\n', conversionUtilities.matrix_tostring(mean(Zhat{4})));
            fprintf(stdout,'\tx_i=x_0 with x_41=100,000 (brown), E[Delta|t,x_i,D_0,D^1/D^2]=[%s]\n', conversionUtilities.matrix_tostring(mean(Zhat{5})));

            fprintf(stdout,'Loading example 2\n');
            %Run example 2 (Internet Explorer 7.0): x_39=4 (baseline), x_39=1, x_39=3, and x_39=5
            simUuid='ExpertJudgment3Empirical3';
            [retSequence(2)]=mcmcBayes.sequence.loadSequence(seqType, simUuid);
            [Nhat, Msfe]=mcmcBayes.sequence.bma(retSequence(2), 'baseline', false, false);

            fprintf(stdout,'Simulating scaled SR and SAPs for example 2\n');
            Zhat(1)={obj.simulateScaledVDM(Nhat,[1000; 500; 3; 10; 3; 4; .25; .25; .5; .5])};
            Zhat(2)={obj.simulateScaledVDM(Nhat,[1000; 500; 3; 10; 3; 1; .25; .25; .5; .5])};
            Zhat(3)={obj.simulateScaledVDM(Nhat,[1000; 500; 3; 10; 3; 3; .25; .25; .5; .5])};
            Zhat(4)={obj.simulateScaledVDM(Nhat,[1000; 500; 3; 10; 3; 5; .25; .25; .5; .5])};

            %plot them
            fprintf(stdout,'Plotting scaled SR and SAPs for example 2\n');
            fig;
            hold on;
            plot(retSequence(2).simulations(1).posteriorPredictedData.mean.indVarsValues{:},retSequence(2).simulations(1).posteriorData.depVarsValues{:},'b*');%orig, just use the first simulation
            plot(retSequence(2).simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(Zhat{1}),'r');%baseline, x_39=4
            plot(retSequence(2).simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(Zhat{2}),'c');%x_39=1
            plot(retSequence(2).simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(Zhat{3}),'g');%x_39=3
            plot(retSequence(2).simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(Zhat{4}),'m');%x_39=5
            hold off;
%                 filename=sprintf('%s%s-%s-%s-chainid%d-tempid%d-PredictionsSet.fig', archivedSequence.osInterface.figuresDir, archivedSequence.uuid, figHash, simUuid, ...
%                     chainid, tempid);            
%                 savefig(filename);               
            fprintf(stdout,'Example 2 results for Internet Explorer 7.0:\n');
            fprintf(stdout,'\tx_0 (i.e., x_39=4) (baseline, red), E[Delta|t,D_0]=[%s]\n', conversionUtilities.matrix_tostring(mean(Zhat{1})));
            fprintf(stdout,'\tx_i=x_0 with x_39=1 (cyan), E[Delta|t,x_i,D_0,D^1/D^2]=[%s]\n', conversionUtilities.matrix_tostring(mean(Zhat{2})));
            fprintf(stdout,'\tx_i=x_0 with x_39=3 (green), E[Delta|t,x_i,D_0,D^1/D^2]=[%s]\n', conversionUtilities.matrix_tostring(mean(Zhat{3})));
            fprintf(stdout,'\tx_i=x_0 with x_39=5 (magenta), E[Delta|t,x_i,D_0,D^1/D^2]=[%s]\n', conversionUtilities.matrix_tostring(mean(Zhat{4})));

            fprintf(stdout,'Loading example 3\n');
            %Run example 3 (Safari 1.0): x_23=10 (baseline), x_23=1, x_23=9, x_23=11, and x_23=25
            simUuid='ExpertJudgment5Empirical5';
            [retSequence(3)]=mcmcBayes.sequence.loadSequence(seqType, simUuid);
            [Nhat, Msfe]=mcmcBayes.sequence.bma(retSequence(3), 'baseline', false, false);

            fprintf(stdout,'Simulating scaled SR and SAPs for example 3\n');
            Zhat(1)={obj.simulateScaledVDM(Nhat,[1000; 500; 3; 10; 3; 4; .25; .25; .5; .5])};
            Zhat(2)={obj.simulateScaledVDM(Nhat,[1000; 500; 3; 1; 3; 4; .25; .25; .5; .5])};
            Zhat(3)={obj.simulateScaledVDM(Nhat,[1000; 500; 3; 8; 3; 4; .25; .25; .5; .5])};
            Zhat(4)={obj.simulateScaledVDM(Nhat,[1000; 500; 3; 12; 3; 4; .25; .25; .5; .5])};
            Zhat(5)={obj.simulateScaledVDM(Nhat,[1000; 500; 3; 25; 3; 4; .25; .25; .5; .5])};

            %plot them
            fprintf(stdout,'Plotting scaled SR and SAPs for example 3\n');
            fig;
            hold on;
            plot(retSequence(3).simulations(1).posteriorPredictedData.mean.indVarsValues{:},retSequence(3).simulations(1).posteriorData.depVarsValues{:},'b*');%orig, just use the first simulation
            plot(retSequence(3).simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(Zhat{1}),'r');%baseline, x_23=10
            plot(retSequence(3).simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(Zhat{2}),'c');%x_23=1
            plot(retSequence(3).simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(Zhat{3}),'g');%x_23=9
            plot(retSequence(3).simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(Zhat{4}),'m');%x_23=11
            plot(retSequence(3).simulations(1).posteriorPredictedData.mean.indVarsValues{:},mean(Zhat{5}),'color',[139/256,69/256,19/256]);%x_23=25 (brown)
            hold off;
%                 filename=sprintf('%s%s-%s-%s-chainid%d-tempid%d-PredictionsSet.fig', archivedSequence.osInterface.figuresDir, archivedSequence.uuid, figHash, simUuid, ...
%                     chainid, tempid);            
%                 savefig(filename);            
            fprintf(stdout,'Example 3 results for Safari 1.0:\n'); 
            fprintf(stdout,'\tx_0 (i.e., x_23=10) (baseline, red), E[Delta|t,D_0]=[%s]\n', conversionUtilities.matrix_tostring(mean(Zhat{1})));
            fprintf(stdout,'\tx_i=x_0 with x_23=1 (cyan), E[Delta|t,x_i,D_0,D^1/D^2]=[%s]\n', conversionUtilities.matrix_tostring(mean(Zhat{2})));
            fprintf(stdout,'\tx_i=x_0 with x_23=9 (green), E[Delta|t,x_i,D_0,D^1/D^2]=[%s]\n', conversionUtilities.matrix_tostring(mean(Zhat{3})));
            fprintf(stdout,'\tx_i=x_0 with x_23=11 (magenta), E[Delta|t,x_i,D_0,D^1/D^2]=[%s]\n', conversionUtilities.matrix_tostring(mean(Zhat{4})));
            fprintf(stdout,'\tx_i=x_0 with x_23=25 (brown), E[Delta|t,x_i,D_0,D^1/D^2]=[%s]\n', conversionUtilities.matrix_tostring(mean(Zhat{5})));
        end      
        
        function [Zhat]=simulateScaledVDM(obj, Nhat, xi)
%             for row=1:size(xi,1)
%                 switch row
%                     case 1
%                         varstr='Number of functions';
%                     case 2
%                         varstr='Product unit price';
%                     case 3
%                         varstr='Analysis tool quality';
%                     case 4
%                         varstr='Analysis personnel effort (number of full-time analysts)';
%                     case 5
%                         varstr='Analysis average personnel quality (skill)';
%                     case 6
%                         varstr='Level of dynamic access to artifacts';
%                     case 7
%                         varstr='% of software size reused from previous release';
%                     case 8
%                         varstr='% of available design information';
%                     case 9
%                         varstr='% of obfuscated software';
%                     case 10
%                         varstr='% of cleansed software';
%                 end                       
%                 fprintf(1,'xi(%d)=%f, ''%s''\n',row,xi(row),varstr);
%             end
            [xiOut]=mcmcBayes.VDMCaseStudy2.featureScaling(xi);%feature scale xi
            
            %load the parameters for scaling function and create its function handle
            varid=1;
            chainid=1;
            tempid=obj.simulations(1).getDefaultPowerPosteriorN+1;%just use the first simulation
            c=obj.simulations(1).posteriorVariables(varid,chainid,tempid).MCMC_mean;
            varid=2;
            r=obj.simulations(1).posteriorVariables(varid,chainid,tempid).MCMC_mean;            
            if strcmp(obj.simulations(1).type,'scalingLinear')
                sim_fhandle_scaling=@(x,c) 1+c'*x;
            elseif strcmp(obj.simulations(1).type,'scalingExponential')
                sim_fhandle_scaling=@(x,c) exp(c'*x);
            else
                error('Unsupported scaling type.');
            end

            %generate Shat
            pd=makedist('Normal',sim_fhandle_scaling(xiOut,c),1/sqrt(r));
            Shat=random(pd,size(Nhat,2),1);            

            %generate scaled predictions
            Zhat=Nhat'.*Shat;
        end                
    end
    
    methods (Static, Access=private)        

    end
    
    methods %getter/setter functions

    end
end