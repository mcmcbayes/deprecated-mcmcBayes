classdef OpenBUGS < mcmcBayes.samplerInterfaces
%this class performs all interfacing with OpenBUGS
    properties
        OpenBUGS_projdir %string
        OpenBUGS_scriptdir %string
        OpenBUGS_scriptfiletemplate %string, dependency on OpenBUGS_scriptdir, modeltype, and modelid
        OpenBUGS_shortcut
        
        filename_bashscript
        
        filename_OpenBUGSscript%used in setupMCMCSampling,performMCMCSampling
        filename_OpenBUGSproject%used in setupMCMCSampling
        filename_OpenBUGSinits%used in setupMCMCSampling
        filename_OpenBUGSdata%used in setupMCMCSampling
        filename_OpenBUGSlog
        %OpenBUGS's output files are in CODA format
        filename_CODAindex%used in readSamples
        filename_CODAchain%used in readSamples
    end
        
    methods
        function obj = OpenBUGS(sequence) %constructor
            warning('update models with the Tau, R, vR changes');
            
            if nargin == 0
                superargs={};
            else
                superargs{1}=mcmcBayes.t_samplerInterfaces.OpenBUGS;
                superargs{2}=sequence.osInterface;
                superargs{3}=sequence.cfgMcmcBayes.cfgSimulation.runView;
                superargs{4}=sequence.repositories;
            end

            obj=obj@mcmcBayes.samplerInterfaces(superargs{:});            
            
            obj.OpenBUGS_projdir=strcat(sequence.osInterface.getmcmcBayesPath,mcmcBayes.osInterfaces.getSubPath(mcmcBayes.t_paths.samplerProjectsDir),'OpenBUGS');
            obj.OpenBUGS_scriptdir=strcat(sequence.osInterface.getmcmcBayesPath,mcmcBayes.osInterfaces.getSubPath(mcmcBayes.t_paths.samplerScriptsDir),'OpenBUGS');
            obj.OpenBUGS_shortcut=sequence.cfgMcmcBayes.cfgSampler.OpenBugs.shortCut;            
            if isempty(obj.OpenBUGS_shortcut)
                error('OpenBUGS shortcut is missing!');
            end
        end
        
        %setup OpenBUGS script, inits, and data files for sampling
        function retobj=setupMCMCSampling(obj, settype, setsubtype, setnchains, settempid, setphi, outputFileNamePrefix, setVariables, setData, setosInterface)               
            obj.modeltype=settype;
            obj.modelsubtype=setsubtype;
            obj.nchains=setnchains;
            obj.tempid=settempid;            
            obj.phi=setphi;
            obj.outputFileNamePrefix=outputFileNamePrefix;
            obj.variables=setVariables;
            obj.data=setData;
            obj.osInterface=setosInterface;

            name=obj.modeltype;
            obj.OpenBUGS_scriptfiletemplate=sprintf('%sOpenBUGS.txt',name);
            
            %copy OpenBUGS_scriptfiletemplate to script_seqid_tempid
            temp=mcmcBayes.osInterfaces.convPathStr(sprintf('%s/%s',obj.OpenBUGS_scriptdir,obj.OpenBUGS_scriptfiletemplate));
            if ~(mcmcBayes.osInterfaces.doesFileExist(temp)) %is a file
                temp=mcmcBayes.osInterfaces.convPathStr(sprintf('%s/%s',obj.OpenBUGS_scriptdir_ext,obj.OpenBUGS_scriptfiletemplate));%use the ext directory
            end
            
            obj.filename_bashscript=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-bashscript.txt', obj.osInterface.tempDir, obj.outputFileNamePrefix));            
            obj.filename_OpenBUGSscript=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-script.txt', obj.osInterface.tempDir, ...
                obj.outputFileNamePrefix));            
            copyfile(temp,obj.filename_OpenBUGSscript);      

            obj.filename_OpenBUGSproject=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%sOpenBUGS.txt', name, obj.modelsubtype));     
            
            obj.filename_OpenBUGSdata=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-data.txt', ...
                mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataInDir), obj.outputFileNamePrefix));
                obj.filename_OpenBUGSinits=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-inits.txt', ...
                    mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerInitsDir), obj.outputFileNamePrefix));
            
            %setup script_seqid_tempid for sampling sequence (samplesCoda,modelSaveLog)
            obj.filename_CODAindex=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-CODAindex.txt', ...
                mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataOutDir), obj.outputFileNamePrefix));%use the MCMC chain for final parameter
            obj.filename_CODAchain=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-CODAchain1.txt', ...
                mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataOutDir), obj.outputFileNamePrefix));%use the MCMC chain for final parameter
            
            obj.filename_OpenBUGSlog=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-samplerLog.txt', ...
                mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.logsDir), obj.outputFileNamePrefix));
                             
            obj.setupBashScript();
            
            %setup obj.filename_OpenBUGSscript for sampling sequence
            runPreprocessor_scriptfile(obj);
            
            %setup obj.filename_OpenBUGSdata for sampling sequence
            setupData(obj, obj.filename_OpenBUGSdata)
            
            %setup obj.filename_OpenBUGSinits for sampling sequence
            setupInits(obj, obj.filename_OpenBUGSinits)
            
            retobj=obj;
        end

        function setupBashScript(obj)
            if mcmcBayes.osInterfaces.doesFileExist(obj.filename_bashscript) %remove any old script files (with the same name)
                delete(obj.filename_bashscript);
            end
            
            %write output to ovbj.filename_Stanscript
            fid=fopen(obj.filename_bashscript,'wt');
            
            if obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.Windows10 || obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.Windows7
                fprintf(fid,'#!/usr/bin/env bash\n%s //PAR %s\n', mcmcBayes.osInterfaces.convPathStrUnix(obj.OpenBUGS_shortcut), ...
                        mcmcBayes.osInterfaces.convPathStrUnix(obj.filename_OpenBUGSscript));%need to escape / in /PAR with //

            else
                fprintf(fid,'#!/usr/bin/env bash\n%s %s\n', mcmcBayes.osInterfaces.convPathStrUnix(obj.OpenBUGS_shortcut), ...
                        mcmcBayes.osInterfaces.convPathStrUnix(obj.filename_OpenBUGSscript));
            end
            fclose(fid);
            if obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.UbuntuLinux || obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.MacOS
                syscommand=sprintf('system(''chmod +x "%s"'');',obj.filename_bashscript);%make the script executable
                eval(syscommand);
            end         
        end        
        
        function runPreprocessor_scriptfile(obj)
        %Script sequence:
        %   Read model file
        %   Read data file
        %   Compile model
        %   Read inits file
        %   Initialize model
        %   List the samplers
        %   Sample from model (burnin)
        %   Set trace monitors for nodes
        %   Sample from model
        %   Save coda files
        %   Exit                 

            %setup logging for windows OpenBUGS
            tstring=sprintf('modelDisplay(''log'')');%Script command not supported in Linux
            finsertbuffer(obj.filename_OpenBUGSscript,'##MODELDISPLAYLOG##',tstring);        
        
            %All OpenBUGS paths require forward slashes '/'            
            %On Windows, we have to save the log file using modelSaveLog() script command; for Linux, just save the console output
            if obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.Windows7 || obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.Windows10
                tstring=obj.filename_OpenBUGSlog;
                tstring=strrep(tstring,'.txt','_details.txt');
                tstring=strrep(tstring,'\','/');
                buffer=sprintf('modelSaveLog("%s")\n',tstring);
                finsertbuffer(obj.filename_OpenBUGSscript,'##MODELSAVELOG##',buffer);%ModelSaveLog command not supported in Linux
            end
            
            %setup obj.OpenBUGS_codadir ##MODELSETWD##
            buffer=sprintf('modelSetWD("%s")\n',strrep(strrep(mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataOutDir),'\','/'),'/samplerDataOut/','/samplerDataOut'));%do not have a '/' for the last directory for modelSetWD!!!
            finsertbuffer(obj.filename_OpenBUGSscript,'##MODELSETWD##',buffer);
            %setup obj.filename_OpenBUGSscript ##MODELCHECK##
            tstring=sprintf('modelCheck("%s")',strrep(obj.filename_OpenBUGSproject,'\','/'));
            finsertbuffer(obj.filename_OpenBUGSscript,'##MODELCHECK##',tstring);
            %setup obj.filename_OpenBUGSscript ##MODELDATA##
            tstring=sprintf('modelData("%s")',strrep(obj.filename_OpenBUGSdata,'\','/'));
            finsertbuffer(obj.filename_OpenBUGSscript,'##MODELDATA##',tstring);
            tstring=sprintf('modelCompile(%d)',obj.nchains);
            finsertbuffer(obj.filename_OpenBUGSscript,'##MODELCOMPILE##',tstring);           
            %setup obj.filename_OpenBUGSscript ##MODELINITS##
            for chainid=1:obj.nchains%BUGS needs an inits file for each chain, just use the same file      
                if chainid==1
                    tstring=sprintf('modelInits("%s",%d)',strrep(obj.filename_OpenBUGSinits,'\','/'),chainid);
                else
                    tstring=sprintf('%s\nmodelInits("%s",%d)',tstring,strrep(obj.filename_OpenBUGSinits,'\','/'),chainid);
                end
            end
            finsertbuffer(obj.filename_OpenBUGSscript,'##MODELINITS##',tstring);
            %setup obj.filename_OpenBUGSscript (burnin) ##MODELUPDATE,1##
            tstring=sprintf('modelUpdate(%d,1,100,"F")#burn-in',obj.variables(1).MCMC_nsamples_burnin);
            finsertbuffer(obj.filename_OpenBUGSscript,'##MODELUPDATE,1##',tstring);
            %setup node monitors (thinning) ##SAMPLESSET##     
            clear tstring;
            for varid=1:size(obj.variables,1) 
                if (obj.variables(varid,chainid).type~=mcmcBayes.t_variable.constant)%only monitor stochastic nodes
                    %for stochastic processes, strip off, for example, (t) from Theta(t) in the name
                    if obj.variables(varid,chainid).type==mcmcBayes.t_variable.stochasticProcess
                        if isempty(strfind(obj.variables(varid,chainid).name,'('))
                            strname=obj.variables(varid,chainid).name;
                        else
                            strname=obj.variables(varid,chainid).name(1:strfind(obj.variables(varid,chainid).name,'(')-1);
                        end
                    else
                        strname=obj.variables(varid,chainid).name;
                    end
                    
                    if exist('tstring','var')
                        tstring=sprintf('%s\nsamplesSet("%s")',tstring,strname);
                    else
                        tstring=sprintf('samplesSet("%s")',strname);
                    end
                end
            end
            freplacebuffer(obj.filename_OpenBUGSscript,'##SAMPLESSET,START##','##SAMPLESSET,STOP##',tstring);
            %setup obj.filename_OpenBUGSscript (samples,thinning) ##MODELUPDATE,2##
            %For this OpenBUGS command, here you specify the number of samples to keep (software determines number of samples necessary with the specified lag to achieve this number)
            tstring=sprintf('modelUpdate(%d,%d,100,"F")#data',obj.variables(1,1).MCMC_nsamples_data/obj.variables(1,1).MCMC_nsamples_lag,obj.variables(1,1).MCMC_nsamples_lag);%just use the first var here to lookup
            finsertbuffer(obj.filename_OpenBUGSscript,'##MODELUPDATE,2##',tstring);
            %setup Model.scriptfile ##SAMPLESCODA##
            buffer=sprintf('samplesCoda("*","%s-")\n',obj.outputFileNamePrefix);
            finsertbuffer(obj.filename_OpenBUGSscript,'##SAMPLESCODA##',buffer);                    
        end
                        
        %calls openbugs to perform the previously setup sampling from variables(1:end,tempid) using datain
        function [retobj]=performMCMCSampling(obj)
            stdout=1;
            %Run the script to generate the MCMC computations for theta(i|j) (cannot run BUGS in the background as the next loop iteration can't start without results)
            if mcmcBayes.osInterfaces.doesFileExist(obj.filename_OpenBUGSlog)%remove any old log files (with the same name)
                delete(obj.filename_OpenBUGSlog);
            end
            if mcmcBayes.osInterfaces.doesFileExist(obj.filename_CODAchain) %remove any old CODAchain files (with the same name)
                delete(obj.filename_CODAchain);
            end
            if mcmcBayes.osInterfaces.doesFileExist(obj.filename_CODAindex) %remove any old CODAindex files (with the same name)
                delete(obj.filename_CODAindex);
            end
            
            fprintf(stdout,'\tStarting OpenBUGS sampling for tempid=%d.\n', obj.tempid);
            obj.osInterface=obj.osInterface.spawnBashCommand( obj.filename_bashscript, obj.filename_OpenBUGSlog, obj.runView );
            
            finished=false;
            samplingTimerStart=tic;
            setcommandstr='';
            while finished==false
                pause(3);%sleep 3 seconds
                elapsedtime=toc(samplingTimerStart);
                elapsedseconds=seconds(elapsedtime);
                elapsedminutes=minutes(elapsedseconds);
                statusstr=sprintf('For %s model, tempid=%d, OpenBUGS sampler elapsed time is %e minutes', obj.modeltype, ...
                    obj.tempid, elapsedminutes);
                setDesktopStatus(statusstr)
                [obj.osInterface, retcommandstr]=obj.osInterface.updateBashCmdStatus(setcommandstr);
                setcommandstr=retcommandstr;
                obj.osInterface.checkForKillFile();
                if (obj.osInterface.bashCmdState==mcmcBayes.t_runState.Finished)
                    for count=1:12%will be about 12*(5) seconds
                        if checkFileStatic(obj.filename_CODAchain,5)
                            break;%file exists and is not changing
                        end
                        drawnow;
                        pause(1);
                    end
                    if count==12
                        type(obj.filename_OpenBUGSlog);
                        error('Sampler failed.');   
                    end                    
                    finished=true;
                    tempstr=sprintf('\tElapsed sampling time for %s model, tempid=%d was %e minutes.', obj.modeltype, obj.tempid, elapsedminutes);
                    disp(tempstr);    
                    setDesktopStatus(tempstr);
                    obj.osInterface.deleteCmdMsgFile()         
                    
                    if obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.Windows7 || obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.Windows10
                        tstring=obj.filename_OpenBUGSlog;
                        tempLog=strrep(tstring,'.txt','_details.txt');
                        [status,message,messageId] = movefile(tempLog,obj.filename_OpenBUGSlog);
                    end                    
                end
            end                        
            retobj=obj;            
        end
        
        function [retobj, fieldindices, data]=readSamples(obj)%index specifies the coda file to read all the variables sampled
            for chainid=1:obj.nchains
                clear tfieldindices targlist;
            	tfilename_CODAchain=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-CODAchain%d.txt', ...
                    mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataOutDir), obj.outputFileNamePrefix, chainid));
                OpenBUGSdatastruct=mcmcBayes.coda2mat(obj.filename_CODAindex,tfilename_CODAchain);
                %samples shall be stored as a 3-dimensional matrix within a cell
                %scalar[]-{[1 x 1 x numSamples]}
                %vector[]-{[numRows x 1 x numSamples]}
                %matrix[]-{[numRows x numCols x numSamples}]              
                for varid=1:size(obj.variables,1)
                    tsize=obj.variables(varid,chainid).MCMC_nsamples_data/obj.variables(varid,chainid).MCMC_nsamples_lag;
                    switch obj.variables(varid,chainid).type                 
                        case mcmcBayes.t_variable.constant
                            if (obj.variables(varid,chainid).distribution.type==mcmcBayes.t_distribution.DiscreteConstant)
                                tvar=obj.variables(varid,chainid).distribution.parameters(1).value*int32(ones(tsize,1,1));
                            else
                                tvar=obj.variables(varid,chainid).distribution.parameters(1).value*ones(tsize,1,1);
                            end
                            if exist('tfieldindices','var')==1
                                tfieldindices=[tfieldindices varid];
                                targlist=[targlist; {tvar}];
                            else
                                tfieldindices=varid;
                                targlist=[{tvar}];
                            end
                        case {mcmcBayes.t_variable.stochastic, mcmcBayes.t_variable.stochasticProcess}
                            names=fieldnames(OpenBUGSdatastruct);%need to identify the fields present in the coda               
                            for i=1:numel(names)
                                if strcmp(obj.variables(varid,chainid).name,cell2mat(names(i)))==1
                                    evalstr=sprintf('OpenBUGSdatastruct.%s',obj.variables(varid,chainid).name);
                                    tvar=eval(evalstr);
                                    if exist('tfieldindices','var')==1 %is this not the first entry (i.e., vector already created)
                                        tfieldindices=[tfieldindices varid];
                                        targlist=[targlist; {tvar}];
                                    else
                                        tfieldindices=varid;
                                        targlist=[{tvar}];
                                    end
                                    break;
                                end
                            end
                    end
                end
                data(:,chainid)=targlist;
                fieldindices(:,chainid)=tfieldindices;
            end    
            retobj=obj;
        end                      
    end
    
    methods %getter/setter functions
        function obj=set.OpenBUGS_projdir(obj,setOpenBUGS_projdir) 
            setOpenBUGS_projdir=mcmcBayes.osInterfaces.convPathStr(setOpenBUGS_projdir);
            obj.OpenBUGS_projdir=setOpenBUGS_projdir;
        end
        
        function value=get.OpenBUGS_projdir(obj)  
            value=obj.OpenBUGS_projdir;
        end
        
        function obj=set.OpenBUGS_scriptdir(obj,setOpenBUGS_scriptdir) 
            setOpenBUGS_scriptdir=mcmcBayes.osInterfaces.convPathStr(setOpenBUGS_scriptdir);
            obj.OpenBUGS_scriptdir=setOpenBUGS_scriptdir;
        end
        
        function value=get.OpenBUGS_scriptdir(obj)
            value=obj.OpenBUGS_scriptdir;
        end
        
        function obj=set.OpenBUGS_scriptfiletemplate(obj,setOpenBUGS_scriptfiletemplate)
            setOpenBUGS_scriptfiletemplate=mcmcBayes.osInterfaces.convPathStr(setOpenBUGS_scriptfiletemplate);
            if ~isempty(setOpenBUGS_scriptfiletemplate) %support case when loading an mcmcInterface object via load('*.mat')  
                temp=mcmcBayes.osInterfaces.convPathStr(sprintf('%s/%s',obj.OpenBUGS_scriptdir,setOpenBUGS_scriptfiletemplate));
                if mcmcBayes.osInterfaces.doesFileExist(temp) %is a file
                    obj.OpenBUGS_scriptfiletemplate=setOpenBUGS_scriptfiletemplate;
                else
                    temp=mcmcBayes.osInterfaces.convPathStr(sprintf('%s/%s',obj.OpenBUGS_scriptdir_ext,setOpenBUGS_scriptfiletemplate));
                    if mcmcBayes.osInterfaces.doesFileExist(temp) %is a file
                        obj.OpenBUGS_scriptfiletemplate=setOpenBUGS_scriptfiletemplate;
                    else
                        error('OpenBUGS_scriptfiletemplate folder does not exist');
                    end
                end
            end
        end
        
        function value=get.OpenBUGS_scriptfiletemplate(obj)  
            value=obj.OpenBUGS_scriptfiletemplate;
        end
        
        function obj=set.filename_OpenBUGSproject(obj,setfilename_OpenBUGSproject)
            setfilename_OpenBUGSproject=mcmcBayes.osInterfaces.convPathStr(setfilename_OpenBUGSproject);
            if ~isempty(setfilename_OpenBUGSproject) %support case when loading an mcmcInterface object via load('*.mat')  
                temp=mcmcBayes.osInterfaces.convPathStr(sprintf('%s/%s',obj.OpenBUGS_projdir,setfilename_OpenBUGSproject));
                if mcmcBayes.osInterfaces.doesFileExist(temp) %is a file
                    obj.filename_OpenBUGSproject=temp;
                else
                    temp=mcmcBayes.osInterfaces.convPathStr(sprintf('%s/%s',obj.OpenBUGS_projdir_ext,setfilename_OpenBUGSproject));
                    if mcmcBayes.osInterfaces.doesFileExist(temp) %is a file
                        obj.filename_OpenBUGSproject=temp;
                    else
                        error('OpenBUGS_projectdir folder does not exist');
                    end
                end
            end
        end
        
        function value=get.filename_OpenBUGSproject(obj)  
            value=obj.filename_OpenBUGSproject;
        end           
               
        function obj=set.OpenBUGS_shortcut(obj,setOpenBUGS_shortcut) 
            %setOpenBUGS_shortcut=mcmcBayes.osInterfaces.convPathStr(setOpenBUGS_shortcut);
            if mcmcBayes.osInterfaces.doesFileExist(setOpenBUGS_shortcut) %is a file
                obj.OpenBUGS_shortcut=setOpenBUGS_shortcut;
            else 
                error('OpenBUGS_shortcut does not exist');
            end
        end
        
        function value=get.OpenBUGS_shortcut(obj) 
            value=obj.OpenBUGS_shortcut;
        end
    end
end