classdef samplerInterfaces
%Use CODA (Convergence Diagnostic and Output Analysis) file format for archiving MCMC sample data: 
% Best, Nicky, Mary Kathryn Cowles, and Karen Vines. "CODA* Convergence Diagnosis and Output Analysis Software for Gibbs sampling output Version 0.30." (1996), p39.
    properties
        type%mcmcBayes.t_samplerInterfaces
        osInterface
        modeltype
        modelsubtype
        tempid
        nchains
        phi
        outputFileNamePrefix
        variables
        data
        runView
        repositories
    end
    
    methods (Static)
        function clearTempProjects(sampler_projdir)
            directory=mcmcBayesosInterfaces.convPathStr(sprintf('%stemp',sampler_projdir));
            files = dir(fullfile(directory,'*.txt'));
            for fileindex=1:numel(files)
                filename=mcmcBayesosInterfaces.convPathStr(sprintf('%s/%s',directory,files(fileindex).name));
                delete(filename);
            end
        end
                
        function clearTempScripts(sampler_scriptdir)
            directory=sprintf('%stemp',mcmcBayesosInterfaces.getPath(t_mcmcBayesPaths.SCRIPTS_DIR));%clear scripts/temp
            files = dir(fullfile(directory,'*.txt'));
            for fileindex=1:numel(files)
                filename=sprintf('%s/%s',directory,files(fileindex).name);
                delete(filename);
            end
            fclose('all');%sometimes MATLAB leaves these open
            files = dir(fullfile(directory,'*.py'));
            for fileindex=1:numel(files)
                filename=sprintf('%s/%s',directory,files(fileindex).name);
                delete(filename);
            end
            
            directory=mcmcBayesosInterfaces.convPathStr(sprintf('%stemp',sampler_scriptdir));%clear scripts/sampler/temp
            files = dir(fullfile(directory,'*.txt'));
            for fileindex=1:numel(files)
                filename=mcmcBayesosInterfaces.convPathStr(sprintf('%s/%s',directory,files(fileindex).name));
                delete(filename);
            end
            fclose('all');%can MATLAB leaves these open?
            files = dir(fullfile(directory,'*.py'));
            for fileindex=1:numel(files)
                filename=mcmcBayesosInterfaces.convPathStr(sprintf('%s/%s',directory,files(fileindex).name));
                delete(filename);
            end                 
        end        
    end
    
    methods (Abstract)
        retobj=setupMCMCSampling(obj, setmodeltype, setmodelsubtype, setpriordatatypeid, setdatatypeid, setdatasetid, settempid, setphi, setIndependentVariables, setModelVariables, setDependentVariables)%set up the MCMCsampler for the next sampling iteration
        retobj=setupBashScript(obj)
        [retobj]=performMCMCSampling(obj)%executes the MCMCsampler and returns resulting sample data    
        [retobj, fieldindices, data]=readSamples(obj)
    end
    
    methods
        function obj = samplerInterfaces(settype, setosInterface, setrunView, setRepositories)%constructor
            if nargin > 0
                obj.type=settype;
                obj.osInterface=setosInterface;
                obj.runView=setrunView;     
                obj.repositories=setRepositories;
            end
        end
        
        function setupInits(obj, initsfilename)
%set all the variables            
            chainid=1;%just use the first chain
            for varid=1:size(obj.variables,1)
                if exist('inits','var')==0 %first entry
                    initsliststr=sprintf('\t%s Inits: ', mcmcBayes.t_samplerInterfaces.tostring(obj.type));
                end
                if (obj.variables(varid,chainid).type~=mcmcBayes.t_variable.constant)%only initialize stochastic nodes
                    %for stochastic processes, strip off, for example, (t) from Theta(t) in the name
                    if obj.variables(varid,chainid).type==mcmcBayes.t_variable.stochasticProcess
                        if isempty(strfind(obj.variables(varid,chainid).name,'('))
                            strname=obj.variables(varid,chainid).name;
                        else
                            strname=obj.variables(varid,chainid).name(1:strfind(obj.variables(varid,chainid).name,'(')-1);
                        end
                    else
                        strname=obj.variables(varid,chainid).name;
                    end
                    if exist('inits','var')==0 %first entry
                        inits=[{strname} obj.variables(varid,chainid).MCMC_initvalue  {obj.variables(varid,chainid).dimensions}];
                    else                        
                        inits=[inits; {strname} obj.variables(varid,chainid).MCMC_initvalue {obj.variables(varid,chainid).dimensions}];
                    end
                    initsliststr=sprintf('%s%s=%s',initsliststr,cell2mat(inits(end,1)),conversionUtilities.matrix_tostring(cell2mat(inits(end,2))));
                    if ~(varid==size(obj.variables,1)) %add a trailing comma if there are more variables
                        initsliststr=sprintf('%s,',initsliststr);
                    end
                end
            end
            if exist('inits','var')
                %disp(initsliststr);
                if obj.type==mcmcBayes.t_samplerInterfaces.OpenBUGS
                    mcmcBayes.mat2bugs(initsfilename,inits);
                else
                    mcmcBayes.mat2rdump(initsfilename,inits);
                end
            end                
        end
        
        function setupData(obj, datafilename)
            defdims=obj.data.indVarsDimensions;
            %assuming that we have the same number of data points, Tau, for all data variables            
            if defdims~=mcmcBayes.t_numericDimensions.null
                for varid=1:size(obj.data.indVarsValues,1)
                    varSizes=cast(size(obj.data.indVarsValues{varid}),'int32');
                    if defdims==mcmcBayes.t_numericDimensions.scalar
                        Tau=varSizes(1);%numDataPoints
                        R(varid,1)=int32(1);
                    elseif defdims==mcmcBayes.t_numericDimensions.vector
                        R(varid,1)=varSizes(2);%numRows 
                        Tau=varSizes(1);%numDataPoints
                    else
                        error('todo');
                    end             
                end
            else %must not have ind variables (e.g., Gaussian)
                defdims=obj.data.depVarsDimensions;
                varSizes=cast(size(obj.data.depVarsValues{1},1),'int32');%assumes one dep variable
                if defdims==mcmcBayes.t_numericDimensions.scalar
                    Tau=varSizes(1);%numDataPoints
                    R(1,1)=int32(1);
                elseif defdims==mcmcBayes.t_numericDimensions.vector
                    R(1,1)=varSizes(2);%numRows 
                    Tau=varSizes(1);%numDataPoints
                else
                    error('todo');
                end                   
            end
                    
            data=[{'phi'} {obj.phi} {mcmcBayes.t_numericDimensions.scalar}; {'Tau'} {Tau} {mcmcBayes.t_numericDimensions.scalar}; ...
                {'R'} {R} {mcmcBayes.t_numericDimensions.vector}];%Even for prior sampling of preset and none datatype we create a non-empty dependentVariables
            dataliststr=sprintf('\t%s Data: phi=%f, Tau=%f, R=%f', mcmcBayes.t_samplerInterfaces.tostring(obj.type), obj.phi, Tau, R);
            
            for varid=1:size(obj.variables,1)
                defdims=obj.variables(varid).dimensions;
                varSizes=cast(size(obj.variables(varid).MCMC_initvalue{:}),'int32');%MCMC_initvalue will be array with contents (1 x 1), (M x 1), or (M x N x 1)
                if defdims==mcmcBayes.t_numericDimensions.scalar
                    vR(varid,1)=int32(1);
                elseif defdims==mcmcBayes.t_numericDimensions.vector
                    vR(varid,1)=varSizes(1);%numRows 
                else
                    error('todo');
                end       
                if obj.variables(varid).distribution.type==mcmcBayes.t_distribution.Flat
                    continue;
                end
                dataliststr=sprintf('%s,\n\t\t',dataliststr);
                chainid=1;
                for paramid=1:numel(obj.variables(varid).distribution.parameters)
                    %for stochastic processes, strip off, for example, (t) from Theta(t) in the name
                    if obj.variables(varid,chainid).type==mcmcBayes.t_variable.stochasticProcess
                        if isempty(strfind(obj.variables(varid).distribution.parameters(paramid).name,'('))
                            strname=obj.variables(varid).distribution.parameters(paramid).name;
                        else
                            strname=obj.variables(varid).distribution.parameters(paramid).name(1:strfind(obj.variables(varid).distribution.parameters(paramid).name,'(')-1);
                        end
                    else
                        strname=obj.variables(varid).distribution.parameters(paramid).name;
                    end                    
                    
                    data=[data; {strname} {obj.variables(varid).distribution.parameters(paramid).values} {obj.variables(varid).distribution.parameters(paramid).dimensions}]; %data requires transposes of vectors for mat2rdump translations to R-dump format
                    dataliststr=sprintf('%s%s=%s',dataliststr, strname, conversionUtilities.matrix_tostring(obj.variables(varid).distribution.parameters(paramid).values));
                    if ~(paramid==numel(obj.variables(varid).distribution.parameters)) %add a trailing comma if there are more
                        dataliststr=sprintf('%s,',dataliststr);
                    end
                end
            end
            data=[data; {'vR'} {vR} {mcmcBayes.t_numericDimensions.vector}];

            datavarid=1;
            if ~(isempty(obj.data(datavarid).indVarsValues))%some models do not have independent variables (e.g., basic Gaussian)
                %override the name, obj.data.independentVars{}.name, with the independent variable names used in the sampler model                
                handle=mcmcBayes.simulation.getHandleDefaultDataVariableNames(obj.modeltype);
                evalstr=sprintf('[indvarnames]=handle(mcmcBayes.t_dataCategory.independent);');
                eval(evalstr);

                for datavarid=1:size(obj.data.indVarsName,1)                   
                    data=[data; indvarnames(datavarid) obj.data.indVarsValues(datavarid) {obj.data.indVarsDimensions(datavarid)}];
                    if datavarid==1
                        dataliststr=sprintf('%s,\n\t\t%s=%s', dataliststr, indvarnames{datavarid}, conversionUtilities.matrix_tostring(obj.data.indVarsValues{datavarid}));
                    elseif datavarid>1
                        dataliststr=sprintf('%s,%s=%s', dataliststr, indvarnames{datavarid}, conversionUtilities.matrix_tostring(obj.data.indVarsValues{datavarid}));
                    end
                end
            end
            
            %override the name, obj.data.dependentVars{}.name, with the dependent variable names used in the sampler model
            handle=mcmcBayes.simulation.getHandleDefaultDataVariableNames(obj.modeltype);
            evalstr=sprintf('[depvarnames]=handle(mcmcBayes.t_dataCategory.dependent);');
            eval(evalstr);
            
            for datavarid=1:size(obj.data.depVarsName,1)
                data=[data; depvarnames(datavarid) obj.data.depVarsValues(datavarid) {obj.data.depVarsDimensions(datavarid)}];
                if datavarid==1
                    dataliststr=sprintf('%s,\n\t\t%s=%s', dataliststr, depvarnames{datavarid}, conversionUtilities.matrix_tostring(obj.data.depVarsValues{datavarid}));
                elseif datavarid>1
                    dataliststr=sprintf('%s,%s=%s', dataliststr, depvarnames{datavarid}, conversionUtilities.matrix_tostring(obj.data.depVarsValues{datavarid}));
                end
            end
            %disp(dataliststr);
            if obj.type==mcmcBayes.t_samplerInterfaces.OpenBUGS
                mcmcBayes.mat2bugs(datafilename,data);
            else
                mcmcBayes.mat2rdump(datafilename,data);
            end           
        end   
    end
    
    methods %getter/setter functions
        function obj = set.type(obj,settype)
            if ~isa(settype,'mcmcBayes.t_samplerInterfaces')
                error('samplerInterfaces.type must be type mcmcBayes.t_samplerInterfaces');
            else
                switch settype
                    case {mcmcBayes.t_samplerInterfaces.OpenBUGS,mcmcBayes.t_samplerInterfaces.JAGS,mcmcBayes.t_samplerInterfaces.Stan}
                        obj.type = settype;
                    otherwise
                        error('unsupported samplerInterfaces subclass');
                end
            end
        end
        
        function value = get.type(obj)
            value=obj.type;
        end               
    end
end