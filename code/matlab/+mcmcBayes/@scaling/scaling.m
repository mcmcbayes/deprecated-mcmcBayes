classdef scaling < mcmcBayes.model    
%mcmcBayes.regression is a base class for models using regression.  It is a sub class of mcmcBayes.model and mcmcBayes.simulation.
% The currently implemented models, listed hierarchically, are:
%  * mcmcBayes.simulation.model.scaling.linear
%  * mcmcBayes.simulation.model.scaling.exponential
    properties (SetAccess=public, GetAccess=public)
        predictionsNSamples=mcmcBayes.regression.getDefaultNSamples();%Default number of samples to run in calls to simPredictions()
    end

    methods (Abstract, Static)        

    end
    
    methods (Abstract)

    end

    methods (Static)
        function test()
            display('Hello!');
        end
                
        function [predictionsNSamples]=getDefaultNSamples()
            predictionsNSamples=50000;
        end        
        
        function [names]=getDefaultDataVariableNames(dataCategory)
            switch dataCategory
                case mcmcBayes.t_dataCategory.independent
                    names=[{'x1'} {'x2'}];
                case mcmcBayes.t_dataCategory.dependent
                    names={'y'};
                otherwise
                    error('dataCategory must be mcmcbayes.t_dataCategory.');
            end
        end        
        
        function [loglikelihood]=computeLogLikelihood(type, vars, data)
        % [loglikelihood]=computeLogLikelihood(type, vars, data)
        %   Computes the loglikelihood of the data given the model and parameter values.  This is used in the marginal likelihood computation.
        %   Input: 
        %     type - Specifies the model to use for computing the likelihood
        %     vars - Model variables that is an array of mcmcBayes.variable objects 
        %     data - Independent and dependent data variables for the model
        %   Output:
        %     loglikelihood - Computed loglikelihood (vector)
            if ~strcmp(type, 'scalingLinear') && ~strcmp(type, 'scalingExponential')
                error('Incorrect model type.');
            end
            
            datavarid=1;
%     warning('Temporary: this is necessary to replicate marginal log-likelihood estimates'); 
%     x=x-mean(x);%This is necessary to replicate marginal log-likelihood estimates
            x1=cast(data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent, datavarid), 'double');
            datavarid=2;
            x2=cast(data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent, datavarid), 'double');
            Tau=size(x1,2);%Number of data (we are assuming scalar independent variables)
            datavarid=1;
            yorig=cast(data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.dependent, datavarid), 'double');

            % Ideally numSamples would be obj.modelVariables(index).MCMC_nsamples_data.  If the MCMC sampler has issues 
            %   (e.g., improper values for the inits) it may stop early.  The user is separately warned of 
            %   improperly sized sample arrays when they are stored by mcmcBayesVariable.set.MCMC_samples(). Set
            %   tsize to the variable with the fewest number of samples.
            numSamples=size(vars(1).MCMC_samples,2);           
            loglikelihood=zeros(numSamples,1);

%todo: need to already have called transformVarsPost on variables
            c=cast(vars(1).MCMC_samples,'double');
            r=cast(vars(2).MCMC_samples,'double');
            if strcmp(type, 'scalingLinear')
            	mufunc = @(x1,x2,c) (1+c'*x1)./(1+c'*x2);
            elseif strcmp(type, 'scalingExponential')
            	mufunc = @(x1,x2,c) c'*(x1-x2);
            else
                error('unsupported model for mcmcBayes.scaling::computeLogLikelihood()');
            end
            
            for i=1:numSamples
                for j=1:Tau %row for variable samples, col for each independent variable value
                    mu(i,j)=mufunc(x1(:,j),x2(:,j),c(:,i));
                end
            end

            tloglikelihood=zeros(numSamples,Tau);
            for i=1:numSamples
                tloglikelihood(i,:)=1/2*log(r(i))-1/2*log(2*pi)-r(i)/2.*((yorig((1:end),1)'-mu(i,(1:end))).^2);
            end
            for i=1:numSamples
                loglikelihood(i,1)=sum(tloglikelihood(i,:));
            end                         
            
            if (~isempty(find(isinf(loglikelihood)==1)))
                error('Error condition in computeLogLikelihood(), loglikelihood contains -Inf or Inf');
            elseif ~isempty(find(isnan(loglikelihood)==1))
                error('Error condition in computeLogLikelihood(), loglikelihood contains Nan');
            end            
        end

        function [dependentVariables]=generateTestData(type, predictionsNSamples, data, varargin)
            dependentVariables=generateTestData@mcmcBayes.model(type, predictionsNSamples, data, varargin{:});
        end
        
        function [depVarsValuesHat, retYhat]=simPredictions(type, predictionsNSamples, predictionsToCapture, hingeThreshold, data, varargin)
        % [dependentVariablesHat]=simPredictions(modeltype, predictionsNSamples, dependentVariablesHatTypes, hingeThreshold, independentVariables, dependentVariableName, description, arglist)
        %   Function to simulate predictions using one of the regression models (it is static so we can simulate data without instantiating a class)
        %   Input:
        %     modeltype - Specifies the model to use for generating the predictions
        %     predictionsNSamples - Number of samples to run when generating the predictions
        %     dependentVariablesHatTypes - Vector of t_mcmcBayesDependentVariablesHat that specifies which thetahat to generate predictions for
        %     hingeThreshold - Specifies the hinge threshold for determining the thetaHat edge cases for generating dependentVariablesHat.{hingelo/hingehi}
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     dependentVariableName - Name to use for the predictions data
        %     description - Description for the predictions data
        %     arglist - Cell array with the values to use for the model variables when prediction with the model (i.e., these are the thetaHat)
        %   Output:
        %     dependentVariablesHat - Struct with elements for each of the t_mcmcBayesDependentVariablesHat types specified in dependentVariablesHatTypes 
        %     (e.g., the mean and the {hingelo/hingehi} values from the histogram of the predicted values over predictionsNSamples samples)           
            if ~strcmp(type, 'scalingLinear') && ~strcmp(type, 'scalingExponential') 
                error('Incorrect model type.');
            end
            datavarid=1;
            x1=cast(data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent, datavarid),'double');
            datavarid=2;
            x2=cast(data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent, datavarid),'double');
            for j=1:size(x1,2)
                x1_(:,j)=mcmcBayes.VDMCaseStudy2.featureScaling(x1(:,j));
                x2_(:,j)=mcmcBayes.VDMCaseStudy2.featureScaling(x2(:,j));
            end
            x1=x1_;
            x2=x2_;
            depdatavarid=1;
            depVarName=data.depVarsName{depdatavarid};
            depVarDesc=data.depVarsDesc{depdatavarid};
            depVarTransformPre=data.depVarsTfPre(depdatavarid);
            depVarTransformPost=data.depVarsTfPost(depdatavarid);
            
            numdatapoints=size(x1,2);                        
            c=varargin{1};
            r=varargin{2};
            if strcmp(type, 'scalingLinear')
                for datapoint=1:numdatapoints
                    %convert r to MATLAB's normal(mu,sigma), or sigma=1/sqrt(r)
                    m(datapoint)=(1+c'*x1(:,datapoint))./(1+c'*x2(:,datapoint));
                    pd=makedist('Normal',m(datapoint),1/sqrt(r));
                    Yhat(datapoint,:)=random(pd,1,predictionsNSamples);
                end
            elseif strcmp(type, 'scalingExponential')
                for datapoint=1:numdatapoints
                    %convert r to MATLAB's normal(mu,sigma), or sigma=1/sqrt(r)
                    m(datapoint)=c'*(x1(:,datapoint)-x2(:,datapoint));
                    pd=makedist('Normal',m(datapoint),1/sqrt(r));
                    Yhat(datapoint,:)=random(pd,1,predictionsNSamples);
                end
            else
                error('unsupported model for mcmcBayes.regression::simPredictions()');
            end

            binprec=(ceil(max(max(Yhat)))-floor(min(min(Yhat))))/1000;
            bins=floor(min(min(Yhat))):binprec:ceil(max(max(Yhat)));
            depVarsValuesHat=mcmcBayes.model.capturePredictionThresholds(predictionsNSamples, predictionsToCapture, hingeThreshold, bins, ...
                mcmcBayes.t_numericDimensions.vector, Yhat, data.depVarsType{depdatavarid}); 
            retYhat=Yhat;
        end
    end
    
    methods 
        function obj=scaling(settype, setsubType, setname, setdescription, setauthor, setreference, setprefix)
        % obj=regression(setmodeltype, setpriordatatype, setdatatype, setsamplerinterface, setosInterface)
        %   The constructor for regression.
        % Input:
        %   setmodeltype - Specifies the type of the sub-model being instantiated
        %   setpriordatatype - t_mcmcBayesData type that specifies the priorDependentVariables (e.g., CCMPerformanceWeighting, EqualWeighting, Individual, and Simulated)
        %   setdatatype - t_mcmcBayesData type that specifies dependentVariables (e.g., ConFR, LinIFR, ExpIFR, LinDFR, ExpDFR, and Simulated)
        %   setdatasetid - Specifies the dataset id to load for this model evaluation
        %   setsamplerinterface - instanted sampler interface, mcmcBayesSamplerInterfaces class (e.g., mcmcBayesSamplerInterfacesOpenBUGS or mcmcBayesSamplerInterfacesJAGS)
        %   setosInterface - instantiated os interface, mcmcBayesosInterfaces class (e.g., mcmcBayesosInterfaces_UbuntuLinux or mcmcBayesosInterfaces_Windows7)
        % Output:
        %   obj Constructed regression object
            if nargin == 0
                superargs={};
            else
                superargs{1}=settype;
                superargs{2}=setsubType;
                superargs{3}=setname;
                superargs{4}=setdescription;
                superargs{5}=setauthor;
                superargs{6}=setreference;
                superargs{7}=setprefix;                                
            end

            obj=obj@mcmcBayes.model(superargs{:});           
        end
                
        function displayPredictions(obj, tempid)
        % displayPredictions(obj)
        %   Displays the values (in the console) for the already generated model predictions in obj.dependentVariablesHat.
        %   Input:
        %     tempid - Temperature id to compute the display the predictions for
            stdout=1;
            %print the original data
            datavarid=1;                
            if tempid==0
                error('Not supported.');
            else
                x1=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.independent, datavarid);
                datavarid=2;        
                x2=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.independent, datavarid);
                for i=1:10
                    fprintf(stdout,'x1(%d,:)=%s\n',i,conversionUtilities.matrix_tostring(x1(i,:)));
                end
                for i=1:10
                    fprintf(stdout,'x2(%d,:)=%s\n',i,conversionUtilities.matrix_tostring(x2(i,:)));
                end                    
                datavarid=1;
                Yorig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.dependent, datavarid);
                fprintf(stdout,'Yorig=%s\n',conversionUtilities.matrix_tostring(Yorig));
            end
            
            %print the predictions
            for Yhattype=obj.predictionsToCapture%use the vector obj.predictionsToCapture to determine which structure elements from priorPredictedData to show
                datavarid=1;
                if tempid==0
                    error('Not supported.');
                else
                    x1=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.independent, datavarid, 'mean');%just use mean (all the same)
                    datavarid=2;
                    x2=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.independent, datavarid, 'mean');%just use mean (all the same)
                end
                for j=1:size(x1,2)
                    x1_(:,j)=mcmcBayes.VDMCaseStudy2.featureScaling(x1(:,j));
                    x2_(:,j)=mcmcBayes.VDMCaseStudy2.featureScaling(x2(:,j));
                end
                x1=x1_;
                x2=x2_;                
                datavarid=1;
                switch Yhattype
                    case mcmcBayes.t_dataResult.hingelo
                        if tempid==0
                            error('not supported');
                        else
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');%just use mean (all the same)
                        end
                        name='Yhat.hingelo';
                    case mcmcBayes.t_dataResult.mean
                        if tempid==0
                            error('not supported');
                        else
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');%just use mean (all the same)
                        end
                        name='Yhat.mean';
                    case mcmcBayes.t_dataResult.hingehi
                        if tempid==0
                            error('not supported');
                        else
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');%just use mean (all the same)
                        end
                        name='Yhat.hingehi';
                    otherwise
                        error('Cannot identify the appropriate structure element in obj.predictionsToCapture');
                end
                %only display the mean for now
                if Yhattype==mcmcBayes.t_dataResult.mean
                    for i=1:10
                        fprintf(stdout,'x1(%d,:)=%s\n',i,conversionUtilities.matrix_tostring(x1(i,:)));
                    end
                    for i=1:10
                        fprintf(stdout,'x2(%d,:)=%s\n',i,conversionUtilities.matrix_tostring(x2(i,:)));
                    end
                    fprintf(stdout,'%s=%s\n',name,conversionUtilities.matrix_tostring(Yhat));
                end
            end
        end
        
        function plotPredictions(obj, chainID, tempID)
        % plotPredictions(obj, chainID, tempID)
        %   Plots the model simulation results, using point estimates for Thetahat, over the original data.
            warning('Plots are not supported');
        end                
    end

    methods (Access=protected)
        function [status]=checkDataCompatibility(obj, datatype)
        % [status]=checkDataCompatibility(obj, datatype)
        %   Checks the compatibility of priorDependentVariables with the specified mcmcBayesModelRegression type
            switch datatype
                case mcmcBayes.t_data.priordata
                	status=obj.priorData.checkDataForInfNanValues();
                case mcmcBayes.t_data.priorbasedpredictions
                	status=obj.priorPredictedData.checkDataForInfNanValues();
                case mcmcBayes.t_data.posteriordata
                	status=obj.posteriorData.checkDataForInfNanValues();
                case mcmcBayes.t_data.posteriorbasedpredictions
                	status=obj.posteriorPredictedData.checkDataForInfNanValues();
            end
        end
    end
    
    methods %getter/setter functions       

    end
end
