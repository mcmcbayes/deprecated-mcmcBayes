classdef t_paths < int32
    enumeration
        %model types
        archivesDir (1) %containts *.mat files for restoring previous states (rewind & replay)
        configDir (2) %contains configuration files
        configSimsDir (3)
        docDir (4) %contains documentation
        figuresDir (5) %contains generated images
        inputDataDir (6) %contains the input data files
        logsDir (7) %contains log files from sampler and MATLAB sequences
        matlabDir (8) %contains MATLAB programs
        mcmcBayesDir (9) %directory for the mcmcBayes project
        poisonFile (10) %path to the kill file that shuts down any MATLAB engines and sampler subprocesses that are running
        pythonUtilsFile (11) %path for the mcmcBayesUtils.py utilities python script
        samplerDataInDir (12) %contains sampler data input and dtd files
        samplerInitsDir (13) %contains sampler initialization files
        samplerProjectsDir (14) %contains sampler project file templates and generated project files
        samplerScriptsDir (15) %contains sampler script file templates, generated script files, and python scripts generated by the OS interfaces
        samplerDataOutDir (16) %contains sampler output files
        formattedOutputDir (17) %path for the formatted *.csv output files
        tempDir (18) %contains temporary files created during a sequence (e.g., files used for messaging between MATLAB and python scripts controlling samplers)
        javaDir (19) %contains java jars
    end
end