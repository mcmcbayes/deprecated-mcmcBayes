classdef t_samplerInterfaces < int32
    enumeration
        OpenBUGS (1)
        JAGS (2)
        Stan (3)
    end
    
    methods (Static)        
        function intvalue=toint(enumobj)
            switch enumobj
                case mcmcBayes.t_samplerInterfaces.OpenBUGS
                    intvalue=int32(1);
                case mcmcBayes.t_samplerInterfaces.JAGS
                    intvalue=int32(2);
                case mcmcBayes.t_samplerInterfaces.Stan
                    intvalue=int32(3);
                otherwise
                    error('unsupported mcmcBayes.t_samplerInterfaces type');
            end                
        end        
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_samplerInterfaces.OpenBUGS
                    strvalue='mcmcBayes.t_samplerInterfaces.OpenBUGS';
                case mcmcBayes.t_samplerInterfaces.JAGS
                    strvalue='mcmcBayes.t_samplerInterfaces.JAGS';
                case mcmcBayes.t_samplerInterfaces.Stan
                    strvalue='mcmcBayes.t_samplerInterfaces.Stan';
                otherwise
                    error('unsupported mcmcBayes.t_samplerInterfaces type');
            end
        end                
        
        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_samplerInterfaces.OpenBUGS)
                    enumobj=mcmcBayes.t_samplerInterfaces.OpenBUGS;
                case int32(mcmcBayes.t_samplerInterfaces.JAGS)
                    enumobj=mcmcBayes.t_samplerInterfaces.JAGS;
                case int32(mcmcBayes.t_samplerInterfaces.Stan)
                    enumobj=mcmcBayes.t_samplerInterfaces.Stan;
                otherwise
                    error('unsupported mcmcBayes.t_samplerInterfaces type');
            end 
        end    
        
        function enumobj=str2enum(strval)
                if strcmp('mcmcBayes.t_samplerInterfaces.OpenBUGS',strval)==1
                    enumobj=mcmcBayes.t_samplerInterfaces.OpenBUGS;
                elseif strcmp('mcmcBayes.t_samplerInterfaces.JAGS',strval)==1
                    enumobj=mcmcBayes.t_samplerInterfaces.JAGS;
                elseif strcmp('mcmcBayes.t_samplerInterfaces.Stan',strval)==1
                    enumobj=mcmcBayes.t_samplerInterfaces.Stan;
                else
                    error('unsupported mcmcBayes.t_samplerInterfaces type');
                end
        end
    end
end