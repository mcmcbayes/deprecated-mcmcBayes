classdef polynomialDegree2 < mcmcBayes.regression   
% Polynomial (degree 2) regression model, DeGroot 2005, 1972.  Approach for modeling the relationship between a scalar dependent variable y and one or more 
% independent variables assuming a quadratic rate in which the dependent variable changes per the independent variable values.  An alternative use defines 
% the independent variables as unique values in a set of times.
% modelsubtype
% 'a' - Normal Inverse Gamma priors, unknown precision, hierarchical (i.e., beta0~normal(a,(1/(b*r))^-1), beta1~normal(c,(1/(d*r))^-1), beta2~normal(g,(1/(h*r))^-1), r~gamma(p,q))
% 'b' - Normal Inverse Gamma priors, unknown precision (i.e., beta0~normal(a,(1/b)^-1), beta2~normal(g,(1/h)^-1), r~gamma(p,q))
% 'c' - Flat priors (i.e., beta0~flat, beta1~flat, beta2~flat, r~T[0,]*flat)
    methods(Access = public, Static)
        function test()
            display('Hello!');
        end
        
        function retdata=transformDataPre(vars, data)            
            retdata=data;%no special transforms for this model (everything handled by mcmcBayes.data.constructData())
        end
        
        function [solverInits]=determineSolverInits(vars, data)
        %needs to return a cell array due to some models having different sized variables
            for varid=1:size(vars,1)
                solverInits(varid,1)=[{vars(varid).MCMC_initvalue}];%needs to be a row vector
            end
            %Compute solverInits (bootstrap values for mle/ls that solve for solverInits) using fsolve and fhandle1
            if ~((data.dataSource==mcmcBayes.t_dataSource.None) || (data.dataSource==mcmcBayes.t_dataSource.Preset))
                indVarsValues=data.indVarsValues;
                depVarsValues=data.depVarsValues;
                %Solve for beta0,beta1 given r and Y(t=end)=beta0+beta1*x
                datavarid=1;
                x=indVarsValues{datavarid, 1};
                y=cast(depVarsValues{datavarid, 1},'double');
                %Solve for beta0,beta1,beta2 given r and N(t=end)=beta0+beta1*x+beta2*x^2
                evalstr=sprintf('fhandle=@(a) a(1)+a(2)*%f+a(3)*%f^2-%f',x(end),x(end),y(end));      
                eval(evalstr);%N(t=end) equals dependentVariables(end)
                tsolverInits=[fsolve(fhandle, [solverInits{1:3}])];
                solverInits=[{tsolverInits(1)}; {tsolverInits(2)}; {tsolverInits(3)}; {vars(4).distribution.estimateMeanFromHyperparameters}];
            end
        end        
        
        %this is only used for determining the inits when the data source is not mcmcBayes.t_dataSource.None or mcmcBayes.t_dataSource.Preset 
        function [thetahat]=SolverMeanMLE(runPowerPosteriorMCMCSequence, subtype, vars, data)            
        % [thetahat]=mlefitModel(runPowerPosteriorMCMCSequence, subtype, vars, data)
        %   Wrapper function to call MATLAB's mle routine that estimates the model variable point estimates.  The structure
        %   of MATLAB's mle routine requires globals to pass the independent variables, phi, and any values for variables that
        %   are assumed as a constant (thus, are not being estimated by the mle).
        %   Input:
        %     runPowerPosteriorMCMCSequence - Boolean that specifies either running the power-posterior sequence, or not running
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     modelVariables - Distribution model variables that is an array of mcmcBayesVariable (indexed by varid, tempid) objects 
        %     dependentVariables - Prior dependent variables for the model that is a cell array of mcmcBayesNumeric objects.
        %   Output:
        %     thetahat - Cell array of model variable point estimates values computed (cell array due to some models having different sized variables)
            options = statset('MaxIter',800, 'MaxFunEvals',1200);     

            if runPowerPosteriorMCMCSequence
                phi=mcmcBayes.getDefaultPhi(2);%identify the inits for tempid=2
            else
                phi=1; 
            end

           	datavarid=1;
            x=cast(data(datavarid).indVarsValues{:},'double');            
            
            if (vars(1).type==mcmcBayes.t_variable.Stochastic) && (vars(2).type==mcmcBayes.t_variable.Stochastic) && ...
                    (vars(3).type==mcmcBayes.t_variable.Stochastic) && (vars(4).type==mcmcBayes.t_variable.Stochastic)
                mypdf=@(y,beta0,beta1,beta2,r) mcmcBayes.linear.computeLogLikelihood_alt(y,phi,x,beta0,beta1,beta2,r);
                varsDefaultInits=[cast(vars(1).MCMC_initvalue,'double') cast(vars(2).MCMC_initvalue,'double') cast(vars(3).MCMC_initvalue,'double') ...
                    cast(vars(4).MCMC_initvalue,'double')];
                varsLowerBounds=[cast(vars(1).solverMin,'double') cast(vars(2).solverMin,'double') cast(vars(3).solverMin,'double') ...
                    cast(vars(4).solverMin,'double')];
                varsUpperBounds=[cast(vars(1).solverMax,'double') cast(vars(2).solverMax,'double') cast(vars(3).solverMax,'double') ...
                    cast(vars(4).solverMax,'double')];
            else
                error('Unsupported combination of modelVariables().type');
            end
            
            datavarid=1;
            y=cast(data(datavarid).depVarsValues{:},'double');
            [thetahat]=mle(y,'logpdf',mypdf,'start',varsDefaultInits,'lowerbound',varsLowerBounds,'upperbound',varsUpperBounds,'options',options);
        end
        
        %this is only used for determining the inits when the data source is not mcmcBayes.t_dataSource.None or mcmcBayes.t_dataSource.Preset 
        function [thetahat]=SolverMeanLS(runPowerPosteriorMCMCSequence, subtype, vars, data)
        % [thetahat]=lsfitModel(independentVariables, modelVariables, dependentVariables)
        %   Wrapper function to call MATLAB's lsqcurvefit routine that estimates the model variable point estimates.  
        %   Input:
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     modelVariables - Distribution model variables that is an array of mcmcBayesVariable (indexed by varid, tempid) objects 
        %     dependentVariables - Prior dependent variables for the model that is a cell array of mcmcBayesNumeric objects.
        %   Output:
        %     thetahat - Cell array of model variable point estimates values computed (cell array due to some models having different sized variables)
           	datavarid=1;
            x=cast(data(datavarid).indVarsValues{:},'double');

            fhandle=sprintf('beta0+beta1*x+beta2*x^2');
            coef={'beta0','beta1','beta2'};
            
            varsDefaultInits=[cast(vars(1).MCMC_initvalue,'double') cast(vars(2).MCMC_initvalue,'double') cast(vars(3).MCMC_initvalue,'double')];
            varsLowerBounds=[cast(vars(1).solverMin,'double') cast(vars(2).solverMin,'double') cast(vars(3).solverMin,'double')];
            varsUpperBounds=[cast(vars(1).solverMax,'double') cast(vars(2).solverMax,'double') cast(vars(3).solverMax,'double')];
            
            options = optimoptions('lsqcurvefit','Display','none','MaxFunEvals',3000,'MaxIter',1000);            

            datavarid=1;
            y=cast(data(datavarid).depVarsValues{:},'double');
            myfittype = fittype(fhandle, 'dependent', {'y'}, 'independent', {'x'}, 'coefficients', coef);%customnonlinear fittype
            xhat = fit(x,y,myfittype,'StartPoint',varsDefaultInits,'Lower',varsLowerBounds,'Upper',varsUpperBounds);
            %[xhat,resnorm,residual,exitflag,output]=lsqcurvefit(fhandle,modelVariableDefaultInits,x,y,modelVariableLowerBounds,modelVariableUpperBounds,options);
            %if exitflag~=1%converged
            %    disp(output.message)
            %end

            %Debugging plot
            %F=@(beta0,beta1,beta2,x) beta0+beta1.*x+beta2.*x.^2;
            %fig; hold on; scatter(x,y,'rx'); scatter(x,F(xhat.beta0,xhat.beta1,xhat.beta2,x),'g*'); hold off;
            
            thetahat=[{xhat.beta0} {xhat.beta1} {xhat.beta2} {vars(4).distribution.estimateMeanFromHyperparameters}];
        end                                             
    end

    methods(Access = private, Static)        
        function [loglikelihood]=computeLogLikelihood_alt(data,phi,x,varargin)
        % [loglikelihood]=computeLogLikelihood_alt(data,varargin)
        % Function that computes the model's log-likelihood of the data given the parameters.  It is used by MATLAB's mle function.
        % It was necessary to utilize a global variable for passing the independent variables and phi.
        % Input:
        %   data - Model input data used in computing the log-likelihood
        %   varargin - Point estimates for the model variables used in computing the log-likelihood
        % Output:
        %   loglikelihood - Computed log-likelihood
            y=cast(data,'double');
            beta0=varargin{1};
            beta1=varargin{2};   
            beta2=varargin{3};
            r=varargin{4};
            for i=1:numel(y)
                m(i)=beta0+beta1*x(i)+beta2*x(i)^2;
                logpdf(i)=phi*((1/2)*log(r/(2*pi))-(r/2)*(y(i)-m(i))^2);
            end
            loglikelihood=sum(logpdf);
%             tempstr=sprintf('linear regression loglikelihood(beta0=%f,beta1=%f,r=%f)=%f',beta0,beta1,r,loglikelihood);
%             disp(tempstr);
        end        
    end
    
    methods 
        function obj=polynomialDegree2(setsimulationStruct, setprefix, setcomputemsfe)
        % polynomialDegree2(setsimulationStruct, setprefix)
        %   Constructor for the polynomialDegree2 class
        %   Input:
        %     setsimulationStruct - 
        %     setprefix - 
        %   Output:
        %     obj - instantiated polynomialDegree2 object
            if nargin == 0
                superargs={};
            else
                switch setsimulationStruct.subType
                    case 'a'
                        %disp('Polynomial degree 2 regression model is using modelsubtype a (beta0~normal(a,b*r), beta1~normal(c,d*r), beta2~normal(g,h*r), r~gamma(p,q))');
                    case 'b'
                        %disp('Polynomial degree 2 regression model is using modelsubtype b (beta0~normal(a,b), beta1~normal(c,d), beta2~normal(g,h), r~gamma(p,q))');
                    case 'c'
                        %disp('Polynomial degree 2 regression model is using modelsubtype c (beta0~flat, beta1~flat, beta2~flat, r~T[0,]*flat)');
                    otherwise
                        error('Unsupported modelsubtype');
                end
                superargs{1}='polynomialDegree2';
                superargs{2}=setsimulationStruct.subType;
                superargs{3}='Polynomial (degree 2) regression';
                superargs{4}=strcat('Approach for modeling the relationship between a scalar dependent variable y and one or more independent ', ...
                    'variables assuming a quadratic rate in which the dependent variable changes per the independent variable values.');
                superargs{5}='DeGroot 2005, 1972';
                superargs{6}='reuben@reubenjohnston.com';
                superargs{7}=setprefix;   
                superargs{8}=setcomputemsfe;
            end
            
            obj=obj@mcmcBayes.regression(superargs{:});

            if nargin>0 %i am not sure why, but it is necessary to set these when loading
                obj.predictionsNSamples=mcmcBayes.regression.getDefaultNSamples();
            end
        end
        
        function [retObj]=transformVarsPre(obj, chainID, tempID)
        %needs to be performed on all vars
        %this function simply adjusts the hyperparameters, for all tempIDs
            retObj=obj;

            if (retObj.priorVariables(1,chainID).tfPre==mcmcBayes.t_variableTransformPre.none)
                return;
            end
                        
            datavarid=1;%only one independent/dependent variable
            if (tempID==0) && (retObj.priorVariables(1,chainID).tfPre==mcmcBayes.t_variableTransformPre.adjustForFit2NormalizedData)
                mean_x=retObj.priorData.indVarsOrigMeans{datavarid};
                std_x=retObj.priorData.indVarsOrigStd{datavarid};
                mean_y=retObj.priorData.depVarsOrigMeans{datavarid};
                std_y=retObj.priorData.depVarsOrigStd{datavarid};
                beta0=retObj.priorVariables(1,chainID).distribution.estimateMeanFromHyperparameters;
                beta1=retObj.priorVariables(2,chainID).distribution.estimateMeanFromHyperparameters;
                beta2=retObj.priorVariables(3,chainID).distribution.estimateMeanFromHyperparameters;
                r=retObj.priorVariables(4,chainID).distribution.estimateMeanFromHyperparameters;
                zbeta0=(beta0-mean_y+(beta1+2*beta2*mean_x)*mean_x-beta2*mean_x^2)/std_y;
                zbeta1=(beta1*std_x+2*beta2*std_x*mean_x)/std_y;
                zbeta2=(beta2*std_x^2)/std_y;
                zr=r*std_y^2;
                retObj.priorVariables(1,chainID).distribution.parameters(1).values=zbeta0;
                retObj.priorVariables(1,chainID).distribution.parameters(2).values=1;%standard normal precision is 1/1
                retObj.priorVariables(2,chainID).distribution.parameters(1).values=zbeta1;
                retObj.priorVariables(2,chainID).distribution.parameters(2).values=1;%standard normal precision is 1/1
                retObj.priorVariables(3,chainID).distribution.parameters(1).values=zbeta2;
                retObj.priorVariables(3,chainID).distribution.parameters(2).values=1;%standard normal precision is 1/1
                retObj.priorVariables(4,chainID).distribution=retObj.priorVariables(4,chainID).distribution.estimateHyperparameters(zr);%assuming shape is 1
            elseif (retObj.posteriorVariables(1,chainID,tempID).tfPre==mcmcBayes.t_variableTransformPre.adjustForFit2NormalizedData)
                mean_x=retObj.posteriorData.indVarsOrigMeans{datavarid};
                std_x=retObj.posteriorData.indVarsOrigStd{datavarid};
                mean_y=retObj.posteriorData.depVarsOrigMeans{datavarid};
                std_y=retObj.posteriorData.depVarsOrigStd{datavarid};
                beta0=retObj.posteriorVariables(1,chainID,tempID).distribution.estimateMeanFromHyperparameters;
                beta1=retObj.posteriorVariables(2,chainID,tempID).distribution.estimateMeanFromHyperparameters;
                beta2=retObj.posteriorVariables(3,chainID,tempID).distribution.estimateMeanFromHyperparameters;
                r=retObj.posteriorVariables(4,chainID,tempID).distribution.estimateMeanFromHyperparameters;
                zbeta0=(beta0-mean_y+(beta1+2*beta2*mean_x)*mean_x-beta2*mean_x^2)/std_y;
                zbeta1=(beta1*std_x+2*beta2*std_x*mean_x)/std_y;
                zbeta2=(beta2*std_x^2)/std_y;
                zr=r*std_y^2;
                retObj.posteriorVariables(1,chainID,tempID).distribution.parameters(1).values=zbeta0;
                retObj.posteriorVariables(1,chainID,tempID).distribution.parameters(2).values=1;%standard normal precision is 1/1
                retObj.posteriorVariables(2,chainID,tempID).distribution.parameters(1).values=zbeta1;
                retObj.posteriorVariables(2,chainID,tempID).distribution.parameters(2).values=1;%standard normal precision is 1/1
                retObj.posteriorVariables(3,chainID,tempID).distribution.parameters(1).values=zbeta2;
                retObj.posteriorVariables(3,chainID,tempID).distribution.parameters(2).values=1;%standard normal precision is 1/1
                retObj.posteriorVariables(4,chainID).distribution=retObj.posteriorVariables(4,chainID).distribution.estimateHyperparameters(zr);%assuming shape is 1
            end
        end          
        
        function obj=transformVarsPost(obj, chainID, tempID)
        %Overriding mcmcBayes.model.transformVarsPost()
        %needs to be performed on all vars, simultaneously
            if tempID==0
                numVars=size(obj.priorVariables,1);
                for varID=1:numVars
                    if obj.priorVariables(varID,chainID).tfPost==mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData
                        if varID==1
                            zbeta0=obj.priorVariables(varID,chainID).MCMC_samples;
                        elseif varID==2
                            zbeta1=obj.priorVariables(varID,chainID).MCMC_samples;
                        elseif varID==3
                            zbeta2=obj.priorVariables(varID,chainID).MCMC_samples;
                        elseif varID==4
                            zr=obj.priorVariables(varID,chainID).MCMC_samples;
                        end
                    end
                end
                if obj.priorVariables(1,chainID).tfPost==mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData%just check beta0
                    datavarid=1;%only one independent/dependent variable
                    mean_x=obj.priorData.indVarsOrigMeans{datavarid};
                    std_x=obj.priorData.indVarsOrigStd{datavarid};
                    mean_y=obj.priorData.depVarsOrigMeans{datavarid};
                    std_y=obj.priorData.depVarsOrigStd{datavarid};
                    obj.priorVariables(1,chainID).MCMC_samples=zbeta0*std_y+mean_y-zbeta1*std_y*mean_x/std_x+zbeta2*std_y*mean_x^2/std_x^2;
                    obj.priorVariables(2,chainID).MCMC_samples=zbeta1*std_y/std_x-2*zbeta2*std_y*mean_x/std_x^2;
                    obj.priorVariables(3,chainID).MCMC_samples=zbeta2*std_y/std_x^2;
                    obj.priorVariables(4,chainID).MCMC_samples=zr/std_y^2;
                end                
            else
                numVars=size(obj.posteriorVariables,1);
                for varID=1:numVars
                    if obj.posteriorVariables(varID,chainID,tempID).tfPost==mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData
                        if varID==1
                            zbeta0=obj.posteriorVariables(varID,chainID,tempID).MCMC_samples;
                        elseif varID==2
                            zbeta1=obj.posteriorVariables(varID,chainID,tempID).MCMC_samples;
                        elseif varID==3
                            zbeta2=obj.posteriorVariables(varID,chainID,tempID).MCMC_samples;
                        elseif varID==4
                            zr=obj.posteriorVariables(varID,chainID,tempID).MCMC_samples;
                        end
                    end
                end
                if obj.posteriorVariables(varID,chainID,tempID).tfPost==mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData
                    datavarid=1;%only one independent/dependent variable
                    mean_x=obj.posteriorData.indVarsOrigMeans{datavarid};
                    std_x=obj.posteriorData.indVarsOrigStd{datavarid};
                    mean_y=obj.posteriorData.depVarsOrigMeans{datavarid};
                    std_y=obj.posteriorData.depVarsOrigStd{datavarid};
                    obj.posteriorVariables(1,chainID,tempID).MCMC_samples=zbeta0*std_y+mean_y-zbeta1*std_y*mean_x/std_x+zbeta2*std_y*mean_x^2/std_x^2;
                    obj.posteriorVariables(2,chainID,tempID).MCMC_samples=zbeta1*std_y/std_x-2*zbeta2*std_y*mean_x/std_x^2;
                    obj.posteriorVariables(3,chainID,tempID).MCMC_samples=zbeta2*std_y/std_x^2;
                    obj.posteriorVariables(4,chainID,tempID).MCMC_samples=zr/std_y^2;
                end                
            end
        end
    end
end
        