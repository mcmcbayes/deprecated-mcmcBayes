function setTerminateSessions(enable)
% setTerminateSessions(enable)
%   Wrapper function that calls the simulationOSInterfaces.setTerminateSessions function.
%   Input:
%     enable - boolean that specifies whether or not to enable/disable termination of any MATLAB python engines that are running
    mcmcBayes.osInterfaces.setTerminateSessions(enable);
end             
        