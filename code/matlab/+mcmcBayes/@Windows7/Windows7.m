classdef Windows7 < mcmcBayes.osInterfaces
    %all class properties are inherited
    properties

    end
    
    methods   
        function obj = Windows7(cfgMcmcBayes)%constructor
            obj=obj@mcmcBayes.osInterfaces(mcmcBayes.t_osInterfaces.Windows7, cfgMcmcBayes);             
        end
    end
        
    methods %getter/setter functions

    end
end