classdef t_runState < int32
    enumeration
        Idle (1)
        Running (2)
        Finished (3)
        Error (4)
    end
    
    methods (Static)
        function value=fromstring(strvalue)
            if strcmp('mcmcBayes.t_runState.Idle',strvalue)==1
                value=mcmcBayes.t_runState.Idle;
            elseif strcmp('mcmcBayes.t_runState.Running',strvalue)==1
                value=mcmcBayes.t_runState.Running;
            elseif strcmp('mcmcBayes.t_runState.Finished',strvalue)==1
                value=mcmcBayes.t_runState.Finished;
            elseif strcmp('mcmcBayes.t_runState.Error',strvalue)==1
                value=mcmcBayes.t_runState.Error;
            else
                error('Unknown mcmcBayes.t_runState');
            end
        end
    end
end