function [cfgRepository] = xml2RepositoriesStruct(xmlfile)
	doc = xmlread(xmlfile);
    fprintf(1,'Parsed %s\n',xmlfile);    
    %javaaddpath(mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.javaDir));
    %call our java hacks method to insert the DOCTYPE element
    entityname='repositories';
    dtdfilename=sprintf('%s%s.dtd',mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.configDir),entityname);
    xmlStr=XmlHacks.insert(doc, entityname, dtdfilename);%call Java function  (XmlHacks must be on javaclasspath)
    %now parse the hacked xml
    doc = XmlHacks.xmlreadstring(xmlStr);
    
    doc.getDocumentElement().normalize();
    root=doc.getDocumentElement();
    rootname=root.getNodeName();
    if ~(strcmp(rootname,'repositories')==1)
        error('Root element should be repositories');
    end
    
    %parse the cfgRepository
    cfgRepositoryNode=doc.getElementsByTagName("cfgRepository").item(0);
    
    warning('Need to check numRepos against repositories.xml contents.');
    
    numRepos=str2num(cast(cfgRepositoryNode.getAttribute('numRepos'),'char'));
    nList=cfgRepositoryNode.getElementsByTagName('repository');
    for repoid=1:numRepos
        vNode=nList.item(repoid-1);
        repository(repoid,1).name=cast(vNode.getElementsByTagName('name').item(0).getTextContent(),'char');
        repository(repoid,1).path=cast(vNode.getElementsByTagName('path').item(0).getTextContent(),'char');
    end
    if exist('repository')
        cfgRepository.repository=repository;
    end
    cfgRepository.numRepos=numRepos;
end