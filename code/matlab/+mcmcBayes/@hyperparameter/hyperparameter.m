classdef hyperparameter
% numeric Is a class for numeric objects in scalar, vector, or matrix form.  It provides fields that name and describe the
% variable, some of its properties (valid range, resolution), as well as the value.
    properties
        name%name for the parameter
        description%describes the parameter
        type%'double', 'int32', 'categorical'
        dimensions%mcmcBayes.t_numericDimensions
        range=mcmcBayes.t_numericRange.null;%t_mcmcBayesNumericRange
        resolution=mcmcBayes.t_numericResolution.null%t_mcmcBayesNumericResolution
        values%values for the parameter that may be a vector and is checked against range, resolution
    end
    
    methods (Static)       

    end
    
    methods
%todo: make a simpler alternate constructor function in mcmcBayesDistribution that will determine and set range, resolution appropriate to the parameter's distribution type
        function obj = hyperparameter(setname,setdescription,setrange,setresolution,setvalues,settype,setdimensions)
            if nargin > 0 %necessary condition for creating empty object arrays
                obj.name=setname;
                obj.description=setdescription;
                obj.range=setrange;
                obj.resolution=setresolution;
                obj.values=setvalues;%depends on obj.range and obj.resolution;
                obj.type=settype;
                obj.dimensions=setdimensions;
            end
        end    
    end
    
    methods %getter/setter functions
        function obj = set.name(obj,setname)
            if ~ischar(setname)
                error('setname must be a char[]');
            else
                obj.name = setname;
            end
        end
        
        function value = get.name(obj)
            value=obj.name;
        end
        
        function obj = set.description(obj,setdescription)
            if ~ischar(setdescription)
                error('setdescription must be a char[]');
            else
                obj.description = setdescription;
            end
        end
        
        function value = get.description(obj)
            value=obj.description;
        end    
        
        function obj = set.range(obj,setrange)
            if ~isa(setrange,'mcmcBayes.t_numericRange')
                error('setrange must be type mcmcBayes.t_numericRange');
            else
                obj.range = setrange;
            end
        end
        
        function value = get.range(obj)
            value=obj.range;
        end              

        function obj = set.resolution(obj,setresolution)
            if ~isa(setresolution,'mcmcBayes.t_numericResolution')
                error('setresolution must be type mcmcBayes.t_numericResolution');
            else
                obj.resolution = setresolution;
            end
        end
        
        function value = get.resolution(obj)
            value=obj.resolution;
        end                   
                              
        function obj = set.values(obj,setvalues)
            rows=size(setvalues,1);
            cols=size(setvalues,2);
            temp=[];%empty case
            for row=1:rows
                for col=1:cols
                    checkvalues(obj, setvalues(row,col)); %checkValue will output warning/error for setvalue
                    temp(row,col) = setvalues(row,col);
                end
            end
            obj.values=temp;
        end
        
        function values = get.values(obj)
            values=obj.values;
        end             
        
        function status = checkvalues(obj, setvalues)
            status=true;
            if ~isnumeric(setvalues) && ~iscategorical(setvalues)
                error('numeric::checkvalues, setvalues must be numeric or categorical');
            else
                switch obj.resolution
                    case mcmcBayes.t_numericResolution.null
%                         warning('numeric::checkvalues, not checking setvalues resolution');
                    case mcmcBayes.t_numericResolution.discrete
                        if ~isinteger(setvalues) && ~iscategorical(setvalues)%confirm the values is an integer
                            status=false;
                            %warning('numeric::checkvalues, setvalues is not an integer or categorical but the resolution is discrete');
                        end                        
                    case mcmcBayes.t_numericResolution.continuous
                        if ~isa(setvalues,'double')%confirm the values is a double
                            status=false;
                            %warning('numeric::checkvalues, setvalues is not a double but the resolution is continuous');
                        end
                    otherwise
                        error('numeric::checkvalues, invalid type for obj.resolution');
                end
                if ~iscategorical(setvalues)
                    switch obj.range
                        case mcmcBayes.t_numericRange.null
                            %warning('numeric::checkvalues, not checking setvalues range');
                        case mcmcBayes.t_numericRange.gteq0
                            if ~(setvalues>=0)
                                status=false;
                                %warning('numeric values must be >= 0');
                            end
                        case mcmcBayes.t_numericRange.gteq0ANDltInf
                            if ~((setvalues>=0) && ~isinf(setvalues))
                                status=false;
                                %warning('numeric values must be >= 0 and < Inf');
                            end                           
                        case mcmcBayes.t_numericRange.gt0
                            if ~(setvalues>0)
                                status=false;
                                %warning('numeric values must be > 0');
                            end
                        case mcmcBayes.t_numericRange.gt0ANDlt1
                            if ~((setvalues>0) && (setvalues<1))
                                status=false;
                                %warning('numeric values must be > 0 and < 1');
                            end
                        case mcmcBayes.t_numericRange.gt0ANDltInf
                            if ~((setvalues>0) && ~isinf(setvalues))
                                status=false;
                                %warning('numeric values must be > 0 and < Inf');
                            end                        
                        case mcmcBayes.t_numericRange.eq0OReq1
                            if ~((setvalues==0) || (setvalues==1))
                                status=false;
                                %warning('numeric::checkvalues, setvalues must be = 0 or 1');
                            end
                        case mcmcBayes.t_numericRange.gtNegInfANDltInf
                            if isinf(setvalues)
                                status=false;
                                %warning('numeric::checkvalues, setvalues must not be infinite');
                            end                        
                        otherwise
                            error('numeric::checkvalues, invalid type for obj.range');
                    end
                end
            end       
        end
    end  
end