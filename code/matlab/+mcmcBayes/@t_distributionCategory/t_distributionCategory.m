classdef t_distributionCategory < int32
    enumeration
        Univariate (1)
        Multivariate (2)
    end
end