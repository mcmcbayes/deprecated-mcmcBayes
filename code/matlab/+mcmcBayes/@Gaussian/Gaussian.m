classdef Gaussian < mcmcBayes.normal
% Basic Normal model.  
% modelsubtype
% 'a' - Normal Inverse Gamma priors, unknown precision, hierarchical (i.e., mu~normal(a,(1/(b*r))^-1), r~gamma(c,d))
% 'b' - Normal Inverse Gamma priors, unknown precision (i.e., mu~normal(a,(1/b)^-1), r~gamma(c,d))
% 'c' - Flat priors (i.e., mu~flat, r~T[0,]*flat)
    
    methods(Access = public, Static)
        function test()
            display('Hello!');
        end
        
        function retData=transformDataPre(vars, data)            
            retData=data;%no special transforms for this model (everything handled by mcmcBayes.data.constructData())
        end          

        function [solverInits]=determineSolverInits(vars, data)
            %needs to return a cell array due to some models having different sized variables
            for varid=1:size(vars,1)
                solverInits(varid,1)=[{vars(varid).MCMC_initvalue}];%needs to be a row vector
            end
            %Compute solverInits (bootstrap values for mle/ls that solve for solverInits) using fsolve and fhandle
            if ~((data.dataSource==mcmcBayes.t_dataSource.None) || (data.dataSource==mcmcBayes.t_dataSource.Preset))
                datavarid=1;
                y=cast(data.depVarsValues{datavarid, 1},'double');
                solverInits=[{mean(y)}; {vars(2).distribution.estimateMeanFromHyperparameters}];
            end
        end
        
        %this is only used for determining the inits when the data source is not mcmcBayes.t_dataSource.None or mcmcBayes.t_dataSource.Preset 
        function [thetahat]=SolverMeanMLE(runPowerPosteriorMCMCSequence, subtype, vars, data)            
        % [thetahat]=mlefitModel(runPowerPosteriorMCMCSequence, independentVariables, modelVariables, dependentVariables)
        %   Wrapper function to call MATLAB's mle routine that estimates the model variable point estimates.  The structure
        %   of MATLAB's mle routine requires globals to pass the independent variables, phi, and any values for variables that
        %   are assumed as a constant (thus, are not being estimated by the mle).
        %   Input:
        %     runPowerPosteriorMCMCSequence - Boolean that specifies either running the power-posterior sequence, or not running
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     modelVariables - Distribution model variables that is an array of mcmcBayesVariable (indexed by varid, tempid) objects 
        %     dependentVariables - Prior dependent variables for the model that is a cell array of mcmcBayesNumeric objects.
        %   Output:
        %     thetahat - Cell array of model variable point estimates values computed
            options = statset('MaxIter',800, 'MaxFunEvals',1200);     
                        
            if runPowerPosteriorMCMCSequence
                phi=mcmcBayes.simulation.getDefaultPhi(2);%identify the inits for tempid=2
            else
                phi=1; 
            end
            
            mypdf=@(y,mu,r) mcmcBayes.Gaussian.computeLogLikelihood_alt(y, phi, mu, r);
            varsDefaultInits=[cast(vars(1).MCMC_initvalue,'double') cast(vars(2).MCMC_initvalue,'double')];
            varsLowerBounds=[cast(vars(1).solverMin,'double') cast(vars(2).solverMin,'double')];
            varsUpperBounds=[cast(vars(1).solverMax,'double') cast(vars(2).solverMax,'double')];
           
           	datavarid=1;
            y=cast(data(datavarid).depVarsValues{:},'double');
            [xhat]=mle(y,'logpdf',mypdf,'start',varsDefaultInits,'lowerbound',varsLowerBounds,'upperbound',varsUpperBounds,'options',options);                

            thetahat=[{xhat(1)} {xhat(2)}];
            
            clear global global_phi;
        end        
    end

    methods(Access = private, Static)       
        function [loglikelihood]=computeLogLikelihood_alt(data, phi, varargin)
        % [loglikelihood]=computeLogLikelihood_alt(data,varargin)
        %   Function that computes the model's log-likelihood of the data given the parameters.  It is used by MATLAB's mle function.
        %   It was necessary to utilize a global variable for passing phi.
        % Input:
        %   data - Model input data used in computing the log-likelihood
        %   varargin - Point estimates for the model variables used in computing the log-likelihood
        % Output:
        %   loglikelihood - Computed log-likelihood
            y=cast(data','double');
            mu=varargin{1};
            r=varargin{2};
            for i=1:numel(y)
                logpdf(i)=phi*(-(1/2)*log(2*pi)+(1/2)*log(r)-(r/2)*(y(i)-mu)^2);
            end
            loglikelihood=sum(logpdf);
%             tempstr=sprintf('normal loglikelihood(mu=%f,r=%f)=%f',mu,r,loglikelihood);
%             disp(tempstr);
        end        
    end
    
    methods 
        function obj=Gaussian(setsimulationStruct, setprefix)
        % Gaussian(setsimulationStruct, setprefix)
        %   Constructor for the Gaussian class
        %   Input:
        %     setsimulationStruct - 
        %     setprefix - 
        %   Output:
        %     obj - instantiated Gaussian object
            if nargin == 0
                superargs={};
            else
                switch setsimulationStruct.subType
                    case 'a'
                        %disp('Normal model is using modelsubtype a (mu~normal(a,b*r), r~gamma(c,d))');
                    case 'b'
                        %disp('Normal model is using modelsubtype b (mu~normal(a,b), r~gamma(c,d))');
                    case 'c'
                        %disp('Normal model is using modelsubtype c (mu~flat, r~T[0,]*flat)');
                    otherwise
                        error('Unsupported modelsubtype');
                end                
                superargs{1}='Gaussian';
                superargs{2}=setsimulationStruct.subType;
                superargs{3}='Basic Normal model';
                superargs{4}='Static failure rate';
                superargs{5}='Brooks, Motley 1980';
                superargs{6}='reuben@reubenjohnston.com';
                superargs{7}=setprefix;
            end
            
            obj=obj@mcmcBayes.normal(superargs{:});

            if nargin>0 %i am not sure why, but it is necessary to set these when loading
                obj.predictionsNSamples=mcmcBayes.normal.getDefaultNSamples();
            end
        end
        
        function [retObj]=transformVarsPre(obj, chainID, tempID)
        %needs to be performed on all vars
        %this function simply adjusts the hyperparameters, for all tempIDs, when prior is preset
            retObj=obj;
            if ~(retObj.priorData.dataSource==mcmcBayes.t_dataSource.Preset)
                return;%we only need to transform preset hyperparameters
            end
            
            datavarid=1;%only one independent/dependent variable
            if (tempID==0) && (retObj.priorVariables(1,chainID).tfPre==mcmcBayes.t_variableTransformPre.adjustForFit2NormalizedData)
                mean_y=retObj.priorData.depVarsOrigMeans{datavarid};
                std_y=retObj.priorData.depVarsOrigStd{datavarid};
                mu=retObj.priorVariables(1,chainID).distribution.estimateMeanFromHyperparameters;
                r=retObj.priorVariables(2,chainID).distribution.estimateMeanFromHyperparameters;
                zmu=(mu-mean_y)/std_y;                
                zr=r*std_y^2;
                retObj.priorVariables(1,chainID).distribution.parameters(1).values=zmu;
                retObj.priorVariables(1,chainID).distribution.parameters(2).values=1;%standard normal precision is 1/1
                retObj.priorVariables(2,chainID).distribution=retObj.priorVariables(2,chainID).distribution.estimateHyperparameters(zr);%assuming shape is 1
            elseif (retObj.posteriorVariables(1,chainID,tempID).tfPre==mcmcBayes.t_variableTransformPre.adjustForFit2NormalizedData)
                mean_y=retObj.posteriorData.depVarsOrigMeans{datavarid};
                std_y=retObj.posteriorData.depVarsOrigStd{datavarid};
                mu=retObj.posteriorVariables(1,chainID,tempID).distribution.estimateMeanFromHyperparameters;
                r=retObj.posteriorVariables(2,chainID,tempID).distribution.estimateMeanFromHyperparameters;
                zmu=(mu-mean_y)/std_y;                
                zr=r*std_y^2;
                retObj.posteriorVariables(1,chainID,tempID).distribution.parameters(1).values=zmu;
                retObj.posteriorVariables(1,chainID,tempID).distribution.parameters(2).values=1;%standard normal precision is 1/1
                retObj.posteriorVariables(2,chainID,tempID).distribution=retObj.posteriorVariables(2,chainID,tempID).distribution.estimateHyperparameters(zr);%assuming shape is 1
            end
        end        
        
        function obj=transformVarsPost(obj, chainID, tempID)
        %Overriding mcmcBayes.model.transformVarsPost()
        %needs to be performed on all vars, simultaneously
            if tempID==0
                numVars=size(obj.priorVariables,1);
                for varID=1:numVars
                    if varID==1
                        zmu=obj.priorVariables(varID,chainID).MCMC_samples;
                    elseif varID==2
                        zr=obj.priorVariables(varID,chainID).MCMC_samples;
                    end
                end
                if obj.priorVariables(1,chainID).tfPost==mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData%just check beta0
                    datavarid=1;%only one independent/dependent variable
                    mean_y=obj.priorData.depVarsOrigMeans{datavarid};
                    std_y=obj.priorData.depVarsOrigStd{datavarid};
                    obj.priorVariables(1,chainID).MCMC_samples=zmu*std_y+mean_y;
                    obj.priorVariables(2,chainID).MCMC_samples=zr/std_y^2;
                end                
            else
                numVars=size(obj.posteriorVariables,1);
                for varID=1:numVars
                    if varID==1
                        zmu=obj.posteriorVariables(varID,chainID,tempID).MCMC_samples;
                    elseif varID==2
                        zr=obj.posteriorVariables(varID,chainID,tempID).MCMC_samples;
                    end
                end
                if obj.posteriorVariables(varID,chainID,tempID).tfPost==mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData
                    datavarid=1;%only one independent/dependent variable
                    mean_y=obj.posteriorData.depVarsOrigMeans{datavarid};
                    std_y=obj.posteriorData.depVarsOrigStd{datavarid};
                    obj.posteriorVariables(1,chainID,tempID).MCMC_samples=zmu*std_y+mean_y;
                    obj.posteriorVariables(2,chainID,tempID).MCMC_samples=zr/std_y^2;                
                end                
            end
        end        
    end
end
        