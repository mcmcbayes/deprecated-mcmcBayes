function inspectLog()
% inspectLog()
%   Prompts the user with a log file selection dialog.
%   Input:
    filterspec=sprintf('*log.txt');
    mcmcBayes.inspectLogFiltered(filterspec);
end