classdef Gompertz < mcmcBayes.growthCurve  
% Gompertz growth curve model, Gompertz, 1825.  Growth rate that increases somewhat exponentially for some time, then when the population approaches 
% saturation (capacity), the rate transitions to somewhat exponentially decreasing and approaches zero as time approaches infinity
% modelsubtype
% 'a' - Normal Inverse Gamma priors, unknown precision, hierarchical (i.e., beta0~normal(a,(1/(b*r))^-1), beta1~normal(c,(1/(d*r))^-1), beta2~normal(g,(1/(h*r))^-1), r~gamma(p,q))
% 'b' - Normal Inverse Gamma priors, unknown precision (i.e., beta0~normal(a,(1/b)^-1), beta1~normal(c,(1/d)^-1), beta2~normal(g,(1/h)^-1), r~gamma(p,q))
% 'c' - Flat priors (i.e., beta0~flat, beta1~flat, beta2~flat, r~T[0,]*flat)
    methods(Access = public, Static)
        function test()
            display('Hello!');
        end

        function retData=transformDataPre(vars, data)            
            retData=data;%no special transforms for this model (everything handled by mcmcBayes.data.constructData())
        end            
        
        function [solverInits]=determineSolverInits(vars, data)
            %needs to return a cell array due to some models having different sized variables
            for varid=1:size(vars,1)
                solverInits(varid,1)=[{vars(varid).MCMC_initvalue}];%needs to be a row vector
            end
            %Compute solverInits (bootstrap values for mle/ls that solve for solverInits) using fsolve and fhandle1
            if ~((data.dataSource==mcmcBayes.t_dataSource.None) || (data.dataSource==mcmcBayes.t_dataSource.Preset))
                indVarsValues=data.indVarsValues;
                depVarsValues=data.depVarsValues;
                %Solve for beta0,beta1,beta2 given r and N(t=end)=beta1/(1+exp(beta0-beta2*t))
                datavarid=1;
                t=indVarsValues{datavarid, 1};
                N=cast(depVarsValues{datavarid, 1},'double');
                fhandle=@(a) a(2)*exp(-exp(a(1)-a(3)*t(end)))-N(end);%N(t=end) equals dependentVariables(end)
                tsolverInits=[fsolve(fhandle, [solverInits{1:3}])];
                solverInits=[{tsolverInits(1)}; {tsolverInits(2)}; {tsolverInits(3)}; {vars(4).distribution.estimateMeanFromHyperparameters}];
            end
        end
        
        %this is only used for determining the inits when the data source is not mcmcBayes.t_dataSource.None or mcmcBayes.t_dataSource.Preset 
        function [thetahat]=SolverMeanMLE(runPowerPosteriorMCMCSequence, subtype, vars, data)
        % [thetahat]=mlefitModel(runPowerPosteriorMCMCSequence, independentVariables, modelVariables, dependentVariables)
        %   Wrapper function to call MATLAB's mle routine that estimates the model variable point estimates.  The structure
        %   of MATLAB's mle routine requires globals to pass the independent variables, phi, and any values for variables that
        %   are assumed as a constant (thus, are not being estimated by the mle).
        %   Input:
        %     runPowerPosteriorMCMCSequence - Boolean that specifies either running the power-posterior sequence, or not running
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     modelVariables - Distribution model variables that is an array of mcmcBayesVariable (indexed by varid, tempid) objects
        %     dependentVariables - Prior dependent variables for the model that is a cell array of mcmcBayesNumeric objects.
        %   Output:
        %     thetahat - Cell array of model variable point estimates values computed (cell array due to some models having different sized variables)
            options = statset('MaxIter',1600, 'MaxFunEvals',2400);
 
            if runPowerPosteriorMCMCSequence
                phi=mcmcBayes.getDefaultPhi(2);%identify the inits for tempid=2
            else
                phi=1; 
            end

           	datavarid=1;
            t=cast(data(datavarid).indVarsValues{:},'double');            
            
            mypdf=@(N,beta0,beta1,beta2,r) mcmcBayes.Gompertz.computeLogLikelihood_alt(N, phi, t, beta0, beta1, beta2, r);
            varsDefaultInits=[cast(vars(1).MCMC_initvalue,'double') cast(vars(2).MCMC_initvalue,'double') ...
                cast(vars(3).MCMC_initvalue,'double') cast(vars(4).MCMC_initvalue,'double')];
            varsLowerBounds=[cast(vars(1).solverMin,'double') cast(vars(2).solverMin,'double')  ...
                cast(vars(3).solverMin,'double') cast(vars(4).solverMin,'double')];
            varsUpperBounds=[cast(vars(1).solverMax,'double') cast(vars(2).solverMax,'double') ...
                cast(vars(3).solverMax,'double') cast(vars(4).solverMax,'double')];
            
            datavarid=1;
            t=cast(data(datavarid).depVarsValues{:},'double');
            [xhat]=mle(t,'logpdf',mypdf,'start',varsDefaultInits,'lowerbound',varsLowerBounds,'upperbound',varsUpperBounds,'options',options);
            
            thetahat=[{xhat(1)} {xhat(2)} {xhat(3)} {xhat(4)}];
        end
        
        %this is only used for determining the inits when the data source is not mcmcBayes.t_dataSource.None or mcmcBayes.t_dataSource.Preset 
        function [thetahat]=SolverMeanLS(runPowerPosteriorMCMCSequence, subtype, vars, data)
        % [thetahat]=lsfitModel(independentVariables, modelVariables, dependentVariables)
        %   Wrapper function to call MATLAB's lsqcurvefit routine that estimates the model variable point estimates.
        %   Input:
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     modelVariables - Distribution model variables that is an array of mcmcBayesVariable (indexed by varid, tempid) objects
        %     dependentVariables - Prior dependent variables for the model that is a cell array of mcmcBayesNumeric objects.
        %   Output:
        %     thetahat - Cell array of model variable point estimates values computed (cell array due to some models having different sized variables)
           	datavarid=1;
            t=cast(data(datavarid).indVarsValues{:},'double');
            
            %fhandle=@(x,t) x(2).*exp(-exp(x(1)-x(3).*t));
            fhandle=sprintf('beta1*exp(-exp(beta0-beta2.*t))');
            coef={'beta0','beta1','beta2'};
            
            varsDefaultInits=[cast(vars(1).MCMC_initvalue,'double') cast(vars(2).MCMC_initvalue,'double') cast(vars(3).MCMC_initvalue,'double')];
            varsLowerBounds=[cast(vars(1).solverMin,'double') cast(vars(2).solverMin,'double') cast(vars(3).solverMin,'double')];
            varsUpperBounds=[cast(vars(1).solverMax,'double') cast(vars(2).solverMax,'double') cast(vars(3).solverMax,'double')];
            
            options = optimoptions('lsqcurvefit','Display','none','MaxFunEvals',3000,'MaxIter',1000);
            
            datavarid=1;
            N=cast(data(datavarid).depVarsValues{:},'double');
            myfittype = fittype(fhandle, 'dependent', {'N'}, 'independent', {'t'}, 'coefficients', coef);%customnonlinear fittype
            Nhat = fit(t,N,myfittype,'StartPoint',varsDefaultInits,'Lower',varsLowerBounds,'Upper',varsUpperBounds);
%             [Nhat,resnorm,residual,exitflag,output]=lsqcurvefit(fhandle,varsDefaultInits,t,data,varsLowerBounds,varsUpperBounds,options);
%             if exitflag~=1%converged
%                 disp(output.message)
%             end
            
            thetahat=[{Nhat.beta0} {Nhat.beta1} {Nhat.beta2} {vars(4).distribution.estimateMeanFromHyperparameters}];
            %thetahat=[{Nhat(1)} {Nhat(2)} {Nhat(3)} {vars(4).distribution.estimateMeanFromHyperparameters}];
        end
    end
    
    methods(Access = private, Static)               
        function [loglikelihood]=computeLogLikelihood_alt(data,phi,t,varargin)
        % [loglikelihood]=computeLogLikelihood_alt(data,varargin)
        %   Function that computes the model's log-likelihood of the data given the parameters.  It is used by MATLAB's mle function.
        %   It was necessary to utilize a global variable for passing the independent variables and phi.
        % Input:
        %   data - Model input data used in computing the log-likelihood
        %   varargin - Point estimates for the model variables used in computing the log-likelihood
        % Output:
        %   loglikelihood - Computed log-likelihood
            n=cast(data','double');
            %global_t must be passed in as a global due to the calling constraints imposed by mle function
            beta0=varargin{1};
            beta1=varargin{2};
            beta2=varargin{3};
            r=varargin{4};
            for i=1:numel(n)
                m(i)=beta1.*exp(-exp(beta0-beta2*t(i)));                
                logpdf(i)=phi*((1/2)*log(r/(2*pi))-(r/2)*(n(i)-m(i))^2);
            end
            loglikelihood=sum(logpdf);
%             tempstr=sprintf('gompertz growth loglikelihood(beta0=%f,beta1=%f,beta2=%f,r=%f)=%f',beta0,beta1,beta2,r,loglikelihood);
%             disp(tempstr);
        end                     
    end
    
    methods 
        function obj=Gompertz(setsimulationStruct, setprefix, setcomputemsfe)
        % Gompertz(setsimulationStruct, setprefix, setcomputemsfe)
        %   Constructor for the Gompertz class
        %   Input:
        %     setsimulationStruct - 
        %     setprefix - 
        %   Output:
        %     obj - instantiated Gompertz object
            if nargin == 0
                superargs={};
            else
                switch setsimulationStruct.subType
                    case 'a'
                        %disp('Gompertz growth curve model is using modelsubtype a (beta0~truncatednormal(a,b*r), beta1~truncatednormal(c,d*r), beta2~truncatednormal(g,h*r), r~gamma(p,q))');
                    case 'b'
                        %disp('Gompertz growth curve model is using modelsubtype b (beta0~truncatednormal(a,b), beta1~truncatednormal(c,d), beta2~truncatednormal(g,h), r~gamma(p,q))');
                    case 'c'
                        %disp('Gompertz growth curve model is using modelsubtype e (beta0~flat, beta1~flat, beta2~flat, r~T[0,]*flat)');
                    otherwise
                        error('Unsupported modelsubtype');
                end
                superargs{1}='Gompertz';
                superargs{2}=setsimulationStruct.subType;
                superargs{3}='Gompertz growth';
                superargs{4}=strcat('Growth rate that increases somewhat exponentially for some time, then when the population approaches saturation (capacity), the rate transitions to somewhat exponentially decreasing and approaches zero as time approaches infinity');
                superargs{5}='Gompertz, 1825';
                superargs{6}='reuben@reubenjohnston.com';
                superargs{7}=setprefix;        
                superargs{8}=setcomputemsfe;
            end
            
            obj=obj@mcmcBayes.growthCurve(superargs{:});

            if nargin>0 %i am not sure why, but it is necessary to set these when loading
                obj.predictionsNSamples=mcmcBayes.growthCurve.getDefaultNSamples();
            end
        end   
    end
end
        