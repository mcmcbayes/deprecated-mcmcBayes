function displayFigureFiltered(filterspec)
% displayFigureFiltered(filterspec)
%   Prompts the user with a figure file selection dialog that is filtered per filterspec.
%   Input:
%     filterspec - Specifies the filter for figure files
    figuredir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.figuresDir);
    filterspec=sprintf('%s%s',figuredir,filterspec);
    [filename, pathname]=uigetfile(filterspec,'Open simulation figure file','MultiSelect','on');
    if isa(filename,'cell')
        for index=1:size(filename,2)
            filestr=mcmcBayes.osInterfaces.convPathStr(strcat(pathname,filename{index}));
            openfig(filestr);
         end
    else
        if ~filename==0
            filestr=mcmcBayes.osInterfaces.convPathStr(strcat(pathname,filename));
            openfig(filestr,'visible');
        else
            disp('No file selected.');
        end                       
    end
end