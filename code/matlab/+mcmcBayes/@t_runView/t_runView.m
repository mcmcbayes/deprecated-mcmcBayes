classdef t_runView < int32
    enumeration
        Background (1)
        Foreground (2)
    end
    
    methods (Static)
        function intvalue=toint(enumobj)
            switch enumobj          
                case {mcmcBayes.t_runView.Background, mcmcBayes.t_runView.Foreground}
                    intvalue=int32(enumobj);
                otherwise
                    error('unsupported mcmcBayes.t_runView type');
            end                
        end        
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_runView.Background
                    strvalue='mcmcBayes.t_runView.Background';
                case mcmcBayes.t_runView.Foreground
                    strvalue='mcmcBayes.t_runView.Foreground';  
                otherwise
                    error('unsupported mcmcBayes.t_runView type');
            end
        end

        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_runView.Background)
                    enumobj=mcmcBayes.t_runView.Background;
                case int32(mcmcBayes.t_runView.Foreground)
                    enumobj=mcmcBayes.t_runView.Foreground;                 
                otherwise
                    error('unsupported mcmcBayes.t_runView type');
            end
        end
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_runView.Background')==1
                enumobj=mcmcBayes.t_runView.Background;
            elseif strcmp(strval,'mcmcBayes.t_runView.Foreground')==1
                enumobj=mcmcBayes.t_runView.Foreground;
            else
                error('unsupported mcmcBayes.t_runView type');
            end
        end              
    end
end