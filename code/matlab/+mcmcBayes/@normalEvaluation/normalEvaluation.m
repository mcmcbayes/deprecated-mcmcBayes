classdef normalEvaluation < mcmcBayes.sequence
% normalEvaluation is a class for demonstrating mcmcBayes with a simple Gaussian model.  
% * GaussianFlatSimulated - (subtype 'c') uses a flat prior with simulated data for the posterior data
% * GaussianPresetSimulated - (subtype 'b') uses a preset prior with simulated data for the posterior data
% * GaussianEmpirical1BEmpirical1A - (subtype 'b') replicates "A First Course in Multivariate Statistics", 
%   Flury 2013 (who cites Grogan and Wirth, 1981 as the original source) in a study of the wing span of two newly discovered species of midges 
%   (small gnatlike insects).  The posterior dataset uses the af (harmless) midge data and the empirical prior dataset uses the apf (deadly) 
%   midge data.
% * GaussianEmpirical2BEmpirical2A - (subtype 'b') replicates "A First Course in Multivariate Statistics", 
%   Flury 2013 (who cites Grogan and Wirth, 1981 as the original source) in a study of the antenna length of two newly discovered species of 
%   midges (small gnatlike insects).  The posterior dataset uses the af (harmless) midge data and the empirical prior dataset uses the apf 
%   (deadly) midge data.
% * GaussianPresetEmpirical1A - (subtype 'b') replicates "A First Course in Multivariate Statistics", 
%   Flury 2013 (who cites Grogan and Wirth, 1981 as the original source) in a study of the wing span of two newly discovered species of midges 
%   (small gnatlike insects).  The posterior dataset uses the af (harmless) midge data and a preset prior from Hoff 2009, "A First Course in 
%   Bayesian Statistical Methods".  Analytical estimates of the posterior for gaussianPresetEmpiricalStudy1 is in Hoff 2009, and mu=1.814, 
%   r=1/0.015324=65.25711.

    properties
        %None
    end
    
    % Gaussian-a (JAGS,BUGS,Stan) - Normal Inverse Gamma priors, unknown precision, hierarchical (i.e., mu~normal(a,(1/(b*r))^-1), r~gamma(c,d))
    % Gaussian-b (JAGS,BUGS,Stan) - Normal Inverse Gamma priors, unknown precision (i.e., mu~normal(a,(1/b)^-1), r~gamma(c,d))
    % Gaussian-c (BUGS,Stan) - Flat priors (i.e., mu~flat, r~T[0,]*flat)
    
    methods (Static)                               
        function [simuuids]=getUuids()
            simuuids=[{'GaussianFlatSimulated'}, ...          %Gaussian-c 
                   {'GaussianPresetSimulated'}, ...           %Gaussian-b             
                   {'GaussianEmpirical1BEmpirical1A'}, ...    %Gaussian-b
                   {'GaussianEmpirical2BEmpirical2A'}, ...    %Gaussian-b
                   {'GaussianPresetEmpirical1A'}];            %Gaussian-a
        end      
        
        function [retSims]=runUnitTests()
            norunpp=false;
            runpp=true;
            dbgmcmc=true;
            nodbgmcmc=false;
            seqType='normalEvaluation';
            seqUuid='normalEvaluation-runUnitTests';
            simuuids=mcmcBayes.normalEvaluation.getUuids();
            %simOverrides[simuuid, {runpp}, {dbgmcmc}, {[samplerType]}]
            simOverrides=[simuuids(1), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %GaussianFlatSimulated
                         simuuids(2), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ...  %GaussianPresetSimulated
                         simuuids(3), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ...  %GaussianEmpirical1BEmpirical1A
                         simuuids(4), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ...  %GaussianEmpirical2BEmpirical2A
                         simuuids(5), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ...  %GaussianPresetEmpirical1A
                         simuuids(1), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ...  %GaussianFlatSimulated
                         simuuids(2), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ...  %GaussianPresetSimulated
                         simuuids(3), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ...  %GaussianEmpirical1BEmpirical1A
                         simuuids(4), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ...  %GaussianEmpirical2BEmpirical2A
                         simuuids(5), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ...  %GaussianPresetEmpirical1A
                         simuuids(1), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ...  %GaussianFlatSimulated
                         simuuids(2), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ...  %GaussianPresetSimulated
                         simuuids(3), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ...  %GaussianEmpirical1BEmpirical1A
                         simuuids(4), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ...  %GaussianEmpirical2BEmpirical2A
                         simuuids(5), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}];     %GaussianPresetEmpirical1A
            for i=1:size(simOverrides,1)
                retSims(i,1)=mcmcBayes.sequence.overrideDefaultsAndRunSingleSimulation(seqType, seqUuid, simOverrides{i,1:4});                      
            end
        end
        
        function getData(type, data, varargin)
        % getData(obj, type, data, varargin)
        %   This function generates a simulated dataset using the parameter point estimates from varargin.

            if (data.dataSource~=mcmcBayes.t_dataSource.Simulated) && (data.dataSource~=mcmcBayes.t_dataSource.PostulatedConFR) && ...
               (data.dataSource~=mcmcBayes.t_dataSource.PostulatedExpDFR) && (data.dataSource~=mcmcBayes.t_dataSource.PostulatedExpIFR) && ...
               (data.dataSource~=mcmcBayes.t_dataSource.PostulatedIthenDFR) && (data.dataSource~=mcmcBayes.t_dataSource.PostulatedLinDFR) && ...
               (data.dataSource~=mcmcBayes.t_dataSource.PostulatedLinIFR)
                error('Requires mcmcBayes.t_dataSource=Simulated or Postulated for creating data.');
            else
                if strcmp(type,'Gaussian')
                    predictionsNSamples=1;
                else
                    error('Unsupported normalEvaluation sequence type=%s', type);
                end
            end
            
            numdatavars=1;%N
            numgenerateddatasets=size(data.depVarsValues,2);
            dependentVars=cell(numdatavars,numgenerateddatasets);
            
            %set the parameter values for the simulated data
            if strcmp(type,'Gaussian')
                mu=varargin{1};%'mu','E[y]','double'
                r=varargin{2};%'r','1/VAR[y]','double'
                %mean should then be 50
                for tdatasetid=1:numgenerateddatasets
                    dependentVars(numdatavars,tdatasetid)=mcmcBayes.model.generateTestData(type, predictionsNSamples, data, mu, r);%varargin=mu, r
                end
            else
                error('normalEvaluation sequence type is not Gaussian.');
            end
            
            disp(conversionUtilities.matrix_tostring(dependentVars{1,1}));
            
            plot(dependentVars{1,1},zeros(numel(dependentVars{1,1}),1),'k*');           
        end        
        
        function [mun,betan,marginalLikelihood]=normalGammaAnalyticSolver(mu0,kappa0,alpha0,beta0,data)
            %> [mun,betan,marginalLikelihood]=mcmcBayes.normalEvaluation.normalGammaAnalyticSolver(1.9,1,2*.5,2*.005,[1.64; 1.7; 1.72; 1.74; 1.82; 1.82; 1.82; 1.9; 2.08])
            %solve for mun,kappan,alphan,betan,pr(data) for the NormalGamma model
            %data should be a row vector
            n=size(data,1);
            xbar=mean(data);
            xvar=var(data);
            mun=(kappa0*mu0+n*xbar)/(kappa0+n);
            kappan=kappa0+n;
            alphan=alpha0+n/2;
            temp=0;
            for i=1:n
                temp=temp+1/2*(data(i)-xbar)^2;
            end
            betan=beta0+temp+kappa0*n*(xbar-mu0)^2/(2*(kappa0+n));            
            marginalLikelihood=gamma(alphan)*beta0^alpha0/(gamma(alpha0)*betan^alphan)*(kappa0/kappan)^1/2*(2*pi)^(-n/2);
        end
    end
    
    methods
        function [obj]=normalEvaluation(cfgMcmcBayes,repositories,sequenceUuid)           
        % obj=normalEvaluation(cfgMcmcBayes,repositories,sequenceUuid)
        %   The constructor for normalEvaluation.
        % Output:
        %   obj Constructed normalEvaluation object
            if nargin > 0
                superargs{1}='normalEvaluation';
                superargs{2}=cfgMcmcBayes;
                superargs{3}=repositories;
                superargs{4}=sequenceUuid;
            elseif nargin == 0
                superargs={};
            end
            obj=obj@mcmcBayes.sequence(superargs{:});            
        end
    end
    
    methods (Static, Access=private)     
   
    end    
    
    methods %getter/setter functions

    end
end