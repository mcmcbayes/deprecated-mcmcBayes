classdef VDMCaseStudy1 < mcmcBayes.sequence
% VDMCaseStudy1 is a class for demonstrating mcmcBayes with the vulnerability discovery phenomenon.  The prior datasets were generated
% from an expert-judgment workshop (see Johnston 2017) and the ten-models may be evaluated using this paired with multiple postulated datasets.  It
% also supports simulating data and running the analysis on this set.
    properties
        gamma % The expert-judgment workshop assumed a quality level (i.e., total vulnerability counts) in the elicitation scenarios
        productArray=[{'ffv'},{'ffv'},{'iev'},{'iev'},{'asv'}];%indexed on dataset index (Empirical1:5)
        productVersionArray=[19.0,3.0,7.0,6.0,1.0];%indexed on datasetid, ffv 19.0 has 138, ffv 3.0 has 50, gcv 20.0 has 20, iev 6.0 has 7, asv 1.0 has 3
    end
        
    methods (Static)
        function [uuids]=getUuids(seqUuid)
            if strcmp(seqUuid,'ExpertJudgment1Empirical1')==1            
                uuids=[{'VDMCaseStudy1LinearExpertJudgment1aEmpirical1a'}; ...
                    {'VDMCaseStudy1PolynomialDegree2ExpertJudgment1aEmpirical1a'}; ...
                    {'VDMCaseStudy1VerhulstExpertJudgment1aEmpirical1a'}; ...
                    {'VDMCaseStudy1GompertzExpertJudgment1aEmpirical1a'}; ...
                    {'VDMCaseStudy1BrooksMotleyExpertJudgment1bEmpirical1b'}; ...
                    {'VDMCaseStudy1GoelOkumotoExpertJudgment1bEmpirical1b'}; ...
                    {'VDMCaseStudy1GoelExpertJudgment1bEmpirical1b'}; ...
                    {'VDMCaseStudy1MusaOkumotoExpertJudgment1bEmpirical1b'}; ...
                    {'VDMCaseStudy1YamadaOhbaOsakiExpertJudgment1bEmpirical1b'}
                    ];
            elseif strcmp(seqUuid,'ExpertJudgment2Empirical2')==1   
                uuids=[{'VDMCaseStudy1LinearExpertJudgment2aEmpirical2a'}; ... 
                    {'VDMCaseStudy1PolynomialDegree2ExpertJudgment2aEmpirical2a'}; ... 
                    {'VDMCaseStudy1VerhulstExpertJudgment2aEmpirical2a'}; ... 
                    {'VDMCaseStudy1GompertzExpertJudgment2aEmpirical2a'}; ... 
                    {'VDMCaseStudy1BrooksMotleyExpertJudgment2bEmpirical2b'}; ... 
                    {'VDMCaseStudy1GoelOkumotoExpertJudgment2bEmpirical2b'}; ... 
                    {'VDMCaseStudy1GoelExpertJudgment2bEmpirical2b'}; ... 
                    {'VDMCaseStudy1MusaOkumotoExpertJudgment2bEmpirical2b'}; ... 
                    {'VDMCaseStudy1YamadaOhbaOsakiExpertJudgment2bEmpirical2b'}; ... 
                    {'VDMCaseStudy1KuoGhoshExpertJudgment2bEmpirical2b'} 
                    ];
            elseif strcmp(seqUuid,'ExpertJudgment3Empirical3')==1   
                uuids=[{'VDMCaseStudy1LinearExpertJudgment3aEmpirical3a'}; ... 
                    {'VDMCaseStudy1PolynomialDegree2ExpertJudgment3aEmpirical3a'}; ... 
                    {'VDMCaseStudy1VerhulstExpertJudgment3aEmpirical3a'}; ... 
                    {'VDMCaseStudy1GompertzExpertJudgment3aEmpirical3a'}; ... 
                    {'VDMCaseStudy1BrooksMotleyExpertJudgment3bEmpirical3b'}; ... 
                    {'VDMCaseStudy1GoelOkumotoExpertJudgment3bEmpirical3b'}; ... 
                    {'VDMCaseStudy1GoelExpertJudgment3bEmpirical3b'}; ... 
                    {'VDMCaseStudy1MusaOkumotoExpertJudgment3bEmpirical3b'}; ... 
                    {'VDMCaseStudy1YamadaOhbaOsakiExpertJudgment3bEmpirical3b'}; ... 
                    {'VDMCaseStudy1KuoGhoshExpertJudgment3bEmpirical3b'}  
                    ];
            elseif strcmp(seqUuid,'ExpertJudgment4Empirical4')==1
                uuids=[{'VDMCaseStudy1LinearExpertJudgment4aEmpirical4a'}; ... 
                    {'VDMCaseStudy1PolynomialDegree2ExpertJudgment4aEmpirical4a'}; ... 
                    {'VDMCaseStudy1VerhulstExpertJudgment4aEmpirical4a'}; ... 
                    {'VDMCaseStudy1GompertzExpertJudgment4aEmpirical4a'}; ... 
                    {'VDMCaseStudy1BrooksMotleyExpertJudgment4bEmpirical4b'}; ... 
                    {'VDMCaseStudy1GoelOkumotoExpertJudgment4bEmpirical4b'}; ... 
                    {'VDMCaseStudy1GoelExpertJudgment4bEmpirical4b'}; ... 
                    {'VDMCaseStudy1MusaOkumotoExpertJudgment4bEmpirical4b'}; ... 
                    {'VDMCaseStudy1YamadaOhbaOsakiExpertJudgment4bEmpirical4b'} 
                    ];
            elseif strcmp(seqUuid,'ExpertJudgment5Empirical5')==1
                uuids=[{'VDMCaseStudy1LinearExpertJudgment5aEmpirical5a'}; ... 
                    {'VDMCaseStudy1PolynomialDegree2ExpertJudgment5aEmpirical5a'}; ... 
                    {'VDMCaseStudy1VerhulstExpertJudgment5aEmpirical5a'}; ... 
                    {'VDMCaseStudy1GompertzExpertJudgment5aEmpirical5a'}; ... 
                    {'VDMCaseStudy1BrooksMotleyExpertJudgment5bEmpirical5b'}; ... 
                    {'VDMCaseStudy1GoelOkumotoExpertJudgment5bEmpirical5b'}; ... 
                    {'VDMCaseStudy1GoelExpertJudgment5bEmpirical5b'}; ... 
                    {'VDMCaseStudy1MusaOkumotoExpertJudgment5bEmpirical5b'}; ... 
                    {'VDMCaseStudy1YamadaOhbaOsakiExpertJudgment5bEmpirical5b'}
                    ];
            elseif strcmp(seqUuid,'LinearScaling')==1
                uuids=[{'VDMCaseStudy1LinearScalingExpertJudgment'}];
            else
                error('seqUuid not recognized');                
            end
        end        
        
        function [retSequence]=runCaseStudy(seqUuid, forkIt)
        % [retSequence]=runCaseStudy(seqUuid, forkIt)
        %   Runs the baseline case study example specified by seqUuid.
            seqType='VDMCaseStudy1';
            retSequence=mcmcBayes.sequence.getDefaultSequence(seqType, seqUuid);
            sims=mcmcBayes.VDMCaseStudy1.getUuids(seqUuid);
            if forkIt==true
                mcmcBayes.simulation.forkThem(mcmcBayes.t_runView.Foreground, seqType, seqUuid, sims);
            else
                for i=1:size(sims,1)
                    retSequence.simulations(i)=mcmcBayes.sequence.useDefaultsAndRunSingleSimulation(seqType, seqUuid, sims{i});                   
                end
            end        
        end    
        
        function getData(name, data, varargin)                                
        % getData(name, data, varargin)
        %   This function generates a simulated dataset using the parameter point estimates from varargin.
            warning('Does nothing, use the evaluation sequences.');
        end        

%todo: update        
        function displayModelPredictionComparison(priordatatype, datatype, datasetid, figuretype)
        % displayModelPredictionComparison(priordatatype, datatype, datasetid, figuretype)
        %   Used to plot the predictions from all the models in one figure (uses subplot)
        %   Input:
        %     priordatatype - t_mcmcBayesData type that specifies priorDependentVariables (e.g., CCMPerformanceWeighting, EqualWeighting, Individual, and Simulated)
        %     datatype - t_mcmcBayesData type that specifies dependentVariables (e.g., ConFR, LinIFR, ExpIFR, LinDFR, ExpDFR, and Simulated)
        %     datasetid - Specifies the dataset identifier to display the predictions for
        %     figuretype - Specifies the t_mcmcBayesFigures to filter the files on (should be MODEL_PREDICTIONS or MODEL_PREDICTIONS_COMBO)
            priordatatypeid=t_mcmcBayesData.toint(priordatatype);
            datatypeid=t_mcmcBayesData.toint(datatype);
            
            %https://www.mathworks.com/matlabcentral/answers/100687-how-do-i-extract-data-from-matlab-figures
            
            figuredir=mcmcBayesOSInterfaces.getPath(t_mcmcBayesPaths.FIGURES_DIR);
            if ((figuretype~=t_mcmcBayesFigures.MODEL_PREDICTIONS) && (figuretype~=t_mcmcBayesFigures.MODEL_PREDICTIONS_COMBO))
                error('%s is an unsupported figuretype for displayModelPredictionComparison.', t_mcmcBayesFigures.getfilenamestring(figuretype))
            end
            filterspec=sprintf('%smodeltypeid*priordatatypeid%ddatatypeid%ddatasetid%d*samplertypeid*_%s.fig', figuredir, priordatatypeid, datatypeid, ...
                datasetid, t_mcmcBayesFigures.getfilenamestring(figuretype));%PredictionsSet
            [filename, pathname]=uigetfile(filterspec,'Open mcmcBayes figure file','MultiSelect','on');
            if size(filename,2)<10
                warning('You did not select ten models');
            end
            if isa(filename,'cell')
                hfig10=figure;
                ax=hfig10.CurrentAxes;
                for file=1:size(filename,2)
                    filestr=mcmcBayesOSInterfaces.convPathStr(strcat(pathname,filename{file}));
                    A=sscanf(filename{file},'modeltypeid%dpriordatatypeid%ddatatypeid%ddatasetid%dtempid%dsamplertypeid%d*');
                    modeltypeid(file)=A(1);
                    set(0,'CurrentFigure',hfig10);
                    if modeltypeid(file)==9 || modeltypeid(file)==10
                        index=modeltypeid(file)+1;
                        subax(index)=subplot(3,4,index);
                    else
                        index=modeltypeid(file);
                        subax(index)=subplot(3,4,index);
                    end
                    hfig=openfig(filestr,'invisible');
                    axtemp=hfig.CurrentAxes;
                    thandles=get(axtemp,'children');%get handle to all the children in the figure
                    copyobj(thandles,subax(index));
                    close(hfig);
                    subax(index).XTick = [0 10 20 30 40 50];
                    xlim(subax(index),[0 50]);
                    xlabel(subax(index),'time');
                    ylabel(subax(index),'E[N(t)]');
                    dataObjs = get(subax(index),'Children');
                    switch modeltypeid(file) %these commands need the copied axes already present
                        case 1
                            title('Linear regression');
                            if (figuretype == t_mcmcBayesFigures.MODEL_PREDICTIONS_COMBO)
                                hlegend=legend('expert data','prior-based low predictions','prior-based mean predictions','prior-based high predictions', ...
                                    'postulated data','posterior-based low predictions','posterior-based mean predictions','posterior-based high predictions');
                            elseif (figuretype ==t_mcmcBayesFigures.MODEL_PREDICTIONS)
                                hlegend=legend('expert data','low predictions','mean predictions','high predictions','');
                            else
                                error('Unsupported t_mcmcBayesFigures type.');
                            end
                               
                            set(hlegend,'EdgeColor',[1 1 1]); 
                        case 2
                            title('Polynomial (degree-2) regression');
                        case 3
                            title('Brooks-Motley HPP');
                        case 4
                            title('Goel-Okumoto NHPP');
                        case 5
                            title('Goel''s generalized Goel-Okumoto NHPP');
                        case 6
                            title('Musa-Okumoto NHPP');
                            subax(index).XTick = [0 4000 8000 12000 16000 20000];
                            xlim(subax(index),[0 20000]);
                            xlabel(subax(index),'assessment time');
                            ylabel(subax(index),'E[N(tau)]');
                        case 7
                            title('YamadaOhbaOsaki (s-shaped) NHPP');
                        case 8
                            title('Kuo-Ghosh NHPP');             
                        case 9
                            title('Verhulst growth curve');
                        case 10
                            title('Gompertz growth curve');                                
                    end           
                end
            else
                if ~filename==0
                    filestr=mcmcBayesOSInterfaces.convPathStr(strcat(pathname,filename));
                    openfig(filestr,'visible');
                else
                    disp('No file selected.');
                    return
                end                       
            end
        end                
        
        function plotCumulativeVulnerabilities()
            filename='C:\sandbox\datasets\webBrowserCumVulns.xlsx';
            sheet='ffv';
            xlTable = readtable(filename, 'Sheet', sheet);
            versions(1)={xlTable.version};
            cumVuln(1)={xlTable.cumVuln};
            sheet='gcv';
            xlTable = readtable(filename, 'Sheet', sheet);
            versions(2)={xlTable.version};
            cumVuln(2)={xlTable.cumVuln};
            sheet='iev';
            xlTable = readtable(filename, 'Sheet', sheet);
            versions(3)={xlTable.version};
            cumVuln(3)={xlTable.cumVuln};
            sheet='asv';
            xlTable = readtable(filename, 'Sheet', sheet);
            versions(4)={xlTable.version};
            cumVuln(4)={xlTable.cumVuln};
            fig;
            h(1)=subplot(4,1,1);
            h(2)=subplot(4,1,2);
            h(3)=subplot(4,1,3);
            h(4)=subplot(4,1,4);
            for j=1:4
                plot(h(j),0:size(versions{j},1)-1,cumVuln{j});
                clear tempstrarray
                for i=1:size(versions{j},1)
                    tempstrarray(i)={num2str(versions{j}(i))};
                end
                axes(h(j));
                set(gca,'xtick',[0:size(versions{j},1)-1]);
                set(gca,'xticklabel',[tempstrarray]);
                set(gca,'xlim',[0 size(versions{j},1)-1]);
            end            
        end
                        
        function [x, y]=getEmpiricalData(version, name)
        % [x, y]=getEmpiricalData(version, name)
        %   Extracts cumulative discoveries from the University of Trento empirical datasets.  The parameter version is a double and name includes
        %   'ffv','gcv','asv','iev'.  e.g., [x,y]=mcmcBayes.VDMCaseStudy1.getEmpiricalData(1,'gcv'); plot(x,y)
        %   filenames are: 
        %     C:\Users\johnsra2\Desktop\datasets\UNITN\ffv.xlsx
        %     C:\Users\johnsra2\Desktop\datasets\UNITN\gcv.xlsx            
        %     C:\Users\johnsra2\Desktop\datasets\UNITN\asv.xlsx
        %     C:\Users\johnsra2\Desktop\datasets\UNITN\iev.xlsx

            %read the 'DATASET' sheet
            %read the 'cveDate' and 'minVersion' columns
            retOsInterfaceType=mcmcBayes.osInterfaces.getOSInterfaceType();
            if retOsInterfaceType==mcmcBayes.t_osInterfaces.UbuntuLinux
                filename=strcat('/sandbox/datasets/', name, '.xlsx');
                versionfilename='/sandbox/datasets/webBrowserVersions.xlsx';
            elseif retOsInterfaceType==mcmcBayes.t_osInterfaces.Windows7
                filename=strcat('C:\sandbox\datasets\', name, '.xlsx');
                versionfilename='C:\sandbox\datasets\webBrowserVersions.xlsx';
            else
                error('Unsupported mcmcBayes.t_osInterfaces');
            end   
            
            sheet='DATASET';            
            start=datetime('29-jan-2014');
            xlTable = readtable(versionfilename, 'Sheet', name);
            versions=xlTable.version;
            for i=1:size(xlTable.releaseDate)
                releasedates(i,1)=datetime(xlTable.releaseDate(i),'Format','MM/dd/yyyy');
            end
            xlTable = readtable(filename, 'Sheet', sheet);
            for i=1:size(xlTable.cveDate)
                if isa(xlTable.minVersion,'cell')
                    if strcmp(xlTable.minVersion{i},'NA')==1
                        minVersion(i)=NaN;
                    else
                        minVersion(i)=str2double(xlTable.minVersion{i});
                    end
                else%should be a double
                    minVersion(i)=xlTable.minVersion(i);
                end
                if isa(xlTable.cveDate,'cell')
                    if strcmp(xlTable.cveDate{i},'NA')==1
                        cveDate(i)={NaN};
                        numDays(i)=NaN;
                    else
                        cveDate(i)={datetime(xlTable.cveDate{i},'ConvertFrom','excel','Format','MM/dd/yyyy')};
                        temp=(cveDate{i}-releasedates(find(versions==minVersion(i))))/24;
                        if temp<0
                            numDays(i)=NaN;
                        else
                            temp=(cveDate{i}-releasedates(find(versions==minVersion(i))));
                            temp.Format='d';
                            numDays(i)=datenum(temp);
                        end
                    end
                else
                    cveDate(i)=xlTable.cveDate(i);
                    temp=(cveDate(i)-releasedates(find(versions==minVersion(i))))/24;
                    if temp<0
                        numDays(i)=NaN;
                    else
                        temp=(cveDate(i)-releasedates(find(versions==minVersion(i))));
                        temp.Format='d';
                        numDays(i)=datenum(temp);
                    end
                end
            end
            maxbugdays=7*50;
            studylengthdays=start-releasedates(find(versions==version));
            studylengthdays.Format='d';
            studylengthdays=datenum(studylengthdays);
            x=numDays(find(minVersion==version));%I need to ensure that I am not double counting!  That is, I only should be looking at rows matching minVersion and computing the days                   
            x=x(find(~isnan(x)));
            x=x(find(x<=maxbugdays));
%             indices=unique(x);
%             if (indices(1)~=0)
%                 indices=[0, indices];
%             end
            indices=[0:70:maxbugdays];            
            N=histcounts(x,indices);
            y=[0 cumsum(N)];
            x=indices;
%             h=fig;
%             plot(axes(h),[x min(studylengthdays,maxbugdays)],[y y(end)],'*-');%note, extend x at capacity until the UTrento study date (as the data supports this with no more discoveries)
%             plot(axes(h),x,y,'*-')
            %pause(1);%slight delay before closing the figure
            %close(h);
            x=x/7;%have x be 10 weeks increments (do this step after the plot)
        end        
    end
    
    methods
        function [obj]=VDMCaseStudy1(cfgMcmcBayes,repositories,sequenceUuid)           
        % obj=VDMCaseStudy1(cfgMcmcBayes,repositories,sequenceUuid)
        %   The constructor for VDMCaseStudy1.
        % Output:
        %   obj Constructed VDMCaseStudy1 object
            if nargin > 0
                superargs{1}='VDMCaseStudy1';
                superargs{2}=cfgMcmcBayes;
                superargs{3}=repositories;
                superargs{4}=sequenceUuid;
            elseif nargin == 0
                superargs={};
            end
            obj=obj@mcmcBayes.sequence(superargs{:});                        
        end                
    end
    
    methods (Static, Access=private)        
        function [postulatedData]=getPostulatedData(type, t, gamma)
        % [postulatedData]=getPostulatedData(type, t, gamma)
        %   Function that generates postulated data for the independent variable values specified by t and assuming the total quality specified by
        %   gamma.  Generally solves such that the cumulative sum N(0)+N(10)+N(20)+N(30)+N(40)+N(50)=gamma. 
        %   Input:
        %     type - t_mcmcBayesData specifying the type of postulated data (PostulatedConFR, PostulatedLinIFR, PostulatedLinDFR, etc.)
        %     t - Vector with the independent variable values to generate postulated data for
        %     gamma - Value that specifies the total quality assumed for the dataset (i.e., how many vulnerabilities are present or the maximum possible
        %     number of counts)
        %   Output:
        %     postulatedData - Vector containing the postulated data generated by this function
            switch type
                case t_mcmcBayesData.PostulatedConFR %N(t) for constant growth=x0
                    x0=gamma/(numel(t)-1);%divide gamma by the number of intervals
                    r=0;
                    fgrowth=@(x,r,t) x; %r and t are unused, x0 is passed in for x
                    frate=@(x,r,t) x; %r and t are unused, x0 is passed in for x
                case t_mcmcBayesData.PostulatedLinIFR %N(t) for linear growth=x*t+r/2*t^2
                    x0=0; 
                    for index=1:numel(t)
                        if index==1
                            fhandleStr=sprintf('@(x) x/2*t(%d)^2',index);
                        elseif index==numel(t)
                            fhandleStr=sprintf('%s+x/2*t(%d)^2-gamma;',fhandleStr,index);
                        else
                            fhandleStr=sprintf('%s+x/2*t(%d)^2',fhandleStr,index);
                        end
                    end
                    fhandle=eval(fhandleStr);
                    
                    if isequal(t,[0,10,20,30,40,50]) || isequal(t,[0:1:50])
                        inits=[0.1];
                    elseif isequal(t,[0,4000,8000,12000,16000,20000]) %AssessmentTime
                        inits=[0.1];
                    else
                        error('Unsupported t vector (i.e., you will need to determine appropriate inits).');
                    end
                    
                    r=fsolve(fhandle,inits);
                    fgrowth=@(x,r,t) x*t+r/2*t^2;
                    frate=@(x,r,t) x+r*t;
                case t_mcmcBayesData.PostulatedLinDFR %N(t) for linear decay=x*t-r/2*t^2
                    for index=1:numel(t)
                        if index==1
                            fhandleStr=sprintf('@(x) x(1)*t(%d)-x(2)/2*t(%d)^2', index, index);
                        elseif index==numel(t)
                            fhandleStr=sprintf('%s+x(1)*t(%d)-x(2)/2*t(%d)^2-gamma;',fhandleStr, index, index);
                        else
                            fhandleStr=sprintf('%s+x(1)*t(%d)-x(2)/2*t(%d)^2', fhandleStr, index, index);
                        end
                    end
                    fhandle=eval(fhandleStr);                    

                    if isequal(t,[0,10,20,30,40,50]) 
                        inits=[gamma/45,0.1];
                    elseif isequal(t,[0,4000,8000,12000,16000,20000]) %AssessmentTime
                        inits=[gamma/20000,0.01];
                    elseif isequal(t,[0:1:50])
                        inits=[gamma/450,0.001];
                    else
                        error('Unsupported t vector (i.e., you will need to determine appropriate inits).');
                    end
                    
                    [retdata]=fsolve(fhandle,inits);
                    x0=retdata(1);
                    r=retdata(2);
                    fgrowth=@(x,r,t) x*t-r/2*t^2;
                    frate=@(x,r,t) x-r*t;
                case t_mcmcBayesData.PostulatedExpIFR %N(t) for exponential growth=x*exp(r*t)
                    error('t_mcmcBayesData.PostulatedExpIFR is not verified');
                    for index=1:numel(t)
                        if index==1
                            fhandleStr=sprintf('@(x) x(1)*exp(x(2)*t(%d))', index);
                        elseif index==numel(t)
                            fhandleStr=sprintf('%s+x(1)*exp(x(2)*t(%d))-gamma;',fhandleStr, index);
                        else
                            fhandleStr=sprintf('%s+x(1)*exp(x(2)*t(%d))', fhandleStr, index);
                        end
                    end
                    fhandle=eval(fhandleStr);                       

                    if isequal(t,[0,10,20,30,40,50]) 
                        inits=[0.75,0.1];
                    elseif isequal(t,[0:1:50])
                        inits=[0.75,0.01];
                    else
                        error('Unsupported t vector (i.e., you will need to determine appropriate inits).');
                    end
                    
                    [retdata]=fsolve(fhandle,inits);
                    x0=retdata(1);
                    r=retdata(2);
                    fgrowth=@(x,r,t) x*exp(r*t);
                    frate=@(x,r,t) r*x*exp(r*t);
                case t_mcmcBayesData.PostulatedExpDFR %N(t) for exponential decay=x*exp(-r*t)
                    error('t_mcmcBayesData.PostulatedExpIFR is not verified');
                    for index=1:numel(t)
                        if index==1
                            fhandleStr=sprintf('@(x) x(1)*exp(-x(2)*t(%d))', index);
                        elseif index==numel(t)
                            fhandleStr=sprintf('%s+x(1)*exp(-x(2)*t(%d))-gamma;',fhandleStr, index);
                        else
                            fhandleStr=sprintf('%s+x(1)*exp(-x(2)*t(%d))', fhandleStr, index);
                        end
                    end
                    fhandle=eval(fhandleStr); 

                    if isequal(t,[0,10,20,30,40,50]) 
                        inits=[gamma/50,0.1];
                    elseif isequal(t,[0:1:50])
                        inits=[gamma/250,0.01];
                    else
                        error('Unsupported t vector (i.e., you will need to determine appropriate inits).');
                    end
                    
                    [retdata]=fsolve(fhandle,inits);
                    x0=retdata(1);
                    r=retdata(2);
                    fgrowth=@(x,r,t) x*exp(-r*t);
                    frate=@(x,r,t) -r*x*exp(-r*t);
                case t_mcmcBayesData.PostulatedIthenDFR
                    
                otherwise
                    error('Unsupported t_mcmcBayesData type.');
            end
            
            growth(1)=0;
            rate(1)=frate(x0,r,t(1));
            for index=2:numel(t)
                growth(index)=fgrowth(x0,r,t(index))+growth(index-1);
                rate(index)=frate(x0,r,t(index));
            end
            
%             hold on
%             plot(t,growth,'-*');
%             plot(t,rate);
%             set(gca,'XTickMode','manual');
%             set(gca,'xTick',[]);
%             set(gca,'xTick',t);
%             hold off
            
            postulatedData=growth;
        end                                                                        
    end
    
    methods %getter/setter functions
        function value = get.gamma(obj)
            value=obj.gamma;
        end
        
        function obj = set.gamma(obj,setgamma)
            if ~isa(setgamma,'double')
                warning('VDMCaseStudy1.gamma must be a double=>0');
            else
                if ~(setgamma>0)
                    warning('VDMCaseStudy1.gamma must be a double>=0');
                else
                    obj.gamma = setgamma;
                end
            end
        end
    end
end