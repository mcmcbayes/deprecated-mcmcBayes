classdef BrooksMotley < mcmcBayes.NHPP   
% Homogeneous Poisson process model, Brooks, Motley 1980.  Growth rate is constant over time.
% 'a' - Gamma priors (i.e., zeta~gamma(a,b))
    methods(Access = public, Static)
        function test()
            display('Hello!');
        end
        
        function retData=transformDataPre(vars, data)            
            retData=data;%no special transforms for this model (everything handled by mcmcBayes.data.constructData())
        end          

        function [solverInits]=determineSolverInits(vars, data)
            %needs to return a cell array due to some models having different sized variables
            for varid=1:size(vars,1)
                solverInits(varid,1)=[{vars(varid).MCMC_initvalue}];%needs to be a row vector
            end
            %Compute solverInits (bootstrap values for mle/ls that solve for solverInits) using fsolve and fhandle
            if ~((data.dataSource==mcmcBayes.t_dataSource.None) || (data.dataSource==mcmcBayes.t_dataSource.Preset))
                indVarsValues=data.indVarsValues;
                depVarsValues=data.depVarsValues;
                %Solve for zeta given N(t=end)=u*zeta*t
                datavarid=1;
                t=indVarsValues{datavarid, 1};
                N=cast(depVarsValues{datavarid, 1},'double');
                fhandle=@(x) x*t(end)-N(end);%N(t=end) equals data.dependentVars(end)
                solverInits=[{fsolve(fhandle, [solverInits{1}])}];
            end
        end
        
        %this is only used for determining the inits when the data source is not mcmcBayes.t_dataSource.None or mcmcBayes.t_dataSource.Preset 
        function [thetahat]=SolverMeanMLE(runPowerPosteriorMCMCSequence, subtype, vars, data)       
        % [thetahat]=mlefitModel(runPowerPosteriorMCMCSequence, independentVariables, modelVariables, dependentVariables)
        %   Wrapper function to call MATLAB's mle routine that estimates the model variable point estimates.  The structure
        %   of MATLAB's mle routine requires globals to pass the independent variables, phi, and any values for variables that
        %   are assumed as a constant (thus, are not being estimated by the mle).
        %   Input:
        %     runPowerPosteriorMCMCSequence - Boolean that specifies either running the power-posterior sequence, or not running
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     modelVariables - Distribution model variables that is an array of mcmcBayesVariable (indexed by varid, tempid) objects 
        %     dependentVariables - Prior dependent variables for the model that is a cell array of mcmcBayesNumeric objects.
        %   Output:
        %     thetahat - Cell array of model variable point estimates values computed
            options = statset('MaxIter',800, 'MaxFunEvals',1200);     
                        
            if runPowerPosteriorMCMCSequence
                phi=mcmcBayes.getDefaultPhi(2);%identify the inits for tempid=2
            else
                phi=1; 
            end
         
           	datavarid=1;
            t=cast(data(datavarid).indVarsValues{:},'double');

            mypdf=@(N,zeta) mcmcBayes.BrooksMotley.computeLogLikelihood_alt(N, phi, t, zeta);
            varsDefaultInits=[cast(cell2mat(vars(1).MCMC_initvalue),'double')];
            varsLowerBounds=[cast(cell2mat(vars(1).solverMin),'double')];
            varsUpperBounds=[cast(cell2mat(vars(1).solverMax),'double')];
            
            datavarid=1;
            N=cast(data(datavarid).depVarsValues{:},'double');
            [xhat]=mle(N,'logpdf',mypdf,'start',varsDefaultInits,'lowerbound',varsLowerBounds,'upperbound',varsUpperBounds,'options',options);
            
            thetahat=[{xhat(1)}];
        end        
        
        %this is only used for determining the inits when the data source is not mcmcBayes.t_dataSource.None or mcmcBayes.t_dataSource.Preset 
        function [thetahat]=SolverMeanLS(runPowerPosteriorMCMCSequence, subtype, vars, data)
            %needs to return a cell array due to some models having different sized variables                    
        % [thetahat]=lsfitModel(independentVariables, modelVariables, dependentVariables)
        %   Wrapper function to call MATLAB's lsqcurvefit routine that estimates the model variable point estimates.  
        %   Input:
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     modelVariables - Distribution model variables that is an array of mcmcBayesVariable (indexed by varid, tempid) objects 
        %     dependentVariables - Prior dependent variables for the model that is a cell array of mcmcBayesNumeric objects.
        %   Output:
        %     thetahat - Cell array of model variable point estimates values computed
                
           	datavarid=1;
            t=cast(data(datavarid).indVarsValues{:},'double');

            fhandle=@(x,xdata) x(1)*xdata;
            varsDefaultInits=[cast(vars(1).MCMC_initvalue,'double')];
            varsLowerBounds=[cast(vars(1).solverMin,'double')];
            varsUpperBounds=[cast(vars(1).solverMax,'double')];
            
            options = optimoptions('lsqcurvefit','Display','none','MaxFunEvals',3000,'MaxIter',1000);            

            datavarid=1;
            N=cast(data(datavarid).depVarsValues{:},'double');
            [xhat,resnorm,residual,exitflag,output]=lsqcurvefit(fhandle,varsDefaultInits,t,N,varsLowerBounds,varsUpperBounds,options);
            if exitflag~=1%converged
                disp(output.message)
            end

            thetahat=[{xhat(1)}];
        end        
    end

    methods(Access = protected, Static)
        function [fhandle]=get_fhandle_CIF()
        % [fhandle]=get_fhandle_CIF()
        %   Provides the cumulative intensity function for the NHPP.generatePredictions function
        %   Output:
        %     fhandle - returns the handle to the equation for the CIF
            fhandle=@(t,zeta) cast(zeta,'double')*t;
        end        
    end

    methods(Access = private, Static)
        function [loglikelihood]=computeLogLikelihood_alt(data, phi, t, varargin)
        % [loglikelihood]=computeLogLikelihood_alt(data,varargin)
        %   Function that computes the model's log-likelihood of the data given the parameters.  It is used by MATLAB's mle function.
        %   It was necessary to utilize a global variable for passing the independent variable and the u parameter (due to the calling constraints 
        %   imposed by mle function.
        %   Input:
        %     data - Model input data used in computing the log-likelihood
        %     varargin - Point estimates for the model variables used in computing the log-likelihood
        %   Output:
        %     loglikelihood - Computed log-likelihood
            n=cast(data,'double');
            zeta=varargin{1};
            for i=2:numel(n)
                A(i-1)=zeta*t(i)-zeta*t(i-1);
                B(i-1)=n(i)-n(i-1);
                logpdf(i-1)=phi*(B(i-1)*log(A(i-1))-log(factorial(B(i-1)))-A(i-1));
                if (isinf(logpdf(i-1)) && sign(logpdf(i-1))==-1)
                    error('Error condition in computeLogLikelihood(), [zeta=%f], loglikelihood(%d)=-Inf',zeta,i-1);
                elseif (isinf(logpdf(i-1)) && sign(logpdf(i-1))==1)
                    error('Error condition in computeLogLikelihood(), [zeta=%f], loglikelihood(%d)=Inf',zeta,i-1);
                elseif isnan(logpdf(i-1))
                    error('Error condition in computeLogLikelihood(), [zeta=%f], loglikelihood(%d)=Nan',zeta,i-1);
                end
            end
            loglikelihood=sum(logpdf);
%             tempstr=sprintf('HPP loglikelihood(zeta=%f)=%f',zeta,loglikelihood);
%             disp(tempstr);
        end        
    end

    methods 
        function obj=BrooksMotley(setsimulationStruct, setprefix, setcomputemsfe)
        % BrooksMotley(setsimulationStruct, setprefix, setcomputemsfe)
        %   Constructor for the BrooksMotley class
        %   Input:
        %     setsimulationStruct - 
        %     setprefix - 
        %   Output:
        %     obj - instantiated BrooksMotley object
            if nargin == 0
                superargs={};
            else
                switch setsimulationStruct.subType
                    case 'a'
                        %disp('Brooks-Motley HPP model is using modelsubtype a (zeta~gamma(a,b))');
                    otherwise
                        error('Unsupported subtype');
                end
                superargs{1}='BrooksMotley';
                superargs{2}=setsimulationStruct.subType;
                superargs{3}='HPP';
                superargs{4}='Static failure rate';
                superargs{5}='Brooks, Motley 1980';
                superargs{6}='reuben@reubenjohnston.com';
                superargs{7}=setprefix;
                superargs{8}=setcomputemsfe;
            end
            
            obj=obj@mcmcBayes.NHPP(superargs{:});
            
            if nargin>0 %i am not sure why, but it is necessary to set these when loading
                obj.predictionsNSamples=mcmcBayes.NHPP.getDefaultNSamples();
                obj.predictionsIntervalNmax=mcmcBayes.NHPP.getDefaultPredictionsIntervalNmax();
                obj.predictionsfunctionHandleCif=mcmcBayes.BrooksMotley.get_fhandle_CIF; 
            end
        end
    end
end
        