function mat2bugs(varargin)
%MAT2BUGS - save matlab variables in S-PLUS format for BUGS
% This code was not readable so I refined a significant portion of it, including the api! RAJ
%
%mat2bugs(filename, [{name1} {value1}; {name2} {value2} , ... ])
%  Writes S-PLUS data specification to the standard output for given variable names and values.
%  Any number of name,value pairs can be given in the array.
%
%Examples:
%  mat2bugs('bugs_data.dat', [{'x'}, {x}; {'y'}, {y}])
%
%BUGS: (no pun intended)
%  Supports only scalars and 2D tables.  Also, 1D tables are converted to 2d (i.e., a 1xN).
%
% (c) jouko.lampinen@hut.fi, 1998-1999
% 2002-11-01 Aki.Vehtari@hut.fi - Added handling for NaN
% 2003-01-14 Aki.Vehtari@hut.fi - Option to force use of 1D and 2D tables

%input validation
if ischar(varargin{1})
    ofile=fopen(varargin{1},'w');
    if ofile<1
        error(['Cannot open ' varargin{1}]);
    end
    i=1;
else
    error('There was an issue with creating/opening the data file for the sampler');
end

%input validation
if size(varargin,2)~=2
    error('You passed in an empty data list');
end
nargin=size(varargin{2},1);

fprintf(ofile,'list(');
while i<=nargin
    name=varargin{2}{i,1};
    val=varargin{2}{i,2};
    [dims]=size(val);
    R=dims(1);
    C=dims(2);
    if size(dims,2)>2
        error('Unsupported matrix size');
    end
    if R==1 && C==1 %is it just a scalar?
        fprintf(ofile,'%s=',name);
        fprintval(ofile,val);
    else %it is a vector or a matrix, just use a structure for either
        fprintf(ofile,'%s=structure(\n  .Data=c(\n',name);
        for k=1:R
            fprintf(ofile,'\t');
            for l=1:(C-1)
                fprintval(ofile,val(k,l));
                fprintf(ofile,', ');
            end
            if k<R
                fprintval(ofile,val(k,C));
                fprintf(ofile,',\n');
            else
                fprintval(ofile,val(k,C));
                fprintf(ofile,'),\n');
            end
        end
        fprintf(ofile,'  .Dim=c(%G,%G))',R,C);
    end
    if i<nargin
        fprintf(ofile,',\n');
        i=i+1;
    else
        i=i+1;
    end
end
fprintf(ofile,')\n');
if ofile>2, fclose(ofile); end

function fprintval(ofile, val)
if iscategorical(val)
    fprintf(ofile,'%G',val);
elseif isnan(val)
    fprintf(ofile,'NA');
else
    tempstr=sprintf('%G',val);
    if strfind(tempstr,'E+0')>0
        tempstr=strrep(tempstr,'E+0','E+');
    elseif strfind(tempstr,'E-0')>0
        tempstr=strrep(tempstr,'E-0','E-');
    end
    if isempty(strfind(tempstr,'.')) & (strfind(tempstr,'E')>0) %todo: this is bad code
        tempstr=strrep(tempstr,'E','.0E');
    end
    fprintf(ofile,'%s',  tempstr);
end
