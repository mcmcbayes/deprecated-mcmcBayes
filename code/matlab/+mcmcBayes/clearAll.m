function clearAll()   
% clearAll()
%   Wrapper function that calls the simulationOSInterfaces.clearAll function.
    logging.stopLogging();
    mcmcBayes.osInterfaces.clearAll();            
end