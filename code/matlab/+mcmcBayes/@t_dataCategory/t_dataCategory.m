classdef t_dataCategory < int32
    enumeration
        independent (1)
        dependent (2)
    end
    
    methods (Static)        
        function intvalue=toint(enumobj)
            switch enumobj          
                case {mcmcBayes.t_dataCategory.independent, ...
                        mcmcBayes.t_dataCategory.dependent}
                    intvalue=int32(enumobj);
                otherwise
                    error('unsupported mcmcBayes.t_dataCategory type');
            end                
        end        
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_dataCategory.independent
                    strvalue='mcmcBayes.t_dataCategory.independent';
                case mcmcBayes.t_dataCategory.dependent
                    strvalue='mcmcBayes.t_dataCategory.dependent';                    
                otherwise
                    error('unsupported mcmcBayes.t_dataCategory type');
            end
        end

        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_dataCategory.independent)
                    enumobj=mcmcBayes.t_dataCategory.independent;
                case int32(mcmcBayes.t_dataCategory.dependent)
                    enumobj=mcmcBayes.t_dataCategory.dependent;
                otherwise
                    error('unsupported mcmcBayes.t_dataCategory type');
            end
        end
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_dataCategory.independent')==1
                enumobj=mcmcBayes.t_dataCategory.independent;
            elseif strcmp(strval,'mcmcBayes.t_dataCategory.dependent')==1
                enumobj=mcmcBayes.t_dataCategory.dependent;
            else
                error('unsupported mcmcBayes.t_dataCategory type');
            end
        end          
    end
end