classdef t_osInterfaces < int32
    enumeration
        Windows7 (1)
        UbuntuLinux (2)
        MacOS (3)
        Windows10 (4)
    end
end