classdef t_variableTransformPre < int32
    enumeration
        none (1)%do not solve
        adjustForFit2NormalizedData (2)
        adjustForFit2MeanCenteredData (3)
        reparameterize1 (4)%KuoGhosh has to approximate Gamma process prior in JAGS/OpenBUGS by using a categorical random variable sampler.
                           %Requires estimating equivalent pmf and indices lookup tables and passing these into the MCMC sampler, in place of
                           %Lambda(t) and c (the parameters for the Gamma process prior).  Categorical random variables are sampled in
                           %the MCMC sampler according to the pmf and the resulting samples are used as a key into the indices array (i.e.,
                           %indices[categorical sample] approximates the sample from the Gamma process).
    end
    methods (Static)        
        function intvalue=toint(enumobj)
            switch enumobj          
                case {mcmcBayes.t_variableTransformPre.none, mcmcBayes.t_variableTransformPre.adjustForFit2NormalizedData,...
                      mcmcBayes.t_variableTransformPre.adjustForFit2MeanCenteredData, mcmcBayes.t_variableTransformPre.reparameterize1}
                    intvalue=int32(enumobj);                    
                otherwise
                    error('unsupported mcmcBayes.t_variableTransformPre type');
            end                
        end        
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_variableTransformPre.none
                    strvalue='mcmcBayes.t_variableTransformPre.none';
                case mcmcBayes.t_variableTransformPre.adjustForFit2NormalizedData
                    strvalue='mcmcBayes.t_variableTransformPre.adjustForFit2NormalizedData';
                case mcmcBayes.t_variableTransformPre.adjustForFit2MeanCenteredData
                    strvalue='mcmcBayes.t_variableTransformPre.adjustForFit2MeanCenteredData';
                case mcmcBayes.t_variableTransformPre.reparameterize1
                    strvalue='mcmcBayes.t_variableTransformPre.reparameterize1';
                otherwise
                    error('unsupported mcmcBayes.t_variableTransformPre type');
            end
        end
        
        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_variableTransformPre.none)
                    enumobj=mcmcBayes.t_variableTransformPre.none;
                case int32(mcmcBayes.t_variableTransformPre.adjustForFit2NormalizedData)
                    enumobj=mcmcBayes.t_variableTransformPre.adjustForFit2NormalizedData;
                case int32(mcmcBayes.t_variableTransformPre.adjustForFit2MeanCenteredData)
                    enumobj=mcmcBayes.t_variableTransformPre.adjustForFit2MeanCenteredData;
                case int32(mcmcBayes.t_variableTransformPre.reparameterize1)
                    enumobj=mcmcBayes.t_variableTransformPre.reparameterize1;
                otherwise
                    error('unsupported mcmcBayes.t_variableTransformPre');                    
            end
        end
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_variableTransformPre.none')==1
                enumobj=mcmcBayes.t_variableTransformPre.none;
            elseif strcmp(strval,'mcmcBayes.t_variableTransformPre.adjustForFit2NormalizedData')==1
                enumobj=mcmcBayes.t_variableTransformPre.adjustForFit2NormalizedData;
            elseif strcmp(strval,'mcmcBayes.t_variableTransformPre.adjustForFit2MeanCenteredData')==1
                enumobj=mcmcBayes.t_variableTransformPre.adjustForFit2MeanCenteredData;
            elseif strcmp(strval,'mcmcBayes.t_variableTransformPre.reparameterize1')==1
                enumobj=mcmcBayes.t_variableTransformPre.reparameterize1;
            else
                error('unsupported mcmcBayes.t_variableTransformPre');
            end
        end
    end
end