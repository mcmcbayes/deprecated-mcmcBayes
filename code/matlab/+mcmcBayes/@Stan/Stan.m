classdef Stan < mcmcBayes.samplerInterfaces
%this class performs all interfacing with Stan
    properties           
        cmdStanDir
        cmdStanShortCut
        
        Stan_projdir %string
        Stan_shortcut %this is the executable compiled from the project below
        
        filename_bashscript       
        
        filename_Stanscript%used in setupMCMCSampling,performMCMCSampling
        filename_Stanproject%*.stan file that we compile using cmdStan
        filename_Staninits%used in setupMCMCSampling
        filename_Standata%used in setupMCMCSampling
        filename_Stanlog
        %Stan's output file is a *.csv
        filename_Stanoutput
    end

    methods
        function obj = Stan(sequence) %constructor
            if nargin == 0
                superargs={};
            else
                superargs{1}=mcmcBayes.t_samplerInterfaces.Stan;
                superargs{2}=sequence.osInterface;
                superargs{3}=sequence.cfgMcmcBayes.cfgSimulation.runView;
                superargs{4}=sequence.repositories;
            end

            obj=obj@mcmcBayes.samplerInterfaces(superargs{:});                                  
                      
            obj.cmdStanDir=sequence.cfgMcmcBayes.cfgSampler.Stan.cmdStanDir;
            obj.cmdStanShortCut=sequence.cfgMcmcBayes.cfgSampler.Stan.cmdStanShortCut;
        end
        
        %setup Stan script, inits, and data files for sampling
        function retobj=setupMCMCSampling(obj, setmodeltype, setmodelsubtype, setnchains, settempid, setphi, outputFileNamePrefix, setVariables, ...
                setData, setOsInterface)  
            obj.modeltype=setmodeltype;
            obj.modelsubtype=setmodelsubtype;
            obj.nchains=setnchains;
            obj.tempid=settempid;            
            obj.phi=setphi;
            obj.outputFileNamePrefix=outputFileNamePrefix;
            obj.variables=setVariables;
            obj.data=setData;
            obj.osInterface=setOsInterface;
                                  
            obj.filename_bashscript=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-bashscript.txt', obj.osInterface.tempDir, obj.outputFileNamePrefix));            
            obj.filename_Stanscript=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-script.txt', obj.osInterface.tempDir, obj.outputFileNamePrefix));
            
            obj.filename_Standata=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-data.txt', mcmcBayes.osInterfaces.getPath( ...
                mcmcBayes.t_paths.samplerDataInDir), obj.outputFileNamePrefix));
            obj.filename_Staninits=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-inits.txt', mcmcBayes.osInterfaces.getPath( ...
                mcmcBayes.t_paths.samplerInitsDir), obj.outputFileNamePrefix));           
            
            %filename_Stanlog and filename_Stanoutput are set by preprocessor
            
            %find the repository with the sampler files and then set JAGS_projdir and JAGS_scriptdir
            stdout=1;
            fprintf(stdout,'Model %s%s\n',setmodeltype,obj.modelsubtype);
            for i=1:size(obj.repositories,1)
                tmpprojdir=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%sStan/%s', obj.repositories{i}, ...
                    mcmcBayes.osInterfaces.getSubPath(mcmcBayes.t_paths.samplerProjectsDir), setmodeltype));
 
                tmpfilename_Stanproject=mcmcBayes.osInterfaces.convPathStr(sprintf('%s/%s%s.stan', tmpprojdir, setmodeltype, setmodelsubtype)); 
                switch obj.osInterface.getOSInterfaceType()
                    case {mcmcBayes.t_osInterfaces.UbuntuLinux,mcmcBayes.t_osInterfaces.MacOS}                      
                        tmpfilename_Stanshortcut=mcmcBayes.osInterfaces.convPathStr(sprintf('%s/%s%s', tmpprojdir, setmodeltype, setmodelsubtype));
                    case {mcmcBayes.t_osInterfaces.Windows7, mcmcBayes.t_osInterfaces.Windows10}
                        tmpfilename_Stanshortcut=mcmcBayes.osInterfaces.convPathStr(sprintf('%s/%s%s.exe', tmpprojdir, setmodeltype, setmodelsubtype));
                    otherwise
                        error('unsupported t_mcmcBayes.osInterfaces');
                end                
                if mcmcBayes.osInterfaces.doesFileExist(tmpfilename_Stanproject) %is a file
                    break;
                elseif i==size(obj.repositories,1)
                    error('Did not locate sampler project file.');                    
                end
            end
            obj.Stan_projdir=tmpprojdir;
            obj.filename_Stanproject=tmpfilename_Stanproject;         
            obj.Stan_shortcut=tmpfilename_Stanshortcut;
            
            obj.setupBashScript();
                
            %preprocessor needs to run in sampling loop (output filename for chainid differs)              
            
            %setup obj.filename_Standata for sampling sequence
            setupData(obj, obj.filename_Standata)
            
            %setup obj.filename_Staninits for sampling sequence
            setupInits(obj, obj.filename_Staninits)
            
            retobj=obj;
        end
        
        function buildCmdStan(obj)
            stdout=1;           
            fprintf(stdout,'Compiling CmdStan binary\n');
            tempscript=mcmcBayes.osInterfaces.convPathStr(sprintf('%sbuildCmdStanScript.txt', obj.osInterface.tempDir));    
            templog=mcmcBayes.osInterfaces.convPathStr(sprintf('%sbuildCmdStanLog.txt', obj.osInterface.tempDir));     
            fid=fopen(tempscript,'wt');            
            fprintf(fid,'#!/usr/bin/env bash\n echo sleeping 5 seconds to allow os-agnostic-utils time to fork a process and determine its pid\n');
            fprintf(fid,'sleep 5\n');
            fprintf(fid,'cd %s\n',mcmcBayes.osInterfaces.convPathStrUnix(obj.cmdStanDir));
            fprintf(fid,'make clean-all && make build -j4\n');
            if obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.UbuntuLinux || obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.MacOS
                syscommand=sprintf('system(''chmod +x "%s"'');',tempscript);%make the script executable
                eval(syscommand); 
            end  
            fclose(fid);            

            compileTimerStart=tic;
            %needs to run and wait, as otherwise, *.exe appears prior to completion of entire build
            obj.osInterface=obj.osInterface.spawnBashCommand( tempscript, templog, mcmcBayes.t_runView.Foreground);
            finished=false;

            setcommandstr='';
            while finished==false
                drawnow;                
                pause(1);%sleep 1 seconds
                elapsedtime=toc(compileTimerStart);
                durelapsedseconds=seconds(elapsedtime);
                elapsedminutes=minutes(durelapsedseconds);
                statusstr=sprintf('Duration for building CmdStan is %e', elapsedminutes);
                setDesktopStatus(statusstr)
                [obj.osInterface, retcommandstr]=obj.osInterface.updateBashCmdStatus(setcommandstr);
                setcommandstr=retcommandstr;
                %disp(setcommandstr);
                obj.osInterface.checkForKillFile();
                if (obj.osInterface.bashCmdState==mcmcBayes.t_runState.Finished)
                    finished=true;
                    tempstr=sprintf('\tTotal build time for CmdStan was %e', elapsedminutes);
                    disp(tempstr);    
                    setDesktopStatus(tempstr);
                end
            end
            obj.osInterface.deleteCmdMsgFile()
        end
        
        function buildStanProgram(obj)
            stdout=1;
            fprintf(stdout,'Compiling stan binary\n');
            hppname=mcmcBayes.osInterfaces.convPathStrUnix(sprintf('%s/%s%s.hpp',obj.Stan_projdir,obj.modeltype,obj.modelsubtype));
            projname=mcmcBayes.osInterfaces.convPathStrUnix(sprintf('%s/%s%s.stan',obj.Stan_projdir,obj.modeltype,obj.modelsubtype));
            switch obj.osInterface.getOSInterfaceType()
                case {mcmcBayes.t_osInterfaces.Windows7, mcmcBayes.t_osInterfaces.Windows10}
                    binname=mcmcBayes.osInterfaces.convPathStrUnix(sprintf('%s/%s%s.exe',obj.Stan_projdir,obj.modeltype,obj.modelsubtype));
                case {mcmcBayes.t_osInterfaces.UbuntuLinux,mcmcBayes.t_osInterfaces.MacOS}                      
                    binname=mcmcBayes.osInterfaces.convPathStrUnix(sprintf('%s/%s%s',obj.Stan_projdir,obj.modeltype,obj.modelsubtype));
            end
            
            tempscript=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-buildStanScript.txt', obj.osInterface.tempDir, obj.outputFileNamePrefix));    
            templog=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-buildStanLog.txt', obj.osInterface.tempDir, obj.outputFileNamePrefix));     
            fid=fopen(tempscript,'wt');            
            fprintf(fid,'#!/usr/bin/env bash\n');
            fprintf(fid,'cd %s\n',mcmcBayes.osInterfaces.convPathStrUnix(obj.cmdStanDir));
            fprintf(fid,'%s --name="%s%s" --o="%s" %s\n', mcmcBayes.osInterfaces.convPathStrUnix(obj.cmdStanShortCut), obj.modeltype, obj.modelsubtype, hppname, projname);
            fprintf(fid,'make %s\n',binname);       
            if obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.UbuntuLinux || obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.MacOS
                syscommand=sprintf('system(''chmod +x "%s"'');',tempscript);%make the script executable
                eval(syscommand); 
            end  
            fclose(fid);
            
            compileTimerStart=tic;
            %needs to run and wait, as otherwise, *.exe appears prior to completion of entire build
            obj.osInterface=obj.osInterface.spawnBashCommand(tempscript, templog, mcmcBayes.t_runView.Foreground);
            finished=false;

            setcommandstr='';
            while finished==false
                drawnow;                
                pause(1);%sleep 1 seconds
                elapsedtime=toc(compileTimerStart);
                durelapsedseconds=seconds(elapsedtime);
                elapsedminutes=minutes(durelapsedseconds);
                statusstr=sprintf('Stan compile time for %s model is %e', obj.modeltype, elapsedminutes);
                setDesktopStatus(statusstr)
                [obj.osInterface, retcommandstr]=obj.osInterface.updateBashCmdStatus(setcommandstr);
                setcommandstr=retcommandstr;
                %disp(setcommandstr);
                obj.osInterface.checkForKillFile();
                if (obj.osInterface.bashCmdState==mcmcBayes.t_runState.Finished)
                    finished=true;
                    tempstr=sprintf('\tTotal Stan compile time for %s model was %e', obj.modeltype, elapsedminutes);
                    disp(tempstr);    
                    setDesktopStatus(tempstr);
                end
            end
            obj.osInterface.deleteCmdMsgFile()                     
        end
        
        function setupBashScript(obj)
            if (mcmcBayes.osInterfaces.doesFileExist(obj.filename_bashscript)) %remove any old script files (with the same name)
                delete(obj.filename_bashscript);
            end
            
            %write output to obj.filename_Stanscript
            fid=fopen(obj.filename_bashscript,'wt');
            
            fprintf(fid,'#!/usr/bin/env bash\n echo sleeping 5 seconds to allow os-agnostic-utils time to fork a process and determine its pid\n');
            fprintf(fid,'sleep 5\n');
            fprintf(fid,'%s\n', mcmcBayes.osInterfaces.convPathStrUnix(obj.filename_Stanscript));
            fclose(fid);
            if obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.UbuntuLinux || obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.MacOS
                syscommand=sprintf('system(''chmod +x "%s"'');',obj.filename_bashscript);%make the script executable
                eval(syscommand);
            end         
        end
        
        %Using a script file to spawn a sampling sequence
        function retobj=runPreprocessor_scriptfile(obj, chainID)
            %Initializes the script file to run the project file binary with appropriate arguments
            stdout=1;
            
            if (mcmcBayes.osInterfaces.doesFileExist(obj.filename_Stanscript)) %remove any old script files (with the same name)
                delete(obj.filename_Stanscript);
            end
            
            obj.filename_Stanoutput=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-chainid%d-samples.txt', mcmcBayes.osInterfaces.getPath( ...
                mcmcBayes.t_paths.samplerDataOutDir), obj.outputFileNamePrefix, chainID));
            obj.filename_Stanlog=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-chainid%d-log.txt', mcmcBayes.osInterfaces.getPath( ...
                mcmcBayes.t_paths.logsDir), obj.outputFileNamePrefix, chainID));           
            
            %write output to obj.filename_Stanscript
            fid=fopen(obj.filename_Stanscript,'wt');
            
            %sample num_samples=%d, num_warmup=%d, thin=%d
            varID=1;%we just get the number of data samples, burnin, and lag from the first var
            fprintf(fid,'#!/usr/bin/env bash\n%s sample num_samples=%d num_warmup=%d thin=%d adapt delta=0.99 data file=%s init=%s output file=%s\n', ...
                mcmcBayes.osInterfaces.convPathStrUnix(obj.Stan_shortcut), ...
                obj.variables(varID,chainID).MCMC_nsamples_data, ...
                obj.variables(varID,chainID).MCMC_nsamples_burnin, ...
                obj.variables(varID,chainID).MCMC_nsamples_lag, ...
                mcmcBayes.osInterfaces.convPathStrUnix(obj.filename_Standata), mcmcBayes.osInterfaces.convPathStrUnix(obj.filename_Staninits), ...
                mcmcBayes.osInterfaces.convPathStrUnix(obj.filename_Stanoutput));
            fclose(fid);
            %fprintf(stdout,'created %s\n',obj.filename_Stanscript);
            if obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.UbuntuLinux || obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.MacOS
                syscommand=sprintf('system(''chmod +x "%s"'');',obj.filename_Stanscript);%make the script executable
                %disp(strcat('running: ',syscommand));
                eval(syscommand);
            end   
            retobj=obj;
        end
                               
        %calls Stan to perform the previously setup sampling sequence
        function [retobj]=performMCMCSampling(obj)
            stdout=1;
            %Run the script to generate the MCMC computations for theta(i|j) (cannot run Stan in the background as the next loop iteration can't start without results)
            for chainID=1:obj.nchains
                fprintf(stdout, '\tStarting Stan sampling for tempid=%d, chainID=%d.\n', obj.tempid, chainID);
                %setup obj.filename_Stanscript for sampling sequence
                obj=runPreprocessor_scriptfile(obj,chainID);              

                if (mcmcBayes.osInterfaces.doesFileExist(obj.filename_Stanlog))%remove any old log files (with the same name)
                    delete(obj.filename_Stanlog);
                end
                if (mcmcBayes.osInterfaces.doesFileExist(obj.filename_Stanoutput)) %remove any old output files (with the same name)
                    delete(obj.filename_Stanoutput);
                end                               

                obj.osInterface=obj.osInterface.spawnBashCommand( obj.filename_bashscript, obj.filename_Stanlog, obj.runView);

                finished=false;
                samplingTimerStart=tic;
                setcommandstr='';
                while finished==false
                    drawnow;
                    pause(3);%sleep 3 seconds
                    elapsedtime=toc(samplingTimerStart);
                    elapsedseconds=seconds(elapsedtime);
                    elapsedminutes=minutes(elapsedseconds);
                    statusstr=sprintf('For modeltype=%s, tempid=%d, Stan sampler elapsed time is %e minutes', obj.modeltype, obj.tempid, elapsedminutes);
                    setDesktopStatus(statusstr)
                    [obj.osInterface, retcommandstr]=obj.osInterface.updateBashCmdStatus(setcommandstr);
                    setcommandstr=retcommandstr;
                    obj.osInterface.checkForKillFile();               
                    %CmdStan output file update timing is wonky, check for the output file being static first and then wait for last line containing '#  Elapsed Time:'
                    if (obj.osInterface.bashCmdState==mcmcBayes.t_runState.Finished)
                        for count=1:12%will be about 12*(5) seconds
                            if checkFileStatic(obj.filename_Stanoutput,5)
                                break;%file exists and is not changing
                            end
                            drawnow;
                            pause(1);
                        end
                        if count==12
                            type(obj.filename_Stanlog);
                            error('Sampler failed.');   
                        else%stan sometimes finishes writing slowly
                            filecontents=fileread(obj.filename_Stanoutput);
                            foundstring=strfind(filecontents,'#  Elapsed Time:');
                            while (isempty(foundstring))
                                drawnow;
                                pause(1);
                                filecontents=fileread(obj.filename_Stanoutput);
                                foundstring=strfind(filecontents,'#  Elapsed Time:');
                            end
                         end
                        finished=true;
                        tempstr=sprintf('\tElapsed sampling time for modeltype=%s, tempid=%d was %e minutes.', obj.modeltype, ...
                            obj.tempid, elapsedminutes);
                        disp(tempstr);    
                        setDesktopStatus(tempstr);
                        obj.osInterface.deleteCmdMsgFile()                        
                    end
                end                        
            end
            retobj=obj;
        end
              
        function [retobj, fieldindices, data]=readSamples(obj)%index specifies the coda file to read all the variables sampled
            %Standatastruct=Stan2mat(obj.filename_CODAindex,obj.filename_CODAchain);            
            for chainID=1:obj.nchains
                clear tfieldindices targlist;
                %otherwise, trim chainid%dsamples.txt and repeat with the current chainid%d
                endloc=strfind(obj.filename_Stanoutput,'chainid');
                if isempty(endloc)
                    endloc=strfind(obj.filename_Stanoutput,'samples.txt');
                end
                tempstr=obj.filename_Stanoutput(1:endloc-1);
                tfilename_samples=sprintf('%schainid%d-samples.txt',tempstr,chainID); %%ssamples.txt                                      
            
                Standatastruct=mcmcBayes.stan2mat(obj.variables,tfilename_samples);
                %samples shall be stored as a 3-dimensional matrix within a cell
                %scalar[]-{[numSamples x 1]}
                %vector[]-{[numRows x 1 x numSamples]}
                %matrix[]-{[numRows x numCols x numSamples}]              
                for varid=1:size(obj.variables,1)
                    tsize=obj.variables(varid,chainID).MCMC_nsamples_data/obj.variables(varid,chainID).MCMC_nsamples_lag;
                    switch obj.variables(varid,chainID).type                 
                        case mcmcBayes.t_variable.constant
                            if (obj.variables(varid,chainID).distribution.type==mcmcBayes.t_distribution.DiscreteConstant)
                                tvar=obj.variables(varid,chainID).distribution.parameters(1).values*int32(ones(tsize,1,1));
                            else
                                tvar=obj.variables(varid,chainID).distribution.parameters(1).values*ones(tsize,1,1);
                            end
                            if exist('tfieldindices','var')==1
                                tfieldindices=[tfieldindices varid];
                                targlist=[targlist; {tvar}];
                            else
                                tfieldindices=varid;
                                targlist=[{tvar}];
                            end
                        case {mcmcBayes.t_variable.stochastic, mcmcBayes.t_variable.stochasticProcess}
                            names=fieldnames(Standatastruct);%need to identify the fields present in the coda               
                            for i=1:numel(names)
                                if strcmp(obj.variables(varid,chainID).name,cell2mat(names(i)))==1
                                    evalstr=sprintf('Standatastruct.%s',obj.variables(varid,chainID).name);
                                    tvar=eval(evalstr);
                                    if exist('tfieldindices','var')==1 %is this not the first entry (i.e., vector already created)
                                        tfieldindices=[tfieldindices varid];
                                        targlist=[targlist; {tvar}];
                                    else
                                        tfieldindices=varid;
                                        targlist=[{tvar}];
                                    end
                                    break;
                                end
                            end
                    end
                end
                data(:,chainID)=targlist;
                fieldindices(:,chainID)=tfieldindices;
            end    
            retobj=obj;
        end        
    end
    
    methods %getter/setter functions       
        function obj=set.cmdStanDir(obj,setcmdStanDir)
            if mcmcBayes.osInterfaces.doesFileExist(setcmdStanDir)
                obj.cmdStanDir=setcmdStanDir;
            else
                error('cmdStanDir is missing!');
            end
        end
        
        function value=get.cmdStanDir(obj)
            value=obj.cmdStanDir;
        end
        
        function obj=set.cmdStanShortCut(obj,setcmdStanShortCut)
            if mcmcBayes.osInterfaces.doesFileExist(setcmdStanShortCut)
                obj.cmdStanShortCut=setcmdStanShortCut;
            else
                obj.buildCmdStan();
                mcmcBayes.osInterfaces.waitFileExists_timeout(setcmdStanShortCut,60);
                obj.cmdStanShortCut=setcmdStanShortCut;
            end
        end
        
        function value=get.cmdStanShortCut(obj)
            value=obj.cmdStanShortCut;
        end

        function obj=set.Stan_projdir(obj,setStan_projdir)
            if mcmcBayes.osInterfaces.doesFileExist(setStan_projdir)
                obj.Stan_projdir=setStan_projdir;
            else
                error('Stan_projdir is missing!');
            end
        end
        
        function value=get.Stan_projdir(obj)
            value=obj.Stan_projdir;
        end        
                   
        function obj=set.filename_Stanproject(obj,setfilename_Stanproject)
            if mcmcBayes.osInterfaces.doesFileExist(setfilename_Stanproject)
                obj.filename_Stanproject=setfilename_Stanproject;
            else
                error('filename_Stanproject is missing!');
            end
        end
        
        function value=get.filename_Stanproject(obj)
            value=obj.filename_Stanproject;
        end              
        
        function obj=set.Stan_shortcut(obj,setStan_shortcut)
            if mcmcBayes.osInterfaces.doesFileExist(setStan_shortcut)
                obj.Stan_shortcut=setStan_shortcut;
            else
                obj.buildStanProgram();
                mcmcBayes.osInterfaces.waitFileExists_timeout(setStan_shortcut,10);
                obj.Stan_shortcut=setStan_shortcut;
            end
        end
        
        function value=get.Stan_shortcut(obj)
            value=obj.Stan_shortcut;
        end
    end
end