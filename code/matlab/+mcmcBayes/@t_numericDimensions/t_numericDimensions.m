classdef t_numericDimensions < int32
    enumeration
        null (0)
        scalar (1)        
        vector (2)       
        matrix2D (3)
    end
    
    methods (Static)        
        function intvalue=toint(enumobj)
            switch enumobj          
                case {mcmcBayes.t_numericDimensions.null, mcmcBayes.t_numericDimensions.scalar, mcmcBayes.t_numericDimensions.vector, mcmcBayes.t_numericDimensions.matrix2D}
                    intvalue=int32(enumobj);
                otherwise
                    error('unsupported mcmcBayes.t_numericDimensions type');
            end                
        end        
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_numericDimensions.null
                    strvalue='mcmcBayes.t_numericDimensions.null';
                case mcmcBayes.t_numericDimensions.scalar
                    strvalue='mcmcBayes.t_numericDimensions.scalar';
                case mcmcBayes.t_numericDimensions.vector
                    strvalue='mcmcBayes.t_numericDimensions.vector';    
                case mcmcBayes.t_numericDimensions.matrix2D
                    strvalue='mcmcBayes.t_numericDimensions.matrix2D';    
                otherwise
                    error('unsupported mcmcBayes.t_numericDimensions type');
            end
        end

        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_numericDimensions.null)
                    enumobj=mcmcBayes.t_numericDimensions.null;
                case int32(mcmcBayes.t_numericDimensions.scalar)
                    enumobj=mcmcBayes.t_numericDimensions.scalar;
                case int32(mcmcBayes.t_numericDimensions.vector)
                    enumobj=mcmcBayes.t_numericDimensions.vector;
                case int32(mcmcBayes.t_numericDimensions.matrix)
                    enumobj=mcmcBayes.t_numericDimensions.matrix2D;                    
                otherwise
                    error('unsupported mcmcBayes.t_numericDimensions type');
            end
        end
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_numericDimensions.null')==1
                enumobj=mcmcBayes.t_numericDimensions.null;
            elseif strcmp(strval,'mcmcBayes.t_numericDimensions.scalar')==1
                enumobj=mcmcBayes.t_numericDimensions.scalar;
            elseif strcmp(strval,'mcmcBayes.t_numericDimensions.vector')==1
                enumobj=mcmcBayes.t_numericDimensions.vector;
            elseif strcmp(strval,'mcmcBayes.t_numericDimensions.matrix2D')==1
                enumobj=mcmcBayes.t_numericDimensions.matrix2D;
            else
                error('unsupported mcmcBayes.t_numericDimensions type');
            end
        end          
    end    
end