classdef regressionEvaluation < mcmcBayes.sequence
% regressionEvaluation is a class for demonstrating mcmcBayes with a regression models.  
% * linearFlatSimulated - (subtype 'c') uses a flat prior with simulated data for the posterior data
% * linearPresetSimulated - (subtype 'b') uses a preset prior with simulated data for the posterior data
% * linearPresetEmpirical1 - (subtype 'b') uses a preset prior with posterior data obtained from "Regression Analysis", Williams 1959 (Wiley), in a study 
%   of specimens of radiata pine.  The posterior data uses is maximum compressive strength parallel to the grain vs density, while the preset values 
%   for the prior were taken from Han, Carlin 2001.
% * linearPresetEmpirical2 - (subtype 'b') uses a preset prior with posterior data obtained from "Regression Analysis", Williams 1959 (Wiley), in a study 
%   of specimens of radiata pine.  The posterior data uses is maximum compressive strength parallel to the grain vs density adjusted for resin content,
%   while the preset values for the prior were taken from Han, Carlin 2001.
% * linearFlatEmpirical3 - (subtype 'c') uses a flat prior with posterior data obtained from Elster et al 2015, in "A Guide to Bayesian Inference
%   for Regression Problems," as part of a straight line example fit problem.  Elster results were beta0=0.117 and beta1=0.818.
% * polynomialDegree2FlatSimulated - (subtype 'c') uses a flat prior with simulated data for the posterior data
% * polynomialDegree2PresetSimulated - (subtype 'b') uses a preset prior with simulated data for the posterior data
% * polynomialDegree2FlatEmpirical4 - (subtype 'c') uses a flat prior with posterior data obtained from Anscombe 1973, in "Graphs in Statistical Analysis,"
%   as part of a quadratic curve example fit problem (dataset 2).  Polysolve solution (https://arachnoid.com/polysolve/) is beta0=-5.9957, beta1=2.7808, beta2=-0.1267.

% Bayes factor validation can be performed by running linearPresetEmpirical1-2, where, "by using traditional (non-Monte Carlo) numerical integration, 
%   Green and O'Hagan (1998) found that the exact posterior probability for model 1 under the present specification is .29135, which corresponds to a 
%   .70865 posterior probability for model 2 and a Bayes factor of about 4862 in favor of model 2." Han, Carlin 2001.  At present, we're getting 4859 
%   (using only 10 temperature steps), which is very close to 4862.

    properties
        %None
    end
    
    methods (Static)          
        function test
           disp('Hello!'); 
        end
        
        % linear-a (JAGS, BUGS, Stan) - Normal Inverse Gamma priors, unknown precision, hierarchical (i.e., beta0~normal(a,(1/(b*r))^-1), beta1~normal(c,(1/(d*r))^-1), r~gamma(g,h))
        % linear-b (JAGS, BUGS, Stan) - Normal Inverse Gamma priors, unknown precision (i.e., beta0~normal(a,(1/b)^-1), beta1~normal(c,(1/d)^-1), r~gamma(g,h))
        % linear-c (BUGS, Stan)- Flat priors (i.e., beta0~flat, beta1~flat, r~T[0,]*flat)

        % polynomialDegree2-a (JAGS, BUGS, Stan) - Normal Inverse Gamma priors, unknown precision, hierarchical (i.e., beta0~normal(a,(1/(b*r))^-1), beta1~normal(c,(1/(d*r))^-1), beta2~normal(g,(1/(h*r))^-1), r~gamma(p,q))
        % polynomialDegree2-b (JAGS, BUGS, Stan) - Normal Inverse Gamma priors, unknown precision (i.e., beta0~normal(a,(1/b)^-1), beta2~normal(g,(1/h)^-1), r~gamma(p,q))
        % polynomialDegree2-c (BUGS, Stan) - Flat priors (i.e., beta0~flat, beta1~flat, beta2~flat, r~T[0,]*flat)
        
        function [simuuids]=getUuids()
            simuuids=[{'linearFlatSimulated'}, ...              %linear-c
                   {'linearPresetSimulated'}, ...               %linear-b
                   {'linearPresetEmpirical1'}, ...              %linear-b
                   {'linearPresetEmpirical2'}, ...              %linear-b
                   {'linearFlatEmpirical3'}, ...                %linear-c
                   {'polynomialDegree2FlatSimulated'}, ...      %polynomialDegree2-c
                   {'polynomialDegree2PresetSimulated'}, ...    %polynomialDegree2-b
                   {'polynomialDegree2FlatEmpirical4'}];        %polynomialDegree2-c
        end

        function [retSims]=runUnitTests()
            norunpp=false;
            runpp=true;
            dbgmcmc=true;
            nodbgmcmc=false;       
            seqType='regressionEvaluation';
            seqUuid='regressionEvaluation-runUnitTests';
            simuuids=mcmcBayes.regressionEvaluation.getUuids();
            %simOverrides[simuuid, runpp, dbgmcmc, {[samplerType]}]
            simOverrides=[simuuids(1), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %linearFlatSimulated
                          simuuids(2), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %linearPresetSimulated
                          simuuids(3), {runpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ...  %linearPresetEmpirical1
                          simuuids(4), {runpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ...  %linearPresetEmpirical2
                          simuuids(5), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %linearFlatEmpirical3
                          simuuids(6), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %polynomialDegree2FlatSimulated
                          simuuids(7), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %polynomialDegree2PresetSimulated
                          simuuids(8), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %polynomialDegree2FlatEmpirical4
%                           simuuids(1), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %linearFlatSimulated
                          simuuids(2), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %linearPresetSimulated
                          simuuids(3), {runpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ...  %linearPresetEmpirical1
                          simuuids(4), {runpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ...  %linearPresetEmpirical2
%                           simuuids(5), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %linearFlatEmpirical3
%                           simuuids(6), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %polynomialDegree2FlatSimulated
                          simuuids(7), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %polynomialDegree2PresetSimulated
%                           simuuids(8), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %polynomialDegree2FlatEmpirical4
                          simuuids(1), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %linearFlatSimulated
                          simuuids(2), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %linearPresetSimulated
                          simuuids(3), {runpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ...  %linearPresetEmpirical1
                          simuuids(4), {runpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ...  %linearPresetEmpirical2
                          simuuids(5), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %linearFlatEmpirical3
                          simuuids(6), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %polynomialDegree2FlatSimulated
                          simuuids(7), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %polynomialDegree2PresetSimulated
                          simuuids(8), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}];    %polynomialDegree2FlatEmpirical4                              
            for i=1:size(simOverrides,1)
                retSims(i,1)=mcmcBayes.sequence.overrideDefaultsAndRunSingleSimulation(seqType, seqUuid, simOverrides{i,1:4});                      
            end
        end
        
        function getData(type, data, varargin)                                
        % getData(obj, type, data, varargin)
        %   This function generates a simulated dataset using the parameter point estimates from varargin.
            if (data.dataSource~=mcmcBayes.t_dataSource.Simulated) && (data.dataSource~=mcmcBayes.t_dataSource.Postulated)
                error('Requires mcmcBayes.t_dataSource=Simulated or Postulated for creating data.');
            else
                if strcmp(type,'linear') || strcmp(type,'polynomialDegree2')
                    predictionsNSamples=1;
                else
                    error('Unsupported regressionEvaluation sequence type=%s', type);
                end
            end
            
            numdatavars=1;%N(t)
            numgenerateddatasets=size(data.depVarsValues,2);
            dependentVars=cell(numdatavars,numgenerateddatasets);
            
            %set the parameter values for the simulated data
            if strcmp(type,'linear') % varargin=beta0, beta1, r
                beta0=varargin{1};%'beta0','discoveries at y intercept', 'double'
                beta1=varargin{2};%'beta1','security assessment performance slope parameter', 'double'
                r=varargin{3};%'r','precision for zero mean error term', 'double'
                %mean at at t=50 should then be beta0+beta1*50                
                for tdatasetid=1:numgenerateddatasets
                    dependentVars(numdatavars,tdatasetid)=mcmcBayes.model.generateTestData(type, predictionsNSamples, data, beta0, beta1, r);%varargin=beta0, beta1, r
                end
            elseif strcmp(type,'polynomialDegree2') % varargin=beta0, beta1, beta2, r                
                beta0=varargin{1};%'beta0','discoveries at y intercept', 'double'
                beta1=varargin{2};%'beta1','security assessment performance slope parameter', 'double'
                beta2=varargin{3};%'beta2','security assessment performance second degree parameter', 'double'
                r=varargin{4};%'r','precision for zero mean error term', 'double'
                %mean at at t=50 should then be beta0+beta1*50+beta2*50^2                
                for tdatasetid=1:numgenerateddatasets
                    dependentVars(numdatavars,tdatasetid)=mcmcBayes.regression.generateTestData(type, predictionsNSamples, data, beta0, beta1, beta2, r);%varargin=beta0, beta1, beta2, r
                end
            else
                error('regressionEvaluation sequence type is not linear or polynomialDegree2.');
            end
            
            disp(conversionUtilities.matrix_tostring(data.indVarsValues{1,1}));
            disp(conversionUtilities.matrix_tostring(dependentVars{1,1}));     
            
            plot(data.indVarsValues{1,1},dependentVars{1,1});            
        end                             
    end
    
    methods
        function [obj]=regressionEvaluation(cfgMcmcBayes,repositories,sequenceUuid)           
        % obj=regressionModelEvaluation(cfgMcmcBayes,repositories,sequenceUuid)
        %   The constructor for regressionModelEvaluation.
        % Output:
        %   obj Constructed regressionModelEvaluation object
            if nargin > 0
                superargs{1}='regressionEvaluation';
                superargs{2}=cfgMcmcBayes;
                superargs{3}=repositories;
                superargs{4}=sequenceUuid;
            elseif nargin == 0
                superargs={};
            end
            obj=obj@mcmcBayes.sequence(superargs{:});            
        end        
    end
    
    methods (Static, Access=private)     
   
    end    
    
    methods %getter/setter functions

    end
end