classdef data     
%Class used to store model data values and related details (independent/dependent variable description and values, source, usage, references, etc).
    properties
        type%mcmcBayes.t_data (priordata, posteriordata, priorbasedpredictions, posteriorbasedpredictions)
        dataSource%mcmcBayes.t_dataSource (none, preset, empirical, simulated, postulated, EJ*, PredictedFrom*, ...)

        uuid
        description
        author%author for the data file
        reference%reference for the data file
        
        indVarsName%cell array of names for the data, size [numIndVars x 1] with a {string} for each element
        indVarsDesc%cell array of descriptions for the data, size [numIndVars x 1] with a {string} for each element
        indVarsType%cell array of strings describing the data type (e.g., 'int32', 'double', 'categorical'), size [numIndVars x 1] with a {string} for each element
        indVarsDimensions%array of mcmcBayes.t_numericDimensions, size [numIndVars x 1]
        indVarsRange=mcmcBayes.t_numericRange.null;%array of mcmcBayes.t_numericRange, size [numIndVars x 1]
        indVarsRes=mcmcBayes.t_numericResolution.null%array of mcmcBayes.t_numericResolution, size [numIndVars x 1]
        indVarsTfPre%cell array of mcmcBayes.t_dataTransformPre and mcmcBayes.t_dataTransformPre_ext elements, size [numIndVars x 1]
        indVarsTfPost%cell array of mcmcBayes.t_dataTransformPost and mcmcBayes.t_dataTransformPost_ext elements, size [numIndVars x 1]
        indVarsValues%cell array with the data values, size [numIndVars x 1] with a {valuesArray} for each element that is size [Tau x 1 x 1] for scalars,
            %[Nx x Tau x 1] for vectors, and [Nx x Ny x Tau] for 2d matrices
        indVarsOrigMeans%array of mean of the original indVarsValues (before they were transformed), size [numIndVars x 1] with a {meanArray} for 
            %each element that is size [1] for scalars, [Nx x 1] for vectors, and ??? for 2d matrices
        indVarsOrigStd%array of std of the original indVarsValues (before they were transformed), size [numIndVars x 1] with a {stdArray} for 
            %each element that is size [1] for scalars, [Nx x 1] for vectors, and ??? for 2d matrices
        
        %see documentation for independent data variables above
        depVarsName
        depVarsDesc
        depVarsType
        depVarsDimensions
        depVarsRange=mcmcBayes.t_numericRange.null;
        depVarsRes=mcmcBayes.t_numericResolution.null
        depVarsTfPre
        depVarsTfPost
        depVarsValues
        depVarsOrigMeans
        depVarsOrigStd
    end
    
    methods (Static)
        function [retData]=getDataFromXML(sequence, dataEntryUuid, type, predictionsToCapture)        
            %get the xml data            
            for i=1:size(sequence.repositories,1)
                inputDataDir=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s', sequence.repositories{i}, ...
                    mcmcBayes.osInterfaces.getSubPath(mcmcBayes.t_paths.inputDataDir)));
                tmpfilename_data=sprintf('%s%s_dataLUT.xml',inputDataDir,dataEntryUuid);

                if mcmcBayes.osInterfaces.doesFileExist(tmpfilename_data) %is a file
                    break;
                elseif i==size(sequence.repositories,1)
                    error('Did not locate simulation configuration file.');                    
                end
            end
            filename_data=tmpfilename_data;
            
            if mcmcBayes.osInterfaces.doesFileExist(filename_data)
                definputDataDir=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s', sequence.repositories{1}, ...
                    mcmcBayes.osInterfaces.getSubPath(mcmcBayes.t_paths.inputDataDir)));
                filename_datadtd=sprintf('%sdataEntry.dtd',definputDataDir);               
                tdataEntryStruct=mcmcBayes.xml2DataEntriesStruct(filename_data, filename_datadtd);
            end            

            tdatasetStruct=tdataEntryStruct.retData;
            
            tretData=mcmcBayes.data.constructData(tdataEntryStruct, tdatasetStruct, type);
            switch type
                case {mcmcBayes.t_data.priorData, mcmcBayes.t_data.posteriorData}
                    retData=tretData;
                case {mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_data.posteriorPredictedData}
                    %replicate the predicted data for the predictions to capture
                    for element=predictionsToCapture
                        typestr=replace(mcmcBayes.t_dataResult.tostring(element),'mcmcBayes.t_dataResult','');
                        evalstr=sprintf('retData%s=tretData;', typestr);
                        eval(evalstr);
                    end
            end
        end
        
        function [retValues]=rawTransformedDataValuesPost(tfPost, type, values, varargin)
            %varargin is optional and could be (in order) {}, {mean}, {mean std}
            retValues=values;
            switch tfPost%just use the first datapoint (they will all be the same)
                case mcmcBayes.t_dataTransformPost.fromlog10
                    retValues=10.^retValues;
                case mcmcBayes.t_dataTransformPost.fromln
                    retValues=exp(retValues);
                case mcmcBayes.t_dataTransformPost.fromNormalized
                    retValues=retValues*varargin{2}+varargin{1};
                case mcmcBayes.t_dataTransformPost.fromlog10Normalized
                    retValues=retValues*varargin{2}+varargin{1};
                    retValues=10.^retValues;
                case mcmcBayes.t_dataTransformPost.fromMeanCentered
                    retValues=retValues+varargin{1};                                
                case mcmcBayes.t_dataTransformPost.none
                    %retValues
                otherwise
                    error('Unsupported mcmcBayes.t_dataTransformPost.');
            end
            %type cast to original type
            if strcmp(type,'categorical')==1
                retValues=categorical(retValues);
            else
                retValues=cast(retValues,type);
            end
        end
    end
    
    methods (Static, Access=protected)
        function [data]=constructData(dataEntryStruct, datasetStruct, type)
            if datasetStruct.datasource==mcmcBayes.t_dataSource.Preset || datasetStruct.datasource==mcmcBayes.t_dataSource.None
                uuid=dataEntryStruct.uuid;
                description=dataEntryStruct.description;
                author=dataEntryStruct.author;
                reference=dataEntryStruct.reference;               
                
                indVarsName={''};
                indVarsDesc={''};
                indVarsType={'double'};
                indVarsRange=mcmcBayes.t_numericRange.null;
                indVarsRes=mcmcBayes.t_numericResolution.null;
                indVarsDimensions=mcmcBayes.t_numericDimensions.null;
                indVarsTfPre=mcmcBayes.t_variableTransformPre.none;
                indVarsTfPost=mcmcBayes.t_variableTransformPost.none;
                indVarsOrigMeans={};
                indVarsOrigStd={};
                indVarsValues={};
                
                depVarsName={''};
                depVarsDesc={''};
                depVarsType={'double'};
                depVarsRange=mcmcBayes.t_numericRange.null;
                depVarsRes=mcmcBayes.t_numericResolution.null;
                depVarsDimensions=mcmcBayes.t_numericDimensions.null;
                depVarsTfPre=mcmcBayes.t_variableTransformPre.none;
                depVarsTfPost=mcmcBayes.t_variableTransformPost.none;
                depVarsOrigMeans={};
                depVarsOrigStd={};
                depVarsValues={};
            else
                uuid=dataEntryStruct.uuid;
                description=dataEntryStruct.description;
                author=dataEntryStruct.author;
                reference=dataEntryStruct.reference;
                
                %get the independent variables
                numIndDataVars=datasetStruct.independent.numVars;                
                indVarsValues=cell(numIndDataVars, 1);
                indVarsOrigMeans=cell(numIndDataVars, 1);
                indVarsOrigStd=cell(numIndDataVars, 1);
                indVarsName=cell(numIndDataVars,1);
                indVarsDesc=cell(numIndDataVars,1);
                indVarsType=cell(numIndDataVars,1);
                if(numIndDataVars==0)%need to create some empty object arrays for types without indDataVars
                    indVarsValues=cell.empty(numIndDataVars, 1);
                    indVarsOrigMeans=cell.empty(numIndDataVars, 1);
                    indVarsOrigStd=cell.empty(numIndDataVars, 1);
                    indVarsName=cell.empty(numIndDataVars,1);
                    indVarsDesc=cell.empty(numIndDataVars,1);
                    indVarsType=cell.empty(numIndDataVars,1);
                    indVarsRange=mcmcBayes.t_numericRange.null.empty(numIndDataVars,1);
                    indVarsRes=mcmcBayes.t_numericResolution.null.empty(numIndDataVars,1);
                    indVarsDimensions=mcmcBayes.t_numericDimensions.null;
                    indVarsTfPre=mcmcBayes.t_dataTransformPre.none.empty(numIndDataVars,1);
                    indVarsTfPost=mcmcBayes.t_dataTransformPost.none.empty(numIndDataVars,1);
                else
                    for datavarid=1:numIndDataVars
                        indVarsName(datavarid,1)={datasetStruct.independent.variables(datavarid).name};
                        indVarsDesc(datavarid,1)={datasetStruct.independent.variables(datavarid).description};
                        indVarsType(datavarid,1)={datasetStruct.independent.variables(datavarid).type};
                        indVarsRange(datavarid,1)=datasetStruct.independent.variables(datavarid).range;
                        indVarsRes(datavarid,1)=datasetStruct.independent.variables(datavarid).resolution;
                        indVarsDimensions(datavarid,1)=datasetStruct.independent.variables(datavarid).dimensions;
                        indVarsTfPre(datavarid,1)={datasetStruct.independent.variables(datavarid).tfPre};%because of t_dataTransformPre_ext, could have mixed types so use cell-array
                        indVarsTfPost(datavarid,1)={datasetStruct.independent.variables(datavarid).tfPost};%because of t_dataTransformPost_ext, could have mixed types so use cell-array
                        if ~isempty(datasetStruct.independent.variables(datavarid).values)
%todo: ensure the mean and std are performed appropriate to the size                                    
                            %pre-transform the data
                            switch datasetStruct.independent.variables(datavarid).tfPre
                                case {mcmcBayes.t_dataTransformPre.tolog10, mcmcBayes.t_dataTransformPre.tolog10Normalized}
                                    indVarsValues(datavarid, 1)={log10(datasetStruct.independent.variables(datavarid).values)};
                                case {mcmcBayes.t_dataTransformPre.toln}
                                    indVarsValues(datavarid, 1)={log(datasetStruct.independent.variables(datavarid).values)};
                                case {mcmcBayes.t_dataTransformPre.toNormalized, mcmcBayes.t_dataTransformPre.toMeanCentered, mcmcBayes.t_dataTransformPre.none}
                                    indVarsValues(datavarid, 1)={datasetStruct.independent.variables(datavarid).values};
                                otherwise
                                    %must be a special case handled by model.transformDataPre
                                    indVarsValues(datavarid, 1)={datasetStruct.independent.variables(datavarid).values};
                            end
                        end
 
                        if ~isempty(indVarsValues{datavarid,1})%just check if the first is empty
    %todo: ensure the mean and std are performed appropriate to the size
                            indVarsOrigMeans(datavarid, 1)={max(mean(cast(indVarsValues{datavarid,1},'double')))};
                            indVarsOrigStd(datavarid, 1)={max(std(cast(indVarsValues{datavarid,1},'double')))};
                            switch datasetStruct.independent.variables(datavarid).tfPre
                                case {mcmcBayes.t_dataTransformPre.toNormalized, mcmcBayes.t_dataTransformPre.tolog10Normalized}
                                    indVarsValues(datavarid, 1)={(indVarsValues{datavarid, 1}-indVarsOrigMeans{datavarid})/indVarsOrigStd{datavarid}};
                                case {mcmcBayes.t_dataTransformPre.toMeanCentered}
                                    indVarsValues(datavarid, 1)={(indVarsValues{datavarid, 1}-indVarsOrigMeans{datavarid})};
                            end
                        end
                    end
                end
                
                %get the dependent variables
                numDepDataVars=datasetStruct.dependent.numVars;                     
                depVarsValues=cell(numDepDataVars, 1);
                depVarsOrigMeans=cell(numDepDataVars,1);
                depVarsOrigStd=cell(numDepDataVars,1);
                for datavarid=1:numDepDataVars
                    depVarsName(datavarid,1)={datasetStruct.dependent.variables(datavarid).name};
                    depVarsDesc(datavarid,1)={datasetStruct.dependent.variables(datavarid).description};
                    depVarsType(datavarid,1)={datasetStruct.dependent.variables(datavarid).type};
                    depVarsRange(datavarid,1)=datasetStruct.dependent.variables(datavarid).range;
                    depVarsRes(datavarid,1)=datasetStruct.dependent.variables(datavarid).resolution;
                    depVarsDimensions(datavarid,1)=datasetStruct.dependent.variables(datavarid).dimensions;
                    depVarsTfPre(datavarid,1)={datasetStruct.dependent.variables(datavarid).tfPre};%because of t_dataTransformPre_ext, could have mixed types so use cell-array
                    depVarsTfPost(datavarid,1)={datasetStruct.dependent.variables(datavarid).tfPost};%because of t_dataTransformPost_ext, could have mixed types so use cell-array

                    if ~isempty(datasetStruct.dependent.variables(datavarid).values)
                        %pre-transform the data
                        switch datasetStruct.dependent.variables(datavarid).tfPre
                            case {mcmcBayes.t_dataTransformPre.tolog10, mcmcBayes.t_dataTransformPre.tolog10Normalized}
                                depVarsValues(datavarid, 1)={log10(datasetStruct.dependent.variables(datavarid).values)};
                            case {mcmcBayes.t_dataTransformPre.toln}
                                depVarsValues(datavarid, 1)={log(datasetStruct.dependent.variables(datavarid).values)};
                            case {mcmcBayes.t_dataTransformPre.toNormalized, mcmcBayes.t_dataTransformPre.toMeanCentered, mcmcBayes.t_dataTransformPre.none}
                                depVarsValues(datavarid, 1)={datasetStruct.dependent.variables(datavarid).values};
                            otherwise
                                %must be a special case handled by model.transformDataPre
                                depVarsValues(datavarid, 1)={datasetStruct.dependent.variables(datavarid).values};
                        end
                    end

                    if ~isempty(depVarsValues{datavarid,1})%just check if the first is empty
%todo: ensure the mean and std are performed appropriate to the size
                        depVarsOrigMeans(datavarid, 1)={max(mean(cast(depVarsValues{datavarid,1},'double')))};
                        depVarsOrigStd(datavarid, 1)={max(std(cast(depVarsValues{datavarid,1},'double')))};
                        switch datasetStruct.dependent.variables(datavarid).tfPre
                            case {mcmcBayes.t_dataTransformPre.toNormalized, mcmcBayes.t_dataTransformPre.tolog10Normalized}
                                depVarsValues(datavarid, 1)={(depVarsValues{datavarid, 1}-depVarsOrigMeans{datavarid})/depVarsOrigStd{datavarid}};
                            case {mcmcBayes.t_dataTransformPre.toMeanCentered}
                                depVarsValues(datavarid, 1)={(depVarsValues{datavarid, 1}-depVarsOrigMeans{datavarid})};
                        end
                    end
                end
            end
                
            data=mcmcBayes.data(type, datasetStruct.datasource, uuid, description, author, reference, ...
                indVarsName, indVarsDesc, indVarsType, indVarsRange, indVarsRes, indVarsDimensions, indVarsTfPre, indVarsTfPost, indVarsValues, ...
                indVarsOrigMeans, indVarsOrigStd, depVarsName, depVarsDesc, depVarsType, depVarsRange, depVarsRes, depVarsDimensions, depVarsTfPre, ...
                depVarsTfPost, depVarsValues, depVarsOrigMeans, depVarsOrigStd);
        end
    end
    
    methods 
        function obj = data(settype, setdataSource, setuuid, setdescription, setauthor, setreference, ...
                setindVarsName, setindVarsDesc, setindVarsType, setindVarsRange, setindVarsRes, setindVarsDimensions, setindVarsTfPre, setindVarsTfPost, ...
                setindVarsValues, setindVarsOrigMeans, setindVarsOrigStd, setdepVarsName, setdepVarsDesc, setdepVarsType, ...
                setdepVarsRange, setdepVarsRes, setdepVarsDimensions, setdepVarsTfPre, setdepVarsTfPost, setdepVarsValues, ...
                setdepVarsOrigMeans, setdepVarsOrigStd)
            %for models that do not have indVars, pass in empty values to constructor for related fields (e.g., data for basic Gaussian model)
            if nargin > 0
                obj.type=settype;
                obj.dataSource=setdataSource;
                
                obj.uuid=setuuid;
                obj.description=setdescription;
                obj.author=setauthor;
                obj.reference=setreference;
                
                obj.indVarsName=setindVarsName;
                obj.indVarsDesc=setindVarsDesc;
                obj.indVarsType=setindVarsType;
                obj.indVarsRange=setindVarsRange;
                obj.indVarsRes=setindVarsRes;
                obj.indVarsDimensions=setindVarsDimensions;
                obj.indVarsTfPre=setindVarsTfPre;
                obj.indVarsTfPost=setindVarsTfPost;
                obj.indVarsValues=setindVarsValues;
                obj.indVarsOrigMeans=setindVarsOrigMeans;
                obj.indVarsOrigMeans=setindVarsOrigMeans;
                obj.indVarsOrigStd=setindVarsOrigStd;
                
                obj.depVarsName=setdepVarsName;
                obj.depVarsDesc=setdepVarsDesc;
                obj.depVarsType=setdepVarsType;
                obj.depVarsRange=setdepVarsRange;
                obj.depVarsRes=setdepVarsRes;                
                obj.depVarsDimensions=setdepVarsDimensions;                
                obj.depVarsTfPre=setdepVarsTfPre;
                obj.depVarsTfPost=setdepVarsTfPost;
                obj.depVarsValues=setdepVarsValues;
                obj.depVarsOrigMeans=setdepVarsOrigMeans;
                obj.depVarsOrigStd=setdepVarsOrigStd;
            end
        end
        
        %retDependentVariablesMean - cell array of means of the dependent variables (size number dependent variables)
        function [retDependentVariablesMean] = getDependentVariablesMean(obj)
            for varid=1:size(obj.dependentVars,1)
                retDependentVariablesMean(varid)=mean(cast(mcmcBayesNumeric.toVector(obj.dependentVars(varid,:)),'double'));
            end
        end
        
        function [retValues]=getTransformedDataValuesPost(obj,category,datavarid)
            switch category
                case mcmcBayes.t_dataCategory.independent
                    if ~isempty(obj.indVarsValues)
                        retValues=mcmcBayes.data.rawTransformedDataValuesPost(obj.indVarsTfPost{datavarid}, obj.indVarsType{datavarid}, obj.indVarsValues{datavarid, 1}, ...
                            obj.indVarsOrigMeans{datavarid}, obj.indVarsOrigStd{datavarid});
                    else
                        retValues=[];
                    end
                case mcmcBayes.t_dataCategory.dependent
                    if ~isempty(obj.depVarsValues)
                        retValues=mcmcBayes.data.rawTransformedDataValuesPost(obj.depVarsTfPost{datavarid}, obj.depVarsType{datavarid}, obj.depVarsValues{datavarid, 1}, ...
                            obj.depVarsOrigMeans{datavarid}, obj.depVarsOrigStd{datavarid});
                    else
                        retValues=[];
                    end
                otherwise
                    error('inappropriate mcmcBayes.t_dataCategory.');
            end
        end

        function [retValues]=getRawDataValues(obj,category,datavarid)
            switch category
                case mcmcBayes.t_dataCategory.independent
                    if ~isempty(obj.indVarsValues)
                        retValues=obj.indVarsValues{datavarid, 1};
                    else
                        retValues=[];
                    end
                case mcmcBayes.t_dataCategory.dependent
                    if ~isempty(obj.depVarsValues)
                        retValues=obj.depVarsValues{datavarid, 1};
                    else
                        retValues=[];
                    end
                otherwise
                    error('inappropriate mcmcBayes.t_dataCategory.');
            end
        end
        
%         function [retbool] = checkVarsForInfNanValues(obj)
%             retbool=true;
%             for varid=1:size(obj.independentVars,1)
%                 if (not(isempty(find(isinf(obj.independentVars(varid,:))>0))) || not(isempty(find(isnan(obj.independentVars(varid,:))>0))))
%                     retbool=false;%set the incompatibility
%                 end
%             end
%             for varid=1:size(obj.dependentVars,1)
%                 if (not(isempty(find(isinf(obj.dependentVars(varid,:))>0))) || not(isempty(find(isnan(obj.dependentVars(varid,:))>0))))
%                     retbool=false;%set the incompatibility
%                 end
%             end
%         end
%         
%         function [retbool] = checkDepVarForZeroIntervals(obj, varid)
%             retbool=true;
%             for element=2:size(obj.dependentVars,2)
%                 interval=obj.dependentVars(varid,element)-obj.dependentVars(varid,element-1);
%                 if interval<=0
%                     retbool=false;
%                 end
%             end
%         end        
    end
end