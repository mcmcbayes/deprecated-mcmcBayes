classdef t_dataResult < int32
    enumeration
        hingelo (1)
        mean (2)
        hingehi (3)
    end

    methods (Static)        
        function intvalue=toint(enumobj)
            switch enumobj
                case {mcmcBayes.t_dataResult.hingelo, mcmcBayes.t_dataResult.mean, mcmcBayes.t_dataResult.hingehi}
                    intvalue=int32(enumobj);
                otherwise
                    error('unsupported mcmcBayes.t_dataResult type');
            end
        end
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_dataResult.hingelo
                    strvalue='mcmcBayes.t_dataResult.hingelo';
                case mcmcBayes.t_dataResult.mean
                    strvalue='mcmcBayes.t_dataResult.mean';
                case mcmcBayes.t_dataResult.hingehi
                    strvalue='mcmcBayes.t_dataResult.hingehi';
                otherwise
                    error('unsupported mcmcBayes.t_dataResult type');
            end
        end
        
        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_dataResult.hingelo)
                    enumobj=mcmcBayes.t_dataResult.hingelo;
                case int32(mcmcBayes.t_dataResult.mean)
                    enumobj=mcmcBayes.t_dataResult.mean;
                case int32(mcmcBayes.t_dataResult.hingehi)
                    enumobj=mcmcBayes.t_dataResult.hingehi;
                otherwise
                    error('unsupported mcmcBayes.t_dataResult type');
            end
        end
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_dataResult.hingelo')==1
                enumobj=mcmcBayes.t_dataResult.hingelo;
            elseif strcmp(strval,'mcmcBayes.t_dataResult.mean')==1
                enumobj=mcmcBayes.t_dataResult.mean;
            elseif strcmp(strval,'mcmcBayes.t_dataResult.hingehi')==1
                enumobj=mcmcBayes.t_dataResult.hingehi;
            else
                error('unsupported mcmcBayes.t_dataResult type');
            end
        end        
    end
end
