function [simulation] = xml2SimulationStruct(sequence, xmlfile, dtdfile)
    doc = xmlread(xmlfile);
    fprintf(1,'Parsed %s\n',xmlfile);
    %call our java hacks method to insert the DOCTYPE element
    entityname='simulation';
    xmlStr=XmlHacks.insert(doc, entityname, dtdfile);%call Java function  (XmlHacks must be on javaclasspath)
    %now parse the hacked xml
    doc = XmlHacks.xmlreadstring(xmlStr);
    
    doc.getDocumentElement().normalize();
    root=doc.getDocumentElement();
    rootname=root.getNodeName();
    if ~(strcmp(rootname,'simulation')==1)
        error('Root element should be data');
    end
    
    %get the root attributes
    simulation.uuid=cast(root.getAttribute('uuid'),'char');
    simulation.type=cast(root.getAttribute('type'),'char');
    simulation.subType=cast(root.getAttribute('subType'),'char');

    samplerInterfaceType=mcmcBayes.t_samplerInterfaces.str2enum(cast(root.getAttribute('samplerType'),'char'));    
    switch samplerInterfaceType
        case mcmcBayes.t_samplerInterfaces.OpenBUGS
            samplerInterface=mcmcBayes.OpenBUGS(sequence);
        case mcmcBayes.t_samplerInterfaces.JAGS
            samplerInterface=mcmcBayes.JAGS(sequence);
        case mcmcBayes.t_samplerInterfaces.Stan
            samplerInterface=mcmcBayes.Stan(sequence);
        otherwise
            error('Unsupported mcmcBayes.t_samplerInterfaces.');
    end
    simulation.samplerInterface=samplerInterface;
    
    simulation.xmlDescription=cast(root.getAttribute('description'),'char');
    simulation.xmlAuthor=cast(root.getAttribute('author'),'char');
    simulation.xmlReference=cast(root.getAttribute('reference'),'char');

    %parse the variables
    tList=doc.getElementsByTagName("variables");
    variablesNode=tList.item(0);
    variables.numVars=str2num(cast(variablesNode.getAttribute('numVars'),'char'));
    variables.numChains=str2num(cast(variablesNode.getAttribute('numChains'),'char'));
    variables.defaultSolver=mcmcBayes.t_variablePointEstimateSolver.string2enum(cast(variablesNode.getAttribute('defaultSolver'),'char'));
    vList=variablesNode.getElementsByTagName('variable');
    for varid=1:variables.numVars
        %get the next variable
        clear variable
        vNode=vList.item(varid-1);
        variable.name=cast(vNode.getAttribute('name'),'char');
        variable.description=cast(vNode.getAttribute('description'),'char');
        variable.type=mcmcBayes.t_variable.string2enum(cast(vNode.getAttribute('type'),'char'));
        variable.dimensions=mcmcBayes.t_numericDimensions.string2enum(cast(vNode.getAttribute('dimensions'),'char'));
        variable.id=str2num(cast(vNode.getAttribute('id'),'char'));
        tList=vNode.getElementsByTagName('tfPre');
        tNode=tList.item(0);
        variable.tfPre=mcmcBayes.t_variableTransformPre.string2enum(cast(tNode.getTextContent(),'char'));
        tList=vNode.getElementsByTagName('tfPost');
        tNode=tList.item(0);
        variable.tfPost=mcmcBayes.t_variableTransformPost.string2enum(cast(tNode.getTextContent(),'char'));
        tList=vNode.getElementsByTagName('precision');
        tNode=tList.item(0);
        variable.precision=str2num(cast(tNode.getTextContent(),'char'));
        tList=vNode.getElementsByTagName('priorDistribution');
        tNode=tList.item(0);
        variable.priorDistribution.type=mcmcBayes.t_distribution.string2enum(cast(tNode.getAttribute('type'),'char'));
        variable.priorDistribution.numParams=str2num(cast(tNode.getAttribute('numParams'),'char'));
        hpList=tNode.getElementsByTagName('hyperParameter');  
        for hpid=1:variable.priorDistribution.numParams
            hpNode=hpList.item(hpid-1);
            if ~isempty(hpNode)
                hyperParameter.name=cast(hpNode.getAttribute('name'),'char');
                hyperParameter.description=cast(hpNode.getAttribute('description'),'char');
                hyperParameter.type=cast(hpNode.getAttribute('type'),'char');
                hyperParameter.dimensions=mcmcBayes.t_numericDimensions.string2enum(cast(hpNode.getAttribute('dimensions'),'char'));
                hyperParameter.id=str2num(cast(hpNode.getAttribute('id'),'char'));
                tList=hpNode.getElementsByTagName('range');
                tNode=tList.item(0);
                hyperParameter.range=mcmcBayes.t_numericRange.string2enum(cast(tNode.getTextContent(),'char'));
                tList=hpNode.getElementsByTagName('resolution');
                tNode=tList.item(0);            
                hyperParameter.resolution=mcmcBayes.t_numericResolution.string2enum(cast(tNode.getTextContent(),'char'));
                tList=hpNode.getElementsByTagName('values');
                tNode=tList.item(0);
                if strcmp(hyperParameter.type,'double')==1
                    hyperParameter.values=str2num(cast(tNode.getTextContent(),'char'));
                elseif strcmp(hyperParameter.type,'categorical')==1
                    hyperParameter.values=categorical(eval(cast(tNode.getTextContent(),'char')));
                elseif strcmp(hyperParameter.type,'int32')==1
                    hyperParameter.values=int32(eval(cast(tNode.getTextContent(),'char')));
                else
                    error('Unsupported data conversion.');            
                end                
            else
                hyperParameter.name='Null';
                hyperParameter.description='Null';
                hyperParameter.type='double';
                hyperParameter.dimensions=mcmcBayes.t_numericDimensions.null;
                hyperParameter.range=mcmcBayes.t_numericRange.null;
                hyperParameter.resolution=mcmcBayes.t_numericResolution.null;
                hyperParameter.values=NaN;
            end
            variable.priorDistribution.hyperParameters(hpid,1)=hyperParameter;
        end
        tList=vNode.getElementsByTagName('independent');
        independentNode=tList.item(0);
        if ~isempty(independentNode)
            independent.numDataPoints=str2num(cast(independentNode.getAttribute('numDataPoints'),'char'));
            independent.numVars=str2num(cast(independentNode.getAttribute('numVars'),'char'));
            nList=independentNode.getElementsByTagName('datavariable');
            for ivarid=1:independent.numVars
                ivNode=nList.item(ivarid-1);
                ivariable.name=cast(ivNode.getAttribute('name'),'char');
                ivariable.description=cast(ivNode.getAttribute('description'),'char');
                ivariable.type=cast(ivNode.getAttribute('type'),'char');
                ivariable.dimensions=mcmcBayes.t_numericDimensions.string2enum(cast(ivNode.getAttribute('dimensions'),'char'));
                ivariable.id=str2num(cast(ivNode.getAttribute('id'),'char'));
                tList=ivNode.getElementsByTagName('range');
                tNode=tList.item(0);
                ivariable.range=mcmcBayes.t_numericRange.string2enum(cast(tNode.getTextContent(),'char'));
                tList=ivNode.getElementsByTagName('resolution');
                tNode=tList.item(0);
                ivariable.resolution=mcmcBayes.t_numericResolution.string2enum(cast(tNode.getTextContent(),'char'));                    
                tList=ivNode.getElementsByTagName('values');
                tNode=tList.item(0);
                if strcmp(ivariable.type,'double')==1
                    ivariable.values=str2num(cast(tNode.getTextContent(),'char'));
                elseif strcmp(ivariable.type,'categorical')==1
                    ivariable.values=categorical(eval(cast(tNode.getTextContent(),'char')));
                elseif strcmp(ivariable.type,'int32')==1
                    ivariable.values=int32(eval(cast(tNode.getTextContent(),'char')));
                else
                    error('Unsupported data conversion.');            
                end
                independent.variables(ivarid,1)=ivariable;
            end            
        else
            independent.numDataPoints=0;
            independent.numVars=0;
            ivariable.name='';
            ivariable.description='';
            ivariable.type='';
            ivariable.dimensions=mcmcBayes.t_numericDimensions.null;
            ivariable.id=0;
            ivariable.range=mcmcBayes.t_numericRange.null;
            ivariable.resolution=mcmcBayes.t_numericResolution.null;
            ivariable.values=[];
            independent.variables(1,1)=ivariable;            
        end
        variable.independentVariables=independent.variables;
        tList=vNode.getElementsByTagName('solver');
        solverNode=tList.item(0);
        if ~isempty(solverNode)
            solver.solverOverride=mcmcBayes.t_variablePointEstimateSolver.string2enum(cast(solverNode.getAttribute('solverOverride'),'char'));
            tList=solverNode.getElementsByTagName('min');
            tNode=tList.item(0);
            solver.min=eval(cast(tNode.getTextContent(),'char'));
            tList=solverNode.getElementsByTagName('max');
            tNode=tList.item(0);
            solver.max=eval(cast(tNode.getTextContent(),'char'));
            tList=solverNode.getElementsByTagName('priorInitialSolverStartValue');
            tNode=tList.item(0);
            solver.priorInitialSolverStartValue=eval(cast(tNode.getTextContent(),'char'));
            tList=solverNode.getElementsByTagName('posteriorInitialSolverStartValue');
            tNode=tList.item(0);
            solver.posteriorInitialSolverStartValue=eval(cast(tNode.getTextContent(),'char'));
        else
            solver.solverOverride=mcmcBayes.t_variablePointEstimateSolver.None;
            solver.min=NaN;
            solver.max=NaN;
            solver.priorInitialSolverStartValue=NaN;
            solver.posteriorInitialSolverStartValue=NaN;
        end
        variable.solver=solver;
        variables.variable(varid)=variable;        
    end
    simulation.variables=variables;
    
    tList=doc.getElementsByTagName("mcmcSettings");
    mcmcNode=tList.item(0);
    tList=mcmcNode.getElementsByTagName('numAdaptSteps');
    tNode=tList.item(0);
    simulation.mcmcSettings.numAdaptSteps=int32(str2num(cast(tNode.getTextContent(),'char')));
    tList=mcmcNode.getElementsByTagName('numBurnIn');
    tNode=tList.item(0);
    simulation.mcmcSettings.numBurnIn=int32(str2num(cast(tNode.getTextContent(),'char')));
    tList=mcmcNode.getElementsByTagName('numSamples');
    tNode=tList.item(0);
    simulation.mcmcSettings.numSamples=int32(str2num(cast(tNode.getTextContent(),'char')));
    tList=mcmcNode.getElementsByTagName('numLagSteps');
    tNode=tList.item(0);
    simulation.mcmcSettings.numLagSteps=int32(str2num(cast(tNode.getTextContent(),'char')));
    
    tList=doc.getElementsByTagName("data");
    dataNode=tList.item(0);
    tList=dataNode.getElementsByTagName('priorDataUuid');
    tNode=tList.item(0);
    simulation.data.priorDataUuid=cast(tNode.getTextContent(),'char');
    tList=dataNode.getElementsByTagName('priorPredictedDataUuid');
    tNode=tList.item(0);
    simulation.data.priorPredictedDataUuid=cast(tNode.getTextContent(),'char');
    tList=dataNode.getElementsByTagName('posteriorDataUuid');
    tNode=tList.item(0);
    simulation.data.posteriorDataUuid=cast(tNode.getTextContent(),'char');
    tList=dataNode.getElementsByTagName('posteriorPredictedDataUuid');
    tNode=tList.item(0);
    simulation.data.posteriorPredictedDataUuid=cast(tNode.getTextContent(),'char');
end