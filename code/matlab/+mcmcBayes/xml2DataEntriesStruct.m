function [dataEntry] = xml2DataEntriesStruct(xmlfile, dtdfile)
	doc = xmlread(xmlfile);
    fprintf(1,'Parsed %s\n',xmlfile);
    %javaaddpath(mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.javaDir));
    %call our java hacks method to insert the DOCTYPE element
    entityname='dataEntry';
    xmlStr=XmlHacks.insert(doc, entityname, dtdfile);%call Java function  (XmlHacks must be on javaclasspath)
    %now parse the hacked xml
    doc = XmlHacks.xmlreadstring(xmlStr);   
    
    doc.getDocumentElement().normalize();
    root=doc.getDocumentElement();
    rootname=root.getNodeName();
    if ~(strcmp(rootname,'dataEntry')==1)
        error('Root element should be dataEntry');
    end
    
    %get the root attributes
    dataEntry.uuid=cast(root.getAttribute('uuid'),'char');
    dataEntry.description=cast(root.getAttribute('description'),'char');
    dataEntry.author=cast(root.getAttribute('author'),'char');
    dataEntry.reference=cast(root.getAttribute('reference'),'char');
    
    %get the list of dataset nodes
    dsList=doc.getElementsByTagName("dataset");
    dsNode=dsList.item(0);
    if ~isempty(dsNode) %empty for preset or none prior type
        dataset.datasource=mcmcBayes.t_dataSource.string2enum(cast(dsNode.getAttribute('datasource'),'char'));

        %parse the independent variables
        nList=dsNode.getElementsByTagName("independent");
        nNode=nList.item(0);%will be empty for some sources (e.g., preset prior)
        if ~isempty(nNode)
            independent.numVars=str2num(cast(nNode.getAttribute('numVars'),'char'));
            nList=nNode.getElementsByTagName('variable');
            for varid=1:independent.numVars
                vNode=nList.item(varid-1);
                variable.name=cast(vNode.getAttribute('name'),'char');
                variable.type=cast(vNode.getAttribute('type'),'char');
                variable.description=cast(vNode.getAttribute('description'),'char');
                variable.dimensions=mcmcBayes.t_numericDimensions.string2enum(cast(vNode.getAttribute('dimensions'),'char'));
                variable.id=str2num(cast(vNode.getAttribute('id'),'char'));
                tList=vNode.getElementsByTagName('tfPre');
                tNode=tList.item(0);
                variable.tfPre=mcmcBayes.t_dataTransformPre.string2enum(cast(tNode.getTextContent(),'char'));
                tList=vNode.getElementsByTagName('tfPost');
                tNode=tList.item(0);
                variable.tfPost=mcmcBayes.t_dataTransformPost.string2enum(cast(tNode.getTextContent(),'char'));                        
                tList=vNode.getElementsByTagName('range');
                tNode=tList.item(0);
                variable.range=mcmcBayes.t_numericRange.string2enum(cast(tNode.getTextContent(),'char'));
                tList=vNode.getElementsByTagName('resolution');
                tNode=tList.item(0);
                variable.resolution=mcmcBayes.t_numericResolution.string2enum(cast(tNode.getTextContent(),'char'));    
                tList=vNode.getElementsByTagName('values');
                tNode=tList.item(0);
                if strcmp(variable.type,'double')==1
                    variable.values=str2num(cast(tNode.getTextContent(),'char'));
                elseif strcmp(variable.type,'categorical')==1
                    variable.values=categorical(eval(cast(tNode.getTextContent(),'char')));
                elseif strcmp(variable.type,'int32')==1
                    variable.values=int32(eval(cast(tNode.getTextContent(),'char')));
                else
                    error('Unsupported data conversion.');            
                end
                if isempty(variable.values)
                    error('Issue with data values');
                end                    
                independent.variables(varid,1)=variable;
            end
        else
            independent.numVars=0;
            variable.name='';
            variable.type='';
            variable.description='';
            variable.dimensions=mcmcBayes.t_numericDimensions.null;
            variable.id=0;
            variable.tfPre=mcmcBayes.t_dataTransformPre.none;
            variable.tfPost=mcmcBayes.t_dataTransformPost.none;
            variable.range=mcmcBayes.t_numericRange.null;
            variable.resolution=mcmcBayes.t_numericResolution.null;
            variable.values=[];
            independent.variables(1,1)=variable;
        end
        dataset.independent=independent;

        %parse the dependent variables
        nList=dsNode.getElementsByTagName("dependent");
        nNode=nList.item(0);
        if ~isempty(nNode)
            dependent.numVars=str2num(cast(nNode.getAttribute('numVars'),'char'));
            nList=nNode.getElementsByTagName('variable');
            for varid=1:dependent.numVars
                vNode=nList.item(varid-1);
                variable.name=cast(vNode.getAttribute('name'),'char');
                variable.type=cast(vNode.getAttribute('type'),'char');
                variable.description=cast(vNode.getAttribute('description'),'char');
                variable.dimensions=mcmcBayes.t_numericDimensions.string2enum(cast(vNode.getAttribute('dimensions'),'char'));
                variable.id=str2num(cast(vNode.getAttribute('id'),'char'));
                tList=vNode.getElementsByTagName('tfPre');
                tNode=tList.item(0);
                if ~isempty(tNode)
                    variable.tfPre=mcmcBayes.t_dataTransformPre.string2enum(cast(tNode.getTextContent(),'char'));
                else
                    variable.tfPre=mcmcBayes.t_dataTransformPre.none;
                end
                tList=vNode.getElementsByTagName('tfPost');
                tNode=tList.item(0);
                if ~isempty(tNode)
                    variable.tfPost=mcmcBayes.t_dataTransformPost.string2enum(cast(tNode.getTextContent(),'char'));                  
                else
                    variable.tfPost=mcmcBayes.t_dataTransformPost.none;
                end
                tList=vNode.getElementsByTagName('range');
                tNode=tList.item(0);
                variable.range=mcmcBayes.t_numericRange.string2enum(cast(tNode.getTextContent(),'char'));
                tList=vNode.getElementsByTagName('resolution');
                tNode=tList.item(0);
                variable.resolution=mcmcBayes.t_numericResolution.string2enum(cast(tNode.getTextContent(),'char'));
                tList=vNode.getElementsByTagName('values');
                tNode=tList.item(0);
                if ~isempty(tNode)
                    if strcmp(variable.type,'double')==1
                        variable.values=str2num(cast(tNode.getTextContent(),'char'));
                    elseif strcmp(variable.type,'int32')==1
                        variable.values=cast(str2num(cast(tNode.getTextContent(),'char')),'int32');
                    else
                        error('Unsupported data conversion.');
                    end
                    if isempty(variable.values)
                        error('Issue with data values');
                    end
                else
                    variable.values=[];%empty (should be because this data entry is type t_data.priorPredictedData or t_data.posteriorPredictedData)
                end
                dependent.variables(varid,1)=variable;       
            end
        else
            dependent.numVars=0;
            variable.name='';
            variable.type='';
            variable.description='';
            variable.dimensions=mcmcBayes.t_numericDimensions.null;
            variable.id=0;
            variable.tfPre=mcmcBayes.t_dataTransformPre.none;
            variable.tfPost=mcmcBayes.t_dataTransformPost.none;
            variable.range=mcmcBayes.t_numericRange.null;
            variable.resolution=mcmcBayes.t_numericResolution.null;
            variable.values=[];
            dependent.variables(1,1)=variable;
        end
        dataset.dependent=dependent;
    end
    dataEntry.retData=dataset;    
end