classdef simulation < matlab.mixin.Heterogeneous
    % simulation is the base class for the model to be simulated and it includes everything that is necessary to perform a simulation  
    properties (Access=public)
        runView                         %Controls opening the MATLAB desktop for forked sessions and inhibiting MATLAB from grabbing focus (e.g., t_runView.{Foreground,Background})
        filenamePrefix                  %Prefix used when naming generated files.
        doMsfe
        
        osInterface                     %Instantiated mcmcBayes.osInterfaces object (e.g., mcmcBayes.{UbuntuLinux,Windows7,MacOS})
        samplerInterface                %Instantiated mcmcBayes.samplerInterfaces object (e.g., mcmcBayes.{OpenBUGS,JAGS,Stan})
        
        xmlDescription                  %simulation xml file description
        xmlAuthor                       %simulation xml file author
        xmlReference                    %simulation xml file reference
        
        uuid                            %simulation unique identifier 
        type                            %simulation model type (e.g., mcmcBayes.{Gaussian,Linear,Verhulst,Gompertz,PolynomialDegree2,BrooksMotley,GoelOkumoto,Goel,MusaOkumoto,YamadaOhbaOsaki,KuoGhosh})
        subType                         %simulation model subType that identifies the combined model structure type & prior distribution assumption and is a character appended to the model name (e.g., 'a', 'b', 'c')        
        name                            %simulation model name 
        description                     %simulation model description 
        author                          %simulation model author 
        reference                       %simulation model reference 

        predictionsToCapture            %Bin thresholds that are measured for observables (e.g., 5th, 50th, and 95th percentiles)
        predictionsHingeThreshold       %Threshold used in the histogram analysis of predictions to collect the range of sample values within the inner two quartiles        
        
        priorVariables                  %Array of instantiated mcmcBayes.variable objects that is indexed by VARID x CHAINID
        priorData                       %Instantiated mcmcBayes.data object for the prior's data
        priorPredictedData              %Instantiated mcmcBayes.data object for the prior-based observables

        priorMsfe                       %Mean square forecasting error for prior-based observables
        
        posteriorVariables              %Array of instantiated mcmcBayes.variable objects that is indexed by VARID x CHAINID x TEMPID
        posteriorData                   %Instantiated mcmcBayes.data object for the posterior's data
        posteriorPredictedData          %Instantiated mcmcBayes.data object for the posterior-based observables

        posteriorMsfe                   %Mean square forecasting error for posterior-based observables
        posteriorMarginalLogLikelihood  %Marginal loglikelihood of posterior given data that is computed using power-posterior method (Friel et al. 2008)
        posteriorMarginalLogLikelihoodAlt   %Bias reduced alternate form (Friel et al. 2013)
        powerPosteriorMce               %Total monte carlo error for the power-posterior estimate (Friel et al. 2008)
        powerPosteriorN                 %Power-posterior method's number of temperature iterations to perform for marginal loglikelihood estimation (Friel et al. 2008)
        powerPosteriorK                 %Power-posterior method's near zero compression factor (ensures higher sampling entropy nearer to the prior) (Friel et al. 2008)
        klDistance                      %Kullback-Liebler distance measure (Friel et al. 2008)
    end       
    
    methods (Static)       
        function test()
            disp('Hello!');
        end    
        
        function [retobj]=loadArchive(filename)
        % [retobj]=loadArchive(filename)
        %   Loads the archived filename and returns its object (that should be a sequence object)
            stdout=1;
            archivedir=mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.archivesDir);
            tfile=sprintf('%s%s',archivedir,filename);
            if (mcmcBayes.osInterfaces.doesFileExist(tfile))%the kuo-ghosh model is not valid for some datasets
                S=load(tfile);%setup variables
                if isa(S.obj,'mcmcBayes.simulation')
                    retobj=S.obj;
                else
                    error('Not a mcmcBayes.simulation object.');
                end
            else
                fprintf(stdout, 'File does not exist here: %s\n', tfile);
                retobj=[{}];
            end
        end        
        
        function handle=getHandleDetermineSolverInits(strVal)
            classStr=strcat('mcmcBayes.',strVal);
            if (exist(classStr)==8)%is it a valid class on the path
                evalStr=sprintf('handle=@%s.determineSolverInits;',classStr);%construct the expression to get the handle
                eval(evalStr);%get the handle
            else
                error('not a valid class on the path.');
            end
        end             
        
        function handle=getHandleConstructor(strVal)
            classStr=strcat('mcmcBayes.',strVal);
            if (exist(classStr)==8)%is it a valid class on the path
                evalStr=sprintf('handle=@%s;',classStr);%construct the expression to get the handle
                eval(evalStr);%get the handle
            else
                error('not a valid class on the path.');
            end
        end    
        
        function handle=getHandleVariablePointEstimateSolver(strVal, solverType)
            classStr=strcat('mcmcBayes.',strVal);
            if (exist(classStr)==8)%is it a valid class on the path
                strSolverType=strrep(mcmcBayes.t_variablePointEstimateSolver.tostring(solverType),'mcmcBayes.t_variablePointEstimateSolver.','');
                evalStr=sprintf('handle=@%s.%s;', classStr, strSolverType);%construct the expression to get the handle
                eval(evalStr);%get the handle
            else
                error('not a valid class on the path.');
            end
        end      
        
        function handle=getHandleDefaultDataVariableNames(strVal)
            classStr=strcat('mcmcBayes.',strVal);
            if (exist(classStr)==8)%is it a valid class on the path
                evalStr=sprintf('handle=@%s.getDefaultDataVariableNames;',classStr);%construct the expression to get the handle
                eval(evalStr);%get the handle
            else
                error('not a valid class on the path.');
            end
        end            
        
        function handle=getHandleTransformDataPre(strVal)
            classStr=strcat('mcmcBayes.',strVal);
            if (exist(classStr)==8)%is it a valid class on the path
                evalStr=sprintf('handle=@%s.transformDataPre;',classStr);%construct the expression to get the handle
                eval(evalStr);%get the handle
            else
                error('not a valid class on the path.');
            end
        end           

        function handle=getHandleTransformVarsPre(strVal)
            classStr=strcat('mcmcBayes.',strVal);
            if (exist(classStr)==8)%is it a valid class on the path
                evalStr=sprintf('handle=@%s.transformVarsPre;',classStr);%construct the expression to get the handle
                eval(evalStr);%get the handle
            else
                error('not a valid class on the path.');
            end
        end           

        function handle=getHandleGenerateTestData(strVal)
            classStr=strcat('mcmcBayes.',strVal);
            if (exist(classStr)==8)%is it a valid class on the path
                evalStr=sprintf('handle=@%s.generateTestData;',classStr);%construct the expression to get the handle
                eval(evalStr);%get the handle
            else
                error('not a valid class on the path.');
            end
        end          

        function handle=getHandleSimPredictions(strVal)
            classStr=strcat('mcmcBayes.',strVal);
            if (exist(classStr)==8)%is it a valid class on the path
                evalStr=sprintf('handle=@%s.simPredictions;',classStr);%construct the expression to get the handle
                eval(evalStr);%get the handle
            else
                error('not a valid class on the path.');
            end
        end          
        
        function handle=getHandleComputeLogLikelihood(strVal)
            classStr=strcat('mcmcBayes.',strVal);
            if (exist(classStr)==8)%is it a valid class on the path
                superClassStrs=superclasses(classStr);
                evalStr=sprintf('handle=@%s.computeLogLikelihood;',superClassStrs{1});%construct the expression to get the handle
                eval(evalStr);%get the handle
            else
                error('not a valid class on the path.');
            end
        end                    
        
%todo: update me        
        function [retObj, phi]=rewindMCMCpre(type, subType, priordatatype, datatype, datasetid, tempid, samplertype)
        % [retobj, phi]=rewindMCMCpre(modeltype, priordatatype, datatype, datasetid, tempid, samplertype)
        %   Loads a previous state prior to performing MCMC sampling for troubleshooting any errors occurring during sampling.  The items saved in
        %   performMCMCSamplingSequence() that are loaded here include the simulation object and the power posterior phi variable.
        %   Input:
        %     modeltype - Specifies the model to load for
        %     priordatatype - t_simulationData type that specifies priorDependentVariables (e.g., CCMPerformanceWeighting, EqualWeighting, Individual, and Simulated)
        %     to load for
        %     datatype - t_simulationData type that specifies dependentVariables (e.g., ConFR, LinIFR, ExpIFR, LinDFR, ExpDFR, and Simulated) to load for
        %     datasetid - Specifies the dataset identifier to load for
        %     tempid - Specifies the temperature id to load for
        %     samplertype-t_simulationSamplerInterfaces type that specifies either OpenBUGS or JAGS
        %   Output:
        %     retobj Loaded simulation model object (state of the simulation object prior to the sequence iteration)
        %     phi Loaded value for the power-posterior phi variable for the sequence iteration
            filename=sprintf('%stypeid%d%spriordatatypeid%ddatatypeid%ddatasetid%dtempid%dsamplertypeid%dpreMCMC.mat', mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.archivesDir), ...
                t_simulation.toint(type), subType, t_simulationData.toint(priordatatype), t_simulationData.toint(datatype), datasetid, tempid, t_simulationSamplerInterfaces.toint(samplertype));

            if mcmcBayes.osInterfaces.doesFileExist(filename)
                S=load(filename);%setup variables
                retObj=S.obj;
                phi=S.phi;%setup variables
            else
                warning('Filename ''%s'' does not exist.',filename);
                retObj=-1;
            end
        end

%todo: update me        
        function [retObj, phi]=rewindMCMCpost(type, subType, priordatatype, datatype, datasetid, tempid, samplertype)
        % [retobj, phi]=rewindMCMCpost(modeltype, priordatatype, datatype, datasetid, tempid, samplertype)
        %   Loads a previous state prior to performing MCMC sampling for troubleshooting any errors occurring during sampling.  The items saved in
        %  performMCMCSamplingSequence() that are loaded here include the simulation object and the power posterior phi variable.
        %   Input:
        %     modeltype - Specifies the model to load for
        %     priordatatype - t_simulationData type that specifies priorDependentVariables (e.g., CCMPerformanceWeighting, EqualWeighting, Individual, and Simulated)
        %     to load for
        %     datatype - t_simulationData type that specifies dependentVariables (e.g., ConFR, LinIFR, ExpIFR, LinDFR, ExpDFR, and Simulated) to load for
        %     datasetid - Specifies the dataset identifier to load for
        %     tempid - Specifies the temperature id to load for
        %     samplertype-t_simulationSamplerInterfaces type that specifies either OpenBUGS or JAGS
        %   Output:
        %     retobj Loaded simulation model object (state of the simulation object prior to the sequence iteration)
        %     phi Loaded value for the power-posterior phi variable for the sequence iteration
            filename=sprintf('%stypeid%d%spriordatatypeid%ddatatypeid%ddatasetid%dtempid%dsamplertypeid%dpostMCMC.mat', mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.archivesDir), ...
                t_simulation.toint(type), subType, t_simulationData.toint(priordatatype), t_simulationData.toint(datatype), datasetid, tempid, t_simulationSamplerInterfaces.toint(samplertype));

            if mcmcBayes.osInterfaces.doesFileExist(filename)
                S=load(filename);%setup variables
                obj=S.obj;
                phi=S.phi;%setup variables       
                
                for tvarid=1:varid%main sampling sequence loop for each of the modelVariables
                    obj.samplerInterface.filename_CODAindex=simulationosInterfaces.convPathStr(sprintf('%smodeltypeid%d%spriordatatypeid%ddatatypeid%ddatasetid%dvarid%dtempid%dsamplertypeid%dCODAindex.txt', ...
                        simulationosInterfaces.getPath(t_simulationPaths.CODA_DIR), t_simulation.toint(type), subType, t_simulationData.toint(priordatatype), ...
                        t_simulationData.toint(datatype), datasetid, varid, tempid, t_simulationSamplerInterfaces.toint(samplertype)));%use the MCMC chain for final parameter
                    obj.samplerInterface.filename_CODAchain=simulationosInterfaces.convPathStr(sprintf('%smodeltypeid%d%spriordatatypeid%ddatatypeid%ddatasetid%dvarid%dtempid%dsamplertypeid%dCODAchain1.txt', ...
                        simulationosInterfaces.getPath(t_simulationPaths.CODA_DIR), t_simulation.toint(type), subType, t_simulationData.toint(priordatatype), ...
                        t_simulationData.toint(datatype), datasetid, varid, tempid, t_simulationSamplerInterfaces.toint(samplertype)));%use the MCMC chain for final parameter                   
                    [obj.samplerInterface, fieldindices, data]=obj.samplerInterface.readSamples();%load the CODA results
                    idx=find(fieldindices==varid);%get the appropriate index for varid
                    obj.modelVariables(varid,tempid).MCMC_samples=data(idx);%save the samples for varid
                    obj.modelVariables(varid,tempid)=obj.modelVariables(varid,tempid).updateStatistics();%update the variable stats for modelVariables(varid,tempid)
                end
                retObj=obj;
            else
                warning('Filename ''%s'' does not exist.',filename);
                retObj=-1;
            end
        end               
               
        function [phi]=getDefaultPhi(tempid)
        % [phi]=getDefaultPhi(tempid)
            ppN=mcmcBayes.simulation.getDefaultPowerPosteriorN();
            ppK=mcmcBayes.simulation.getDefaultPowerPosteriorK();
            phi=mcmcBayes.simulation.getPhi(tempid, ppN, ppK);
        end

        function [phi]=getPhi(tempid, ppN, ppK)
        % [phi]=getPhi(tempid, powerposterior_n, powerposterior_k)
        %   Returns the power-posterior value phi (i.e., the current discretization step), for the specified temperature identifier
            phi=((cast(tempid,'double')-1)/cast(ppN,'double'))^cast(ppK,'double');%setup phi
        end        
        
        function [ppN]=getDefaultPowerPosteriorN()
        % [powerposterior_n]=getDefaultpowerposterior_n()
        %   Returns the power-posterior constant n, that is used in determining a temperature schedule with equal spacing of the n points in the 
        %   interval [0, 1]
            ppN=double(30);
        end
        
        function [ppK]=getDefaultPowerPosteriorK()
        % [powerposterior_k]=getDefaultpowerposterior_k()
        %   Returns the power-posterior constant k, that is used in the computations of phi to ensure the values of phi are chosen with higher frequency 
        %   for temperature identifiers closer to zero
            ppK=double(5);
        end
        
        function [predictionsToCapture]=getDefaultPredictionsToCapture()
            predictionsToCapture=[mcmcBayes.t_dataResult.hingelo, mcmcBayes.t_dataResult.mean, mcmcBayes.t_dataResult.hingehi];
        end
        
        function [simulation]=getSimulationFromXML(sequence, simulationUuid, predictionsToCapture, init)                           
            stdout=1;
            %get the xml data            
            for i=1:size(sequence.repositories,1)
                configSimsDir=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s', sequence.repositories{i}, ...
                    mcmcBayes.osInterfaces.getSubPath(mcmcBayes.t_paths.configSimsDir)));
                tmpfilename_simulation=sprintf('%s%s_simulationLUT.xml',configSimsDir,simulationUuid);

                if mcmcBayes.osInterfaces.doesFileExist(tmpfilename_simulation) %is a file
                    break;
                elseif i==size(sequence.repositories,1)
                    error('Did not locate simulation configuration file.');                    
                end
            end
            filename_simulation=tmpfilename_simulation;
            
            if mcmcBayes.osInterfaces.doesFileExist(filename_simulation)         
                configSimsDir=mcmcBayes.osInterfaces.convPathStr(sprintf('%s', mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.configSimsDir)));               
                filename_simulationdtd=sprintf('%ssimulation.dtd',configSimsDir);               
                simulationStruct=mcmcBayes.xml2SimulationStruct(sequence, filename_simulation, filename_simulationdtd);
            end
            
            priorData=mcmcBayes.data.getDataFromXML(sequence, simulationStruct.data.priorDataUuid, mcmcBayes.t_data.priorData, predictionsToCapture);
            priorPredictedData=mcmcBayes.data.getDataFromXML(sequence, simulationStruct.data.priorPredictedDataUuid, mcmcBayes.t_data.priorPredictedData, predictionsToCapture);
            posteriorData=mcmcBayes.data.getDataFromXML(sequence, simulationStruct.data.posteriorDataUuid, mcmcBayes.t_data.posteriorData, predictionsToCapture);
            posteriorPredictedData=mcmcBayes.data.getDataFromXML(sequence, simulationStruct.data.posteriorPredictedDataUuid, mcmcBayes.t_data.posteriorPredictedData, predictionsToCapture);
                  
            hashStr=num2str(string2hash(sprintf('%s%d',sequence.type, mcmcBayes.t_samplerInterfaces.toint(simulationStruct.samplerInterface.type))));
            setPrefix=strcat(sequence.uuid, '-' , hashStr);
            if (init==true)
                fprintf(stdout,'Hash=%s created using sequenceType=%s + samplerType=%s.\n', hashStr, sequence.type, ...
                    mcmcBayes.t_samplerInterfaces.tostring(simulationStruct.samplerInterface.type));
            end
            
            handle=mcmcBayes.simulation.getHandleConstructor(simulationStruct.type);
            evalstr='simulation=handle(simulationStruct, setPrefix, sequence.cfgMcmcBayes.cfgSimulation.computeMsfe);';
            eval(evalstr);
            
            simulation.osInterface=sequence.osInterface;
            simulation.samplerInterface=simulationStruct.samplerInterface;
            simulation.runView=sequence.cfgMcmcBayes.cfgSimulation.runView;

            simulation.uuid=simulationStruct.uuid;
            simulation.xmlDescription=simulationStruct.xmlDescription;
            simulation.xmlAuthor=simulationStruct.xmlAuthor;
            simulation.xmlReference=simulationStruct.xmlReference;            
            
            if (init==true)
                %create defaultPriorModelVariables using tsimulation
                for varid=1:simulationStruct.variables.numVars
                    clear variable;
                    variable=simulationStruct.variables.variable(varid);
                    priorDistribution=simulationStruct.variables.variable(varid).priorDistribution;
                    clear setparameters;
                    for hpid=1:priorDistribution.numParams
                        hyperparam=priorDistribution.hyperParameters(hpid);
                        setparameters(hpid,1)=mcmcBayes.hyperparameter(hyperparam.name, hyperparam.description, hyperparam.range, hyperparam.resolution, ...
                            hyperparam.values, hyperparam.type, hyperparam.dimensions);
                    end
                    if ~exist('setparameters','var')
                        setdistribution=mcmcBayes.distribution(priorDistribution.type,[]);
                    else
                        setdistribution=mcmcBayes.distribution(priorDistribution.type,setparameters);
                    end

                    defaultPriorModelVariables(varid,1)=mcmcBayes.variable(variable.name, variable.description, variable.type, variable.dimensions, setdistribution, ...
                        variable.precision, variable.tfPre, variable.tfPost, variable.solver.min, variable.solver.max, ...
                        simulationStruct.mcmcSettings.numAdaptSteps, simulationStruct.mcmcSettings.numBurnIn, simulationStruct.mcmcSettings.numSamples, ...
                        simulationStruct.mcmcSettings.numLagSteps, variable.solver.priorInitialSolverStartValue, double(0.05), ...
                        {variable.independentVariables.name}, {variable.independentVariables.description}, {variable.independentVariables.type}, ...
                        {variable.independentVariables.range}, {variable.independentVariables.resolution}, {variable.independentVariables.values});%MCMC_hpdalpha                                          
                end

                %run transformDataPre for priorData
                handle=mcmcBayes.simulation.getHandleTransformDataPre(simulationStruct.type);
                evalstr=sprintf('[priorData]=handle(defaultPriorModelVariables, priorData);');
                eval(evalstr);      

                %get the cell array with the solver's (e.g., MLE or LS) initial values for the prior variables
                [priorSolverInits]=mcmcBayes.model.determineSolverInits(simulationStruct.type, defaultPriorModelVariables, priorData);          

                %update initial values in defaultPriorModelVariables with priorSolverInits
                for varid=1:simulationStruct.variables.numVars
                    defaultPriorModelVariables(varid,1).MCMC_initvalue=priorSolverInits{varid};
                    if ~(priorData.dataSource==mcmcBayes.t_dataSource.None) && ~(priorData.dataSource==mcmcBayes.t_dataSource.Preset)
                        fprintf(stdout,'\tPrior solver initial for %s=%s.\n',defaultPriorModelVariables(varid,1).name,num2str(defaultPriorModelVariables(varid,1).MCMC_initvalue));
                    end
                    solverOverrides(varid,1)=simulationStruct.variables.variable(varid).solver.solverOverride;
                end

                %determine the point estimates for the prior
                priorModelVariables=mcmcBayes.model.determinePriorPointEstimates(sequence.cfgMcmcBayes.cfgSimulation.runPowerPosteriorMcmcSequence, ...
                    simulationStruct.variables.defaultSolver, solverOverrides, simulationStruct.type, ...
                    simulationStruct.subType, defaultPriorModelVariables, priorData);

                %copy the prior into defaultPosteriorModelVariables
                posteriorModelVariables=priorModelVariables;
                for varid=1:numel(posteriorModelVariables)
                    posteriorModelVariables(varid).MCMC_initvalue=simulationStruct.variables.variable(varid).solver.posteriorInitialSolverStartValue;
                end

                %run transformDataPre for posteriorData
                handle=mcmcBayes.simulation.getHandleTransformDataPre(simulationStruct.type);
                evalstr=sprintf('[posteriorData]=handle(posteriorModelVariables, posteriorData);');
                eval(evalstr);    

                %get the cell array with the solver's (e.g., MLE or LS) initial values for the posterior variables
                [posteriorSolverInits]=mcmcBayes.model.determineSolverInits(simulationStruct.type, posteriorModelVariables, posteriorData);          

                %update initial values in posteriorModelVariables with posteriorSolverInits
                for varid=1:simulationStruct.variables.numVars
                    posteriorModelVariables(varid,1).MCMC_initvalue=posteriorSolverInits{varid};
                    fprintf(stdout,'\tPosterior solver initial for %s=%s.\n',posteriorModelVariables(varid).name,conversionUtilities.matrix_tostring(posteriorModelVariables(varid).MCMC_initvalue));
                end

                %solve for the prior inits
                priorModelVariables=mcmcBayes.model.determineMCMCInits(sequence.cfgMcmcBayes.cfgSimulation.runPowerPosteriorMcmcSequence, ...
                    simulationStruct.variables.defaultSolver, solverOverrides, simulationStruct.type,...
                    simulationStruct.subType, priorModelVariables, priorData);

                %solve for the posterior inits            
                posteriorModelVariables=mcmcBayes.model.determineMCMCInits(sequence.cfgMcmcBayes.cfgSimulation.runPowerPosteriorMcmcSequence, ...
                    simulationStruct.variables.defaultSolver, solverOverrides, simulationStruct.type, ...
                    simulationStruct.subType, posteriorModelVariables, posteriorData);                                

                for varid=1:size(priorModelVariables,1)
                    for chainid=1:simulationStruct.variables.numChains
                        tpriorModelVariables(varid,chainid,1)=priorModelVariables(varid);%depends on numModelVariables
                        tpriorModelVariables(varid,chainid,1).MCMC_nsamples_adapt=simulationStruct.mcmcSettings.numAdaptSteps;
                        tpriorModelVariables(varid,chainid,1).MCMC_nsamples_burnin=simulationStruct.mcmcSettings.numBurnIn;
                        tpriorModelVariables(varid,chainid,1).MCMC_nsamples_data=simulationStruct.mcmcSettings.numSamples;
                        tpriorModelVariables(varid,chainid,1).MCMC_nsamples_lag=simulationStruct.mcmcSettings.numLagSteps;
                        for tempid=1:simulation.powerPosteriorN+1
                            tposteriorModelVariables(varid,chainid,tempid)=posteriorModelVariables(varid);%depends on numModelVariables        
                            tposteriorModelVariables(varid,chainid,tempid).MCMC_nsamples_adapt=simulationStruct.mcmcSettings.numAdaptSteps;
                            tposteriorModelVariables(varid,chainid,tempid).MCMC_nsamples_burnin=simulationStruct.mcmcSettings.numBurnIn;
                            tposteriorModelVariables(varid,chainid,tempid).MCMC_nsamples_data=simulationStruct.mcmcSettings.numSamples;
                            tposteriorModelVariables(varid,chainid,tempid).MCMC_nsamples_lag=simulationStruct.mcmcSettings.numLagSteps;
                        end
                    end
                end
                simulation.priorVariables=tpriorModelVariables;
                simulation.posteriorVariables=tposteriorModelVariables;
                simulation.priorData=priorData;
                simulation.priorPredictedData=priorPredictedData;
                simulation.posteriorData=posteriorData;
                simulation.posteriorPredictedData=posteriorPredictedData;                    
            end
        end            
        
%todo: update me                
        function forkThem(cfgRunView, seqType, seqUuid, simUuids)
        % forkThem(obj)
        %   Similar to fork_executeModelAndVerifySequences but for one model and indexed on datasetids.  This is a wrapper function that forks 
        %   execution of the model and verify sequence for posterior estimation on the specified model (see the documentation for 
        %   executeModelAndVerifySequence below).  Spawns a new MATLAB instance and initiates executeModelAndVerifySequence.
        %   Input:
        %     sequenceType-t_sequence(VDM, Normal) type that specifies sequence 
        %     the to perform the sampling sequence for
        %     modeltype - Specifies the model to use for performing the sampling sequences for
        %     priordatatype - t_simulationData type that specifies priorDependentVariables (e.g., CCMPerformanceWeighting, EqualWeighting, Individual, and Simulated)
        %     datatype - t_simulationData type that specifies dependentVariables (e.g., ConFR, LinIFR, ExpIFR, LinDFR, ExpDFR, and Simulated)
        %     datasetids - Specifies the vector of dataset identifiers to perform the sampling sequences for (should have a range of 1:n)
        %     numgenerateddatasets - Specifies how many simulated datasets to generate
        %   Output:
        %     NA
            matlabfuncsstr=[{}];
            logfilenamesstr=[{}];      
            tmpOsInterface=mcmcBayes.osInterfaces.getOsInterface();
            for i=1:size(simUuids,1) %finer grained resolution for performance speedup (e.g., slower models will run sequences in parallel after all the fast models have finished)
                simUuid=simUuids{i};
                matlabfuncsstr(i,1)={sprintf('mcmcBayes.sequence.useDefaultsAndRunSingleSimulation(%s,''%s'',''%s'');', mcmcBayes.t_sequence.tostring(seqType), ...
                    seqUuid, simUuid)};
                logfilenamesstr(i,1)={sprintf('%s-%s-runLog%d.txt', seqUuid, simUuid, i)};
            end
            if ~isempty(matlabfuncsstr)
                tmpOsInterface.forkMATLABScripts(cfgRunView, matlabfuncsstr, logfilenamesstr);
            end
        end                        
    end
    
    methods (Abstract, Static)
        [loglikelihood]=computeLogLikelihood(type, variables, data);
        [dependentVariables]=generateTestData(type, predictionsNSamples, data, dependentvariablename, vararg)                   
        [MSFE]=computeMSFE(dependentVariables, dependentVariablesHat);
        openModelPDFs(type, subType, datasetid, tempid);
        openMCMCDiagnostics(type, subType, datasetid, varid, tempid);
        [retData]=transformDataPre(modelVariables, data);
    end
    
    methods (Abstract)
        displayPredictions(obj, tempID);           
        plotPredictions(obj, tempID);
        [retPredictedData, retNhat]=generatePredictions(obj, chainid, tempID);        
        [retObj]=generatePredictionsSet(obj, bool_plotpredictions, chainid, tempID);
        displayVariablesStats(obj, varID, chainID, tempID);
        plotVariableDiagnostics(obj, varID, chainID, tempID);
        [retObj]=transformVarsPre(obj, chainID, tempID);
        [retObj]=transformVarsPost(obj, chainID, tempID);
        [retValues]=getTransformedDataValuesPost(obj, datatype, category, varID, datasetID, result);
        [retObj]=computeMargLogLikelihood(obj, bool_debugEnable);
    end    

    methods (Abstract, Access=protected)
        [status]=checkDataCompatibility(obj, datatype);
    end
    
    methods 
        function [obj]=simulation(setType, setSubType, setName, setDescription, setAuthor, setReference, setPrefix, setComputeMsfe)
        % obj=simulation(settype, setsubType, setname, setdescription, setauthor, setreference, setprefix)
        %   The constructor for simulation.
            if nargin > 0
                obj.type=setType;
                obj.subType=setSubType;
                obj.name=setName;
                obj.description=setDescription;
                obj.author=setAuthor;
                obj.reference=setReference;
                obj.filenamePrefix=setPrefix;
                obj.doMsfe=setComputeMsfe;
                
                obj.posteriorMarginalLogLikelihood=double(0);
                obj.posteriorMarginalLogLikelihoodAlt=double(0);
                obj.klDistance=double(0);      
                
                obj.predictionsToCapture=mcmcBayes.simulation.getDefaultPredictionsToCapture();
                obj.predictionsHingeThreshold=0.05;
                obj.powerPosteriorN=double(mcmcBayes.simulation.getDefaultPowerPosteriorN());
                obj.powerPosteriorK=double(mcmcBayes.simulation.getDefaultPowerPosteriorK());
            end
        end
 
        function [retObj]=performMCMCSampling(obj, cfgMcmcBayes, temperatures)
        % [retobj]=performMCMCSamplingSequence(obj, cfgDebug, cfgSampleMcmcResults, temperatures, cfgPlotDiagnostics)
        %   Either loads MCMC data from a previous sampling results file or performs a new MCMC sampling sequence.
            stdout=1;
            for tempId=temperatures
                fprintf(stdout, 'Run sampling for Uuid=%s, tempId=%d.\n', obj.uuid, tempId);
                if tempId==0
                    numVars=size(obj.priorVariables,1);
                    numChains=size(obj.priorVariables,2);    
                    numDatasets=size(obj.priorData,2);                    
                    phi=0;
                else
                    numVars=size(obj.posteriorVariables,1);
                    numChains=size(obj.posteriorVariables,2);
                    numDatasets=size(obj.posteriorData,2);                    
                    phi=mcmcBayes.simulation.getPhi(tempId, obj.powerPosteriorN, obj.powerPosteriorK);
                end
                if tempId>2 && cfgMcmcBayes.cfgSimulation.runPowerPosteriorMcmcSequence %don't use the estimates for tempid==1 and tempid==2!
                    chainId=1;%just use first chain's mean
                    for varId=1:numVars%for runpp, need to set MCMC_initvalues to the last temperature's MCMC_mean
                        %should be using the already postTransformed variables from the last temperature
%todo: We need to run preTransform for MCMC_mean (otherwise, for KG model this will not work)           
%                        obj.posteriorVariables(varID, chainID, tempID).MCMC_initvalue=obj.posteriorVariables(varID, chainID, tempID-1).MCMC_mean_raw;%what about when this is < minimum
                    end
                end
                sessiondetails=sprintf('%s model (%s), subType=%s, simUuid=%s, \n\tsamplerType=%s,priordatatype=%s, dataType=%s, tempId=%d, phi=%e', ...
                    obj.name, obj.reference, obj.subType, obj.uuid, mcmcBayes.t_samplerInterfaces.tostring(obj.samplerInterface.type), ...
                    mcmcBayes.t_dataSource.tostring(obj.priorData.dataSource), mcmcBayes.t_dataSource.tostring(obj.posteriorData.dataSource), tempId, phi);
                if cfgMcmcBayes.cfgSimulation.sampleMcmcResults
                    dispstr=sprintf('Performing MCMC sampling for %s',sessiondetails);
                else
                    dispstr=sprintf('Loading stored MCMC samples for %s',sessiondetails);
                end
                disp(dispstr);
                outputFileNamePrefix=sprintf('%s-%s-tempid%d', obj.filenamePrefix, obj.uuid, tempId);
                %if running power-posterior, the independent variables for prior and posterior must match
                if (tempId==0)
                    for chainId=1:numChains
                        obj=obj.transformVarsPre(chainId, tempId);%needs to be performed on all vars, simultaneously
                    end
                    obj.samplerInterface=obj.samplerInterface.setupMCMCSampling(obj.type, obj.subType, numChains, tempId, phi, outputFileNamePrefix, ...
                        obj.priorVariables, obj.priorData, obj.osInterface);%complete sampling setup
                else
                    if obj.priorData.dataSource==mcmcBayes.t_dataSource.Preset && cfgMcmcBayes.cfgSimulation.plotDiagnostics%for Preset case, the prior distributions were not shown
                        chainId=1;%just plot one chain (they are all the same)
                        for varId=1:numVars
                            obj.posteriorVariables(varId,chainId,tempId).distribution.plot(obj.posteriorVariables(varId,chainId,tempId).name);
                        end
                    end                                                  
                    for chainId=1:numChains
                        obj=obj.transformVarsPre(chainId, tempId);%needs to be performed on all vars, simultaneously
                    end                                             
                    obj.samplerInterface=obj.samplerInterface.setupMCMCSampling(obj.type, obj.subType, numChains, tempId, phi, outputFileNamePrefix, ...
                        obj.posteriorVariables(:,:,tempId), obj.posteriorData, obj.osInterface);%complete sampling setup
                end                
                if cfgMcmcBayes.cfgSimulation.sampleMcmcResults       
                    if cfgMcmcBayes.cfgSimulation.debug
                        filename=sprintf('%s%s-preMCMC.mat', mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.archivesDir), outputFileNamePrefix);
                        save(filename, 'obj', 'phi');%save the model pre MCMC
                    end
                    [obj.samplerInterface]=obj.samplerInterface.performMCMCSampling(); %run sampling sequence
                    [obj.samplerInterface, fieldindices, data]=obj.samplerInterface.readSamples();
                else
                    [obj.samplerInterface, fieldindices, data]=obj.samplerInterface.readSamples();
                end
                if (tempId==0)
                    for varId=1:numVars
                        for chainId=1:numChains
                            idx=find(fieldindices(:,chainId)==varId);%get the appropriate index for varid
                            obj.priorVariables(varId, chainId).MCMC_samples=data{idx, chainId};%save the samples for varid
                        end
                    end
                    
                    for chainId=1:numChains
                        obj=obj.transformVarsPost(chainId, tempId);%needs to be performed on all vars, simultaneously
                    end
                    
                    for varId=1:numVars
                        for chainId=1:numChains                        
                            obj.priorVariables(varId, chainId)=obj.priorVariables(varId, chainId).updateStatistics();%update the variable stats for modelVariables(varid,tempid)
                            obj.displayVariablesStats(varId, chainId, tempId);%display the calculated statistics for the temperature iteration
                            if cfgMcmcBayes.cfgSimulation.plotDiagnostics
                                obj.plotVariableDiagnostics(varId,chainId,tempId);
                            end
                        end
                    end
                    
                    %Always save because this file is used to postprocess and compute MSE of the prior
                    filename=sprintf('%s%s-postMCMC.mat', mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.archivesDir), outputFileNamePrefix);
                    save(filename, 'obj', 'phi');%save the model post MCMC
                else
                    for varId=1:numVars
                        for chainId=1:numChains
                            idx=find(fieldindices(:,chainId)==varId);%get the appropriate index for varid
                            obj.posteriorVariables(varId, chainId, tempId).MCMC_samples=data{idx, chainId};%save the samples for varid
%todo: this is a hack to save the MCMC_mean_raw (MCMC_mean of the unadjusted samples)
                            obj.posteriorVariables(varId, chainId, tempId)=obj.posteriorVariables(varId, chainId, tempId).updateStatistics();
                            obj.posteriorVariables(varId, chainId, tempId).MCMC_mean_raw=obj.posteriorVariables(varId, chainId, tempId).MCMC_mean;
                        end
                    end                    
                    
                    for chainId=1:numChains
                        obj=obj.transformVarsPost(chainId, tempId);%needs to be performed on all vars, simultaneously
                    end                   
                    
                    for varId=1:numVars
                        for chainId=1:numChains                        
                            obj.posteriorVariables(varId, chainId, tempId)=obj.posteriorVariables(varId, chainId, tempId).updateStatistics();%update the variable stats for modelVariables(varid,tempid)
                        end
                    end                                 
                    for chainId=1:numChains
                        if numDatasets==1
                            printStatus=true;
                            mcmcBayes.model.computeLogLikelihoods(obj.osInterface, obj.type, obj.posteriorVariables, obj.posteriorData, chainId, tempId, printStatus);%calling computeLogLikelihoods() to print loglikelihood | theta
                        else
                            warning('multiple data sets is problematic with the present implementation of computeLogLikelihoods');
                        end                        
                    end               
                    
                    for varId=1:numVars
                        for chainId=1:numChains                        
                            obj.displayVariablesStats(varId, chainId, tempId);%display the calculated statistics for the temperature iteration
                            if cfgMcmcBayes.cfgSimulation.plotDiagnostics
                                obj.plotVariableDiagnostics(varId,chainId,tempId);
                            end
                        end
                    end                       
                                        
                    %Always save because this file is used to postprocess and compute MSE of the prior
                    if tempId==1 || tempId==temperatures(end) || cfgMcmcBayes.cfgSimulation.debug
                        filename=sprintf('%s%s-postMCMC.mat', mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.archivesDir), outputFileNamePrefix);
                        save(filename, 'obj', 'phi');%save the model post MCMC
                    end
                end
            end
            retObj=obj;
        end
        
        function [retObj]=run(obj, cfgMcmcBayes)                    
        % [retobj]=run(obj, cfgDebug, cfgRewindEnable, cfgPlotDiagnostics, cfgSampleMcmcResults, cfgRunPowerPosteriorMcmcSequence, ...
        % cfgSkipPriorComputations, cfgGeneratePredictionsUsingPrior, cfgPlotPredictionsUsingPrior, cfgSkipPosteriorComputations, ...
        % cfgGeneratePredictionsUsingPosterior, cfgPlotPredictionsUsingPosterior)
        %   This function estimates the prior distribution, then uses the prior distribution and the data to estimate the posterior distribution, 
        %   next it uses both of these to make predictions, and performs computations while making the predictions to support later computing Bayes 
        %   factors.
            
            if ((obj.priorData.dataSource==mcmcBayes.t_dataSource.None) && (cfgMcmcBayes.cfgSimulation.runPowerPosteriorMcmcSequence))
                error('Cannot run power-posterior simulation without appropriate priordatatype (e.g., None is not appropriate)!');
            end            
            
warning('todo: check priorData and posteriorData for compatibility with simulation.type and set dataok');
%add the function for the above in the mode category class (e.g., mcmcBayes.NHPP)

            dataOk=true;
            stdout=1;
            if dataOk==true
                timestr=datestr(clock,0);%save the date for metrics
                timerStart=tic;%save the timestamp for metrics
                                
                fprintf(stdout,'######################################################\n');
                fprintf(stdout, 'Setup simulation Uuid=%s, using %s (%s) implemented in %s.\n', obj.uuid, ...
                    obj.name, obj.reference, mcmcBayes.t_samplerInterfaces.tostring(obj.samplerInterface.type));
                fprintf(stdout, '\tThe timestamp for starting is %s.\n', timestr);
                
                if cfgMcmcBayes.cfgSimulation.debug
                    warning('Temporary adjustment of MCMC settings to reduce simulation times.')
                    numVars=size(obj.priorVariables,1);%numVars and numChains is same for prior and posterior
                    numChains=size(obj.priorVariables,2);
                    for varId=1:numVars
                        for chainId=1:numChains
                            for tempId=cast(1:obj.powerPosteriorN+1,'double')
                                obj.priorVariables(varId,chainId,1)=obj.priorVariables(varId,chainId,1).overrideMCMCSettings( ...
                                    int32(5000),int32(5000),int32(5000),int32(1));
                                obj.posteriorVariables(varId,chainId,tempId)=obj.posteriorVariables(varId,chainId,tempId).overrideMCMCSettings( ...
                                    int32(5000),int32(5000),int32(5000),int32(1));
                            end
                        end
                    end
                end
                
                if ~(cfgMcmcBayes.cfgSimulation.skipPriorComputations)
                    tempId=0;%reset the tempid for the prior sequence (used for file names, plot names, etc.)
                    temperatures=0;%temperatures index for the prior is 1, phi(1)=0 (notably, the sampling sequence uses this to compute phi for the MCMC project files)
                                        
                    if ~(obj.priorData.dataSource==mcmcBayes.t_dataSource.None) && ~(obj.priorData.dataSource==mcmcBayes.t_dataSource.Preset)
                        obj=obj.performMCMCSampling(cfgMcmcBayes, temperatures);
                    end

                    if cfgMcmcBayes.cfgSimulation.generatePredictionsUsingPrior && ~(obj.priorPredictedData.mean.dataSource==mcmcBayes.t_dataSource.None) %generate predictions using the parameter values determined via mle
                        chainId=1;%just use the values from the first chain
                        obj=obj.generatePredictionsSet(cfgMcmcBayes.cfgSimulation.plotPredictionsUsingPrior, chainId, tempId);
                    end                    
                end
                
                if ~(cfgMcmcBayes.cfgSimulation.skipPosteriorComputations)
                    tempId=obj.powerPosteriorN+1;%set the tempid for the posterior sequence (used for file names, plot names, etc.)
                    
                    if cfgMcmcBayes.cfgSimulation.runPowerPosteriorMcmcSequence %setup temperature indices to run sampling sequence for
                        temperatures=cast(1:obj.powerPosteriorN+1,'double');%all the temperatures
                    else
                        temperatures=cast(obj.powerPosteriorN+1,'double');%posterior only
                    end
                    
                    obj=obj.performMCMCSampling(cfgMcmcBayes, temperatures);
                    
                    obj.generateFigures(cfgMcmcBayes, temperatures);
                    
                    if cfgMcmcBayes.cfgSimulation.generatePredictionsUsingPosterior
                        chainId=1;%just use the values from the first chain
                        obj=obj.generatePredictionsSet(cfgMcmcBayes.cfgSimulation.plotPredictionsUsingPosterior, chainId, tempId);%(seqid, tempid, datain)
                    end
                    
                    elapsedtime=toc(timerStart);%save the timestamp for metrics
                    fprintf(stdout, '\tElapsed analysis time for simulation Uuid=%s was %d hours.\n', obj.uuid, hours(seconds(elapsedtime)));
                end
                fprintf(stdout, '######################################################\n');
                if ~(cfgMcmcBayes.cfgSimulation.skipPosteriorComputations)
                    if cfgMcmcBayes.cfgSimulation.runPowerPosteriorMcmcSequence
                        fprintf(stdout, 'Compute marginal loglikelihood for Uuid=%s, using %s (%s) implemented in %s.\n', obj.uuid, ...
                            obj.name, obj.reference, mcmcBayes.t_samplerInterfaces.tostring(obj.samplerInterface.type));
                        timerStart=tic;%save the timestamp for metrics
                        obj=obj.computeMargLogLikelihood(cfgMcmcBayes.cfgSimulation.rewindEnable);
                        elapsedtime=toc(timerStart);%save the timestamp for metrics
                        fprintf(stdout, '\tElapsed computeMargLogLikelihood() time for Uuid=%s was %d hours.\n', hours(seconds(elapsedtime)));
                    end
                end
            else
                warning('Invalid data for Uuid=%s, not running it.', obj.uuid);
            end
            fprintf(stdout, '######################################################\n');                   
            
            retObj=obj;                
        end
        
        function generateFigures(obj, cfgMcmcBayes, temperatures)             
            chainId=1;
            numVars=size(obj.posteriorVariables,1);%numVars and numChains is same for prior and posterior
            if numVars==2 && cfgMcmcBayes.cfgSimulation.plotDiagnostics
                fig;
                for tempId=temperatures
                    %gather the joint samples
                    for varId=1:numVars
                        chainId=1;%only plot one chain
                        X(varId,:)=obj.posteriorVariables(varId,chainId,tempId).MCMC_samples;
                    end

                    if cfgMcmcBayes.cfgSimulation.plotDiagnostics
                        histogram2(X(1,:),X(2,:),100);
                    end   
                end
            else
                tempstr=sprintf('Histogram of %d-dimensional set of variables is unsupported.\n',numVars);
                warning(tempstr);
            end
        end
    end
    
    methods %getter/setter functions       
%         function obj = set.name(obj,setname)
%             if ~ischar(setname)
%                 error('simulation.name must be a char[]');
%             else
%                 obj.name = setname;
%             end
%         end
%         
%         function value = get.name(obj)
%             value=obj.name;
%         end
%         
%         function obj = set.description(obj,setdescription)
%             if ~ischar(setdescription)
%                 error('simulation.description must be a char[]');
%             else
%                 obj.description = setdescription;
%             end
%         end
%         
%         function value = get.description(obj)
%             value=obj.description;
%         end
%         
%         function obj = set.reference(obj,setreference)
%             if ~ischar(setreference)
%                 error('simulation.reference must be a char[]');
%             else
%                 obj.reference = setreference;
%             end
%         end
%         
%         function value = get.reference(obj)
%             value=obj.reference;
%         end
% 
%         function obj = set.author(obj,setauthor)
%             if ~ischar(setauthor)
%                 error('simulation.author must be a char[]');
%             else
%                 obj.author = setauthor;
%             end
%         end
%         
%         function value = get.author(obj)
%             value=obj.author;
%         end        
%         
%         function obj = set.numModelVariables(obj,setnumModelVariables)
%             if ~isnumeric(setnumModelVariables)
%                 error('numModelVariables must be numeric');
%             else
%                 obj.numModelVariables= setnumModelVariables;
%             end
%         end      
%         
%         function value = get.numModelVariables(obj)
%             value=obj.numModelVariables;
%         end   
%         
%         function obj = set.runView(obj,setrunView)
%             if ~isa(setrunView,'t_simulationRunView')
%                 error('runView must be type t_simulationRunView');
%             else
%                 obj.runView=setrunView;
%             end
%         end
%         
%         function value = get.runView(obj)
%             value=obj.runView;
%         end           
%         
%         function obj = set.modelVariables(obj,setmodelVariables)%depends on numModelVariables
%             status=true;
%             if ~((size(setmodelVariables,1)==obj.numModelVariables) && (size(setmodelVariables,2)==obj.powerposterior_n+1))%check size
%                 error('modelVariables must size %dxobj.powerposterior_n+1',obj.numModelVariables);
%             else
%                 for varid=1:obj.numModelVariables
%                     for tempid=1:cast(obj.powerposterior_n+1,'double')
%                         if ~isa(setmodelVariables(varid,tempid),'simulationVariable')
%                             status=false;
%                         end
%                     end
%                 end
%                 if status
%                     obj.modelVariables= setmodelVariables;
%                 else
%                     error('modelVariables must be a simulationVariable object or object array');
%                 end
%             end
%         end
%         
%         function value = get.modelVariables(obj) %access the simulationModelmodelVariables.methods through modelVariables property
%             value=obj.modelVariables;
%         end              
%         
%         function obj = set.powerposterior_n(obj,setpowerposterior_n)
%             if ~isinteger(setpowerposterior_n)
%                 error('simulation.powerposterior_n must be an integer>0');
%             else
%                 if ~(setpowerposterior_n>0)
%                     error('simulation.powerposterior_n must be an integer>0');
%                 else
%                     obj.powerposterior_n = setpowerposterior_n;
%                 end
%             end
%         end
%         
%         function value = get.powerposterior_n(obj)
%             value=obj.powerposterior_n;
%         end
%         
%         function obj = set.powerposterior_k(obj,setpowerposterior_k)
%             if ~isinteger(setpowerposterior_k)
%                 error('simulation.powerposterior_k must be an integer>0');
%             else
%                 if ~(setpowerposterior_k>0)
%                     error('simulation.powerposterior_k must be an integer>0');
%                 else
%                     obj.powerposterior_k = setpowerposterior_k;
%                 end
%             end
%         end
%         
%         function value = get.powerposterior_k(obj)
%             value=obj.powerposterior_k;
%         end
%         
%         function obj = set.powerposterior_marginalLogLikelihood(obj,setpowerposterior_marginalLogLikelihood)
%             if ~isa(setpowerposterior_marginalLogLikelihood,'double')
%                 error('simulation.powerposterior_marginalLogLikelihood must be a double');
%             else
%                 obj.powerposterior_marginalLogLikelihood = setpowerposterior_marginalLogLikelihood;
%             end
%         end
%         
%         function value = get.powerposterior_marginalLogLikelihood(obj)
%             value=obj.powerposterior_marginalLogLikelihood;
%         end
%         
%         function obj = set.klDistance(obj,setklDistance)
%             if ~isa(setklDistance,'double')
%                 warning('simulation.klDistance must be a double=>0');
%             else
%                 if ~(setklDistance>=0)
%                     warning('simulation.klDistance must be a double>=0');
%                 else
%                     obj.klDistance = setklDistance;
%                 end
%             end
%         end
%         
%         function value = get.klDistance(obj)
%             value=obj.klDistance;
%         end            
                      
%         function obj = set.independentVariables(obj,setIndependentVariables)
%             if (obj.checkIndVarDataCompatibility(setIndependentVariables))
%                 obj.independentVariables=setIndependentVariables;
%             else
%                 error('independentVariables is not compatible with model %s', obj.name);
%             end
%         end
%         
%         function value = get.independentVariables(obj)
%             value=obj.independentVariables;
%         end
%                       
%         function obj = set.priorDependentVariables(obj,setpriorDependentVariables)%needs priordatatype set first
%             if ((obj.checkPriorDepVarDataCompatibility(setpriorDependentVariables)) || (obj.priordatatype==t_simulationData.None))
%                 obj.priorDependentVariables=setpriorDependentVariables;
%             else
%                 error('priorDependentVariables is not compatible with model %s', obj.name);
%             end
%         end
%         
%         function value = get.priorDependentVariables(obj)
%             value=obj.priorDependentVariables;
%         end
%                       
%         function obj = set.dependentVariables(obj,setDependentVariables)
%             if (obj.checkDepVarDataCompatibility(setDependentVariables))
%                 obj.dependentVariables=setDependentVariables;
%             else
%                 error('dependentVariables is not compatible with model %s', obj.name);
%             end
%         end
%         
%         function value = get.dependentVariables(obj)
%             value=obj.dependentVariables;
%         end
    end
end