classdef growthCurveEvaluation < mcmcBayes.sequence
% growthCurveEvaluation is a class for demonstrating mcmcBayes with two Growth curve models.  
% * VerhulstFlatSimulated - (subtype 'c') uses a flat prior with simulated data for the posterior data
% * VerhulstPresetSimulated - (subtype 'b') uses a preset prior with simulated data for the posterior data
% * VerhulstEmpirical2Empirical1 - (subtype 'b') uses an empirical prior (dataset from Nolan 1974 was used, see 
%   https://birdsna.org/Species-Account/bna/species/woothr/breeding) to replicate "Are parametric models suitable for estimating avian growth rates?",
%   Brown et al., 2007, in the study of the growth of young Wood Thrushes.
% * VerhulstFlatEmpirical1 - (subtype 'c') uses a flat prior to replicate "Are parametric models suitable for estimating avian growth rates?",
%   Brown et al., 2007, in the study of the growth of young Wood Thrushes.  Brown et al. estimated the 3-parameter Verhulst as beta1a=50.82, beta2a=21.20, 
%   and beta3a=0.095.  For our 3-parameter alternative, beta0=log((beta1a-beta2a)/beta2a), beta1=beta1a, and beta2=beta3a.
% * VerhulstFlatEmpirical3 - (subtype 'c') uses a flat prior to replicate "Fitting a Verhulst Curve to Data", Cavallini, 1993, in the study 
%   of algal biomass growth.  Cavallini estimated beta0=5.496 (i.e., 45.8*.12), beta1=5.1, and beta2=0.12.
% * GompertzFlatSimulated - (subtype 'c') uses a flat prior with simulated data for the posterior data
% * GompertzPresetSimulated - (subtype 'b') uses a preset prior with simulated data for the posterior data
% * GompertzFlatEmpirical4 - (subtype 'c') uses a flat prior to replicate "Least-squares fitting Gompertz curve", Jukic et al, 2004, in a study
%   of the stock of cars in the Netherlands (in period 1965-1989).  Jukic et al. estimated a=8.69571, b=1.53597, and c=0.105687.  For our parameterization,
%   its equivalent would be beta0=log(b), beta1=exp(a), and beta1=c and thus we should see beta0=0.4291621, beta1=5977.215, and beta2=0.105687.

    properties
        %None
    end
    
    methods (Static)             
        function test
           disp('Hello!'); 
        end

        % Verhulst-a (JAGS,BUGS,Stan) - Normal Inverse Gamma priors, unknown precision, hierarchical (i.e., beta0~normal(a,(1/(b*r))^-1), beta1~normal(c,(1/(d*r))^-1), beta2~normal(g,(1/(h*r))^-1), r~gamma(p,q))
        % Verhulst-b (JAGS,BUGS,Stan) - Normal Inverse Gamma priors, unknown precision (i.e., beta0~normal(a,(1/b)^-1), beta1~normal(c,(1/d)^-1), beta2~normal(g,(1/h)^-1), r~gamma(p,q))
        % Verhulst-c (BUGS,Stan) - Flat priors (i.e., beta0~flat, beta1~flat, beta2~flat, r~T[0,]*flat)
        
        % Gompertz-a (JAGS,BUGS,Stan) - Normal Inverse Gamma priors, unknown precision, hierarchical (i.e., beta0~normal(a,(1/(b*r))^-1), beta1~normal(c,(1/(d*r))^-1), beta2~normal(g,(1/(h*r))^-1), r~gamma(p,q))
        % Gompertz-b (JAGS,BUGS,Stan) - Normal Inverse Gamma priors, unknown precision (i.e., beta0~normal(a,(1/b)^-1), beta1~normal(c,(1/d)^-1), beta2~normal(g,(1/h)^-1), r~gamma(p,q))
        % Gompertz-c (BUGS,Stan) - Flat priors (i.e., beta0~flat, beta1~flat, beta2~flat, r~T[0,]*flat)
        
        function [simuuids]=getUuids()
            simuuids=[{'VerhulstPresetSimulated'}, ...      %Verhulst-b
                   {'VerhulstFlatSimulated'}, ...           %Verhulst-c
                   {'VerhulstEmpirical2Empirical1'}, ...    %Verhulst-b
                   {'VerhulstFlatEmpirical1'}, ...          %Verhulst-c
                   {'VerhulstFlatEmpirical3'}, ...          %Verhulst-c
                   {'GompertzPresetSimulated'}, ...         %Verhulst-b
                   {'GompertzFlatSimulated'}, ...           %Verhulst-c
                   {'GompertzFlatEmpirical4'}];             %Verhulst-c
        end    
        
        function [retSims]=runUnitTests()
            norunpp=false;
            runpp=true;
            dbgmcmc=true;
            nodbgmcmc=false;       
            seqType='growthCurveEvaluation';
            seqUuid='growthCurveEvaluation-runUnitTests';
            simuuids=mcmcBayes.growthCurveEvaluation.getUuids();         
            %simOverrides[simuuid, runpp, dbgmcmc, {[samplerType]}]
            simOverrides=[simuuids(1), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %VerhulstPresetSimulated
                          simuuids(2), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %VerhulstFlatSimulated
                          simuuids(3), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %VerhulstEmpirical2Empirical1
                          simuuids(4), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %VerhulstFlatEmpirical1
                          simuuids(5), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %VerhulstFlatEmpirical3
                          simuuids(6), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %GompertzPresetSimulated
                          simuuids(7), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %GompertzFlatSimulated
                          simuuids(8), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.Stan]}; ... %GompertzFlatEmpirical4
                          simuuids(1), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %VerhulstPresetSimulated
                          simuuids(2), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %VerhulstFlatSimulated
                          simuuids(3), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %VerhulstEmpirical2Empirical1
                          simuuids(4), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %VerhulstFlatEmpirical1
                          simuuids(5), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %VerhulstFlatEmpirical3
                          simuuids(6), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %GompertzPresetSimulated
                          simuuids(7), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %GompertzFlatSimulated
                          simuuids(8), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.JAGS]}; ... %GompertzFlatEmpirical4
                          simuuids(1), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %VerhulstPresetSimulated
                          simuuids(2), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %VerhulstFlatSimulated
                          simuuids(3), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %VerhulstEmpirical2Empirical1
                          simuuids(4), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %VerhulstFlatEmpirical1
                          simuuids(5), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %VerhulstFlatEmpirical3
                          simuuids(6), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %GompertzPresetSimulated
                          simuuids(7), {norunpp}, {dbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}; ... %GompertzFlatSimulated
                          simuuids(8), {norunpp}, {nodbgmcmc}, {[mcmcBayes.t_samplerInterfaces.OpenBUGS]}];    %GompertzFlatEmpirical4
            for i=1:size(simOverrides,1)
                retSims(i,1)=mcmcBayes.sequence.overrideDefaultsAndRunSingleSimulation(seqType, seqUuid, simOverrides{i,1:4});                      
            end
        end
        
        function getData(type, data, varargin)         
        % getData(obj, type, data, varargin)
        %   This function generates a simulated dataset using the parameter point estimates from varargin.

            if (data.dataSource~=mcmcBayes.t_dataSource.Simulated) && (data.dataSource~=mcmcBayes.t_dataSource.Postulated)
                error('Requires mcmcBayes.t_dataSource=Simulated or Postulated for creating data.');
            else
                if strcmp(type,'Verhulst') || strcmp(type,'Gompertz')
                    predictionsNSamples=1;
                else
                    error('Unsupported growthCurveEvaluation sequence type=%s', type);
                end
            end            

            numdatavars=1;%N(t)
            numgenerateddatasets=size(data.depVarsValues,2);
            dependentVars=cell(numdatavars,numgenerateddatasets);
            
            %set the parameter values for the simulated data
            if strcmp(type,'Verhulst')
                beta0=varargin{1};%'beta0','discoveries at y intercept', 'double'
                beta1=varargin{2};%'beta1','carrying capacity (maximum population size)', 'double'
                beta2=varargin{3};%'beta2','population growth rate', 'double'
                r=varargin{4};%'r','precision for zero mean error term', 'double'
                %mean at at t=50 should then be ?????                
                for tdatasetid=1:numgenerateddatasets
                    dependentVars(numdatavars,tdatasetid)=mcmcBayes.model.generateTestData(type, predictionsNSamples, data, beta0, beta1, beta2, r);%varargin=beta0, beta1, beta2, r
                end
            elseif strcmp(type,'Gompertz')               
                beta0=varargin{1};%'beta0','discoveries at y intercept', 'double'
                beta1=varargin{2};%'beta1','carrying capacity (maximum population size)', 'double'
                beta2=varargin{3};%'beta2','population growth rate', 'double'
                r=varargin{4};%'r','precision for zero mean error term', 'double'
                %mean at at t=50 should then be ?????                
                for tdatasetid=1:numgenerateddatasets
                    dependentVars(numdatavars,tdatasetid)=mcmcBayes.model.generateTestData(type, predictionsNSamples, data, beta0, beta1, beta2, r);%varargin=beta0, beta1, beta2, r
                end
            else
                error('growthCurveEvaluation sequence type is not Verhulst or Gompertz.');
            end
            
            %need to clean it up such that it is non-decreasing
            tdata=dependentVars{1,1};
            for i=2:size(tdata)
                if tdata(i)<tdata(i-1)
                    tdata(i)=tdata(i-1);
                end
            end
            dependentVars{:,1}=tdata;

            disp(conversionUtilities.matrix_tostring(data.indVarsValues{1,1}));
            disp(conversionUtilities.matrix_tostring(dependentVars{1,1}));    
            
            plot(data.indVarsValues{1,1},dependentVars{1,1});            
        end                                
    end
    
    methods
        function [obj]=growthCurveEvaluation(cfgMcmcBayes,repositories,sequenceUuid)           
        % obj=growthCurveModelEvaluation(cfgMcmcBayes,repositories,sequenceUuid)
        %   The constructor for growthCurveModelEvaluation.
        % Output:
        %   obj Constructed growthCurveModelEvaluation object
            if nargin > 0
                superargs{1}='growthCurveEvaluation';
                superargs{2}=cfgMcmcBayes;
                superargs{3}=repositories;
                superargs{4}=sequenceUuid;
            elseif nargin == 0
                superargs={};
            end
            obj=obj@mcmcBayes.sequence(superargs{:});            
        end        
    end
    
    methods (Static, Access=private)     
   
    end    
    
    methods %getter/setter functions

    end
end