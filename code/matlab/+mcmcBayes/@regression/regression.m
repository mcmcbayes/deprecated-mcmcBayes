classdef regression < mcmcBayes.model    
%mcmcBayes.regression is a base class for models using regression.  It is a sub class of mcmcBayes.model and mcmcBayes.simulation.
% The currently implemented models, listed hierarchically, are:
%  * mcmcBayes.simulation.model.regression.linear
%  * mcmcBayes.simulation.model.regression.polynomialDegree2
    properties (SetAccess=public, GetAccess=public)
        predictionsNSamples=mcmcBayes.regression.getDefaultNSamples();%Default number of samples to run in calls to simPredictions()
    end

    methods (Abstract, Static)        

    end
    
    methods (Abstract)

    end

    methods (Static)
        function test()
            display('Hello!');
        end
                
        function [predictionsNSamples]=getDefaultNSamples()
            predictionsNSamples=50000;
        end        
        
        function [names]=getDefaultDataVariableNames(dataCategory)
            switch dataCategory
                case mcmcBayes.t_dataCategory.independent
                    names={'x'};
                case mcmcBayes.t_dataCategory.dependent
                    names={'y'};
                otherwise
                    error('dataCategory must be mcmcbayes.t_dataCategory.');
            end
        end        
        
        function [loglikelihood]=computeLogLikelihood(type, vars, data)
        % [loglikelihood]=computeLogLikelihood(type, vars, data)
        %   Computes the loglikelihood of the data given the model and parameter values.  This is used in the marginal likelihood computation.
        %   Input: 
        %     type - Specifies the model to use for computing the likelihood
        %     vars - Model variables that is an array of mcmcBayes.variable objects 
        %     data - Independent and dependent data variables for the model
        %   Output:
        %     loglikelihood - Computed loglikelihood (vector)
            if ~strcmp(type, 'linear') && ~strcmp(type, 'polynomialDegree2')
                error('Incorrect model type.');
            end
            
            datavarid=1;%one independent variable for regression, 'x' and one dependent variable for regression, 'y(x)'
%     warning('Temporary: this is necessary to replicate marginal log-likelihood estimates'); 
%     x=x-mean(x);%This is necessary to replicate marginal log-likelihood estimates
            x=cast(data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent, datavarid), 'double');
            Tau=size(x,1);%Number of data (we are assuming scalar independent variables)
            yorig=cast(data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.dependent, datavarid), 'double');

            % Ideally numSamples would be obj.modelVariables(index).MCMC_nsamples_data.  If the MCMC sampler has issues 
            %   (e.g., improper values for the inits) it may stop early.  The user is separately warned of 
            %   improperly sized sample arrays when they are stored by mcmcBayesVariable.set.MCMC_samples(). Set
            %   tsize to the variable with the fewest number of samples.
            numSamples=size(vars(1).MCMC_samples,1);
            for varid=1:size(vars,1)
                if size(vars(varid).MCMC_samples,1)<numSamples
                    numSamples=size(vars(varid).MCMC_samples,1);         
                end
            end             
            loglikelihood=zeros(numSamples,1);

%todo: need to already have called transformVarsPost on variables
            beta0=cast(vars(1).MCMC_samples,'double');
            beta1=cast(vars(2).MCMC_samples,'double');           
            if strcmp(type, 'linear') %normal(beta0+beta1*x,1/r)%matlab uses normal(mu,sigma^2), openbugs uses normal(mu,r)
                if (size(vars,1)~=3)
                    error('incorrect size for variables');
                end
                r=cast(vars(3).MCMC_samples,'double');
                p=2;
                X=ones(Tau,p);
                X(:,2)=x;
            elseif strcmp(type, 'polynomialDegree2') %normal(beta0+beta1*x+beta2*x^2,1/r)%matlab uses normal(mu,sigma^2), openbugs uses normal(mu,r)
                if (size(vars,1)~=4)
                    error('incorrect size for variables');
                end
                beta2=cast(vars(3).MCMC_samples,'double');
                r=cast(vars(4).MCMC_samples,'double');
                p=3;
                X=ones(Tau,p);
                X(:,2)=x;
                X(:,3)=x.^2;
            else
                error('unsupported model for mcmcBayes.regression::computeLogLikelihood()');
            end
            assert(all(eig(X'*X)>0));%confirm X'*X is positive definite
            B=zeros(p,1);                

            for j=1:numSamples
                Om=diag(ones(1,Tau)*r(j));
                if strcmp(type, 'linear') %normal(beta0+beta1*x,1/r)%matlab uses normal(mu,sigma^2), openbugs uses normal(mu,r)
                    B(1)=beta0(j);
                    B(2)=beta1(j);
                elseif strcmp(type, 'polynomialDegree2') %normal(beta0+beta1*x+beta2*x^2,1/r)%matlab uses normal(mu,sigma^2), openbugs uses normal(mu,r)
                    B(1)=beta0(j);
                    B(2)=beta1(j);
                    B(3)=beta2(j);
                end
                mu=X*B;
                %logdetOm=2*sum(log(diag(chol(Om))));%https://xcorr.net/2008/06/11/log-determinant-of-positive-definite-matrices-in-matlab/
                %loglikelihood(j)=-Tau/2*log(2*pi)+1/2*logdetOm-1/2*(yorig-mu)'*Om*(yorig-mu);%MVN(X*B,Om) log density, where Om is the precision matrix                
                loglikelihood(j,1)=-Tau/2*log(2*pi)+Tau/2*log(r(j))-1/2*r(j)*(yorig-mu)'*(yorig-mu);%Identical computation for above when Om=(r^-1)*I, but this is more readable
            end                               
            
            if (~isempty(find(isinf(loglikelihood)==1)))
                error('Error condition in computeLogLikelihood(), loglikelihood contains -Inf or Inf');
            elseif ~isempty(find(isnan(loglikelihood)==1))
                error('Error condition in computeLogLikelihood(), loglikelihood contains Nan');
            end            
        end

        function [dependentVariables]=generateTestData(type, predictionsNSamples, data, varargin)
            dependentVariables=generateTestData@mcmcBayes.model(type, predictionsNSamples, data, varargin{:});
        end
        
        function [depVarsValuesHat, retYhat]=simPredictions(type, predictionsNSamples, predictionsToCapture, hingeThreshold, data, varargin)
        % [dependentVariablesHat]=simPredictions(modeltype, predictionsNSamples, dependentVariablesHatTypes, hingeThreshold, independentVariables, dependentVariableName, description, arglist)
        %   Function to simulate predictions using one of the regression models (it is static so we can simulate data without instantiating a class)
        %   Input:
        %     modeltype - Specifies the model to use for generating the predictions
        %     predictionsNSamples - Number of samples to run when generating the predictions
        %     dependentVariablesHatTypes - Vector of t_mcmcBayesDependentVariablesHat that specifies which thetahat to generate predictions for
        %     hingeThreshold - Specifies the hinge threshold for determining the thetaHat edge cases for generating dependentVariablesHat.{hingelo/hingehi}
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     dependentVariableName - Name to use for the predictions data
        %     description - Description for the predictions data
        %     arglist - Cell array with the values to use for the model variables when prediction with the model (i.e., these are the thetaHat)
        %   Output:
        %     dependentVariablesHat - Struct with elements for each of the t_mcmcBayesDependentVariablesHat types specified in dependentVariablesHatTypes 
        %     (e.g., the mean and the {hingelo/hingehi} values from the histogram of the predicted values over predictionsNSamples samples)           
            if ~strcmp(type, 'linear') && ~strcmp(type, 'polynomialDegree2')
                error('Incorrect model type.');
            end
            datavarid=1;
            x=cast(data.getTransformedDataValuesPost(mcmcBayes.t_dataCategory.independent, datavarid),'double');
            
            depdatavarid=1;
            depVarName=data.depVarsName{depdatavarid};
            depVarDesc=data.depVarsDesc{depdatavarid};
            depVarTransformPre=data.depVarsTfPre(depdatavarid);
            depVarTransformPost=data.depVarsTfPost(depdatavarid);
            
            numdatapoints=numel(x);                        
            if strcmp(type, 'linear')
                beta0=varargin{1};
                beta1=varargin{2};
                r=varargin{3};
                for datapoint=1:numdatapoints
                    %convert r to MATLAB's normal(mu,sigma), or sigma=1/sqrt(r)
                    m(datapoint)=beta0+beta1*x(datapoint);
                    pd=makedist('Normal',m(datapoint),1/sqrt(r));
                    Yhat(datapoint,:)=random(pd,1,predictionsNSamples);
                end
            elseif strcmp(type, 'polynomialDegree2')
                beta0=varargin{1};
                beta1=varargin{2};
                beta2=varargin{3};
                r=varargin{4};
                for datapoint=1:numdatapoints
                    %convert r to MATLAB's normal(mu,sigma), or sigma=1/sqrt(r)
                    m(datapoint)=beta0+beta1*x(datapoint)+beta2*(x(datapoint))^2;
                    pd=makedist('Normal',m(datapoint),1/sqrt(r));
                    Yhat(datapoint,:)=random(pd,1,predictionsNSamples);
                end             
            else
                error('unsupported model for mcmcBayes.regression::simPredictions()');
            end

            binprec=(ceil(max(max(Yhat)))-floor(min(min(Yhat))))/1000;
            bins=floor(min(min(Yhat))):binprec:ceil(max(max(Yhat)));
            depVarsValuesHat=mcmcBayes.model.capturePredictionThresholds(predictionsNSamples, predictionsToCapture, hingeThreshold, bins, ...
                mcmcBayes.t_numericDimensions.vector, Yhat, data.depVarsType{depdatavarid}); 
            retYhat=Yhat;
        end
    end
    
    methods 
        function obj=regression(settype, setsubType, setname, setdescription, setauthor, setreference, setprefix, setcomputemsfe)
        % obj=regression(setmodeltype, setpriordatatype, setdatatype, setsamplerinterface, setosInterface)
        %   The constructor for regression.
        % Input:
        %   setmodeltype - Specifies the type of the sub-model being instantiated
        %   setpriordatatype - t_mcmcBayesData type that specifies the priorDependentVariables (e.g., CCMPerformanceWeighting, EqualWeighting, Individual, and Simulated)
        %   setdatatype - t_mcmcBayesData type that specifies dependentVariables (e.g., ConFR, LinIFR, ExpIFR, LinDFR, ExpDFR, and Simulated)
        %   setdatasetid - Specifies the dataset id to load for this model evaluation
        %   setsamplerinterface - instanted sampler interface, mcmcBayesSamplerInterfaces class (e.g., mcmcBayesSamplerInterfacesOpenBUGS or mcmcBayesSamplerInterfacesJAGS)
        %   setosInterface - instantiated os interface, mcmcBayesosInterfaces class (e.g., mcmcBayesosInterfaces_UbuntuLinux or mcmcBayesosInterfaces_Windows7)
        % Output:
        %   obj Constructed regression object
            if nargin == 0
                superargs={};
            else
                superargs{1}=settype;
                superargs{2}=setsubType;
                superargs{3}=setname;
                superargs{4}=setdescription;
                superargs{5}=setauthor;
                superargs{6}=setreference;
                superargs{7}=setprefix;   
                superargs{8}=setcomputemsfe;
            end

            obj=obj@mcmcBayes.model(superargs{:});           
        end
                
        function displayPredictions(obj, tempid)
        % displayPredictions(obj)
        %   Displays the values (in the console) for the already generated model predictions in obj.dependentVariablesHat.
        %   Input:
        %     tempid - Temperature id to compute the display the predictions for
            stdout=1;
            %print the original data
            datavarid=1;                
            if tempid==0
                x=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorData, mcmcBayes.t_dataCategory.independent, datavarid);
                if obj.priorData.dataSource~=mcmcBayes.t_dataSource.Preset %don't print preset, it is empty
                    fprintf(stdout,'x=%s\n',conversionUtilities.matrix_tostring(x));
                    Yorig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorData, mcmcBayes.t_dataCategory.dependent, datavarid);
                    fprintf(stdout,'Yorig=%s\n',conversionUtilities.matrix_tostring(Yorig));
                end
            else
                x=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.independent, datavarid);
                fprintf(stdout,'x=%s\n',conversionUtilities.matrix_tostring(x));
                Yorig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.dependent, datavarid);
                fprintf(stdout,'Yorig=%s\n',conversionUtilities.matrix_tostring(Yorig));
            end
            
            %print the predictions
            for Yhattype=obj.predictionsToCapture%use the vector obj.predictionsToCapture to determine which structure elements from priorPredictedData to show
                datavarid=1;
                if tempid==0
                    x=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.independent, datavarid, 'mean');%just use mean (all the same)
                else
                    x=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.independent, datavarid, 'mean');%just use mean (all the same)
                end
                switch Yhattype
                    case mcmcBayes.t_dataResult.hingelo
                        if tempid==0
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');%just use mean (all the same)
                        else
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');%just use mean (all the same)
                        end
                        name='Yhat.hingelo';
                    case mcmcBayes.t_dataResult.mean
                        if tempid==0
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');%just use mean (all the same)
                        else
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');%just use mean (all the same)
                        end
                        name='Yhat.mean';
                    case mcmcBayes.t_dataResult.hingehi
                        if tempid==0
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');%just use mean (all the same)
                        else
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');%just use mean (all the same)
                        end
                        name='Yhat.hingehi';
                    otherwise
                        error('Cannot identify the appropriate structure element in obj.predictionsToCapture');
                end
                %only display the mean for now
                if Yhattype==mcmcBayes.t_dataResult.mean
                    fprintf(stdout,'x=%s\n',conversionUtilities.matrix_tostring(x));
                    fprintf(stdout,'%s=%s\n',name,conversionUtilities.matrix_tostring(Yhat));
                end
            end
        end
        
        function plotPredictions(obj, chainID, tempID)
        % plotPredictions(obj, tempid)
        %   Plots the model simulation results, using point estimates for Thetahat, over the original data.
        %   Input:
        %     tempid - Temperature id to compute the display the predictions for
            sessiondetails=sprintf('%s-%s-chainid%d-tempid%d',obj.filenamePrefix, obj.uuid, chainID, tempID);            
                  
            xmin=0;
            xmax=0;
            ymin=0;
            ymax=0;
            
            fig();
            hold on;           
            %plot the original data 
            datavarid=1;
            if tempID==0
                x=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorData, mcmcBayes.t_dataCategory.independent, datavarid);
                if obj.priorData.dataSource~=mcmcBayes.t_dataSource.Preset
                    Yorig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorData, mcmcBayes.t_dataCategory.dependent, datavarid);
                end
            else
                x=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.independent, datavarid);
                Yorig=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorData, mcmcBayes.t_dataCategory.dependent, datavarid); 
            end
            if (tempID==0)
                if exist('Yorig','var')
                    ymin=min([ymin min(Yorig)]);%could be < 0
                    ymax=max([ymax max(Yorig)]);%shouldn't be < 0
                    plot(x,Yorig,'g*');
                end
            else
                ymin=min([ymin min(Yorig)]);%could be < 0
                ymax=max([ymax max(Yorig)]);%shouldn't be < 0
                plot(x,Yorig,'r*');
            end
            %do the scaling after computing it (otherwise, if it is in a loop, it will compound on the scaling)
            ymax=1.1*ymax;
            if (ymin==ymax)%avoid the case when they're both 0
                ymax=ymin+1;
            end
            
            %plot the predicted data 
            datavarid=1;                
            if (tempID==0)
                x=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.independent, datavarid, 'mean');%just use mean (all the same)
            else
                x=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.independent, datavarid, 'mean');%just use mean (all the same)
            end  
            xmin=min([xmin min(x)]);%could be < 0
            xmax=max([xmax max(x)]);%shouldn't be < 0            
            for predictionToCapture=obj.predictionsToCapture%use the vector obj.dependentVariablesHatTypes to determine which rows from dependentVariablesHat to show            
                if (tempID==0)
                    switch predictionToCapture
                        case mcmcBayes.t_dataResult.hingelo
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');
                            plot(x, Yhat, 'g--');%lo bound dashed plot line
                        case mcmcBayes.t_dataResult.mean
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');
                            plot(x, Yhat, 'g-');%mean solid plot line
                        case mcmcBayes.t_dataResult.hingehi
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.priorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');
                            plot(x, Yhat, 'g-.');%hi bound dashed plot line
                        otherwise
                            %won't get here
                    end
                else
                    switch predictionToCapture
                        case mcmcBayes.t_dataResult.hingelo
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingelo');
                            plot(x, Yhat, 'r--');%lo bound dashed plot line
                        case mcmcBayes.t_dataResult.mean
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'mean');
                            plot(x, Yhat, 'r-');%mean solid plot line
                        case mcmcBayes.t_dataResult.hingehi
                            Yhat=obj.getTransformedDataValuesPost(mcmcBayes.t_data.posteriorPredictedData, mcmcBayes.t_dataCategory.dependent, datavarid, 'hingehi');
                            plot(x, Yhat, 'r-.');%hi bound dashed plot line
                        otherwise
                            error('Cannot identify the appropriate structure in obj.predictionsToCapture');
                    end
                end
                ymin=min([ymin min(Yhat)]);%could be < 0
                ymax=max([ymax max(Yhat)]);%shouldn't be < 0
            end                 
            
            if size(unique(x),1)==size(x,1) && size(unique(x),1)<10 %No duplicates
                set(gca,'XTickMode','manual');
                set(gca,'xTick',[]);
                set(gca,'xTick',unique(sort(x)));
            end
                
            %do the scaling after computing it (otherwise, if it is in a loop, it will compound on the scaling)
            if (ymin<0)
                ymin=1.1*ymin;
            else
                ymin=0.9*ymin;
            end
            ymax=1.1*ymax;
            if (ymin==ymax)%avoid the case when they're both 0
                ymax=ymin+1;
            end
            set(gca,'xLim',[xmin xmax]);
            set(gca,'yLim',[ymin ymax]);
            tempstr=sprintf('Model prediction results for %s, %s', obj.name, sessiondetails);
            title(tempstr);
            filename=sprintf('%s%s-PredictionsSet.fig',obj.osInterface.figuresDir,sessiondetails);%need _PredictionsSet for filename parsing
            savefig(gcf,filename);
            
            hold off;        
        end                
    end

    methods (Access=protected)
        function [status]=checkDataCompatibility(obj, datatype)
        % [status]=checkDataCompatibility(obj, datatype)
        %   Checks the compatibility of priorDependentVariables with the specified mcmcBayesModelRegression type
            switch datatype
                case mcmcBayes.t_data.priordata
                	status=obj.priorData.checkDataForInfNanValues();
                case mcmcBayes.t_data.priorbasedpredictions
                	status=obj.priorPredictedData.checkDataForInfNanValues();
                case mcmcBayes.t_data.posteriordata
                	status=obj.posteriorData.checkDataForInfNanValues();
                case mcmcBayes.t_data.posteriorbasedpredictions
                	status=obj.posteriorPredictedData.checkDataForInfNanValues();
            end
        end
    end
    
    methods %getter/setter functions       

    end
end
