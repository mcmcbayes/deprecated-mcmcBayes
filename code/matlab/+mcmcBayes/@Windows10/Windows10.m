classdef Windows10 < mcmcBayes.osInterfaces
    %all class properties are inherited
    properties

    end
    
    methods   
        function obj = Windows10(cfgMcmcBayes)%constructor
            obj=obj@mcmcBayes.osInterfaces(mcmcBayes.t_osInterfaces.Windows10, cfgMcmcBayes);        
        end
    end
        
    methods %getter/setter functions

    end
end