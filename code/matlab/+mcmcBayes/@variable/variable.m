classdef variable     
%Class used to store model variable details (distribution, parameters, etc).
    properties
        name%name for the variable
        description%describes the variable
        type%mcmcBayes.t_variable
        distribution%mcmcBayes.distribution (setter depends on type property, set type first)
        precision%statistics resolution-used for computation and display (1.0E+0=discrete, <1.0E+0=continuous)
        dimensions%string describing the base dimensions of the variable values
        tfPre
        tfPost
        
        %independent variable associated with the stochasticProcess variable (only used for type t_variable.stochasticProcess)
        %MCMC_initvalue and MCMC_samples will for the stochasticProcess variable will both associate with indVars below
        indVarsName={};%cell array of names for the data, size [numIndVars x 1] with a {string} for each element
        indVarsDesc={};%cell array of descriptions for the data, size [numIndVars x 1] with a {string} for each element
        indVarsType={};%cell array of strings describing the data type (e.g., 'int32', 'double', 'categorical'), size [numIndVars x 1] with a {string} for each element
        indVarsDimensions%array of mcmcBayes.t_numericDimensions, size [numIndVars x 1]
        indVarsRange={};%array of mcmcBayes.t_numericRange, size [numIndVars x 1]
        indVarsRes={};%array of mcmcBayes.t_numericResolution, size [numIndVars x 1]
        indVarsValues={};%cell array with the data values, size [numIndVars x 1] with a {valuesArray} for each element that is size [1 x 1] for scalars,
                     %[Nx x 1] for vectors, and [Nx x Ny] for 2d matrices

        %MCMC setup
        MCMC_nsamples_adapt%number of samples to perform in adaptation mode for seeking optimal sampler performance (e.g. whether the rejection rate of a Metropolis-Hasting sampler is close to the theoretical optimum
        MCMC_nsamples_burnin%number of samples to perform for burn-in
        MCMC_nsamples_data%number of samples to perform for data gathering
        MCMC_nsamples_lag%sampling lag for data gathering
        MCMC_hpdalpha%alpha to use for the hpd estimations
        MCMC_acfmaxlag=int32(150);%maximum lag to compute the autocorrelation function using
        MCMC_hpdmaxbins=10000;%number of histogram bins for estimating the hpd intervals
        MCMC_pdfmaxbins=500;%number of histogram bins for estimating the pdf

        MCMC_initvalue%Not a cell array! initialization value (scalar, vector, or matrix) for the MCMC chain (size depends on distribution)
        
        %MCMC data
        MCMC_samples%array of samples (scalar, vector, or matrix), size=MCMC_nsamples_data (size depends on distribution)

        %MCMC statistics
        MCMC_mean%mean of the MCMC samples (size depends on whether distribution is a scalar, vector, or matrix)
        MCMC_mean_raw%array of the mean of the unadjusted MCMC samples  (size depends on whether distribution is a scalar, vector, or matrix)
        MCMC_median%median of the MCMC samples (size depends on whether distribution is a scalar, vector, or matrix)
        MCMC_sd%standard deviation of the MCMC samples (size depends on whether distribution is a scalar, vector, or matrix)
        MCMC_min%minimum of the MCMC samples (size depends on whether distribution is a scalar, vector, or matrix)
        MCMC_max%maximum of the MCMC samples (size depends on whether distribution is a scalar, vector, or matrix)
        MCMC_hpdlo%highest posterior density lower limit (using MCMC_hpdalpha) (size depends on whether distribution is a scalar, vector, or matrix)
        MCMC_hpdhi%highest posterior density higher limit (using MCMC_hpdalpha) (size depends on whether distribution is a scalar, vector, or matrix)       
        MCMC_acf%autocorrelation function values for lag=0:1:MCMC_acfmaxlag
        MCMC_mce%monte carlo error of the MCMC samples

        solverMin%value for the variable's lower bound (size depends on whether distribution is a scalar, vector, or matrix)
        solverMax%value for the variable's upper bound (size depends on whether distribution is a scalar, vector, or matrix)
    end

    methods (Static, Access=public)
        function [mce]=computeMCE(sampledata)
        % [mce]=computeMCE(sampledata)
        %   Computes the Monte Carlo error of the sample data.
        %   Input:
        %     sampledata - vector with the samples
        %   Output:
        %     MCE - computed Monte Carlo error
            numsamples=size(sampledata,1);

            %calculate the batch mean for the parameter
            K=50;
            v=numsamples/K;
            width=floor(v);
            batches=zeros(K,width);
            batchmean=zeros(1,K);
            for b=1:K
                batches(b,:)=sampledata((b-1)*width+1:b*width);
                batchmean(b)=1/v*sum(batches(b,:));
            end
            sampmean=1/K*sum(batchmean);%equates to mean(sampledata)
            
            %calculate the MCMC error for the parameter
            deviationbatchmean=0;
            for b=1:K
                deviationbatchmean(b)=(batchmean(b)-sampmean)^2;
            end
            mce=sqrt((1/K*(K-1))*sum(deviationbatchmean));            
        end 
    end    
    
    methods (Static, Access=private)
        function [lo, hi]=genhpd(precision, sampleRange, samples, alpha)
        %GENHPD Generates HPDs using Chen-Shao, 2000 (Monte Carlo Methods in Bayesian Computation)
        % [LO, HI]=GENHPD(PRECISION, NSAMPLES, BURNIN, PARAMETER_RANGEMAX, SAMPLES, ALPHA)
        % Input:
        %   PRECISION - resolution for the credible region estimation
        %   SAMPLERANGE - 2 x 1 vector with minimum and maximum thresholds for the samples
        %   SAMPLES - raw vector of samples
        %   ALPHA - 100(1-ALPHA)% credible region
        % Output:
        %   LO - HPD low threshold
        %   HI - HPD high threshold
        % Troubleshooting: if the hpd calculations for lo,hi are identical, try increasing the precision 
            try
                [samplessorted, samplesindex]=sort(samples,'ascend');
                %x=parameter_rangemax(1):precision:parameter_rangemax(2);
                if precision==1
                    x=sampleRange(1):1:sampleRange(2);
                else
                    x=sampleRange(1):(sampleRange(2)-sampleRange(1))/precision:sampleRange(2);
                end            

                n=numel(samples);

                min=-1;
                jmax=(n-floor((1-alpha)*n));
                Rp=zeros(jmax,2);
                for j=1:jmax
                    CI(1)=x(find(x<=samplessorted(j),1,'last'));
                    CI(2)=x(find(x<=samplessorted(j+floor((1-alpha)*n)),1,'last'));
                    Rp(j,:)=CI;
                    newwidth=abs(Rp(j,2)-Rp(j,1));
                    if min==-1 || newwidth<minwidth
                        min=j;
                        minwidth=newwidth;
                        %disp(sprintf('HPD=[%f,%f]',Rp(min,1),Rp(min,2)));
                    end
                    pause(0);%helps ctrl-c response
                    drawnow;%Force display to update immediately
                end
                lo=Rp(min,1);
                hi=Rp(min,2);        
            catch
                lo=median(samples);
                hi=median(samples);
                warning('There was an issue with the hpd calculations, setting hpd lo,hi to sample medians.');
            end
        end
                
        function [acf]=computeACF(acfmaxlag, sampledata)
            %shape for sampledata should be (1, Tau)
            %shape for return data should be (1, acfmaxlag+1)
            Tau=size(sampledata,2);
            sampmean=mean(sampledata,2);

            lags=0:1:cast(acfmaxlag,'double');
            acf=zeros(1,numel(lags));
            if ~isequal(sampledata,zeros(Tau,1))
                for j=1:numel(lags)
                    num=0;
                    den=0;
                    lag=lags(j);
                    for k=1:Tau-lag
                        num=num+(sampledata(k)-sampmean)*(sampledata(k+lag)-sampmean);
                    end
                    for k=1:Tau
                        den=den+(sampledata(k)-sampmean)^2;
                    end
                    acf(1,j)=((1/(Tau-lag))*num)/((1/(Tau-1))*den);
                end
            else
                %all zeros
            end
        end                   
    end
    
    methods
        function obj = variable(setname,setdescription,settype,setDimensions,setdistribution,setprecision,settfPre,settfPost,setSolverMin,setSolverMax,...
                setMCMC_nsamples_adapt, setMCMC_nsamples_burnin,setMCMC_nsamples_data,setMCMC_nsamples_lag,setMCMC_initvalue,setMCMC_hpdalpha, ...
                setindVarsName, setindVarsDesc, setindVarsType, setindVarsRange, setindVarsRes, setindVarsValues) %constructor
            if nargin > 0 %necessary condition for creating empty object arrays
                obj.name=setname;
                obj.description=setdescription;
                obj.type=settype;
                obj.distribution=setdistribution;
                obj.dimensions=setDimensions;
                obj.precision=setprecision;
                obj.tfPre=settfPre;
                obj.tfPost=settfPost;
                obj.solverMin=setSolverMin;
                obj.solverMax=setSolverMax;
                
                obj.indVarsName=setindVarsName;
                obj.indVarsDesc=setindVarsDesc;
                obj.indVarsType=setindVarsType;
%obj.indVarsSize=;
                obj.indVarsRange=setindVarsRange;
                obj.indVarsRes=setindVarsRes;
                obj.indVarsValues=setindVarsValues;
                
                %MCMC setup
                obj.MCMC_nsamples_adapt=setMCMC_nsamples_adapt;
                obj.MCMC_nsamples_burnin=setMCMC_nsamples_burnin;
                obj.MCMC_nsamples_data=setMCMC_nsamples_data;
                obj.MCMC_nsamples_lag=setMCMC_nsamples_lag;
                obj.MCMC_initvalue=setMCMC_initvalue;
                obj.MCMC_hpdalpha=setMCMC_hpdalpha;
                %MCMC data/statistics
                obj.MCMC_samples=[];
                obj.MCMC_mean=[];
                obj.MCMC_median=[];
                obj.MCMC_sd=[];
                obj.MCMC_min=[];
                obj.MCMC_max=[];
                obj.MCMC_hpdlo=[];
                obj.MCMC_hpdhi=[];
                obj.MCMC_acf=[];
                obj.MCMC_mce=[];
            end
        end
             
        function plotpdf(obj,hfigarray,figures_dir,sessiondetails)
            %plot the pdf for the variable (currently is a histogram)
            sampledata=obj.MCMC_samples;

            varSizes=size(sampledata);
            defdims=obj.dimensions;
            if defdims==mcmcBayes.t_numericDimensions.scalar
                Tau=varSizes(1);%numDataPoints
                R=1;
            elseif defdims==mcmcBayes.t_numericDimensions.vector
                R=varSizes(1);%numRows (at some point we may have variables that are different sizes, thus we could need an array for R, one for each variable)
                Tau=varSizes(2);%numDataPoints
            else
                error('matrices todo');
            end                    
            
            if obj.type==mcmcBayes.t_variable.constant
                dispstr=sprintf('\tNot plotting pdf for constant variable %s',obj.name);
                disp(dispstr);
                close(hfigarray);
                delete(hfigarray);
                return;
            end

            if defdims==mcmcBayes.t_numericDimensions.scalar
                if ~isequal(sampledata,zeros(Tau,1))%all zeros for nhpp models at t=0
                    parameter_rangemax=[min(sampledata) max(sampledata)];                   
                    if obj.precision>parameter_rangemax(2)-parameter_rangemax(1)
                        precision=(parameter_rangemax(2)-parameter_rangemax(1))/obj.MCMC_pdfmaxbins;
                        x=parameter_rangemax(1):precision:parameter_rangemax(2);%presuming for discrete that precision=1.0e-0
                        y=hist(sampledata,x);%if you run out of memory, adjust the precision
                        y=y/Tau;
                    elseif obj.precision<=(parameter_rangemax(2)-parameter_rangemax(1))/obj.MCMC_pdfmaxbins
                        precision=(parameter_rangemax(2)-parameter_rangemax(1))/obj.MCMC_pdfmaxbins;
                        x=parameter_rangemax(1):precision:parameter_rangemax(2);
                        y=hist(sampledata,x);%if you run out of memory, adjust the precision
                        y=y/Tau;
                    else
                        precision=obj.precision;
                        x=parameter_rangemax(1):precision:parameter_rangemax(2);%presuming for discrete that precision=1.0e-0
                        y=hist(sampledata,x);%if you run out of memory, adjust the precision
                        y=y/Tau;
                    end                   
                    set(0,'CurrentFigure',hfigarray);
                    hold on;
                    if (obj.precision==1.0E+0)%discrete
                        tempstr=sprintf('PMF for %s, %s',obj.name,sessiondetails);
                        filename=sprintf('%s%s-PMF.fig',figures_dir,sessiondetails);
                        bar(x,y);
                    else
                        tempstr=sprintf('PDF for %s, %s',obj.name,sessiondetails);
                        filename=sprintf('%s%s-PDF.fig',figures_dir,sessiondetails);
                        plot(x,y);
                    end
                    title(tempstr,'Interpreter','none');
                    line([obj.MCMC_hpdlo obj.MCMC_hpdlo], [0 1.2*max(y)]);
                    line([obj.MCMC_hpdhi obj.MCMC_hpdhi], [0 1.2*max(y)]);
                    hold off;
                    savefig(filename);
                end
            elseif defdims==mcmcBayes.t_numericDimensions.vector
                for elemid=1:R
                    if ~isequal(sampledata(elemid,:),zeros(1,Tau))%all zeros for nhpp models at t=0
                        parameter_rangemax=[min(sampledata(elemid,:)) max(sampledata(elemid,:))];                   
                        if obj.precision>parameter_rangemax(2)-parameter_rangemax(1)
                            precision=(parameter_rangemax(2)-parameter_rangemax(1))/obj.MCMC_pdfmaxbins;
                            x=parameter_rangemax(1):precision:parameter_rangemax(2);%presuming for discrete that precision=1.0e-0
                            y=hist(sampledata(elemid,:),x);%if you run out of memory, adjust the precision
                            y=y/Tau;
                        elseif obj.precision<=(parameter_rangemax(2)-parameter_rangemax(1))/obj.MCMC_pdfmaxbins
                            precision=(parameter_rangemax(2)-parameter_rangemax(1))/obj.MCMC_pdfmaxbins;
                            x=parameter_rangemax(1):precision:parameter_rangemax(2);
                            y=hist(sampledata(elemid,:),x);%if you run out of memory, adjust the precision
                            y=y/Tau;
                        else
                            precision=obj.precision;
                            x=parameter_rangemax(1):precision:parameter_rangemax(2);%presuming for discrete that precision=1.0e-0
                            y=hist(sampledata(elemid,:),x);%if you run out of memory, adjust the precision
                            y=y/Tau;
                        end                   
                        set(0,'CurrentFigure',hfigarray(elemid));
                        hold on;
                        if (obj.precision==1.0E+0)%discrete
                            tempstr=sprintf('PMF for %s(%d), %s',obj.name,elemid,sessiondetails);
                            filename=sprintf('%s%sPMF%d.fig',figures_dir,sessiondetails,elemid);
                            bar(x,y);
                        else
                            tempstr=sprintf('PDF for %s(%d), %s',obj.name,elemid,sessiondetails);
                            filename=sprintf('%s%sPDF%d.fig',figures_dir,sessiondetails,elemid);
                            plot(x,y);
                        end
                        title(tempstr,'Interpreter','none');
                        line([obj.MCMC_hpdlo(elemid) obj.MCMC_hpdlo(elemid)], [0 1.2*max(y)]);
                        line([obj.MCMC_hpdhi(elemid) obj.MCMC_hpdhi(elemid)], [0 1.2*max(y)]);
                        hold off;
                        savefig(filename);
                    end                    
                end                
            else
                error('matrices todo');                
            end
        end
        
        function plotergodicmean(obj,hfigarray,figures_dir,sessiondetails)%plot the ergodic mean for the variable
            sampledata=obj.MCMC_samples;

            varSizes=size(sampledata);
            defdims=obj.dimensions;
            if defdims==mcmcBayes.t_numericDimensions.scalar
                Tau=varSizes(1);%numDataPoints
                R=1;
            elseif defdims==mcmcBayes.t_numericDimensions.vector
                R=varSizes(1);%numRows (at some point we may have variables that are different sizes, thus we could need an array for R, one for each variable)
                Tau=varSizes(2);%numDataPoints
            else
                error('matrices todo');
            end                           
            
            if obj.type==mcmcBayes.t_variable.constant
                dispstr=sprintf('\tNot plotting ergodic mean for constant variable %s',obj.name);
                disp(dispstr);   
                close(hfigarray);
                delete(hfigarray);
                return;
            end            
            
            if defdims==mcmcBayes.t_numericDimensions.scalar
                if ~isequal(sampledata,zeros(Tau,1))
                    ergodicmean_parameter=cumsum(sampledata);
                    for j=1:Tau
                        ergodicmean_parameter(j)=ergodicmean_parameter(j)/j;
                    end
                    set(0,'CurrentFigure',hfigarray);
                    plot(ergodicmean_parameter);
                    tempstr=sprintf('ergodic mean for %s, %s',obj.name,sessiondetails);
                    title(tempstr,'Interpreter','none');
                    filename=sprintf('%s%s-ErgodicMean.fig',figures_dir,sessiondetails);
                    savefig(filename);
                    pause(1);%delay to allow for viewing
                end                               
            elseif defdims==mcmcBayes.t_numericDimensions.vector
                for elemid=1:R
                    if ~isequal(sampledata(elemid,:),zeros(1,Tau))
                        ergodicmean_parameter=cumsum(sampledata(elemid,:));
                        for j=1:Tau
                            ergodicmean_parameter(j)=ergodicmean_parameter(j)/j;
                        end
                        set(0,'CurrentFigure',hfigarray(elemid));
                        plot(ergodicmean_parameter);
                        tempstr=sprintf('ergodic mean for %s(%d), %s',obj.name,elemid,sessiondetails);
                        title(tempstr,'Interpreter','none');
                        filename=sprintf('%s%s-ErgodicMean%d.fig',figures_dir,sessiondetails,elemid);
                        savefig(filename);
                        pause(1);%delay to allow for viewing
                    end
                end
            else
                error('matrices todo');                
            end
        end
        
        function plothistory(obj,hfigarray,figures_dir,sessiondetails)%plot the samples for the variable
            sampledata=obj.MCMC_samples;

            varSizes=size(sampledata);
            defdims=obj.dimensions;
            if defdims==mcmcBayes.t_numericDimensions.scalar
                Tau=varSizes(1);%numDataPoints
                R=1;
            elseif defdims==mcmcBayes.t_numericDimensions.vector
                R=varSizes(1);%numRows (at some point we may have variables that are different sizes, thus we could need an array for R, one for each variable)
                Tau=varSizes(2);%numDataPoints
            else
                error('matrices todo');
            end             
            
            if obj.type==mcmcBayes.t_variable.constant
                dispstr=sprintf('\tNot plotting history for constant variable %s',obj.name);
                disp(dispstr);   
                close(hfigarray);
                delete(hfigarray);
                return;
            end
            
            if defdims==mcmcBayes.t_numericDimensions.scalar
                if ~isequal(sampledata,zeros(Tau,1))
                    set(0,'CurrentFigure',hfigarray);
                    plot(sampledata);
                    tempstr=sprintf('raw samples after burnin for %s, %s',obj.name,sessiondetails);
                    title(tempstr,'Interpreter','none');
                    filename=sprintf('%s%s-RawSamples.fig',figures_dir,sessiondetails);
                    savefig(filename);
                    pause(1);%delay to allow for viewing
                end
            elseif defdims==mcmcBayes.t_numericDimensions.vector
                for elemid=1:R
                    if ~isequal(sampledata(elemid,:),zeros(1,Tau))
                        set(0,'CurrentFigure',hfigarray(elemid));
                        plot(sampledata(elemid,:));
                        tempstr=sprintf('raw samples after burnin for %s(%d), %s',obj.name,elemid,sessiondetails);
                        title(tempstr,'Interpreter','none');
                        filename=sprintf('%s%s-RawSamples%d.fig',figures_dir,sessiondetails,elemid);
                        savefig(filename);
                        pause(1);%delay to allow for viewing
                    end
                end
            else
                error('matrices todo');                
            end                
        end
        
        function plotautocorrelation(obj,hfigarray,figures_dir,sessiondetails)%plot the variable's autocorrelation
            sampledata=obj.MCMC_samples;

            varSizes=size(sampledata);
            defdims=obj.dimensions;
            if defdims==mcmcBayes.t_numericDimensions.scalar
                Tau=varSizes(1);%numDataPoints
                R=1;
            elseif defdims==mcmcBayes.t_numericDimensions.vector
                R=varSizes(1);%numRows (at some point we may have variables that are different sizes, thus we could need an array for R, one for each variable)
                Tau=varSizes(2);%numDataPoints
            else
                error('matrices todo');
            end   
            
            if obj.type==mcmcBayes.t_variable.constant
                dispstr=sprintf('\tNot plotting autocorrelation function vs lags for constant variable %s',obj.name);
                disp(dispstr); 
                close(hfigarray);
                delete(hfigarray);
                return;
            end
            
            if defdims==mcmcBayes.t_numericDimensions.scalar
                if ~isequal(sampledata,zeros(Tau,1))
                    set(0,'CurrentFigure',hfigarray);
                    bar(double(0:1:obj.MCMC_acfmaxlag),double(obj.MCMC_acf));
                    xlim([0 obj.MCMC_acfmaxlag]);
                    tempstr=sprintf('autocorrelation function vs lags for %s, %s',obj.name,sessiondetails);
                    title(tempstr,'Interpreter','none');
                    ylabel('ACF');
                    xlabel('lags');
                    filename=sprintf('%s%s-ACF.fig',figures_dir,sessiondetails);
                    savefig(filename);
                end
            elseif defdims==mcmcBayes.t_numericDimensions.vector
                for elemid=1:R
                    if ~isequal(sampledata(elemid,:),zeros(1,Tau))
                        set(0,'CurrentFigure',hfigarray(elemid));
                        bar(double(0:1:obj.MCMC_acfmaxlag),double(obj.MCMC_acf(elemid,:)));
                        xlim([0 obj.MCMC_acfmaxlag]);
                        tempstr=sprintf('autocorrelation function vs lags for %s(%d), %s',obj.name,elemid,sessiondetails);
                        title(tempstr,'Interpreter','none');
                        ylabel('ACF');
                        xlabel('lags');
                        filename=sprintf('%s%s-ACF%d.fig',figures_dir,sessiondetails,elemid);
                        savefig(filename);
                    end
                end
            else
                error('matrices todo');                
            end            
        end
        
        function retobj=updateStatistics(obj)
            sampledata=obj.MCMC_samples;

            if obj.type==mcmcBayes.t_variable.constant
                dispstr=sprintf('\tNot computing statistics for constant variable %s=%f',obj.name, sampledata(1,1,1));
                disp(dispstr);                    
                obj.MCMC_mean=sampledata(1,1,1);
                obj.MCMC_median=sampledata(1,1,1);
                if obj.distribution.resolution==mcmcBayes.t_numericResolution.discrete
                    obj.MCMC_sd=int32(0);
                else
                    obj.MCMC_sd=double(0);
                end
                obj.MCMC_min=sampledata(1,1,1);
                obj.MCMC_max=sampledata(1,1,1);
                obj.MCMC_hpdlo=sampledata(1,1,1);
                obj.MCMC_hpdhi=sampledata(1,1,1);
                obj.MCMC_mce=0;
                obj.MCMC_acf=zeros(obj.MCMC_acfmaxlag,1);
                retobj=obj;
                return;
            end            
            varSizes=size(sampledata);
            defdims=obj.dimensions;
            if defdims==mcmcBayes.t_numericDimensions.scalar
                Tau=varSizes(1);%numDataPoints
                R=1;
            elseif defdims==mcmcBayes.t_numericDimensions.vector
                R=varSizes(1);%numRows (at some point we may have variables that are different sizes, thus we could need an array for R, one for each variable)
                Tau=varSizes(2);%numDataPoints
            else
                error('matrices todo');
            end            
            
            if defdims==mcmcBayes.t_numericDimensions.scalar
                %compute common statistics
                obj.MCMC_mean=mean(sampledata,1);
                obj.MCMC_median=median(sampledata,1);
                obj.MCMC_sd=std(sampledata,0,1);%see std documentation for explanation of w=0
                obj.MCMC_min=min(sampledata,[],1);%see min documentation for explanation of []
                obj.MCMC_max=max(sampledata,[],1);%see max documentation for explanation of []

                %compute hpds
                dispstr=sprintf('\tComputing HPDs for %s',obj.name);
                disp(dispstr);
                parameter_rangemax=[obj.MCMC_min obj.MCMC_max];
                if obj.precision==1 %discrete
                    [hpdlo,hpdhi]=obj.genhpd(1, parameter_rangemax, reshape(sampledata,[Tau 1 1]), obj.MCMC_hpdalpha);
                else
                    [hpdlo,hpdhi]=obj.genhpd(obj.MCMC_hpdmaxbins, parameter_rangemax, reshape(sampledata,[Tau 1 1]), obj.MCMC_hpdalpha);
                end
                obj.MCMC_hpdlo=hpdlo;
                obj.MCMC_hpdhi=hpdhi;
                %compute MCE and ACF
                obj.MCMC_mce=obj.computeMCE(sampledata);
                obj.MCMC_acf=obj.computeACF(obj.MCMC_acfmaxlag, reshape(sampledata, [1 Tau]));               
            elseif defdims==mcmcBayes.t_numericDimensions.vector
                %compute common statistics
                obj.MCMC_mean=mean(sampledata,2);
                obj.MCMC_median=median(sampledata,2);
                obj.MCMC_sd=std(sampledata,0,2);%see std documentation for explanation of w=0
                obj.MCMC_min=min(sampledata,[],2);%see min documentation for explanation of []
                obj.MCMC_max=max(sampledata,[],2);%see max documentation for explanation of []

                %compute hpds
                dispstr=sprintf('\tComputing HPDs for %s',obj.name);
                disp(dispstr);
                parameter_rangemax=[obj.MCMC_min obj.MCMC_max];
                acf=zeros(R,obj.MCMC_acfmaxlag+1);
                for elemid=1:R
                    if obj.precision==1 %discrete
                        [hpdlo(elemid),hpdhi(elemid)]=obj.genhpd(1, parameter_rangemax(elemid,:), sampledata(elemid,:), obj.MCMC_hpdalpha);
                    else
                        [hpdlo(elemid),hpdhi(elemid)]=obj.genhpd(obj.MCMC_hpdmaxbins, parameter_rangemax(elemid,:), sampledata(elemid,:), obj.MCMC_hpdalpha);
                    end
                    %compute MCE and ACF
                    mce(elemid)=obj.computeMCE(reshape(sampledata(elemid,:),[Tau 1]));
                    acf(elemid,:)=obj.computeACF(obj.MCMC_acfmaxlag, sampledata(elemid,:));
                end
                obj.MCMC_hpdlo=hpdlo;
                obj.MCMC_hpdhi=hpdhi;
                obj.MCMC_mce=mce;
                obj.MCMC_acf=acf;
            else
                error('matrices todo');
            end

            retobj=obj;
        end                        
        
        function displayStats(obj)
            sampledata=obj.MCMC_samples;

            varSizes=size(sampledata);
            if numel(varSizes==2) %scalar or vector
                if varSizes(2)==1 %scalar
                    Tau=varSizes(1);
                    N=1;
                    M=1;
                else %vector
                    N=varSizes(1);
                    Tau=varSizes(2);
                    M=1;
                end
            else
                N=varSizes(1);
                M=varSizes(2);
                Tau=varSizes(3);
            end                    

            if N==1%scalar
                dispstr=sprintf('\tE[%s]=%e, Median[%s]=%e, SD[%s]=%e, MIN[%s]=%e, MAX[%s]=%e, HPD[%s]=[%e,%e], MCE[%s]=%e', ...
                    obj.name, ...
                    obj.MCMC_mean, ...
                    obj.name, ...
                    obj.MCMC_median, ...
                    obj.name, ...
                    obj.MCMC_sd, ...
                    obj.name, ...
                    obj.MCMC_min, ...
                    obj.name, ...
                    obj.MCMC_max, ...
                    obj.name, ...
                    obj.MCMC_hpdlo, ...
                    obj.MCMC_hpdhi, ...
                    obj.name, ...
                    obj.MCMC_mce);
                disp(dispstr);
            elseif N>1 && M==1%vector
                for elemid=1:N                
                    dispstr=sprintf('\tE[%s(%d)]=%e, Median[%s(%d)]=%e, SD[%s(%d)]=%e, MIN[%s(%d)]=%e, MAX[%s(%d)]=%e, HPD[%s(%d)]=[%e,%e], MCE[%s(%d)]=%e', ...
                        obj.name, ...
                        elemid, ...
                        obj.MCMC_mean(elemid), ...
                        obj.name, ...
                        elemid, ...
                        obj.MCMC_median(elemid), ...
                        obj.name, ...
                        elemid, ...
                        obj.MCMC_sd(elemid), ...
                        obj.name, ...
                        elemid, ...
                        obj.MCMC_min(elemid), ...
                        obj.name, ...
                        elemid, ...
                        obj.MCMC_max(elemid), ...
                        obj.name, ...
                        elemid, ...
                        obj.MCMC_hpdlo(elemid), ...
                        obj.MCMC_hpdhi(elemid), ...
                        obj.name, ...
                        elemid, ...
                        obj.MCMC_mce(elemid));
                    disp(dispstr);
                end
            elseif N>1 && M>1%matrix
                error('todo');
            end            
        end               
        
        function [retobj]=overrideMCMCSettings(obj, setMCMC_nsamples_adapt, setMCMC_nsamples_burnin, setMCMC_nsamples_data, setMCMC_nsamples_lag)
            for varid=1:numel(obj)
                obj(varid).MCMC_nsamples_adapt=setMCMC_nsamples_adapt;
                obj(varid).MCMC_nsamples_burnin=setMCMC_nsamples_burnin;
                obj(varid).MCMC_nsamples_data=setMCMC_nsamples_data;
                obj(varid).MCMC_nsamples_lag=setMCMC_nsamples_lag;
            end
            retobj=obj;
        end           
    end
    
    methods %getter/setter functions
        function obj = set.name(obj,setname)
            if ~ischar(setname)
                error('variable.name must be a char[]');
            else
                obj.name = setname;
            end
        end
        
        function value = get.name(obj)
            value=obj.name;
        end
        
        function obj = set.description(obj,setdescription)
            if ~ischar(setdescription)
                error('variable.description must be a char[]');
            else
                obj.description = setdescription;
            end
        end
        
        function value = get.description(obj)
            value=obj.description;
        end
        
        function obj = set.type(obj,settype)
            if ~isa(settype,'mcmcBayes.t_variable')
                error('variable.type must be type mcmcBayes.t_variable');
            else
                obj.type = settype;
            end
        end
        
        function value = get.type(obj)
            value=obj.type;
        end
        
        function obj = set.distribution(obj,setdistribution)
            if ~isa(setdistribution,'mcmcBayes.distribution')
                error('variable.distribution must be a mcmcBayes.distribution object');
            else
                if (obj.type==mcmcBayes.t_variable.stochasticProcess)                   
                    if ~((setdistribution.type==mcmcBayes.t_distribution.Multinomial) || (setdistribution.type==mcmcBayes.t_distribution.GammaProcess))
                        error('Distributions for stochastic processes should be discrete multivariate');
                    end
                end
                obj.distribution = setdistribution;
            end
        end
        
        function value = get.distribution(obj) %access the variable.methods through distribution property
            value=obj.distribution;
        end        
        
        function obj = set.precision(obj,setprecision)
            if ~isa(setprecision,'double')
                error('variable.precision must be a double>0');
            else
                if ~(setprecision>0)
                    error('variable.precision must be a double>0');
                else
                    obj.precision = setprecision;
                end
            end
        end
        
        function value = get.precision(obj)
            value=obj.precision;
        end

%         function obj = set.solverMin(obj,setSolverMin)
%             if ~obj.distribution.checkValue(setSolverMin)
%                 error('Attempted setting variable.solverMin to an illegal value');
%             else
%                 obj.solverMin = setSolverMin;
%             end
%         end
%         
%         function value = get.solverMin(obj)
%             value=obj.solverMin;
%         end    
%         
%         function obj = set.solverMax(obj,setsolverMax)
%             if ~obj.distribution.checkValue(setsolverMax)
%                 error('Attempted setting variable.solverMax to an illegal value');
%             else
%                 obj.solverMax = setsolverMax;
%             end
%         end
%         
%         function value = get.solverMax(obj)
%             value=obj.solverMax;
%         end          
        
        function obj = set.MCMC_samples(obj,setMCMC_samples)
%todo: need to alter for supporting StochasticProcess            
            if ~(size(setMCMC_samples,1)==obj.MCMC_nsamples_data)%is it an array of the proper size
%                warning('Setting variable.MCMC_samples using improperly sized array');
            end
%            disp('variable::set.MCMC_samples() skipping checkValueArray loop (this is slow)');
%             for index=1:size(setMCMC_samples,1)
%                 if ~(obj.distribution.checkValueArray(setMCMC_samples))
%                     warning('Setting variable.MCMC_samples with an inappropriate value');
%                 end
%             end
            obj.MCMC_samples = setMCMC_samples;
        end
        
        function value = get.MCMC_samples(obj)
            value=obj.MCMC_samples;
        end        
        
        function obj = set.MCMC_nsamples_adapt(obj,setMCMC_nsamples_adapt)
            if ~isinteger(setMCMC_nsamples_adapt)
                error('variable.MCMC_nsamples_adapt must be an integer=>0');
            else
                if ~(setMCMC_nsamples_adapt>=0)
                    error('variable.MCMC_nsamples_adapt must be an integer>=0');
                else
                    obj.MCMC_nsamples_adapt = setMCMC_nsamples_adapt;
                end
            end            
        end                
        
        function value = get.MCMC_nsamples_adapt(obj)
            value=obj.MCMC_nsamples_adapt;
        end        
        
        function obj = set.MCMC_nsamples_burnin(obj,setMCMC_nsamples_burnin)
            if ~isinteger(setMCMC_nsamples_burnin)
                error('variable.MCMC_nsamples_burnin must be an integer=>0');
            else
                if ~(setMCMC_nsamples_burnin>=0)
                    error('variable.MCMC_nsamples_burnin must be an integer>=0');
                else
                    obj.MCMC_nsamples_burnin = setMCMC_nsamples_burnin;
                end
            end            
        end
        
        function value = get.MCMC_nsamples_burnin(obj)
            value=obj.MCMC_nsamples_burnin;
        end
        
        function obj = set.MCMC_nsamples_data(obj,setMCMC_nsamples_data)
            if ~isinteger(setMCMC_nsamples_data)
                error('variable.MCMC_nsamples_data must be an integer=>0');
            else
                if ~(setMCMC_nsamples_data>=0)
                    error('variable.MCMC_nsamples_data must be an integer>=0');
                else
                    obj.MCMC_nsamples_data = setMCMC_nsamples_data;
                end
            end               
        end
        
        function value = get.MCMC_nsamples_data(obj)
            value=obj.MCMC_nsamples_data;
        end

        function obj = set.MCMC_nsamples_lag(obj,setMCMC_nsamples_lag)
            if ~isinteger(setMCMC_nsamples_lag)
                error('variable.MCMC_nsamples_lag must be an integer=>0');
            else
                if ~(setMCMC_nsamples_lag>=0)
                    error('variable.MCMC_nsamples_lag must be an integer>=0');
                else
                    obj.MCMC_nsamples_lag = setMCMC_nsamples_lag;
                end
            end               
        end
        
        function value = get.MCMC_nsamples_lag(obj)
            value=obj.MCMC_nsamples_lag;
        end
        
%         function obj = set.MCMC_initvalue(obj,setMCMC_initvalue)
%             if ~(obj.distribution.checkValue(setMCMC_initvalue))
%                 error('Attempted setting variable.MCMC_initvalue to an illegal value');
%             else
%                 obj.MCMC_initvalue = setMCMC_initvalue;
%             end            
%         end
%         
%         function value = get.MCMC_initvalue(obj)
%             value=obj.MCMC_initvalue;
%         end
        
        function obj = set.MCMC_hpdalpha(obj,setMCMC_hpdalpha)
            %hpdalpha needs to be >0 and <1
            if ~isnumeric(setMCMC_hpdalpha)
                error('Attempted setting variable.MCMC_hpdalpha to an illegal value');
            else
                if ~((setMCMC_hpdalpha>0) && (setMCMC_hpdalpha<1))
                    error('Attempted setting variable.MCMC_hpdalpha to an illegal value');
                else
                    obj.MCMC_hpdalpha = setMCMC_hpdalpha;
                end
            end                     
        end
        
        function value = get.MCMC_hpdalpha(obj)
            value=obj.MCMC_hpdalpha;
        end
        
        function obj = set.MCMC_mean(obj,setMCMC_mean)
%todo: need to alter for supporting StochasticProcess            
%             if ~(obj.distribution.checkValue(setMCMC_mean))
%                  warning('Attempted setting variable.MCMC_mean to an illegal value');
%             else
                obj.MCMC_mean = setMCMC_mean;
%             end               
        end
        
        function value = get.MCMC_mean(obj)
            value=obj.MCMC_mean;
        end
        
        function obj = set.MCMC_sd(obj,setMCMC_sd)
%todo: need to alter for supporting StochasticProcess            
%             if ~(obj.distribution.checkValue(setMCMC_sd))
%                 warning('Attempted setting variable.MCMC_sd to an illegal value');
%             else
                obj.MCMC_sd = setMCMC_sd;
%             end               
        end
        
        function value = get.MCMC_sd(obj)
            value=obj.MCMC_sd;
        end
        
        function obj = set.MCMC_min(obj,setMCMC_min)
%todo: need to alter for supporting StochasticProcess            
%             if ~(obj.distribution.checkValue(setMCMC_min))
%                 warning('Attempted setting variable.MCMC_min to an illegal value');
%             else
                obj.MCMC_min = setMCMC_min;
%             end               
        end
        
        function value = get.MCMC_min(obj)
            value=obj.MCMC_min;
        end
        
        function obj = set.MCMC_max(obj,setMCMC_max)
%todo: need to alter for supporting StochasticProcess            
%             if ~(obj.distribution.checkValue(setMCMC_max))
%                 warning('Attempted setting variable.MCMC_max to an illegal value');
%             else
                obj.MCMC_max = setMCMC_max;
%             end               
        end
        
        function value = get.MCMC_max(obj)
            value=obj.MCMC_max;
        end
        
        function obj = set.MCMC_hpdlo(obj,setMCMC_hpdlo)
%todo: need to alter for supporting StochasticProcess            
%             if ~(obj.distribution.checkValue(setMCMC_hpdlo))
%                 warning('Attempted setting variable.MCMC_hpdlo to an illegal value');
%             else
                obj.MCMC_hpdlo = setMCMC_hpdlo;
%             end               
        end
        
        function value = get.MCMC_hpdlo(obj)
            value=obj.MCMC_hpdlo;
        end
        
        function obj = set.MCMC_hpdhi(obj,setMCMC_hpdhi)
%todo: need to alter for supporting StochasticProcess            
%             if ~(obj.distribution.checkValue(setMCMC_hpdhi))
%                 warning('Attempted setting variable.MCMC_hpdhi to an illegal value');
%             else
                obj.MCMC_hpdhi = setMCMC_hpdhi;
%             end               
        end
        
        function value = get.MCMC_hpdhi(obj)
            value=obj.MCMC_hpdhi;
        end

        function obj = set.MCMC_acf(obj,setMCMC_acf)
%todo: need to alter for supporting StochasticProcess            
%             %MCMC_mce should be numeric and type double
%             if ~isnumeric(setMCMC_acf)
%                 error('Attempted setting variable.MCMC_acf to an illegal value');
%             else
%                 if ~isa(setMCMC_acf,'double')
%                     error('Attempted setting variable.MCMC_acf to an illegal value');
%                 else
                    obj.MCMC_acf = setMCMC_acf;
%                 end
%             end
        end
        
        function value = get.MCMC_acf(obj)
            value=obj.MCMC_acf;
        end         
        
        function obj = set.MCMC_mce(obj,setMCMC_mce)
%todo: need to alter for supporting StochasticProcess            
%             %MCMC_mce should be numeric and >0 
%             if ~isnumeric(setMCMC_mce)
%                 error('Attempted setting variable.MCMC_mce to an illegal value');
%             else
%                 if ~(setMCMC_mce>=0)
%                     error('Attempted setting variable.MCMC_mce to an illegal value');
%                 else
                    obj.MCMC_mce = setMCMC_mce;
%                 end
%             end
        end
        
        function value = get.MCMC_mce(obj)
            value=obj.MCMC_mce;
        end             
    end
end