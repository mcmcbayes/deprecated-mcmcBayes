function S=stan2mat(variables,filename)
    stdout=1;
    %fprintf(stdout,'Reading in %s and removing comments.\n',filename);

    fid=fopen(filename,'r');
    assert(fid>=0, 'Problem opening the file (ie., it does not exist ... perhaps you have cfg_mcmcBayesSampleMCMCResults turned off and cleared the old MCMC data file?)');
    strbuffer=fscanf(fid,'%c');
    fclose(fid);
    delete(filename);
    fid=fopen(filename,'wt');
    strlines=strsplit(strbuffer,'\n');
    if mcmcBayes.osInterfaces.getOSInterfaceType()==mcmcBayes.t_osInterfaces.UbuntuLinux || mcmcBayes.osInterfaces.getOSInterfaceType()==mcmcBayes.t_osInterfaces.MacOS
        for strline=strlines
            if ~(strncmp('#',cell2mat(strline),1)==1), fprintf(fid,'%s\n', cell2mat(strline)); end;%need the '\n' in linux and macOS
        end
    else
        for strline=strlines
            if ~(strncmp('#',cell2mat(strline),1)==1), fprintf(fid,'%s', cell2mat(strline)); end;
        end
    end

    fclose(fid);
    
    fprintf(stdout,'\tImporting the csv formatted table data from %s into samples structure.\n', filename);
    xlTable=readtable(filename);%read the samples file into a table
    
    %Examples of Stan vars are a_1.1.1,a_1.2.1,a_1.3.1,a_1.4.1,a_1.5.1,a_2.1.1,a_2.2.1,a_2.3.1,a_2.4.1,a_2.5.1,a_3.1.1,a_3.2.1,a_3.3.1,a_3.4.1,a_3.5.1,a_4.1.1,a_4.2.1,a_4.3.1,a_4.4.1,a_4.5.1,sigma
    %MATLAB readtable will convert the Stan var names above by replacing '.' with '_' (e.g., 'a_1.1.1' becomes --> 'a_1_1_1')
    %Need to translate corresponding varNames from variables (e.g., a_1, a_2, a_3, a_4, r) into Stan vars above, and then reshape the Stan vars into the appropriate sizes
    for varid=1:size(variables,1)
        if variables(varid).type~=mcmcBayes.t_variable.constant
            tvarlist={};
            varnamePrefix=variables(varid).name;
            varSizes=size(variables(varid).MCMC_initvalue{:});
            if variables(varid).dimensions==mcmcBayes.t_numericDimensions.scalar
                tvarlist=[{varnamePrefix}];
            elseif variables(varid).dimensions==mcmcBayes.t_numericDimensions.vector                
                R=varSizes(1);
                for row=1:R
                    tvarname=sprintf('%s_%d_1',varnamePrefix,row);
                    if isempty(tvarlist)%tvarlist must be a cell array
                        tvarlist=[{tvarname}];
                    else
                        tvarlist=[tvarlist {tvarname}];
                    end
                end
            else%2d-matrix
                error('todo');
            end                        
            
            if ~exist('varNames','var')%tvarlist must be a cell array
                varNames=[tvarlist];
            else
                varNames=[varNames tvarlist];
            end
        end
    end
    
    %trim the table columns
    xlTable=xlTable(:,varNames);
    for varName=varNames
        eval(['samples=xlTable.' cell2mat(varName) ';']);
        eval(['tS.' cell2mat(varName) '=samples;']);
    end
    
    %now need to reshape the samples
    for varid=1:size(variables,1)
        if variables(varid).type~=mcmcBayes.t_variable.constant
            varnamePrefix=variables(varid).name;
            varSizes=size(variables(varid).MCMC_initvalue{:});
            if variables(varid).dimensions==mcmcBayes.t_numericDimensions.scalar
                eval(['S.' varnamePrefix '=tS.' varnamePrefix ';']);
            elseif variables(varid).dimensions==mcmcBayes.t_numericDimensions.vector
                M=varSizes(1);
                for row=1:M
                    tvarname=sprintf('%s_%d_1',varnamePrefix,row);
                    evalstr=sprintf('S.%s(%d,:)=tS.%s;',varnamePrefix,row,tvarname);
                    eval(evalstr);
                end
            else%2d-matrix
                error('todo');
            end
        end
    end    
    %fprintf(stdout,'Import completed.\n');
end