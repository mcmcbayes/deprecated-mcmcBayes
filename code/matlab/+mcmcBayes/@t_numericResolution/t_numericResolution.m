classdef t_numericResolution < int32
    enumeration
        null (0)         %not checked
        discrete (1)
        continuous (2)
    end
    
    methods (Static)        
        function intvalue=toint(enumobj)
            switch enumobj          
                case {mcmcBayes.t_numericResolution.null, ...
                        mcmcBayes.t_numericResolution.discrete, ...
                        mcmcBayes.t_numericResolution.continuous}
                    intvalue=int32(enumobj);
                otherwise
                    error('unsupported mcmcBayes.t_numericResolution type');
            end                
        end        
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_numericResolution.null
                    strvalue='mcmcBayes.t_numericResolution.null';
                case mcmcBayes.t_numericResolution.discrete
                    strvalue='mcmcBayes.t_numericResolution.discrete';                    
                case mcmcBayes.t_numericResolution.continuous
                    strvalue='mcmcBayes.t_numericResolution.continuous';                    
                otherwise
                    error('unsupported mcmcBayes.t_numericResolution type');
            end
        end

        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_numericResolution.null)
                    enumobj=mcmcBayes.t_numericResolution.null;
                case int32(mcmcBayes.t_numericResolution.discrete)
                    enumobj=mcmcBayes.t_numericResolution.discrete;
                case int32(mcmcBayes.t_numericResolution.continuous)
                    enumobj=mcmcBayes.t_numericResolution.continuous;
                otherwise
                    error('unsupported mcmcBayes.t_numericResolution type');
            end
        end
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_numericResolution.null')==1
                enumobj=mcmcBayes.t_numericResolution.null;
            elseif strcmp(strval,'mcmcBayes.t_numericResolution.discrete')==1
                enumobj=mcmcBayes.t_numericResolution.discrete;
            elseif strcmp(strval,'mcmcBayes.t_numericResolution.continuous')==1
                enumobj=mcmcBayes.t_numericResolution.continuous;
            else
                error('unsupported mcmcBayes.t_numericResolution type');
            end
        end          
    end
end
