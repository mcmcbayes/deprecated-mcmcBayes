classdef t_variableTransformPost < int32
    enumeration
        none (1)%do not solve
        adjustForFit2NormalizedData (2)
        adjustForFit2MeanCenteredData (3)
    end
    methods (Static)        
        function intvalue=toint(enumobj)
            switch enumobj          
                case {mcmcBayes.t_variableTransformPost.none,mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData,...
                        mcmcBayes.t_variableTransformPost.adjustForFit2MeanCenteredData}
                    intvalue=int32(enumobj);                    
                otherwise
                    error('unsupported mcmcBayes.t_variableTransformPost type');
            end                
        end        
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_variableTransformPost.none
                    strvalue='mcmcBayes.t_variableTransformPost.none';
                case mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData
                    strvalue='mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData';
                case mcmcBayes.t_variableTransformPost.adjustForFit2MeanCenteredData
                    strvalue='mcmcBayes.t_variableTransformPost.adjustForFit2MeanCenteredData';                                       
                otherwise
                    error('unsupported mcmcBayes.t_variableTransformPost type');
            end
        end
        
        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_variableTransformPost.none)
                    enumobj=mcmcBayes.t_variableTransformPost.none;
                case int32(mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData)
                    enumobj=mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData;
                case int32(mcmcBayes.t_variableTransformPost.adjustForFit2MeanCenteredData)
                    enumobj=mcmcBayes.t_variableTransformPost.adjustForFit2MeanCenteredData;                    
                otherwise
                    error('unsupported mcmcBayes.t_variableTransformPost');                    
            end
        end
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_variableTransformPost.none')==1
                enumobj=mcmcBayes.t_variableTransformPost.none;
            elseif strcmp(strval,'mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData')==1
                enumobj=mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData;
            elseif strcmp(strval,'mcmcBayes.t_variableTransformPost.adjustForFit2MeanCenteredData')==1
                enumobj=mcmcBayes.t_variableTransformPost.adjustForFit2MeanCenteredData;                
            else
                error('unsupported mcmcBayes.t_variableTransformPost');
            end
        end
    end
end