% classdef t_variableInits < int32
%     enumeration
%         None (1)%do not solve
%         Solver (2)%use the defaultSolver (or not if overridden to None)
%         Uniform (3)%e.g., for randomly selecting initial values that are uniformly distributed
%     end
%     methods (Static)        
%         function intvalue=toint(enumobj)
%             switch enumobj          
%                 case {mcmcBayes.t_variableInits.None,mcmcBayes.t_variableInits.Solver,mcmcBayes.t_variableInits.Uniform}
%                     intvalue=int32(enumobj);                    
%                 otherwise
%                     error('unsupported mcmcBayes.t_variableInits type');
%             end                
%         end        
%         
%         function strvalue=tostring(enumobj)
%             switch enumobj
%                 case mcmcBayes.t_variableInits.None
%                     strvalue='mcmcBayes.t_variableInits.None';
%                 case mcmcBayes.t_variableInits.Solver
%                     strvalue='mcmcBayes.t_variableInits.Solver';
%                 case mcmcBayes.t_variableInits.Uniform
%                     strvalue='mcmcBayes.t_variableInits.Uniform';
%                 otherwise
%                     error('unsupported mcmcBayes.t_variableInits type');
%             end
%         end
%         
%         function enumobj=int2enum(intval)
%             switch intval
%                 case int32(mcmcBayes.t_variableInits.None)
%                     enumobj=mcmcBayes.t_variableInits.None;
%                 case int32(mcmcBayes.t_variableInits.Solver)
%                     enumobj=mcmcBayes.t_variableInits.Solver;
%                 case int32(mcmcBayes.t_variableInits.Uniform)
%                     enumobj=mcmcBayes.t_variableInits.Uniform;
%                 otherwise
%                     error('unsupported mcmcBayes.t_variableInits');                    
%             end
%         end
%         
%         function enumobj=string2enum(strval)
%             if strcmp(strval,'mcmcBayes.t_variableInits.None')==1
%                 enumobj=mcmcBayes.t_variableInits.None;
%             elseif strcmp(strval,'mcmcBayes.t_variableInits.Solver')==1
%                 enumobj=mcmcBayes.t_variableInits.Solver;
%             elseif strcmp(strval,'mcmcBayes.t_variableInits.Uniform')==1
%                 enumobj=mcmcBayes.t_variableInits.Uniform;
%             else
%                 error('unsupported mcmcBayes.t_variableInits');
%             end
%         end
%     end
% end