classdef scalingLinear < mcmcBayes.scaling
% Linear regression model, DeGroot 2005, 1972.  Approach for modeling the relationship between a scalar dependent variable y and one or more 
% independent variables assuming a constant rate in which the dependent variable changes per the independent variable values.  An alternative use 
% defines the independent variables as unique values in a set of times.
% modelsubtype
% 'a' - Normal Inverse Gamma priors, unknown precision, hierarchical (i.e., beta0~normal(a,(1/(b*r))^-1), beta1~normal(c,(1/(d*r))^-1), r~gamma(g,h))
% 'b' - Normal Inverse Gamma priors, unknown precision (i.e., beta0~normal(a,(1/b)^-1), beta1~normal(c,(1/d)^-1), r~gamma(g,h))
% 'c' - Flat priors (i.e., beta0~flat, beta1~flat, r~flat)
    methods(Access = public, Static)
        function test()
            display('Hello!');
        end
        
        function retData=transformDataPre(vars, data)       
           	datavarid=1;
            x1=cast(data.getRawDataValues(mcmcBayes.t_dataCategory.independent, datavarid), 'double');
           	datavarid=2;
            x2=cast(data.getRawDataValues(mcmcBayes.t_dataCategory.independent, datavarid), 'double');
            
%todo: create a data.indVarsTfPre==mcmcBayes.t_dataTransformPre.featureScaling
            if ~isempty(x1)
                for j=1:size(x1,2)
                    x1_(:,j)=mcmcBayes.VDMCaseStudy2.featureScaling(x1(:,j));
                    x2_(:,j)=mcmcBayes.VDMCaseStudy2.featureScaling(x2(:,j));
                end
                data.indVarsValues(1,1)={x1_};
                data.indVarsValues(2,1)={x2_};
            else
                %no special transforms for flat prior            
            end
            retData=data;
        end        
        
        function [solverInits]=determineSolverInits(vars, data)
        %needs to return a cell array due to some models having different sized variables
            for varid=1:size(vars,1)
                solverInits(varid,1)=[{vars(varid).MCMC_initvalue}];%needs to be a row vector
            end
            %Compute solverInits (bootstrap values for mle/ls that solve for solverInits) using fsolve and fhandle1
%             if ~((data.dataSource==mcmcBayes.t_dataSource.None) || (data.dataSource==mcmcBayes.t_dataSource.Preset))
%                 datavarid=1;
%                 x1=cast(data.getRawDataValues(mcmcBayes.t_dataCategory.independent, datavarid), 'double');
%                 datavarid=2;
%                 x2=cast(data.getRawDataValues(mcmcBayes.t_dataCategory.independent, datavarid), 'double');
%                 datavarid=1;
%                 y=cast(data.getRawDataValues(mcmcBayes.t_dataCategory.dependent, datavarid), 'double');                
%                 fhandle=@(c) (1+c'*x1)./(1+c'*x2);%N(t=end) equals dependentVariables(end)
%                 tsolverInits=[fsolve(fhandle, [solverInits{1}])];
%                 solverInits(1,1)={tsolverInits};
%                 solverInits(2,1)={vars(2).distribution.estimateMeanFromHyperparameters};
%             end
        end        

        %this is only used for determining the inits when the data source is not mcmcBayes.t_dataSource.None or mcmcBayes.t_dataSource.Preset         
        function [thetahat]=SolverMeanMLE(runPowerPosteriorMCMCSequence, subtype, vars, data)            
        % [thetahat]=mlefitModel(runPowerPosteriorMCMCSequence, subtype, vars, data)
        %   Wrapper function to call MATLAB's mle routine that estimates the model variable point estimates.  The structure
        %   of MATLAB's mle routine requires globals to pass the independent variables, phi, and any values for variables that
        %   are assumed as a constant (thus, are not being estimated by the mle).
        %   Input:
        %     runPowerPosteriorMCMCSequence - Boolean that specifies either running the power-posterior sequence, or not running
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     modelVariables - Distribution model variables that is an array of mcmcBayesVariable (indexed by varid, tempid) objects 
        %     dependentVariables - Prior dependent variables for the model that is a cell array of mcmcBayesNumeric objects.
        %   Output:
        %     thetahat - Cell array of model variable point estimates values computed (cell array due to some models having different sized variables)
            options = statset('MaxIter',800, 'MaxFunEvals',1200);
            
            if runPowerPosteriorMCMCSequence
                phi=mcmcBayes.getDefaultPhi(2);%identify the inits for tempid=2
            else
                phi=1; 
            end

           	datavarid=1;
            x1=cast(data.getRawDataValues(mcmcBayes.t_dataCategory.independent, datavarid), 'double');
           	datavarid=2;
            x2=cast(data.getRawDataValues(mcmcBayes.t_dataCategory.independent, datavarid), 'double');
%todo: cleanup
            r=vars(2).MCMC_initvalue;            
                       
            if ~((vars(1).type==mcmcBayes.t_variable.stochastic) && (vars(2).type==mcmcBayes.t_variable.stochastic))
                error('Unsupported combination of modelVariables().type');
            end            
            
           	datavarid=1;
            data=cast(data.getRawDataValues(mcmcBayes.t_dataCategory.dependent, datavarid), 'double');
            %MATLAB's mle is wonky, have to solve the covariates initially (not ideal)
            for k=1:10
                mypdf=@(y,c) mcmcBayes.scalingLinear.computeLogLikelihood_alt(y,phi,x1(k,:),x2(k,:),r,c);
                varsDefaultInits=[cast(vars(1).MCMC_initvalue(k),'double')];
                varsLowerBounds=[cast(vars(1).solverMin(k),'double')];
                varsUpperBounds=[cast(vars(1).solverMax(k),'double')];                                
                [tthetahat(k,1)]=mle(data,'logpdf',mypdf,'start',varsDefaultInits,'lowerbound',varsLowerBounds,'upperbound',varsUpperBounds,'options',options);
            end
            thetahat=[{tthetahat} {vars(2).distribution.estimateMeanFromHyperparameters} {vars(3).distribution.estimateMeanFromHyperparameters} ...
                {vars(4).distribution.estimateMeanFromHyperparameters}];
        end

        %this is only used for determining the inits when the data source is not mcmcBayes.t_dataSource.None or mcmcBayes.t_dataSource.Preset 
        function [thetahat]=SolverMeanLS(runPowerPosteriorMCMCSequence, subtype, vars, data)
        % [thetahat]=lsfitModel(independentVariables, modelVariables, dependentVariables)
        %   Wrapper function to call MATLAB's lsqcurvefit routine that estimates the model variable point estimates.  
        %   Input:
        %     independentVariables - Independent variables for the model that is a cell array of mcmcBayesNumeric objects
        %     modelVariables - Distribution model variables that is an array of mcmcBayesVariable (indexed by varid, tempid) objects 
        %     dependentVariables - Prior dependent variables for the model that is a cell array of mcmcBayesNumeric objects.
        %   Output:
        %     thetahat - Cell array of model variable point estimates values computed (cell array due to some models having different sized variables)
           	datavarid=1;
            x1=cast(data.getRawDataValues(mcmcBayes.t_dataCategory.independent, datavarid), 'double');
           	datavarid=2;
            x2=cast(data.getRawDataValues(mcmcBayes.t_dataCategory.independent, datavarid), 'double');
            fhandle=sprintf('(1+c*x1)./(1+c*x2)');
            coef={'c'};
            
            varsDefaultInits=[cast(vars(1).MCMC_initvalue','double')];
            varsLowerBounds=[cast(vars(1).solverMin,'double')];
            varsUpperBounds=[cast(vars(1).solverMax,'double')];
            
            options = optimoptions('lsqcurvefit','Display','none','MaxFunEvals',3000,'MaxIter',1000);            

            datavarid=1;
            y=cast(data.getRawDataValues(mcmcBayes.t_dataCategory.dependent, datavarid), 'double');
            for i=1:numel(y)
                myfittype = fittype(fhandle, 'dependent', {'y'}, 'independent', {'x1','x2'}, 'coefficients', coef);%customnonlinear fittype
                xhat(i) = fit([x1(i,:)',x2(i,:)'],y,myfittype,'StartPoint',varsDefaultInits,'Lower',varsLowerBounds,'Upper',varsUpperBounds);
            end
            %[xhat,resnorm,residual,exitflag,output]=lsqcurvefit(fhandle,varsDefaultInits,x,y,varsLowerBounds,varsUpperBounds,options);
            %if exitflag~=1%converged
            %    disp(output.message)
            %end

            %Debugging plot
            %F=@(beta0,beta1,x) beta0+beta1.*x;
            %fig; hold on; scatter(x,y,'rx'); scatter(x,F(xhat.beta0,xhat.beta1,x),'g*'); hold off;            

            thetahat=[{xhat} {vars(2).distribution.estimateMeanFromHyperparameters}];
        end                                               
    end

    methods(Access = private, Static)               
        function [loglikelihood]=computeLogLikelihood_alt(data,phi,x1,x2,r,varargin)
        % [loglikelihood]=computeLogLikelihood_alt(data,varargin)
        % Function that computes the model's log-likelihood of the data given the parameters.  It is used by MATLAB's mle function.
        % It was necessary to utilize a global variable for passing the independent variables and phi.
        % Input:
        %   data - Model input data used in computing the log-likelihood
        %   varargin - Point estimates for the model variables used in computing the log-likelihood
        % Output:
        %   loglikelihood - Computed log-likelihood
            y=cast(data,'double');
            c=varargin{1};
            for j=1:numel(y)
                m(j)=(1+c'*x1(:,j))/(1+c'*x2(:,j));
                logpdf(j)=phi*((1/2)*log(r/(2*pi))-(r/2)*(y(j)-m(j))^2);
            end
            loglikelihood=sum(logpdf);
%             tempstr=sprintf('linear regression loglikelihood(beta0=%f,beta1=%f,r=%f)=%f',beta0,beta1,r,loglikelihood);
%             disp(tempstr);
        end        
    end
    
    methods 
        function obj=scalingLinear(setsimulationStruct, setprefix)
        % Linear(setsimulationStruct, setprefix)
        %   Constructor for the Linear class
        %   Input:
        %     setsimulationStruct - 
        %     setprefix - 
        %   Output:
        %     obj - instantiated Linear object
            if nargin == 0
                superargs={};
            else
                switch setsimulationStruct.subType
                    case 'c'
                        %disp('scalingLinear model is using modelsubtype c (c_1~flat, c_2~flat, ..., c_10~flat, r~flat)');
                    otherwise
                        error('Unsupported modelsubtype');
                end
                superargs{1}='scalingLinear';
                superargs{2}=setsimulationStruct.subType;
                superargs{3}='Scaling Linear';
                superargs{4}=strcat('Todo.');
                superargs{5}='Johnston 2018';
                superargs{6}='reuben@reubenjohnston.com';
                superargs{7}=setprefix;                
            end
            
            obj=obj@mcmcBayes.scaling(superargs{:});

            if nargin>0 %i am not sure why, but it is necessary to set these when loading
                obj.predictionsNSamples=mcmcBayes.scaling.getDefaultNSamples();
            end
        end
        
        function [retObj]=transformVarsPre(obj, chainID, tempID)
        %needs to be performed on all vars
        %this function simply adjusts the hyperparameters, for all tempIDs
            retObj=obj;
            
            if (retObj.priorVariables(1,chainID).tfPre==mcmcBayes.t_variableTransformPre.none)
                return;
            end
            
            datavarid=1;%only one independent/dependent variable
            if (tempID==0) && (retObj.priorVariables(1,chainID).tfPre==mcmcBayes.t_variableTransformPre.adjustForFit2NormalizedData)
                mean_x=retObj.priorData.indVarsOrigMeans{datavarid};
                std_x=retObj.priorData.indVarsOrigStd{datavarid};
                mean_y=retObj.priorData.depVarsOrigMeans{datavarid};
                std_y=retObj.priorData.depVarsOrigStd{datavarid};
                beta0=retObj.priorVariables(1,chainID).distribution.estimateMeanFromHyperparameters;
                beta1=retObj.priorVariables(2,chainID).distribution.estimateMeanFromHyperparameters;
                r=retObj.priorVariables(3,chainID).distribution.estimateMeanFromHyperparameters;
                zbeta0=(beta0+beta1*mean_x-mean_y)/std_y;
                zbeta1=beta1*std_x/std_y;
                zr=r*std_y^2;
                retObj.priorVariables(1,chainID).distribution.parameters(1).values=zbeta0;
                retObj.priorVariables(1,chainID).distribution.parameters(2).values=1;%standard normal precision is 1/1
                retObj.priorVariables(2,chainID).distribution.parameters(1).values=zbeta1;
                retObj.priorVariables(2,chainID).distribution.parameters(2).values=1;%standard normal precision is 1/1
                retObj.priorVariables(3,chainID).distribution=retObj.priorVariables(3,chainID).distribution.estimateHyperparameters(zr);%assuming shape is 1
            elseif (retObj.posteriorVariables(1,chainID,tempID).tfPre==mcmcBayes.t_variableTransformPre.adjustForFit2NormalizedData)
                mean_x=retObj.posteriorData.indVarsOrigMeans{datavarid};
                std_x=retObj.posteriorData.indVarsOrigStd{datavarid};
                mean_y=retObj.posteriorData.depVarsOrigMeans{datavarid};
                std_y=retObj.posteriorData.depVarsOrigStd{datavarid};
                beta0=retObj.posteriorVariables(1,chainID,tempID).distribution.estimateMeanFromHyperparameters;
                beta1=retObj.posteriorVariables(2,chainID,tempID).distribution.estimateMeanFromHyperparameters;
                r=retObj.posteriorVariables(3,chainID,tempID).distribution.estimateMeanFromHyperparameters;
                zbeta0=(beta0+beta1*mean_x-mean_y)/std_y;
                zbeta1=beta1*std_x/std_y;
                zr=r*std_y^2;
                retObj.posteriorVariables(1,chainID,tempID).distribution.parameters(1).values=zbeta0;
                retObj.posteriorVariables(1,chainID,tempID).distribution.parameters(2).values=1;%standard normal precision is 1/1
                retObj.posteriorVariables(2,chainID,tempID).distribution.parameters(1).values=zbeta1;
                retObj.posteriorVariables(2,chainID,tempID).distribution.parameters(2).values=1;%standard normal precision is 1/1
                retObj.posteriorVariables(3,chainID,tempID).distribution=retObj.posteriorVariables(3,chainID,tempID).distribution.estimateHyperparameters(zr);%assuming shape is 1
            elseif (retObj.posteriorVariables(1,chainID,tempID).tfPre==mcmcBayes.t_variableTransformPre.adjustForFit2MeanCenteredData)
                mean_x=retObj.posteriorData.indVarsOrigMeans{datavarid};
                mean_y=retObj.posteriorData.depVarsOrigMeans{datavarid};
                beta0=retObj.posteriorVariables(1,chainID,tempID).distribution.estimateMeanFromHyperparameters;
                beta1=retObj.posteriorVariables(2,chainID,tempID).distribution.estimateMeanFromHyperparameters;
                r=retObj.posteriorVariables(3,chainID,tempID).distribution.estimateMeanFromHyperparameters;
                zbeta0=(beta0+mean_y-beta1*mean_x);
                zbeta1=beta1;
                zr=r;
                retObj.posteriorVariables(1,chainID,tempID).distribution.parameters(1).values=zbeta0;
                retObj.posteriorVariables(1,chainID,tempID).distribution.parameters(2).values=1;%standard normal precision is 1/1
                retObj.posteriorVariables(2,chainID,tempID).distribution.parameters(1).values=zbeta1;
                retObj.posteriorVariables(2,chainID,tempID).distribution.parameters(2).values=1;%standard normal precision is 1/1
                retObj.posteriorVariables(3,chainID,tempID).distribution=retObj.posteriorVariables(3,chainID,tempID).distribution.estimateHyperparameters(zr);%assuming shape is 1
            end
        end        
        
        function [retObj]=transformVarsPost(obj, chainID, tempID)
        %Overriding mcmcBayes.model.transformVarsPost()
        %needs to be performed on all vars, simultaneously
            retObj=obj;
            
            if tempID==0
                numVars=size(retObj.priorVariables,1);
                for varID=1:numVars
                    if varID==1
                        zbeta0=retObj.priorVariables(varID,chainID).MCMC_samples;
                    elseif varID==2
                        zbeta1=retObj.priorVariables(varID,chainID).MCMC_samples;
                    elseif varID==3
                        zr=retObj.priorVariables(varID,chainID).MCMC_samples;
                    end
                end
                if retObj.priorVariables(1,chainID).tfPost==mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData%just check beta0
                    datavarid=1;%only one independent/dependent variable
                    mean_x=retObj.priorData.indVarsOrigMeans{datavarid};
                    std_x=retObj.priorData.indVarsOrigStd{datavarid};
                    mean_y=retObj.priorData.depVarsOrigMeans{datavarid};
                    std_y=retObj.priorData.depVarsOrigStd{datavarid};
                    retObj.priorVariables(1,chainID).MCMC_samples=zbeta0*std_y+mean_y-zbeta1*std_y*mean_x/std_x;
                    retObj.priorVariables(2,chainID).MCMC_samples=zbeta1*std_y/std_x;
                    retObj.priorVariables(3,chainID).MCMC_samples=zr/std_y^2;
                elseif retObj.priorVariables(1,chainID).tfPost==mcmcBayes.t_variableTransformPost.adjustForFit2MeanCenteredData%just check beta0
                    datavarid=1;%only one independent/dependent variable
                    mean_x=retObj.priorData.indVarsOrigMeans{datavarid};
                    mean_y=retObj.priorData.depVarsOrigMeans{datavarid};
                    retObj.priorVariables(1,chainID).MCMC_samples=zbeta0+mean_y-zbeta1*mean_x;
                    retObj.priorVariables(2,chainID).MCMC_samples=zbeta1;
                    retObj.priorVariables(3,chainID).MCMC_samples=zr;
                end                
            else
                numVars=size(retObj.posteriorVariables,1);
                for varID=1:numVars
                    if varID==1
                        zbeta0=retObj.posteriorVariables(varID,chainID,tempID).MCMC_samples;
                    elseif varID==2
                        zbeta1=retObj.posteriorVariables(varID,chainID,tempID).MCMC_samples;
                    elseif varID==3
                        zr=retObj.posteriorVariables(varID,chainID,tempID).MCMC_samples;
                    end
                end
                if retObj.posteriorVariables(varID,chainID,tempID).tfPost==mcmcBayes.t_variableTransformPost.adjustForFit2NormalizedData
                    datavarid=1;%only one independent/dependent variable
                    mean_x=retObj.posteriorData.indVarsOrigMeans{datavarid};
                    std_x=retObj.posteriorData.indVarsOrigStd{datavarid};
                    mean_y=retObj.posteriorData.depVarsOrigMeans{datavarid};
                    std_y=retObj.posteriorData.depVarsOrigStd{datavarid};
                    retObj.posteriorVariables(1,chainID,tempID).MCMC_samples=zbeta0*std_y+mean_y-zbeta1*std_y*mean_x/std_x;
                    retObj.posteriorVariables(2,chainID,tempID).MCMC_samples=zbeta1*std_y/std_x;
                    retObj.posteriorVariables(3,chainID,tempID).MCMC_samples=zr/std_y^2;
                elseif retObj.posteriorVariables(varID,chainID,tempID).tfPost==mcmcBayes.t_variableTransformPost.adjustForFit2MeanCenteredData
                    datavarid=1;%only one independent/dependent variable
                    mean_x=retObj.posteriorData.indVarsOrigMeans{datavarid};
                    mean_y=retObj.posteriorData.depVarsOrigMeans{datavarid};
                    retObj.posteriorVariables(1,chainID,tempID).MCMC_samples=zbeta0+mean_y-zbeta1*mean_x;
                    retObj.posteriorVariables(2,chainID,tempID).MCMC_samples=zbeta1;
                    retObj.posteriorVariables(3,chainID,tempID).MCMC_samples=zr;                   
                end                
            end
        end
    end
end
        