classdef MacOS < mcmcBayes.osInterfaces
    %all class properties are inherited    
    methods (Static)

    end
    
    methods
        function obj = MacOS(cfgMcmcBayes)%constructor
            obj=obj@mcmcBayes.osInterfaces(mcmcBayes.t_osInterfaces.MacOS, cfgMcmcBayes);                        
        end                
    end
    
    methods %getter/setter functions

    end
end