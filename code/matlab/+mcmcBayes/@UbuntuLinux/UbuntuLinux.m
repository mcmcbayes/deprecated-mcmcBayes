classdef UbuntuLinux < mcmcBayes.osInterfaces
    %all class properties are inherited    
    methods (Static)
        function checkCgroups()
            iserror=false;
            [status,cmdout] = system('cat /sys/fs/cgroup/cpu/samplercpulimited/cpu.shares');
            if isempty(strfind(cmdout,'No such file or directory'))
                sampler.cpushares=sscanf(cmdout,'%d');
            else
                iserror=true;
            end
            [status,cmdout] = system('cat /sys/fs/cgroup/cpu/matlabcpulimited/cpu.shares');
            if isempty(strfind(cmdout,'No such file or directory'))
                matlab.cpushares=sscanf(cmdout,'%d');
            else
                iserror=true;
            end
            [status,cmdout] = system('cat /sys/fs/cgroup/cpuset/samplercpulimited/cpuset.cpus');
            if isempty(strfind(cmdout,'No such file or directory'))
                [sampler.cpu]=sscanf(cmdout,'%d-%d');
            else
                iserror=true;
            end
            [status,cmdout] = system('cat /sys/fs/cgroup/cpuset/matlabcpulimited/cpuset.cpus');
            if isempty(strfind(cmdout,'No such file or directory'))
                [matlab.cpu]=sscanf(cmdout,'%d-%d');
            else
                iserror=true;
            end
            if (sampler.cpushares~=500) || (matlab.cpushares~=500) || ...
                (sampler.cpu(1)~=0) || (sampler.cpu(2)<0) || ...
                (matlab.cpu(1)~=0) || (matlab.cpu(2)<0)
                iserror=true;
            end
            if iserror
                error('You need to setup cgroups.');
            end
        end
    end
    
    methods
        function obj = UbuntuLinux(cfgMcmcBayes)%constructor
            obj=obj@mcmcBayes.osInterfaces(mcmcBayes.t_osInterfaces.UbuntuLinux, cfgMcmcBayes);                        
        end                
    end
    
    methods %getter/setter functions

    end
end