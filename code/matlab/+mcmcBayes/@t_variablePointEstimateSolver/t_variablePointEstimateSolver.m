classdef t_variablePointEstimateSolver < int32
    enumeration
        None (1)%do not solve
        SolverMeanAnalytic (2)%preset types computed using hyper-parameters and known equations for the mean of the variable's distribution
        SolverMeanLS (3)%solve for the mean using least squares (LS) and use it for estimating hyperparameters in the variable's prior distribution (MATLAB's lsqcurvefit to solve nonlinear curve-fitting (data-fitting) problems in least-squares sense)
        SolverMeanMLE (4)%solve for the mean using maximum likelihood estimation (MLE) and use it for estimating hyperparameters in the variable's prior distribution (MATLAB's mle for solving for variables in a custom distribution)
        SolverMeanCustom1 (5)%solve for the mean using MATLAB's custom fit type (e.g., custom curve and surface fitting) and use it for estimating hyperparameters in the variable's prior distribution
        SolverMeanCustom2 (6)%e.g., the Kuo-Ghosh NHPP model is simply the mean of the data
        SolverMeanMinSearch (7)%solve for the mean using MATLAB's fminsearch
    end
    methods (Static)
        function intvalue=toint(enumobj)
            switch enumobj          
                case {mcmcBayes.t_variablePointEstimateSolver.None,mcmcBayes.t_variablePointEstimateSolver.SolverMeanAnalytic,...
                      mcmcBayes.t_variablePointEstimateSolver.SolverMeanLS,mcmcBayes.t_variablePointEstimateSolver.SolverMeanMLE,...
                      mcmcBayes.t_variablePointEstimateSolver.SolverMeanCustom1,mcmcBayes.t_variablePointEstimateSolver.SolverMeanCustom2,...
                      mcmcBayes.t_variablePointEstimateSolver.SolverMeanMinSearch}
                    intvalue=int32(enumobj);                    
                otherwise
                    error('unsupported mcmcBayes.t_variablePointEstimateSolver type');
            end                
        end        
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_variablePointEstimateSolver.None
                    strvalue='mcmcBayes.t_variablePointEstimateSolver.None';
                case mcmcBayes.t_variablePointEstimateSolver.SolverMeanAnalytic
                    strvalue='mcmcBayes.t_variablePointEstimateSolver.SolverMeanAnalytic';
                case mcmcBayes.t_variablePointEstimateSolver.SolverMeanLS
                    strvalue='mcmcBayes.t_variablePointEstimateSolver.SolverMeanLS';
                case mcmcBayes.t_variablePointEstimateSolver.SolverMeanMLE
                    strvalue='mcmcBayes.t_variablePointEstimateSolver.SolverMeanMLE';
                case mcmcBayes.t_variablePointEstimateSolver.SolverMeanCustom1
                    strvalue='mcmcBayes.t_variablePointEstimateSolver.SolverMeanCustom1';
                case mcmcBayes.t_variablePointEstimateSolver.SolverMeanCustom2
                    strvalue='mcmcBayes.t_variablePointEstimateSolver.SolverMeanCustom2';
                case mcmcBayes.t_variablePointEstimateSolver.SolverMeanMinSearch
                    strvalue='mcmcBayes.t_variablePointEstimateSolver.SolverMeanMinSearch';
                otherwise
                    error('unsupported mcmcBayes.t_variablePointEstimateSolver type');
            end
        end
        
        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_variablePointEstimateSolver.None)
                    enumobj=mcmcBayes.t_variablePointEstimateSolver.None;
                case int32(mcmcBayes.t_variablePointEstimateSolver.SolverMeanAnalytic)
                    enumobj=mcmcBayes.t_variablePointEstimateSolver.SolverMeanAnalytic;
                case int32(mcmcBayes.t_variablePointEstimateSolver.SolverMeanLS)
                    enumobj=mcmcBayes.t_variablePointEstimateSolver.SolverMeanLS;
                case int32(mcmcBayes.t_variablePointEstimateSolver.SolverMeanMLE)
                    enumobj=mcmcBayes.t_variablePointEstimateSolver.SolverMeanMLE;
                case int32(mcmcBayes.t_variablePointEstimateSolver.SolverMeanCustom1)
                    enumobj=mcmcBayes.t_variablePointEstimateSolver.SolverMeanCustom1;
                case int32(mcmcBayes.t_variablePointEstimateSolver.SolverMeanCustom2)
                    enumobj=mcmcBayes.t_variablePointEstimateSolver.SolverMeanCustom2;
                case int32(mcmcBayes.t_variablePointEstimateSolver.SolverMeanMinSearch)
                    enumobj=mcmcBayes.t_variablePointEstimateSolver.SolverMeanMinSearch;
                otherwise
                    error('unsupported mcmcBayes.t_variablePointEstimateSolver');                    
            end
        end
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_variablePointEstimateSolver.None')==1
                enumobj=mcmcBayes.t_variablePointEstimateSolver.None;
            elseif strcmp(strval,'mcmcBayes.t_variablePointEstimateSolver.SolverMeanAnalytic')==1
                enumobj=mcmcBayes.t_variablePointEstimateSolver.SolverMeanAnalytic;
            elseif strcmp(strval,'mcmcBayes.t_variablePointEstimateSolver.SolverMeanLS')==1
                enumobj=mcmcBayes.t_variablePointEstimateSolver.SolverMeanLS;
            elseif strcmp(strval,'mcmcBayes.t_variablePointEstimateSolver.SolverMeanMLE')==1
                enumobj=mcmcBayes.t_variablePointEstimateSolver.SolverMeanMLE;
            elseif strcmp(strval,'mcmcBayes.t_variablePointEstimateSolver.SolverMeanCustom1')==1
                enumobj=mcmcBayes.t_variablePointEstimateSolver.SolverMeanCustom1;
            elseif strcmp(strval,'mcmcBayes.t_variablePointEstimateSolver.SolverMeanCustom2')==1
                enumobj=mcmcBayes.t_variablePointEstimateSolver.SolverMeanCustom2;
            elseif strcmp(strval,'mcmcBayes.t_variablePointEstimateSolver.SolverMeanMinSearch')==1
                enumobj=mcmcBayes.t_variablePointEstimateSolver.SolverMeanMinSearch;
            else
                error('unsupported mcmcBayes.t_variablePointEstimateSolver');
            end
        end
    end
end