classdef JAGS < mcmcBayes.samplerInterfaces
%this class performs all interfacing with JAGS
    properties                       
        JAGS_projdir %string
        JAGS_scriptdir %string
        filename_JAGSscripttemplate %string, dependency on JAGS_scriptdir, modeltype, and modelid
        JAGS_shortcut %string        
        
        filename_bashscript
        
        filename_JAGSscript%used in setupMCMCSampling,performMCMCSampling
        filename_JAGSproject%used in setupMCMCSampling
        filename_JAGSinits%used in setupMCMCSampling
        filename_JAGSdata%used in setupMCMCSampling
        filename_JAGSlog
        %JAG's output files are in CODA format
        filename_CODAindex%used in readSamples
        filename_CODAchain%used in readSamples
    end

    methods
        function obj = JAGS(sequence) %constructor
            if nargin == 0
                superargs={};
            else
                superargs{1}=mcmcBayes.t_samplerInterfaces.JAGS;
                superargs{2}=sequence.osInterface;
                superargs{3}=sequence.cfgMcmcBayes.cfgSimulation.runView;
                superargs{4}=sequence.repositories;
            end

            obj=obj@mcmcBayes.samplerInterfaces(superargs{:});       
                      
            obj.JAGS_shortcut=sequence.cfgMcmcBayes.cfgSampler.JAGS.shortCut;
            if isempty(obj.JAGS_shortcut)
                error('JAGS shortcut is missing!');
            end
        end
        
        %setup JAGS script, inits, and data files for sampling
        function retobj=setupMCMCSampling(obj, setmodeltype, setmodelsubtype, setnchains, settempid, setphi, outputFileNamePrefix, setVariables, ...
                setData, setOsInterface)
            obj.modeltype=setmodeltype;
            obj.modelsubtype=setmodelsubtype;
            obj.nchains=setnchains;
            obj.tempid=settempid;            
            obj.phi=setphi;
            obj.outputFileNamePrefix=outputFileNamePrefix;
            obj.variables=setVariables;
            obj.data=setData;
            obj.osInterface=setOsInterface;

            name=obj.modeltype;

            %find the repository with the sampler files and then set JAGS_projdir and JAGS_scriptdir
            for i=1:size(obj.repositories,1)
                tmpprojdir=strcat(obj.repositories{i},mcmcBayes.osInterfaces.getSubPath(mcmcBayes.t_paths.samplerProjectsDir),'JAGS');
                tmpscriptdir=strcat(obj.repositories{i},mcmcBayes.osInterfaces.getSubPath(mcmcBayes.t_paths.samplerScriptsDir),'JAGS');

                tmpfilename_JAGSproject=mcmcBayes.osInterfaces.convPathStr(sprintf('%s/%s%sJAGS.txt', tmpprojdir, name, obj.modelsubtype));           
                tmpfilename_JAGSscripttemplate=mcmcBayes.osInterfaces.convPathStr(sprintf('%s/%sJAGS.txt', tmpscriptdir, name));
                
                if mcmcBayes.osInterfaces.doesFileExist(tmpfilename_JAGSproject) && mcmcBayes.osInterfaces.doesFileExist(tmpfilename_JAGSscripttemplate)
                    break;
                elseif i==size(obj.repositories,1)
                    error('Did not locate sampler project or script template file.');                    
                end
            end
            obj.JAGS_projdir=tmpprojdir;
            obj.JAGS_scriptdir=tmpscriptdir;                       
            obj.filename_JAGSproject=tmpfilename_JAGSproject;           
            obj.filename_JAGSscripttemplate=tmpfilename_JAGSscripttemplate;
            
            %copy filename_JAGSscripttemplate to script_seqid_tempid          
            obj.filename_bashscript=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-bashscript.txt', obj.osInterface.tempDir, obj.outputFileNamePrefix));            
            obj.filename_JAGSscript=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-script.txt', obj.osInterface.tempDir, obj.outputFileNamePrefix));            
            copyfile(obj.filename_JAGSscripttemplate,obj.filename_JAGSscript);                       
            
            obj.filename_JAGSdata=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-data.txt', mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataInDir), ...
                obj.outputFileNamePrefix));
            obj.filename_JAGSinits=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-inits.txt', mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerInitsDir),...
                obj.outputFileNamePrefix));
                        
            %setup script_seqid_tempid for sampling sequence (samplesCoda,modelSaveLog)
            obj.filename_CODAindex=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-CODAindex.txt', mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataOutDir), ...
                obj.outputFileNamePrefix));%use the MCMC chain for final parameter
            obj.filename_CODAchain=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-CODAchain1.txt', mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataOutDir), ...
                obj.outputFileNamePrefix));%use the MCMC chain for final parameter
            
            obj.filename_JAGSlog=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-samplerLog.txt', mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.logsDir), ...
                obj.outputFileNamePrefix));
                             
            obj.setupBashScript();

            %setup obj.filename_JAGSscript for sampling sequence
            runPreprocessor_scriptfile(obj);
            
            %setup obj.filename_JAGSinits for sampling sequence
            setupInits(obj, obj.filename_JAGSinits)            
            
            %setup obj.filename_JAGSdata for sampling sequence
            setupData(obj, obj.filename_JAGSdata)
                        
            retobj=obj;
        end
        
        function setupBashScript(obj)
            if mcmcBayes.osInterfaces.doesFileExist(obj.filename_bashscript) %remove any old script files (with the same name)
                delete(obj.filename_bashscript);
            end
            
            %write output to obj.filename_Stanscript
            fid=fopen(obj.filename_bashscript,'wt');
            
            fprintf(fid,'#!/usr/bin/env bash\n%s %s\n', mcmcBayes.osInterfaces.convPathStrUnix(obj.JAGS_shortcut), ...
                    mcmcBayes.osInterfaces.convPathStrUnix(obj.filename_JAGSscript));
            fclose(fid);
            if obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.UbuntuLinux || obj.osInterface.getOSInterfaceType()==mcmcBayes.t_osInterfaces.MacOS
                syscommand=sprintf('system(''chmod +x "%s"'');',obj.filename_bashscript);%make the script executable
                eval(syscommand);
            end         
        end
        
        %JAGS uses the script file to spawn a sampling sequence
        function runPreprocessor_scriptfile(obj)
        %Script sequence:
        %   Read model file
        %   Read data file
        %   Compile model
        %   Read inits file
        %   Initialize model
        %   List the sampler factories
        %   Sample from model (burnin)
        %   Set trace monitors for nodes
        %   Sample from model
        %   Save coda files
        %   Exit                 
            
            %setup obj.JAGS_codadir ##MODELSETWD##
            buffer=sprintf('cd "%s"\n',mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataOutDir));%do not have a '/' for the last directory for modelSetWD!!!
            finsertbuffer(obj.filename_JAGSscript,'##MODELSETWD##',buffer);
            %setup obj.filename_JAGSscript ##MODELCHECK##
            tstring=sprintf('model in "%s"',obj.filename_JAGSproject);
            finsertbuffer(obj.filename_JAGSscript,'##MODELCHECK##',tstring);
            %setup obj.filename_JAGSscript ##MODELDATA##
            tstring=sprintf('data in "%s"',obj.filename_JAGSdata);
            finsertbuffer(obj.filename_JAGSscript,'##MODELDATA##',tstring);
            %setup obj.filename_JAGSscript ##MODELCOMPILE##
            tstring=sprintf('compile,nchains(%d)',obj.nchains);
            finsertbuffer(obj.filename_JAGSscript,'##MODELCOMPILE##',tstring);
            
            chainid=1;%just use the first chain here to lookup
            %setup obj.filename_JAGSscript ##MODELINITS##
            tstring=sprintf('parameters in "%s"',obj.filename_JAGSinits);
            finsertbuffer(obj.filename_JAGSscript,'##MODELINITS##',tstring);
            %setup obj.filename_JAGSscript (burnin) ##MODELUPDATE,1##
            tstring=sprintf('update %d',obj.variables(1,chainid).MCMC_nsamples_burnin);%just use the first var here to lookup
            finsertbuffer(obj.filename_JAGSscript,'##MODELUPDATE,1##',tstring);
            %setup node monitors (thinning) ##SAMPLESSET##                        
            clear tstring;
            for varid=1:size(obj.variables,1)
                if (obj.variables(varid,chainid).type~=mcmcBayes.t_variable.constant)%only monitor stochastic nodes
                    %for stochastic processes, strip off, for example, (t) from Theta(t) in the name
                    if obj.variables(varid,chainid).type==mcmcBayes.t_variable.stochasticProcess
                        if isempty(strfind(obj.variables(varid,chainid).name,'('))
                            strname=obj.variables(varid,chainid).name;
                        else
                            strname=obj.variables(varid,chainid).name(1:strfind(obj.variables(varid,chainid).name,'(')-1);
                        end
                    else
                        strname=obj.variables(varid,chainid).name;
                    end                    
                    
                    if exist('tstring','var')
                        tstring=sprintf('%s\nmonitor %s, thin(%d)',tstring, strname, obj.variables(varid,chainid).MCMC_nsamples_lag);
                    else
                        tstring=sprintf('monitor %s, thin(%d)',strname, obj.variables(varid,chainid).MCMC_nsamples_lag);
                    end
                end
            end
            freplacebuffer(obj.filename_JAGSscript,'##SAMPLESSET,START##','##SAMPLESSET,STOP##',tstring);
            %setup obj.filename_JAGSscript (samples) ##MODELUPDATE,2##
            tstring=sprintf('update %d',obj.variables(1,chainid).MCMC_nsamples_data);%just use the first var here to lookup
            finsertbuffer(obj.filename_JAGSscript,'##MODELUPDATE,2##',tstring);
            %setup obj.filename_JAGSscript ##SAMPLESCODA##
            buffer=sprintf('coda *, stem(%s-CODA)\n', obj.outputFileNamePrefix);
            finsertbuffer(obj.filename_JAGSscript,'##SAMPLESCODA##',buffer);          
        end               
                        
        %calls JAGS to perform the previously setup sampling sequence
        function [retobj]=performMCMCSampling(obj)
            stdout=1;
            %Run the script to generate the MCMC computations for theta(i|j) (cannot run JAGS in the background as the next loop iteration can't start without results)
            if mcmcBayes.osInterfaces.doesFileExist(obj.filename_JAGSlog)%remove any old log files (with the same name)
                delete(obj.filename_JAGSlog);
            end
            for chainid=1:obj.nchains
            	tfilename_CODAchain=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-CODAchain%d.txt', mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataOutDir), ...
                obj.outputFileNamePrefix,chainid));%use the MCMC chain for final parameter               
                if mcmcBayes.osInterfaces.doesFileExist(tfilename_CODAchain) %remove any old CODAchain files (with the same name)
                    delete(tfilename_CODAchain);
                end
            end
            if mcmcBayes.osInterfaces.doesFileExist(obj.filename_CODAindex) %remove any old CODAindex files (with the same name)
                delete(obj.filename_CODAindex);
            end
            
            fprintf(stdout,'\tStarting JAGS sampling for tempid=%d.\n', obj.tempid);            
            obj.osInterface=obj.osInterface.spawnBashCommand( obj.filename_bashscript, obj.filename_JAGSlog,obj.runView );          
            
            finished=false;
            samplingTimerStart=tic;
            setcommandstr='';
            while finished==false
                drawnow;                
                pause(3);%sleep 3 seconds
                elapsedtime=toc(samplingTimerStart);
                elapsedseconds=seconds(elapsedtime);
                elapsedminutes=minutes(elapsedseconds);
                statusstr=sprintf('For %s model, tempid=%d, JAGS sampler elapsed time is %e minutes', obj.modeltype, ...
                    obj.tempid, elapsedminutes);
                setDesktopStatus(statusstr)
                [obj.osInterface, retcommandstr]=obj.osInterface.updateBashCmdStatus(setcommandstr);
                setcommandstr=retcommandstr;
                disp(setcommandstr);
                obj.osInterface.checkForKillFile();               
                if (obj.osInterface.bashCmdState==mcmcBayes.t_runState.Finished)
                    for count=1:12%will be about 12*(5) seconds
                        if checkFileStatic(obj.filename_CODAchain,5)
                            break;%file exists and is not changing
                        end
                        drawnow;
                        pause(1);
                    end
                    if count==12
                        type(obj.filename_JAGSlog);
                        error('Sampler failed.');   
                    end
                    finished=true;
                    tempstr=sprintf('\tElapsed sampling time for %s model, tempid=%d was %e minutes.', obj.modeltype, obj.tempid, elapsedminutes);
                    disp(tempstr);    
                    setDesktopStatus(tempstr);
                    obj.osInterface.deleteCmdMsgFile()                        
                end
            end                        
            retobj=obj;
        end
              
        function [retobj, fieldindices, data]=readSamples(obj)%index specifies the coda file to read all the variables sampled
            for chainid=1:obj.nchains
                clear tfieldindices targlist;
            	tfilename_CODAchain=mcmcBayes.osInterfaces.convPathStr(sprintf('%s%s-CODAchain%d.txt', ...
                    mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.samplerDataOutDir), obj.outputFileNamePrefix, chainid));
                JAGSdatastruct=mcmcBayes.coda2mat(obj.filename_CODAindex,tfilename_CODAchain);
                %samples shall be stored as a 3-dimensional matrix within a cell
                %scalar[]-{[numSamples x 1]}
                %vector[]-{[numRows x 1 x numSamples]}
                %matrix[]-{[numRows x numCols x numSamples}]              
                for varid=1:size(obj.variables,1)
                    tsize=obj.variables(varid,chainid).MCMC_nsamples_data/obj.variables(varid,chainid).MCMC_nsamples_lag;
                    switch obj.variables(varid,chainid).type                 
                        case mcmcBayes.t_variable.constant
                            if (obj.variables(varid,chainid).distribution.type==mcmcBayes.t_distribution.DiscreteConstant)
                                tvar=obj.variables(varid,chainid).distribution.parameters(1).values*int32(ones(tsize,1,1));
                            else
                                tvar=obj.variables(varid,chainid).distribution.parameters(1).values*ones(tsize,1,1);
                            end
                            if exist('tfieldindices','var')==1
                                tfieldindices=[tfieldindices varid];
                                targlist=[targlist; {tvar}];
                            else
                                tfieldindices=varid;
                                targlist=[{tvar}];
                            end
                        case {mcmcBayes.t_variable.stochastic, mcmcBayes.t_variable.stochasticProcess}
                            names=fieldnames(JAGSdatastruct);%need to identify the fields present in the coda               
                            for i=1:numel(names)
                                %for stochastic processes, strip off, for example, (t) from Theta(t) in the name
                                if obj.variables(varid,chainid).type==mcmcBayes.t_variable.stochasticProcess
                                    if isempty(strfind(obj.variables(varid,chainid).name,'('))
                                        strname=obj.variables(varid,chainid).name;
                                    else
                                        strname=obj.variables(varid,chainid).name(1:strfind(obj.variables(varid,chainid).name,'(')-1);
                                    end
                                else
                                    strname=obj.variables(varid,chainid).name;
                                end                                     
                                if strcmp(strname,cell2mat(names(i)))==1                               
                                    evalstr=sprintf('JAGSdatastruct.%s',strname);
                                    tvar=eval(evalstr);
                                    if exist('tfieldindices','var')==1 %is this not the first entry (i.e., vector already created)
                                        tfieldindices=[tfieldindices varid];
                                        targlist=[targlist; {tvar}];
                                    else
                                        tfieldindices=varid;
                                        targlist=[{tvar}];
                                    end
                                    break;
                                end
                            end
                    end
                end
                data(:,chainid)=targlist;
                fieldindices(:,chainid)=tfieldindices;
            end    
            retobj=obj;
        end        
    end
    
    methods %getter/setter functions        
        function obj=set.JAGS_shortcut(obj,setJAGS_shortcut) 
            %setJAGS_shortcut=mcmcBayes.osInterfaces.convPathStr(setJAGS_shortcut);
            if mcmcBayes.osInterfaces.doesFileExist(setJAGS_shortcut) %is a file
                obj.JAGS_shortcut=setJAGS_shortcut;
            else
                error('JAGS_shortcut does not exist');
            end
        end
        
        function value=get.JAGS_shortcut(obj) 
            value=obj.JAGS_shortcut;
        end
    end
end