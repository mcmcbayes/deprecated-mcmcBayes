classdef t_dataTransformPre < int32
    enumeration
        none (1)%do not solve
        tolog10 (2)%data scaled using log10(data)
        toln (3)
        toNormalized (4)%data normalized using (data-mean(data))/std(data)
        tolog10Normalized (5)%log10(t) data that is normalized
        toMeanCentered (6)
        %put externals in t_dataTransformPre_ext
    end
    methods (Static)        
        function intvalue=toint(enumobj)
            switch enumobj          
                case {mcmcBayes.t_dataTransformPre.none,mcmcBayes.t_dataTransformPre.tolog10,...
                      mcmcBayes.t_dataTransformPre.toln, mcmcBayes.t_dataTransformPre.toNormalized,...
                      mcmcBayes.t_dataTransformPre.tolog10Normalized, mcmcBayes.t_dataTransformPre.toMeanCentered}
                    intvalue=int32(enumobj);                    
                otherwise
                    intvalue=mcmcBayes.t_dataTransformPre_ext.toint(enumobj);
            end                
        end        
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcBayes.t_dataTransformPre.none
                    strvalue='mcmcBayes.t_dataTransformPre.none';
                case mcmcBayes.t_dataTransformPre.tolog10
                    strvalue='mcmcBayes.t_dataTransformPre.tolog10';
                case mcmcBayes.t_dataTransformPre.toln
                    strvalue='mcmcBayes.t_dataTransformPre.toln';
                case mcmcBayes.t_dataTransformPre.toNormalized
                    strvalue='mcmcBayes.t_dataTransformPre.toNormalized';
                case mcmcBayes.t_dataTransformPre.tolog10Normalized
                    strvalue='mcmcBayes.t_dataTransformPre.tolog10Normalized';
                case mcmcBayes.t_dataTransformPre.toMeanCentered
                    strvalue='mcmcBayes.t_dataTransformPre.toMeanCentered';
                otherwise
                    strvalue=mcmcBayes.t_dataTransformPre_ext.tostring(enumobj);
            end
        end
        
        function enumobj=int2enum(intval)
            switch intval
                case int32(mcmcBayes.t_dataTransformPre.none)
                    enumobj=mcmcBayes.t_dataTransformPre.none;
                case int32(mcmcBayes.t_dataTransformPre.tolog10)
                    enumobj=mcmcBayes.t_dataTransformPre.tolog10;
                case int32(mcmcBayes.t_dataTransformPre.toln)
                    enumobj=mcmcBayes.t_dataTransformPre.toln;
                case int32(mcmcBayes.t_dataTransformPre.toNormalized)
                    enumobj=mcmcBayes.t_dataTransformPre.toNormalized;
                case int32(mcmcBayes.t_dataTransformPre.tolog10Normalized)
                    enumobj=mcmcBayes.t_dataTransformPre.tolog10Normalized;
                case int32(mcmcBayes.t_dataTransformPre.toMeanCentered)
                    enumobj=mcmcBayes.t_dataTransformPre.toMeanCentered;
                otherwise
                    enumobj=mcmcBayes.t_dataTransformPre_ext.int2enum(intval);
            end
        end
        
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcBayes.t_dataTransformPre.none')==1
                enumobj=mcmcBayes.t_dataTransformPre.none;
            elseif strcmp(strval,'mcmcBayes.t_dataTransformPre.tolog10')==1
                enumobj=mcmcBayes.t_dataTransformPre.tolog10;
            elseif strcmp(strval,'mcmcBayes.t_dataTransformPre.toln')==1
                enumobj=mcmcBayes.t_dataTransformPre.toln;
            elseif strcmp(strval,'mcmcBayes.t_dataTransformPre.toNormalized')==1
                enumobj=mcmcBayes.t_dataTransformPre.toNormalized;
            elseif strcmp(strval,'mcmcBayes.t_dataTransformPre.tolog10Normalized')==1
                enumobj=mcmcBayes.t_dataTransformPre.tolog10Normalized;
            elseif strcmp(strval,'mcmcBayes.t_dataTransformPre.toMeanCentered')==1
                enumobj=mcmcBayes.t_dataTransformPre.toMeanCentered;
            else 
                enumobj=mcmcBayes.t_dataTransformPre_ext.string2enum(strval);
            end
        end
    end
end