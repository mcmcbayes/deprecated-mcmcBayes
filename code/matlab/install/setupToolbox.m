%set initial folder and setup mcmcBayes startup file
%>> setupToolbox('C:\opt\MATLAB\Toolboxes\mcmcBayes\code\matlab')
%edit(fullfile(prefdir,'matlab.settings')) to see the settings variable hierarchy
function setupToolbox(initialWorkingFolder)
	s=settings;
	s.matlab.workingfolder.InitialWorkingFolder.PersonalValue='custom_folder';
	s.matlab.workingfolder.CustomFolderPath.PersonalValue=initialWorkingFolder;
	cd(initialWorkingFolder);
	mcmcBayes.osInterfaces.createStartupFile();