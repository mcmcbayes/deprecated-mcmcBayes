%programatically install mcmcBayes package
%>> installToolbox('C:\sandbox\mcmcBayes\code\matlab\mcmcBayes.mltbx','C:\opt\MATLAB')
%edit(fullfile(prefdir,'matlab.settings')) to see the settings variable hierarchy
function installToolbox(toolbox,installationFolder)
	s=settings;
	s.matlab.addons.InstallationFolder.PersonalValue=installationFolder;
	matlab.addons.toolbox.installToolbox(toolbox);
