import java.io.StringWriter;
import java.io.ByteArrayInputStream;

//Note: 
// For compatibility, we use the *.jar files installed with MATLAB.  MATLAB R2018a requires 
// Java SE 1.8.144  (sadly, javax.catalog.CatalogResolver originated in 1.9).  The imports
// below were determined by using the following command types.
//List MATLAB's java class paths:
// >> javaclasspath
//List the contents of a jar:
// $ jar tf /c/Program\ Files/MATLAB/R2018a/java/jarext/xercesImpl.jar | grep Deferred
//Get more details from a class in a jar:
// $ javap -classpath /c/Program\ Files/MATLAB/R2018a/java/jarext/xercesImpl.jar 'org/apache/xerces/dom/DeferredDocumentImpl'
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;  
import javax.xml.parsers.*;

import org.dom4j.*;
import org.apache.xerces.dom.*;

//Compile:
// $ javac -classpath javap -classpath *:$MATLABHOME/java/jarext/xercesImpl.jar:$MATLABHOME/java/jarext/dom4j.jar XmlHacks.java
//Usage:
// 1) Call xmlread to read a *.xml file into a document
// 2) Pass the document into XmlHacks.insert, along with the root element name and dtd filename to validate against.  
//    This will insert the appropriate doctype command into the document to validate against the specified dtd and return
//    that updated document converted back to a string representing the *.xml.
// 3) Pass the *.xml string into XmlHacks.xmlreadstring to reread into a document but now validates against the dtd.
public class XmlHacks {   
    public static org.w3c.dom.Document xmlreadstring(String xmlstring) 
    //This function parses xmlstring into a document and returns it.  Based off calls from 
    //xmlread implementation.  Also, MATLAB's documentation for xmlread references: 
    //https://docs.oracle.com/javase/8/docs/api
    {
        DocumentBuilderFactory parserFactory=null;
        DocumentBuilder docBuilder=null;
        org.w3c.dom.Document doc=null;
        
        try {
            parserFactory=DocumentBuilderFactory.newInstance();
        }
        catch (Exception e) {
            System.out.println("DocumentBuilderFactory failed");
        }
        finally {
            try {
                parserFactory.setValidating(true);
                docBuilder=parserFactory.newDocumentBuilder();
                doc=docBuilder.parse(new ByteArrayInputStream(xmlstring.getBytes("UTF-8")));
            }
            catch (Exception e) {
                System.out.println("Parse failed: " + e.getMessage());
            }              
            return(doc);        
        }
    }
    
    public static String insert( DeferredDocumentImpl doc, String rootElement, String dtdFilePath )
    //This function inserts a DOCTYPE element into doc, converts the updated doc into a string, and returns it
    {
        String message=null;
        TransformerFactory tf=null;
        Transformer transformer=null;

        message=String.format("Inserting <!DOCTYPE %s SYSTEM \"%s\">",rootElement, dtdFilePath);
        System.out.println( message );       
        
        StringWriter sw = new StringWriter();
        try {
            tf = TransformerFactory.newInstance();
            transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(javax.xml.transform.OutputKeys.DOCTYPE_SYSTEM, dtdFilePath);            
        } catch (Exception e) {
            System.out.println("TransformerFactory failed");
        }

        message=String.format("Converting xml document to string.");   
        System.out.println( message );

        try {
            transformer.transform(new DOMSource(doc), new StreamResult(sw));
        } catch (Exception e) {
            System.out.println("transformer failed");
        }        
        return sw.toString();        
    }
}